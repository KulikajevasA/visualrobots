﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RobotVL.Logical;
using RobotVL.Comparators;
using RobotVL.Members;
using RobotVL.Main;
using RobotVL.Members.Types;

namespace VisualRobots.Tests.CodeGen
{
    [TestClass]
    public class TestsLogicGenerator
    {
        [TestMethod]
        public void IfElseGetterLogic()
        {
            ListNode list = new ListNode("list", ObjectNode.ValueType_T.Numeric);
            var invoke = list.InvokeMethod(ListNode.Methods_T.Count);
            ObjectNode var = ObjectNode.CreateNode("var", ObjectNode.ValueType_T.Numeric);

            EQComparatorNode comparator = new EQComparatorNode(invoke, var);
            IfNode ifNode = new IfNode(comparator);

            string actual = ifNode.Stringify();
            string expected = "if(@list.Count == @var)\n" +
                "{\n" +
                "}";

            Assert.AreEqual(actual, expected);
        }

        [TestMethod]
        public void IfElseMethodLogic()
        {
            ObjectNode val1 = ObjectNode.CreateNode("var1", ObjectNode.ValueType_T.Numeric);
            var invoke = val1.InvokeMethod(ObjectNode.Methods_T.Equals, val1);
            ObjectNode val2 = ObjectNode.CreateNode("var2", ObjectNode.ValueType_T.Numeric);

            EQComparatorNode comparator = new EQComparatorNode(invoke, val2);
            IfNode ifNode = new IfNode(comparator);

            string actual = ifNode.Stringify();
            string expected = "if(@var1.Equals(@var1) == @var2)\n" +
                "{\n" +
                "}";

            Assert.AreEqual(actual, expected);
        }

        [TestMethod]
        public void IfElseLogic()
        {
            ObjectNode val1 = ObjectNode.CreateNode("var1", ObjectNode.ValueType_T.Numeric, 0);
            ObjectNode val2 = ObjectNode.CreateNode("var2", ObjectNode.ValueType_T.Numeric, 0);

            EQComparatorNode comparator = new EQComparatorNode(val1, val2);
            IfNode ifNode = new IfNode(comparator);

            string actual = ifNode.Stringify();
            string expected = "if(@var1 == @var2)\n" +
                "{\n" +
                "}";

            Assert.AreEqual(actual, expected);
        }

        [TestMethod]
        public void IfElseClassLogic()
        {
            ClassNode classNode = new ClassNode("MyClass");
            ObjectNode val1 = ObjectNode.CreateNode("var1", ObjectNode.ValueType_T.Numeric, 0);
            ObjectNode val2 = ObjectNode.CreateNode("var2", ObjectNode.ValueType_T.Numeric, 0);

            classNode.AddVariables(val1, val2);

            MethodNode method = new MethodNode("MyMethod", MethodNode.Accessor_T.Private);

            classNode.AddMethod(method);

            EQComparatorNode comparator = new EQComparatorNode(val1, val2);
            IfNode ifNode = new IfNode(comparator);

            method.AddNode(ifNode);

            string actual = classNode.Stringify();
            string expected = "public class MyClass\n" +
                                "{\n" +
                                    "\tprivate double @var1 = 0;\n" +
                                        "\tprivate double @var2 = 0;\n" +
                                        "\tprivate void MyMethod()\n" +
                                        "\t{\n" +
                                            "\t\tif(@var1 == @var2)\n" +
                                            "\t\t{\n" +
                                            "\t\t}\n" +
                                        "\t}\n" +
                                    "}";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void NestedIfElseClassLogic()
        {
            ClassNode classNode = new ClassNode("MyClass");
            ObjectNode val1 = ObjectNode.CreateNode("var1", ObjectNode.ValueType_T.Numeric, 0);
            ObjectNode val2 = ObjectNode.CreateNode("var2", ObjectNode.ValueType_T.Numeric, 0);

            classNode.AddVariables(val1, val2);

            MethodNode method = new MethodNode("MyMethod", MethodNode.Accessor_T.Private);

            classNode.AddMethod(method);

            EQComparatorNode comparator = new EQComparatorNode(val1, val2);
            IfNode ifNode = new IfNode(comparator);

            method.AddNode(ifNode);

            IfNode nestedNode1 = new IfNode(comparator);
            ifNode.Success.AddNode(nestedNode1);

            string actual = classNode.Stringify();
            string expected = "public class MyClass\n" +
                                "{\n" +
                                    "\tprivate double @var1 = 0;\n" +
                                        "\tprivate double @var2 = 0;\n" +
                                        "\tprivate void MyMethod()\n" +
                                        "\t{\n" +
                                            "\t\tif(@var1 == @var2)\n" +
                                            "\t\t{\n" +

                                                "\t\t\tif(@var1 == @var2)\n" +
                                                "\t\t\t{\n" +
                                                "\t\t\t}\n" +

                                            "\t\t}\n" +
                                        "\t}\n" +
                                    "}";

            Assert.AreEqual(expected, actual);
        }
        
        [TestMethod]
        public void DoubleNestedIfElseClassLogic()
        {
            ClassNode classNode = new ClassNode("MyClass");
            ObjectNode val1 = ObjectNode.CreateNode("var1", ObjectNode.ValueType_T.Numeric, 0);
            ObjectNode val2 = ObjectNode.CreateNode("var2", ObjectNode.ValueType_T.Numeric, 0);

            classNode.AddVariables(val1, val2);

            MethodNode method = new MethodNode("MyMethod", MethodNode.Accessor_T.Private);

            classNode.AddMethod(method);

            EQComparatorNode comparator = new EQComparatorNode(val1, val2);
            IfNode ifNode = new IfNode(comparator);

            method.AddNode(ifNode);

            IfNode nestedNode1 = new IfNode(comparator);
            IfNode nestedNode2 = new IfNode(comparator);
            ifNode.Success.AddNode(nestedNode1);
            ifNode.Otherwise.AddNode(nestedNode2);

            IfNode nestedNestedNode = new IfNode(comparator);
            nestedNode1.Success.AddNode(nestedNestedNode);

            string actual = classNode.Stringify();
            string expected = "public class MyClass\n" +
                                "{\n" +
                                    "\tprivate double @var1 = 0;\n" +
                                        "\tprivate double @var2 = 0;\n" +
                                        "\tprivate void MyMethod()\n" +
                                        "\t{\n" +
                                            "\t\tif(@var1 == @var2)\n" +
                                            "\t\t{\n" +
                                                "\t\t\tif(@var1 == @var2)\n" +
                                                "\t\t\t{\n" +
                                                "\t\t\t\tif(@var1 == @var2)\n" +
                                                "\t\t\t\t{\n" +
                                                "\t\t\t\t}\n" +
                                                "\t\t\t}\n" +
                                            "\t\t}\n" +
                                            "\t\telse\n" +
                                            "\t\t{\n" +
                                                "\t\t\tif(@var1 == @var2)\n" +
                                                "\t\t\t{\n" +
                                                "\t\t\t}\n" +
                                            "\t\t}\n" +
                                        "\t}\n" +
                                    "}";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void DoubleNestedIfElseClassParametrizedLogic()
        {
            ClassNode classNode = new ClassNode("MyClass");
            ObjectNode val1 = ObjectNode.CreateNode("var1", ObjectNode.ValueType_T.Numeric, 0);
            ObjectNode val2 = ObjectNode.CreateNode("var2", ObjectNode.ValueType_T.Numeric, 0);
            ObjectNode param = ObjectNode.CreateNode("param", ObjectNode.ValueType_T.Numeric, 0);

            classNode.AddVariables(val1, val2);

            MethodNode method = new MethodNode("MyMethod", MethodNode.Accessor_T.Private);
            method.AddParameter(param);

            classNode.AddMethod(method);

            EQComparatorNode comparator1 = new EQComparatorNode(val1, val2);
            IfNode ifNode = new IfNode(comparator1);

            method.AddNode(ifNode);

            IfNode nestedNode1 = new IfNode(comparator1);
            IfNode nestedNode2 = new IfNode(comparator1);
            ifNode.Success.AddNode(nestedNode1);
            ifNode.Otherwise.AddNode(nestedNode2);

            EQComparatorNode comparator2 = new EQComparatorNode(val1, param);

            IfNode nestedNestedNode = new IfNode(comparator2);
            nestedNode1.Success.AddNode(nestedNestedNode);

            string actual = classNode.Stringify();
            string expected = "public class MyClass\n" +
                                "{\n" +
                                    "\tprivate double @var1 = 0;\n" +
                                        "\tprivate double @var2 = 0;\n" +
                                        "\tprivate void MyMethod(double @param)\n" +
                                        "\t{\n" +
                                            "\t\tif(@var1 == @var2)\n" +
                                            "\t\t{\n" +
                                                "\t\t\tif(@var1 == @var2)\n" +
                                                "\t\t\t{\n" +
                                                "\t\t\t\tif(@var1 == @param)\n" +
                                                "\t\t\t\t{\n" +
                                                "\t\t\t\t}\n" +
                                                "\t\t\t}\n" +
                                            "\t\t}\n" +
                                            "\t\telse\n" +
                                            "\t\t{\n" +
                                                "\t\t\tif(@var1 == @var2)\n" +
                                                "\t\t\t{\n" +
                                                "\t\t\t}\n" +
                                            "\t\t}\n" +
                                        "\t}\n" +
                                    "}";

            Assert.AreEqual(expected, actual);
        }
    }
}

﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RobotVL.Main;
using RobotDesigner;
using RobotDesigner.Nodes;
using UI.Components;
using RobotDesigner.Nodes.Sensors.Arrays;

namespace VisualRobots.Tests.CodeGen
{
    [TestClass]
    public class TestsRobotGenerator
    {
        [TestMethod]
        [Obsolete]
        public void AvoidingRobotGen()
        {
            var robotNode = new AvoidingRobotNode("MyRobot");

            string actual = robotNode.Stringify();
            string expected = "public class EntryPoint : Simulation.IEntryPoint\n" +
                                            "{\n" +
                                                "\tpublic class MyRobot : Simulation.IRobot\n" +
                                                "\t{\n" +
                                                    "\t\tprivate Simulation.Parts.IMotor @leftMotor = null;\n" +
                                                    "\t\tprivate Simulation.Parts.IMotor @rightMotor = null;\n" +
                                                    "\t\tprivate Simulation.Parts.IMotor @rearMotor = null;\n" +
                                                    "\t\tprivate Simulation.Parts.Sensors.Arrays.ISensorArray @ultrasonicSensorArray = null;\n" +
                                                    "\t\tprivate Simulation.Parts.Sensors.ISensor @colorSensor = null;\n" +
                                                    "\t\tprivate Simulation.Parts.Sensors.ISensor @lightSensor = null;\n" +
                                                    "\t\tpublic void RunFrame()\n" +
                                                    "\t\t{\n" +
                                                            "\t\t\tdouble @dir = @ultrasonicSensorArray.SensorDirection;\n" +
                                                            "\t\t\tdouble @val = @ultrasonicSensorArray.SensorValue;\n" +
                                                            "\t\t\tdouble @speed = 3;\n" +
                                                            "\t\t\tdouble @one = 1;\n" +
                                                            "\t\t\tdouble @zero = 0;\n" +
                                                            "\t\t\tif(@val > @one)\n" +
                                                            "\t\t\t{\n" +
                                                                    "\t\t\t\t@leftMotor.SetSpeed(@speed);\n" +
                                                                    "\t\t\t\t@rightMotor.SetSpeed(@speed);\n" +
                                                            "\t\t\t}\n" +
                                                            "\t\t\tif(@dir > @zero)\n" +
                                                            "\t\t\t{\n" +
                                                                    "\t\t\t\t@leftMotor.SetSpeed(@zero);\n" +
                                                            "\t\t\t}\n" +
                                                            "\t\t\telse\n" +
                                                            "\t\t\t{\n" +
                                                                    "\t\t\t\tif(@dir < @zero)\n" +
                                                                    "\t\t\t\t{\n" +
                                                                            "\t\t\t\t\t@rightMotor.SetSpeed(@zero);\n" +
                                                                    "\t\t\t\t}\n" +
                                                                    "\t\t\t\telse\n" +
                                                                    "\t\t\t\t{\n" +
                                                                            "\t\t\t\t\tif(@val < @one)\n" +
                                                                            "\t\t\t\t\t{\n" +
                                                                                    "\t\t\t\t\t\t@leftMotor.SetSpeed(@zero);\n" +
                                                                                    "\t\t\t\t\t\t@rightMotor.SetSpeed(@zero);\n" +
                                                                            "\t\t\t\t\t}\n" +
                                                                    "\t\t\t\t}\n" +
                                                            "\t\t\t}\n" +
                                                    "\t\t}\n" +
                                                    "\t\tpublic void InitializeParts(System.Collections.Generic.List<Simulation.Parts.IPart> @parts)\n" +
                                                    "\t\t{\n" +
                                                        "\t\t\tforeach(var @part in @parts)\n" +
                                                            "\t\t\t{\n" +
                                                                        "\t\t\t\tif(@part.PartType == Simulation.Parts.PartType_T.Motor)\n" +
                                                                        "\t\t\t\t{\n" +
                                                                                    "\t\t\t\t\tthis.AssignMotor((Simulation.Parts.IMotor)@part);\n" +
                                                                        "\t\t\t\t}\n" +
                                                                        "\t\t\t\telse\n" +
                                                                        "\t\t\t\t{\n" +
                                                                                    "\t\t\t\t\tif(@part.PartType == Simulation.Parts.PartType_T.UltrasonicSensorArray)\n" +
                                                                                    "\t\t\t\t\t{\n" +
                                                                                                "\t\t\t\t\t\tthis.AssignUltrasonicArray((Simulation.Parts.Sensors.Arrays.ISensorArray)@part);\n" +
                                                                                    "\t\t\t\t\t}\n" +
                                                                                    "\t\t\t\t\telse\n" +
                                                                                    "\t\t\t\t\t{\n" +
                                                                                                "\t\t\t\t\t\tif(@part.PartType == Simulation.Parts.PartType_T.ColorSensor)\n" +
                                                                                                "\t\t\t\t\t\t{\n" +
                                                                                                    "\t\t\t\t\t\t\tthis.AssignColorSensor((Simulation.Parts.Sensors.ISensor)@part);\n" +
                                                                                                "\t\t\t\t\t\t}\n" +
                                                                                                "\t\t\t\t\t\telse\n" +
                                                                                                "\t\t\t\t\t\t{\n" +
                                                                                                    "\t\t\t\t\t\t\tif(@part.PartType == Simulation.Parts.PartType_T.LightSensor)\n" +
                                                                                                    "\t\t\t\t\t\t\t{\n" +
                                                                                                        "\t\t\t\t\t\t\t\tthis.AssignLightSensor((Simulation.Parts.Sensors.ISensor)@part);\n" +
                                                                                                    "\t\t\t\t\t\t\t}\n" +
                                                                                                "\t\t\t\t\t\t}\n" +
                                                                                    "\t\t\t\t\t}\n" +
                                                                        "\t\t\t\t}\n" +
                                                            "\t\t\t}\n" +
                                                    "\t\t}\n" +
                                                    "\t\tpublic void AssignMotor(Simulation.Parts.IMotor @motor)\n" +
                                                    "\t\t{\n" +
                                                            "\t\t\tstring @motLeft = \"MotorLeft\";\n" +
                                                            "\t\t\tstring @motRight = \"MotorRight\";\n" +
                                                            "\t\t\tstring @motRear = \"MotorRear\";\n" +
                                                            "\t\t\tif(@motor.PartName == @motLeft)\n" +
                                                            "\t\t\t{\n" +
                                                                    "\t\t\t\t@leftMotor = @motor;\n" +
                                                            "\t\t\t}\n" +
                                                            "\t\t\telse\n" +
                                                            "\t\t\t{\n" +
                                                                    "\t\t\t\tif(@motor.PartName == @motRight)\n" +
                                                                    "\t\t\t\t{\n" +
                                                                            "\t\t\t\t\t@rightMotor = @motor;\n" +
                                                                    "\t\t\t\t}\n" +
                                                                    "\t\t\t\telse\n" +
                                                                    "\t\t\t\t{\n" +
                                                                            "\t\t\t\t\tif(@motor.PartName == @motRear)\n" +
                                                                            "\t\t\t\t\t{\n" +
                                                                                    "\t\t\t\t\t\t@rearMotor = @motor;\n" +
                                                                            "\t\t\t\t\t}\n" +
                                                                    "\t\t\t\t}\n" +
                                                            "\t\t\t}\n" +
                                                    "\t\t}\n" +
                                                    "\t\tpublic void AssignUltrasonicArray(Simulation.Parts.Sensors.Arrays.ISensorArray @ultrasonic)\n" +
                                                    "\t\t{\n" +
                                                    "\t\t\t@ultrasonicSensorArray = @ultrasonic;\n" +
                                                    "\t\t}\n" +
                                                    "\t\tpublic void AssignColorSensor(Simulation.Parts.Sensors.ISensor @color)\n" +
                                                    "\t\t{\n" +
                                                    "\t\t\t@colorSensor = @color;\n" +
                                                    "\t\t}\n" +
                                                    "\t\tpublic void AssignLightSensor(Simulation.Parts.Sensors.ISensor @light)\n" +
                                                    "\t\t{\n" +
                                                    "\t\t\t@lightSensor = @light;\n" +
                                                    "\t\t}\n" +
                                                "\t}\n" +
                                                "\tpublic Simulation.IRobot CreateRobot()\n" +
                                                "\t{\n" +
                                                    "\t\treturn new MyRobot();\n" +
                                                "\t}\n" +
                                            "}";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void RobotGen()
        {
            RobotNode robotNode = new RobotNode("MyRobot");

            string actual = robotNode.Stringify();
            string expected = "public class EntryPoint : Simulation.IEntryPoint\n" +
                                "{\n" +
                                    "\tpublic class MyRobot : Simulation.IRobot\n" +
                                    "\t{\n" +
                                        "\t\tprivate Simulation.Parts.IMotor @leftMotor = null;\n" +
                                        "\t\tprivate Simulation.Parts.IMotor @rightMotor = null;\n" +
                                        "\t\tprivate Simulation.Parts.IMotor @rearMotor = null;\n" +
                                        "\t\tprivate Simulation.Parts.Sensors.Arrays.ISensorArray @ultrasonicSensorArray = null;\n" +
                                        "\t\tprivate Simulation.Parts.Sensors.ISensor @colorSensor = null;\n" +
                                        "\t\tprivate Simulation.Parts.Sensors.ISensor @lightSensor = null;\n" +
                                        "\t\tpublic void RunFrame()\n" +
                                        "\t\t{\n" +
                                        "\t\t}\n" +
                                        "\t\tpublic void InitializeParts(System.Collections.Generic.List<Simulation.Parts.IPart> @parts)\n" +
                                        "\t\t{\n" +
                                            "\t\t\tforeach(var @part in @parts)\n" +
                                                "\t\t\t{\n" +
                                                            "\t\t\t\tif(@part.PartType == Simulation.Parts.PartType_T.Motor)\n" +
                                                            "\t\t\t\t{\n" +
                                                                        "\t\t\t\t\tthis.AssignMotor((Simulation.Parts.IMotor)@part);\n" +
                                                            "\t\t\t\t}\n" +
                                                            "\t\t\t\telse\n" +
                                                            "\t\t\t\t{\n" +
                                                                        "\t\t\t\t\tif(@part.PartType == Simulation.Parts.PartType_T.UltrasonicSensorArray)\n" +
                                                                        "\t\t\t\t\t{\n" +
                                                                                    "\t\t\t\t\t\tthis.AssignUltrasonicArray((Simulation.Parts.Sensors.Arrays.ISensorArray)@part);\n" +
                                                                        "\t\t\t\t\t}\n" +
                                                                        "\t\t\t\t\telse\n" +
                                                                        "\t\t\t\t\t{\n" +
                                                                                    "\t\t\t\t\t\tif(@part.PartType == Simulation.Parts.PartType_T.ColorSensor)\n" +
                                                                                    "\t\t\t\t\t\t{\n" +
                                                                                        "\t\t\t\t\t\t\tthis.AssignColorSensor((Simulation.Parts.Sensors.ISensor)@part);\n" +
                                                                                    "\t\t\t\t\t\t}\n" +
                                                                                    "\t\t\t\t\t\telse\n" +
                                                                                    "\t\t\t\t\t\t{\n" +
                                                                                        "\t\t\t\t\t\t\tif(@part.PartType == Simulation.Parts.PartType_T.LightSensor)\n" +
                                                                                        "\t\t\t\t\t\t\t{\n" +
                                                                                            "\t\t\t\t\t\t\t\tthis.AssignLightSensor((Simulation.Parts.Sensors.ISensor)@part);\n" +
                                                                                        "\t\t\t\t\t\t\t}\n" +
                                                                                    "\t\t\t\t\t\t}\n" +
                                                                        "\t\t\t\t\t}\n" +
                                                            "\t\t\t\t}\n" +
                                                "\t\t\t}\n" +
                                        "\t\t}\n" +
                                        "\t\tpublic void AssignMotor(Simulation.Parts.IMotor @motor)\n" +
                                        "\t\t{\n" +
                                                "\t\t\tstring @motLeft = \"MotorLeft\";\n" +
                                                "\t\t\tstring @motRight = \"MotorRight\";\n" +
                                                "\t\t\tstring @motRear = \"MotorRear\";\n" +
                                                "\t\t\tif(@motor.PartName == @motLeft)\n" +
                                                "\t\t\t{\n" +
                                                        "\t\t\t\t@leftMotor = @motor;\n" +
                                                "\t\t\t}\n" +
                                                "\t\t\telse\n" +
                                                "\t\t\t{\n" +
                                                        "\t\t\t\tif(@motor.PartName == @motRight)\n" +
                                                        "\t\t\t\t{\n" +
                                                                "\t\t\t\t\t@rightMotor = @motor;\n" +
                                                        "\t\t\t\t}\n" +
                                                        "\t\t\t\telse\n" +
                                                        "\t\t\t\t{\n" +
                                                                "\t\t\t\t\tif(@motor.PartName == @motRear)\n" +
                                                                "\t\t\t\t\t{\n" +
                                                                        "\t\t\t\t\t\t@rearMotor = @motor;\n" +
                                                                "\t\t\t\t\t}\n" +
                                                        "\t\t\t\t}\n" +
                                                "\t\t\t}\n" +
                                        "\t\t}\n" +
                                        "\t\tpublic void AssignUltrasonicArray(Simulation.Parts.Sensors.Arrays.ISensorArray @ultrasonic)\n" +
                                        "\t\t{\n" +
                                        "\t\t\t@ultrasonicSensorArray = @ultrasonic;\n" +
                                        "\t\t}\n" +
                                        "\t\tpublic void AssignColorSensor(Simulation.Parts.Sensors.ISensor @color)\n" +
                                        "\t\t{\n" +
                                        "\t\t\t@colorSensor = @color;\n" +
                                        "\t\t}\n" +
                                        "\t\tpublic void AssignLightSensor(Simulation.Parts.Sensors.ISensor @light)\n" +
                                        "\t\t{\n" +
                                        "\t\t\t@lightSensor = @light;\n" +
                                        "\t\t}\n" +
                                    "\t}\n" +
                                    "\tpublic Simulation.IRobot CreateRobot()\n" +
                                    "\t{\n" +
                                        "\t\treturn new MyRobot();\n" +
                                    "\t}\n" +
                                "}";

            Assert.AreEqual(expected, actual);
        }
    }
}

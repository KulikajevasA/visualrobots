﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RobotVL.Members;
using RobotVL.Invocation;
using RobotVL.Members.Types;

namespace VisualRobots.Tests.CodeGen
{
    [TestClass]
    public class TestsMethodGenerator
    {
        [TestMethod]
        public void EmptyPublicMethod()
        {
            MethodNode method = new MethodNode("MyMethod");

            string actual = method.Stringify();
            string expected = "public void MyMethod()\n" +
                 "{\n" +
                 "}";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void EmptyPublicMethod2()
        {
            MethodNode method = new MethodNode("MyMethod", MethodNode.Accessor_T.Public);

            string actual = method.Stringify();
            string expected = "public void MyMethod()\n" +
                 "{\n" +
                 "}";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void EmptyPrivateMethod()
        {
            MethodNode method = new MethodNode("MyMethod", MethodNode.Accessor_T.Private);

            string actual = method.Stringify();
            string expected = "private void MyMethod()\n" +
                 "{\n" +
                 "}";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void EmptyProtectedMethod()
        {
            MethodNode method = new MethodNode("MyMethod", MethodNode.Accessor_T.Protected);

            string actual = method.Stringify();
            string expected = "protected void MyMethod()\n" +
                 "{\n" +
                 "}";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void SingleListParameterEmptyMethod()
        {
            MethodNode method = new MethodNode("MyMethod", MethodNode.Accessor_T.Public);
            ListNode param = new ListNode("myParam", ObjectNode.ValueType_T.Numeric);
            method.AddParameter(param);

            string actual = method.Stringify();
            string expected = "public void MyMethod(System.Collections.Generic.List<double> @myParam)\n" +
                 "{\n" +
                 "}";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void SingleParameterEmptyMethod()
        {
            MethodNode method = new MethodNode("MyMethod", MethodNode.Accessor_T.Public);
            ObjectNode param = ObjectNode.CreateNode("myParam", ObjectNode.ValueType_T.Numeric, 5);
            method.AddParameter(param);

            string actual = method.Stringify();
            string expected = "public void MyMethod(double @myParam)\n" +
                 "{\n" +
                 "}";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void MultipleParameterEmptyMethod()
        {
            MethodNode method = new MethodNode("MyMethod", MethodNode.Accessor_T.Public);
            ObjectNode param1 = ObjectNode.CreateNode("myParam1", ObjectNode.ValueType_T.Numeric, 5);
            ObjectNode param2 = ObjectNode.CreateNode("myParam2", ObjectNode.ValueType_T.Numeric, 5);
            method.AddParameters(param1, param2);

            string actual = method.Stringify();
            string expected = "public void MyMethod(double @myParam1, double @myParam2)\n" +
                 "{\n" +
                 "}";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void NumericReturnMethod()
        {
            MethodNode method = new MethodNode("MyMethod", MethodNode.Accessor_T.Public);
            ObjectNode ret = ObjectNode.CreateNode("retVal", ObjectNode.ValueType_T.Numeric, 5);
            method.Returns = ret;

            string actual = method.Stringify();
            string expected = "public double MyMethod()\n" +
                 "{\n" +
                 "\treturn @retVal;\n" +
                 "}";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [Obsolete]
        public void MethodWithLocalVariables()
        {
            MethodNode method = new MethodNode("MyMethod");
            ObjectNode iVar = ObjectNode.CreateNode("i", ObjectNode.ValueType_T.Numeric, 0);
            ObjectNode jVar = ObjectNode.CreateNode("j", ObjectNode.ValueType_T.Numeric, 0);

            method.AddNodes(iVar, jVar);

            string actual = method.Stringify();
            string expected = "public void MyMethod()\n" +
                 "{\n" +
                 "\tdouble @i = 0;\n" +
                 "\tdouble @j = 0;\n" +
                 "}";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void InvokeEqualsMethod()
        {
            ObjectNode var = ObjectNode.CreateNode("var", ObjectNode.ValueType_T.Numeric, 5);
            InvokeMethod invoke = var.InvokeMethod(ObjectNode.Methods_T.Equals, var);

            string expected = "@var.Equals(@var)";
            string actual = invoke.Stringify();

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void InvokeAddMethod()
        {
            ListNode list = new ListNode("list", ObjectNode.ValueType_T.Numeric);
            ObjectNode n1 = ObjectNode.CreateNode("n1", ObjectNode.ValueType_T.Numeric);

            var invoke = list.InvokeMethod(ListNode.Methods_T.Add, n1);

            string expected = "@list.AddRange(@n1)";
            string actual = invoke.Stringify();

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void InvokeAddMultipleMethod()
        {
            ListNode list = new ListNode("list", ObjectNode.ValueType_T.Numeric);

            ObjectNode n1 = ObjectNode.CreateNode("n1", ObjectNode.ValueType_T.Numeric);
            ObjectNode n2 = ObjectNode.CreateNode("n2", ObjectNode.ValueType_T.Numeric);

            var invoke = list.InvokeMethod(ListNode.Methods_T.Add, n1, n2);

            string expected = "@list.AddRange(@n1, @n2)";
            string actual = invoke.Stringify();

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void InvokeCountMethod()
        {
            ListNode list = new ListNode("list", ObjectNode.ValueType_T.Numeric);

            var invoke = list.InvokeMethod(ListNode.Methods_T.Count);

            string expected = "@list.Count";
            string actual = invoke.Stringify();

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void InvokeRemoveMethod()
        {
            ListNode list = new ListNode("list", ObjectNode.ValueType_T.Numeric);

            ObjectNode n1 = ObjectNode.CreateNode("n1", ObjectNode.ValueType_T.Numeric);

            var invoke = list.InvokeMethod(ListNode.Methods_T.Remove, n1);

            string expected = "@list.Remove(@n1)";
            string actual = invoke.Stringify();

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidCastException))]
        public void InvokeRemoveMethodCastException()
        {
            ListNode list = new ListNode("list", ObjectNode.ValueType_T.Numeric);

            ObjectNode n1 = ObjectNode.CreateNode("n1", ObjectNode.ValueType_T.Object);

            var invoke = list.InvokeMethod(ListNode.Methods_T.Remove, n1);

            invoke.Stringify();
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidCastException))]
        public void InvokeAddMultipleMethodCastException()
        {
            ListNode list = new ListNode("list", ObjectNode.ValueType_T.Numeric);

            ObjectNode n1 = ObjectNode.CreateNode("n1", ObjectNode.ValueType_T.Numeric);
            ObjectNode n2 = ObjectNode.CreateNode("n2", ObjectNode.ValueType_T.Object);

            var invoke = list.InvokeMethod(ListNode.Methods_T.Add, n1, n2);

            invoke.Stringify();
        }
    }
}

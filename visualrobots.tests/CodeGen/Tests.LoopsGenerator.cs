﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RobotVL.Loops;
using RobotVL.Members.Types;

namespace VisualRobots.Tests.CodeGen
{
    [TestClass]
    public class TestsLoopsGenerator
    {
        [TestMethod]
        public void ForeachEmpty()
        {
            ListNode list = new ListNode("collection", ObjectNode.ValueType_T.Numeric);
            ObjectNode it = ObjectNode.CreateNode("iterator", ObjectNode.ValueType_T.Numeric, null);

            ForeachNode node = new ForeachNode(list, it);

            string actual = node.Stringify();
            string expected = "foreach(var @iterator in @collection)\n" +
                "{\n" +
                "}";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void ForeachNested()
        {
            ListNode list = new ListNode("collection", ObjectNode.ValueType_T.Numeric);
            ObjectNode it1 = ObjectNode.CreateNode("iterator", ObjectNode.ValueType_T.Numeric);
            ObjectNode it2 = ObjectNode.CreateNode("iterator2", ObjectNode.ValueType_T.Numeric);

            ForeachNode node = new ForeachNode(list, it1);
            ForeachNode nestedNode = new ForeachNode(list, it2);

            node.AddNode(nestedNode);

            string actual = node.Stringify();
            string expected = "foreach(var @iterator in @collection)\n" +
                "{\n" +
                    "\tforeach(var @iterator2 in @collection)\n" +
                    "\t{\n" +
                    "\t}\n" +
                "}";

            Assert.AreEqual(expected, actual);
        }
    }
}

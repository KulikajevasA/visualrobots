﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RobotVL.Main;
using RobotVL.Generation.DataStructures;
using RobotVL.Generation;

namespace VisualRobots.Tests.CodeGen
{
    [TestClass]
    public class TestsProjectGenerator
    {
        [TestMethod]
        public void GenerateProject()
        {
            RobotNode robot = new RobotNode("MyBot");
            ProjectInfo info = new ProjectInfo(robot);
            string root = "../../../CustomRobots";

            ProjectGenerator.GenerateProject(info, root);
        }
    }
}

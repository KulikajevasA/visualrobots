﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RobotVL.Members;
using RobotVL.Members.Types;

namespace VisualRobots.Tests.CodeGen
{
    [TestClass]
    public class TestsVariableGenerator
    {
        [TestMethod]
        public void CreateNumericNode()
        {
            var node = ObjectNode.CreateNode(ObjectNode.ValueType_T.Numeric);
            Assert.IsInstanceOfType(node, typeof(NumericNode));
        }

        [TestMethod]
        public void EvalNode()
        {
            string expected = "System.Console.Out.WriteLine(\"Hello, world!\");";

            EvalNode node = new EvalNode(expected);

            string actual = node.Stringify();

            Assert.AreEqual(expected, actual);
        }
    }
}

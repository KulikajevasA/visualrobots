﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RobotVL.Main;
using System.Xml;
using RobotVL;

namespace VisualRobots.Tests.CodeGen
{
    [TestClass]
    [Obsolete]
    public class TestSerialization
    {
        [TestMethod]
        public void SerializationTest()
        {
            var xdoc = new XmlDocument();


            var header = xdoc.CreateElement("RobotVL");
            header.SetAttribute("Version", "1.0");
            xdoc.AppendChild(header);

            var robotNode = new AvoidingRobotNode("MyRobot").Robot;

            robotNode.Serialize(xdoc, header);

            var actual = xdoc.AsBeautifiedString();
            //Assert.Fail();
        }
    }
}

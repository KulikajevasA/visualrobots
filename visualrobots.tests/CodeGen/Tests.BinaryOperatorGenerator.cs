﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RobotVL.Members;
using RobotVL.Operators.Binary;
using RobotVL.Members.Types;

namespace VisualRobots.Tests.CodeGen
{
    [TestClass]
    public class TestsBinaryOperatorGenerator
    {
        [TestMethod]
        public void AssignmentOperator()
        {
            MethodNode method = new MethodNode("MyMethod");
            ObjectNode iVar = ObjectNode.CreateNode("i", ObjectNode.ValueType_T.Numeric, 0);
            ObjectNode jVar = ObjectNode.CreateNode("j", ObjectNode.ValueType_T.Numeric, 0);

            AssignmentNode assignment = new AssignmentNode(iVar, jVar);

            method.AddNodes(iVar, jVar, assignment);

            string actual = method.Stringify();
            string expected = "public void MyMethod()\n" +
                 "{\n" +
                 "\tdouble @i = 0;\n" +
                 "\tdouble @j = 0;\n" +
                 "\t@i = @j;\n" +
                 "}";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void AdditionOperator()
        {
            MethodNode method = new MethodNode("MyMethod");
            ObjectNode iVar = ObjectNode.CreateNode("i", ObjectNode.ValueType_T.Numeric, 0);
            ObjectNode jVar = ObjectNode.CreateNode("j", ObjectNode.ValueType_T.Numeric, 0);

            AdditionNode assignment = new AdditionNode(iVar, jVar);

            method.AddNodes(iVar, jVar, assignment);

            string actual = method.Stringify();
            string expected = "public void MyMethod()\n" +
                 "{\n" +
                 "\tdouble @i = 0;\n" +
                 "\tdouble @j = 0;\n" +
                 "\t@i += @j;\n" +
                 "}";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void SubtractionOperator()
        {
            MethodNode method = new MethodNode("MyMethod");
            ObjectNode iVar = ObjectNode.CreateNode("i", ObjectNode.ValueType_T.Numeric, 0);
            ObjectNode jVar = ObjectNode.CreateNode("j", ObjectNode.ValueType_T.Numeric, 0);

            var myOperator = new SubtractionNode(iVar, jVar);

            method.AddNodes(iVar, jVar, myOperator);

            string actual = method.Stringify();
            string expected = "public void MyMethod()\n" +
                 "{\n" +
                 "\tdouble @i = 0;\n" +
                 "\tdouble @j = 0;\n" +
                 "\t@i -= @j;\n" +
                 "}";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void MultiplicationOperator()
        {
            MethodNode method = new MethodNode("MyMethod");
            ObjectNode iVar = ObjectNode.CreateNode("i", ObjectNode.ValueType_T.Numeric, 0);
            ObjectNode jVar = ObjectNode.CreateNode("j", ObjectNode.ValueType_T.Numeric, 0);

            var myOperator = new MultiplicationNode(iVar, jVar);

            method.AddNodes(iVar, jVar, myOperator);

            string actual = method.Stringify();
            string expected = "public void MyMethod()\n" +
                 "{\n" +
                 "\tdouble @i = 0;\n" +
                 "\tdouble @j = 0;\n" +
                 "\t@i *= @j;\n" +
                 "}";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void DivisionOperator()
        {
            MethodNode method = new MethodNode("MyMethod");
            ObjectNode iVar = ObjectNode.CreateNode("i", ObjectNode.ValueType_T.Numeric, 0);
            ObjectNode jVar = ObjectNode.CreateNode("j", ObjectNode.ValueType_T.Numeric, 0);

            var myOperator = new DivisionNode(iVar, jVar);

            method.AddNodes(iVar, jVar, myOperator);

            string actual = method.Stringify();
            string expected = "public void MyMethod()\n" +
                 "{\n" +
                 "\tdouble @i = 0;\n" +
                 "\tdouble @j = 0;\n" +
                 "\t@i /= @j;\n" +
                 "}";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void AssignAdditionOperator()
        {
            MethodNode method = new MethodNode("MyMethod");

            ObjectNode iVar = ObjectNode.CreateNode("i", ObjectNode.ValueType_T.Numeric, 0);
            ObjectNode jVar = ObjectNode.CreateNode("j", ObjectNode.ValueType_T.Numeric, 0);
            ObjectNode sumVar = ObjectNode.CreateNode("sum", ObjectNode.ValueType_T.Numeric, 0);

            AdditionNode additionOp = new AdditionNode(iVar, jVar);
            AssignmentNode assignmentOp = new AssignmentNode(sumVar, additionOp);

            method.AddNodes(iVar, jVar, sumVar, assignmentOp);

            string actual = method.Stringify();
            string expected = "public void MyMethod()\n" +
                 "{\n" +
                 "\tdouble @i = 0;\n" +
                 "\tdouble @j = 0;\n" +
                 "\tdouble @sum = 0;\n" +
                 "\t@sum = @i + @j;\n" +
                 "}";

            Assert.AreEqual(expected, actual);
        }
    }
}

﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RobotVL.Main;
using RobotVL.Members;
using RobotVL.Members.Types;

namespace VisualRobots.Tests.CodeGen
{
    [TestClass]
    public class TestsClassGenerator
    {
        [TestMethod]
        public void EmptyClass()
        {
            ClassNode classNode = new ClassNode("MyClass");

            string actual = classNode.Stringify();
            string expected = "public class MyClass\n" +
                "{\n" +
                "}";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void NestedEmptyClass()
        {
            ClassNode classNode = new ClassNode("MyClass");
            ClassNode nestedClassNode = new ClassNode("MyNestedClass");
            classNode.AddInnerClass(nestedClassNode);

            string actual = classNode.Stringify();
            string expected = "public class MyClass\n" +
                "{\n" +
                    "\tpublic class MyNestedClass\n" +
                    "\t{\n" +
                    "\t}\n" +
                "}";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void EmptyInterfaceClass()
        {
            ClassNode classNode = new ClassNode("MyClass");
            classNode.AddInterface("SomeInterface");

            string actual = classNode.Stringify();
            string expected = "public class MyClass : SomeInterface\n" +
                "{\n" +
                "}";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void EmptyMultiInterfaceClass()
        {
            ClassNode classNode = new ClassNode("MyClass");
            classNode.AddInterfaces("SomeInterface", "SomeInterface2");

            string actual = classNode.Stringify();
            string expected = "public class MyClass : SomeInterface, SomeInterface2\n" +
                "{\n" +
                "}";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void SinglVariableClass()
        {
            ClassNode classNode = new ClassNode("MyClass");
            ObjectNode variable = ObjectNode.CreateNode("myVar", ObjectNode.ValueType_T.Numeric, 5);
            classNode.AddVariable(variable);

            string actual = classNode.Stringify();
            string expected = "public class MyClass" +
                "\n{\n" +
                "\tprivate double @myVar = 5;\n" +
                "}";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void SinglListVariableClass()
        {
            ClassNode classNode = new ClassNode("MyClass");
            ListNode variable = new ListNode("myVar", ObjectNode.ValueType_T.Numeric);
            classNode.AddVariable(variable);

            string actual = classNode.Stringify();
            string expected = "public class MyClass" +
                "\n{\n" +
                "\tprivate System.Collections.Generic.List<double> @myVar = new System.Collections.Generic.List<double>();\n" +
                "}";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void MultipleVariableClass()
        {
            ClassNode classNode = new ClassNode("MyClass");
            ObjectNode variable1 = ObjectNode.CreateNode("myVar1", ObjectNode.ValueType_T.Numeric, 1.4);
            ObjectNode variable2 = ObjectNode.CreateNode("myVar2", ObjectNode.ValueType_T.Numeric, 8.8);
            classNode.AddVariables(variable1, variable2);

            string actual = classNode.Stringify();
            string expected = "public class MyClass\n" +
                "{\n" +
                "\tprivate double @myVar1 = 1.4;\n" +
                "\tprivate double @myVar2 = 8.8;\n" +
                "}";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void SingleMethodClass()
        {
            ClassNode classNode = new ClassNode("MyClass");
            MethodNode method = new MethodNode("MyMethod", MethodNode.Accessor_T.Public);
            classNode.AddMethod(method);

            string actual = classNode.Stringify();
            string expected = "public class MyClass\n" +
                "{\n" +
                "\tpublic void MyMethod()\n" +
                "\t{\n" +
                "\t}\n" +
                "}";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void MixedClass()
        {
            ClassNode classNode = new ClassNode("MyClass");
            ObjectNode variable = ObjectNode.CreateNode("myVar", ObjectNode.ValueType_T.Numeric, 5);
            MethodNode method = new MethodNode("MyMethod", MethodNode.Accessor_T.Public);
            classNode.AddVariable(variable);
            classNode.AddMethod(method);

            string actual = classNode.Stringify();
            string expected = "public class MyClass\n" +
                "{\n" +
                "\tprivate double @myVar = 5;\n" +
                "\tpublic void MyMethod()\n" +
                "\t{\n" +
                "\t}\n" +
                "}";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void MixedReturnClass()
        {
            ClassNode classNode = new ClassNode("MyClass");
            ObjectNode variable = ObjectNode.CreateNode("myVar", ObjectNode.ValueType_T.Numeric, 5);
            MethodNode method = new MethodNode("MyMethod", MethodNode.Accessor_T.Public);
            classNode.AddVariable(variable);
            classNode.AddMethod(method);
            method.Returns = variable;

            string actual = classNode.Stringify();
            string expected = "public class MyClass\n" +
                "{\n" +
                "\tprivate double @myVar = 5;\n" +
                "\tpublic double MyMethod()\n" +
                "\t{\n" +
                "\t\treturn @myVar;\n" +
                "\t}\n" +
                "}";

            Assert.AreEqual(expected, actual);
        }
    }
}

﻿using System;

public class EntryPoint : Simulation.IEntryPoint
{
    public Simulation.IRobot CreateRobot()
    {
        return new DaRobot();
    }

    public class DaRobot : Simulation.IRobot
    {
        public void InitializeParts(System.Collections.Generic.List<Simulation.Parts.IPart> parts)
        {
            throw new NotImplementedException();
        }

        public void RunFrame()
        {
            throw new NotImplementedException();
        }
    }
}
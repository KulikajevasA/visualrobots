﻿using RobotVL.Members.Types;
using System;
using System.Xml;

namespace RobotVL.Comparators
{
    /// <summary>
    /// Greater than comparator node.
    /// </summary>
    public class GTComparatorNode : ComparatorNode
    {
        /// <summary>
        /// Constructor for the comparator node.
        /// </summary>
        /// <param name="node1">First value to compare.</param>
        /// <param name="node2">Second value to compare.</param>
        public GTComparatorNode(ObjectNode node1, ObjectNode node2) : base(node1, node2) { }

        /// <summary>
        /// Used for generating code.
        /// </summary>
        /// <param name="indents">Indentation count.</param>
        /// <returns>Stringified node.</returns>
        public override string Stringify(int indents = 0)
        {
            return Utilities.Utils_Other.GetIndents(indents) +
                string.Format("{0} > {1}", lhsNode.Stringify(), rhsNode.Stringify());
        }

        /// <summary>
        /// Serializes a node.
        /// </summary>
        /// <param name="xdoc">Serialization document.</param>
        /// <param name="parent">Serialization parent.</param>
        public override void Serialize(XmlDocument xdoc, XmlElement parent)
        {
            throw new NotImplementedException();
        }
    }
}

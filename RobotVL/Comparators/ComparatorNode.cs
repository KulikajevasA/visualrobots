﻿using RobotVL.Members.Types;
using System.Xml;

namespace RobotVL.Comparators
{
    /// <summary>
    /// Abstract class for comparator node.
    /// </summary>
    public abstract class ComparatorNode : ISyntaxNode
    {
        protected ObjectNode lhsNode, rhsNode;

        /// <summary>
        /// Constructor for the comparator node.
        /// </summary>
        /// <param name="lhsNode">Left hand value to compare.</param>
        /// <param name="rhsNode">Righ hand value to compare.</param>
        protected ComparatorNode(ObjectNode lhsNode, ObjectNode rhsNode)
        {
            this.lhsNode = lhsNode;
            this.rhsNode = rhsNode;
        }

        /// <summary>
        /// Used for generating code.
        /// </summary>
        /// <param name="indents">Indentation count.</param>
        /// <returns>Stringified node.</returns>
        public abstract string Stringify(int indents = 0);

        /// <summary>
        /// Serializes a node.
        /// </summary>
        /// <param name="xdoc">Serialization document.</param>
        /// <param name="parent">Serialization parent.</param>
        public abstract void Serialize(XmlDocument xdoc, XmlElement parent);
    }
}

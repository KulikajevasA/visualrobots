﻿using RobotVL.Members.Types;
using System;
using System.Xml;

namespace RobotVL.Comparators
{
    /// <summary>
    /// Greater than or equal comparator node.
    /// </summary>
    public class GTEComparatorNode : ComparatorNode
    {
        /// <summary>
        /// Constructor for the comparator node.
        /// </summary>
        /// <param name="lhsNode">Left hand value to compare.</param>
        /// <param name="rhsNode">Right hand value to compare.</param>
        public GTEComparatorNode(ObjectNode lhsNode, ObjectNode rhsNode) : base(lhsNode, rhsNode) { }

        /// <summary>
        /// Used for generating code.
        /// </summary>
        /// <param name="indents">Indentation count.</param>
        /// <returns>Stringified node.</returns>
        public override string Stringify(int indents = 0)
        {
            return Utilities.Utils_Other.GetIndents(indents) +
                string.Format("{0} >= {1}", lhsNode.Stringify(), rhsNode.Stringify());
        }

        /// <summary>
        /// Serializes a node.
        /// </summary>
        /// <param name="xdoc">Serialization document.</param>
        /// <param name="parent">Serialization parent.</param>
        public override void Serialize(XmlDocument xdoc, XmlElement parent)
        {
            throw new NotImplementedException();
        }
    }
}

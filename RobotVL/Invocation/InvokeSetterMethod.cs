﻿using System;
using RobotVL.Members.Types;

namespace RobotVL.Invocation
{
    /// <summary>
    /// Code generation node for setter method.
    /// </summary>
    public class InvokeSetterMethod : InvokeMethod
    {
        /// <summary>
        /// Value to use for setting.
        /// </summary>
        public ObjectNode SetValue { get; private set; }

        /// <summary>
        /// Constructor for setter invocation node.
        /// </summary>
        /// <param name="node">Node of which method to invoke.</param>
        /// <param name="method">Method to invoke.</param>
        /// <param name="setValue">Value to set node.</param>
        public InvokeSetterMethod(ObjectNode node, String method, ObjectNode setValue) : base(node, method)
        {
            this.SetValue = setValue;
        }

        /// <summary>
        /// Used for generating code.
        /// </summary>
        /// <param name="indents">Indentation count.</param>
        /// <returns>Stringified node.</returns>
        public override string Stringify(int indents = 0)
        {
            return string.Format("{0} = {1}", base.Stringify(indents), this.SetValue.VariableName);
        }
    }
}

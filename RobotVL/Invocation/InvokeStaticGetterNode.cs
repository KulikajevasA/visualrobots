﻿namespace RobotVL.Invocation
{
    /// <summary>
    /// Geter code generation node.
    /// </summary>
    public class InvokeStaticGetterNode : InvokeMethod
    {
        /// <summary>
        /// Constructor for method invocation node.
        /// </summary>
        /// <param name="node">Node of which getter to invoke.</param>
        /// <param name="method">Method to invoke.</param>
        public InvokeStaticGetterNode(ValueType_T type, string method) : base(type, method) { }

        public InvokeStaticGetterNode(string type, string method)
        {
            this.type = type;
            this.Method = method;
        }

        /// <summary>
        /// Used for generating code.
        /// </summary>
        /// <param name="indents">Indentation count.</param>
        /// <returns>Stringified node.</returns>
        public override string Stringify(int indents = 0)
        {
            return string.Format("{0}.{1}", this.Type, this.Method);
        }
    }
}

﻿using System.Collections.Generic;
using RobotVL.Members.Types;

namespace RobotVL.Invocation
{
    /// <summary>
    /// Method invocation code generation node.
    /// </summary>
    public class InvokeStaticRegularMethod : InvokeStaticGetterNode
    {
        /// <summary>
        /// Invocation parameters.
        /// </summary>
        public List<ObjectNode> Parameters { get; private set; }

        /// <summary>
        /// Constructor for method invocation node.
        /// </summary>
        /// <param name="type">Node type.</param>
        /// <param name="method">Method to invoke.</param>
        public InvokeStaticRegularMethod(ValueType_T type, string method, params ObjectNode[] @params) : base(type, method)
        {
            this.Parameters = new List<ObjectNode>();
            this.Parameters.AddRange(@params);
        }

        /// <summary>
        /// Used for generating code.
        /// </summary>
        /// <param name="indents">Indentation count.</param>
        /// <returns>Stringified node.</returns>
        public override string Stringify(int indents = 0)
        {
            string @params = string.Empty;

            if (this.Parameters.Count > 0)
                @params += this.Parameters[0].VariableName;

            for (int i = 1; i < this.Parameters.Count; i++)
                @params += ", " + this.Parameters[i].VariableName;

            return string.Format("{0}({1})", base.Stringify(indents), @params);
        }

        /// <summary>
        /// Adds a parameter to the parameter list.
        /// </summary>
        /// <param name="param">Parameter to add.</param>
        public virtual void AddParameter(ObjectNode param)
        {
            this.Parameters.Add(param);
        }

        /// <summary>
        /// Adds multiple parameters to the parameter list.
        /// </summary>
        /// <param name="params">Parameters to add.</param>
        public virtual void AddParameters(params ObjectNode[] @params)
        {
            this.Parameters.AddRange(@params);
        }

        /// <summary>
        /// Removes parameter from the parameter list.
        /// </summary>
        /// <param name="param">Parameter to remove.</param>
        public virtual void RemoveParameter(ObjectNode param)
        {
            this.Parameters.Remove(param);
        }

        /// <summary>
        /// Clears parameter list.
        /// </summary>
        public virtual void ClearParameters()
        {
            this.Parameters.Clear();
        }
    }
}

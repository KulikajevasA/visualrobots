﻿using RobotVL.Members.Types;

namespace RobotVL.Invocation
{
    /// <summary>
    /// Base class for method invocation code generation node.
    /// </summary>
    public abstract class InvokeMethod : ObjectNode
    {
        protected string type;

        /// <summary>
        /// Object methods.
        /// </summary>
        public new enum Methods_T { }

        /// <summary>
        /// Static methods.
        /// </summary>
        public new enum StaticMethods_T { }

        /// <summary>
        /// Node for which to invoke the method.
        /// </summary>
        public ObjectNode Node { get; protected set; }

        /// <summary>
        /// Method to invoke.
        /// </summary>
        //public MethodNode Method { get; protected set; }
        public string Method { get; protected set; }

        /// <summary>
        /// Constructor for method invocation node.
        /// </summary>
        protected InvokeMethod() { }

        /// <summary>
        /// Constructor for method invocation node.
        /// </summary>
        /// <param name="type">Type of the node.</param>
        /// <param name="method">Method to invoke.</param>
        protected InvokeMethod(ValueType_T type, string method)
        {
            this.Node = null;
            this.Method = method;
            this.type = ObjectNode.GetTypeString(type);
        }

        /// <summary>
        /// Constructor for method invocation node.
        /// </summary>
        /// <param name="node">Node of which method to invoke.</param>
        /// <param name="method">Method to invoke.</param>
        protected InvokeMethod(ObjectNode node, string method) : base()
        {
            this.Node = node;
            this.Method = method;

            if (node != null)
                this.type = node.Type;
        }

        /// <summary>
        /// Gets the type of the invoked method.
        /// </summary>
        public override string Type
        {
            get
            {
                return this.type;
            }
        }

        /// <summary>
        /// Used for generating code.
        /// </summary>
        /// <param name="indents">Indentation count.</param>
        /// <returns>Stringified node.</returns>
        public override string Stringify(int indents = 0)
        {
            return string.Format("{0}.{1}", this.Node == null ? "this" : this.Node.VariableName, this.Method);
        }
    }
}

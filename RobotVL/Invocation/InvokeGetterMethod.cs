﻿using RobotVL.Members.Types;

namespace RobotVL.Invocation
{
    /// <summary>
    /// Geter code generation node.
    /// </summary>
    public class InvokeGetterMethod : InvokeMethod
    {
        /// <summary>
        /// Constructor for method invocation node.
        /// </summary>
        /// <param name="node">Node of which getter to invoke.</param>
        /// <param name="method">Method to invoke.</param>
        public InvokeGetterMethod(ObjectNode node, string method) : base(node, method) { }
    }
}

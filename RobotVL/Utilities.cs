﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RobotVL.Utilities
{
    /// <summary>
    /// Utilities related to RobotVL.
    /// </summary>
    public static class Utils_Language
    {
        private static int anonCounter = 0;

        /// <summary>
        /// Generates anonymous name.
        /// </summary>
        /// <returns>Generated name.</returns>
        public static string GetAnonymousName()
        {
            return string.Format("Anonymous_{0}", anonCounter++);
        }
    }

    public static class Utils_Other
    {
        /// <summary>
        /// Gets indentations.
        /// </summary>
        /// <param name="indentCount">Indentation count.</param>
        /// <returns>Indentations.</returns>
        public static string GetIndents(int indentCount)
        {
            string str = string.Empty;
            for (int i = 0; i < indentCount; i++)
                str += "\t";
            return str;
        }
    }
}

﻿using RobotVL.Members.Types;
using System;

namespace RobotVL.Loops
{
    /// <summary>
    /// Node for handling foreach loop.
    /// </summary>
    public class ForeachNode : BaseMethod
    {
        /// <summary>
        /// Getter and setter for iterateable collection.
        /// </summary>
        public ListNode Collection { get; private set; }
        /// <summary>
        /// Getter and setter for collection iterator.
        /// </summary>
        public ObjectNode Iterator { get; private set; }

        /// <summary>
        /// Constructor for foreach node code generator.
        /// </summary>
        /// <param name="collection">Collection to iterate through.</param>
        public ForeachNode(ListNode collection)
        {
            this.Iterator = ObjectNode.CreateNode(collection.ValueType);
            this.Collection = collection;
        }

        /// <summary>
        /// Constructor for foreach node code generator.
        /// </summary>
        /// <param name="collection">Collection to iterate though.</param>
        /// <param name="iterator">Iterator to use for iteration.</param>
        public ForeachNode(ListNode collection, ObjectNode iterator)
        {
            if (iterator.ValueType != collection.ValueType)
                throw new InvalidCastException("Iterator type must match the collection type.");

            this.Iterator = iterator;
            this.Collection = collection;
        }

        /// <summary>
        /// Used for generating code.
        /// </summary>
        /// <param name="indents">Indentation count.</param>
        /// <returns>Stringified node.</returns>
        public override string Stringify(int indents = 0)
        {
            var idnts = Utilities.Utils_Other.GetIndents(indents);

            string @out = string.Format("{2}foreach(var {0} in {1})\n",
                this.Iterator.VariableName, this.Collection.VariableName, idnts);

            @out += string.Format("{0}{{\n", idnts);

            @out += this.StringifyGuts(indents);

            @out += string.Format("{0}}}", idnts);

            return @out;
        }
    }
}

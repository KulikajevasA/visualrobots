﻿using RobotVL;
using RobotVL.Invocation;
using RobotVL.Logical;
using RobotVL.Members;
using RobotVL.Members.Types;
using RobotVL.Operators;
using System.Collections.Generic;
using System.Xml;

namespace RobotVL
{
    /// <summary>
    /// Abstract class for method like generation style.
    /// Example: Methods, Loops.
    /// </summary>
    public abstract class BaseMethod : ISyntaxNode
    {
        /// <summary>
        /// Syntax nodes.
        /// </summary>
        public List<ISyntaxNode> Nodes { get; private set; }

        /// <summary>
        /// Constructor for base method class.
        /// </summary>
        protected BaseMethod()
        {
            this.Nodes = new List<ISyntaxNode>();
        }

        /// <summary>
        /// Used for generating code.
        /// </summary>
        /// <param name="indents">Indentation count.</param>
        /// <returns>Stringified node.</returns>
        public abstract string Stringify(int indents = 0);

        /// <summary>
        /// Adds a single node to the method.
        /// </summary>
        /// <param name="node">Node to add to the method.</param>
        public void AddNode(ISyntaxNode node)
        {
            this.Nodes.Add(node);
        }

        /// <summary>
        /// Inserts nodes to the method at specified intex.
        /// </summary>
        /// <param name="index">Index at which to insert nodes.</param>
        /// <param name="nodes">Nodes to insert.</param>
        public void InsertNodes(int index, params ISyntaxNode[] nodes)
        {
            this.Nodes.InsertRange(0, nodes);
        }

        /// <summary>
        /// Prepends method with nodes.
        /// </summary>
        /// <param name="nodes">Nodes to prepend.</param>
        public void PrependNodes(params ISyntaxNode[] nodes)
        {
            InsertNodes(0, nodes);
        }

        /// <summary>
        /// Adds nodes to the methods.
        /// </summary>
        /// <param name="nodes">Nodes to add to the methods.</param>
        public void AddNodes(params ISyntaxNode[] nodes)
        {
            this.Nodes.AddRange(nodes);
        }

        /// <summary>
        /// Stringifies method guts.
        /// </summary>
        /// <param name="indents">Indentation count.</param>
        /// <returns>Guts of the method.</returns>
        public virtual string StringifyGuts(int indents = 0)
        {
            string idnt = Utilities.Utils_Other.GetIndents(indents);
            var str = string.Empty;

            foreach (var node in this.Nodes)
            {
                if (node is InvokeMethod)
                    str += string.Format("\t{1}{0};\n", node.Stringify(indents + 1), idnt);
                else if (node is IfNode || node is EvalNode || node is BaseMethod || node is OperatorNode)
                    str += string.Format("{0}\n", node.Stringify(indents + 1));
                else if (node is ObjectNode)
                {
                    var vn = node as ObjectNode;
                    str += string.Format("\t{3}{0} {1} = {2};\n",
                        vn.Type, vn.Stringify(), vn.Value, idnt);
                }
                else
                    str += string.Format("{1}{0}\n", node.Stringify(indents + 1), idnt);
            }

            return str;
        }

        public virtual void Serialize(XmlDocument xdoc, XmlElement parent)
        {

        }
    }
}

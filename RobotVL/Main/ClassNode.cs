﻿using RobotVL.Members;
using RobotVL.Members.Types;
using System.Collections.Generic;
using System.Xml;

namespace RobotVL.Main
{
    /// <summary>
    /// Represents class node.
    /// </summary>
    public class ClassNode : ISyntaxNode
    {
        protected List<ObjectNode> variables;
        protected List<MethodNode> methods;
        protected List<ClassNode> innerClasses;
        protected List<string> interfaces;

        /// <summary>
        /// Getter & setter for class name.
        /// </summary>
        public string ClassName { get; set; }

        /// <summary>
        /// Constructor for class node.
        /// </summary>
        public ClassNode()
        {
            this.variables = new List<ObjectNode>();
            this.methods = new List<MethodNode>();
            this.innerClasses = new List<ClassNode>();
            this.interfaces = new List<string>();
            this.ClassName = Utilities.Utils_Language.GetAnonymousName();
        }

        /// <summary>
        /// Constructor for class node.
        /// </summary>
        /// <param name="name">Name of the class.</param>
        public ClassNode(string name)
        {
            this.variables = new List<ObjectNode>();
            this.methods = new List<MethodNode>();
            this.innerClasses = new List<ClassNode>();
            this.interfaces = new List<string>();
            this.ClassName = name;
        }

        /// <summary>
        /// Used for generating code.
        /// </summary>
        /// <param name="indents">Indentation count.</param>
        /// <returns>Stringified node.</returns>
        public virtual string Stringify(int indents = 0)
        {
            string idnt = Utilities.Utils_Other.GetIndents(indents);

            var interfaceString = string.Empty;

            if (this.interfaces.Count > 0)
                interfaceString = string.Format(" : {0}", this.interfaces[0]);

            for (int i = 1; i < this.interfaces.Count; i++)
                interfaceString += string.Format(", {0}", this.interfaces[i]);

            string str = string.Format("{2}public class {0}{1}\n{2}{{\n", ClassName, interfaceString, idnt);

            foreach (var @class in this.innerClasses)
                str += string.Format("{0}\n", @class.Stringify(indents + 1));

            foreach (var node in variables)
                str += string.Format("{3}\tprivate {0} {1} = {2};\n",
                    node.Type, node.VariableName, node.Value, idnt);

            foreach (var node in methods)
                str += string.Format("{0}\n", node.Stringify(indents + 1));

            return string.Format("{0}{1}}}", str, idnt);
        }

        /// <summary>
        /// Add a member variable to class.
        /// </summary>
        /// <param name="variable">Member variable to add.</param>
        public void AddVariable(ObjectNode variable)
        {
            this.variables.Add(variable);
        }

        /// <summary>
        /// Add member variables to class.
        /// </summary>
        /// <param name="variables">List of member variables.</param>
        public void AddVariables(params ObjectNode[] variables)
        {
            this.variables.AddRange(variables);
        }

        /// <summary>
        /// Add member method to class.
        /// </summary>
        /// <param name="method">Member method to add.</param>
        public void AddMethod(MethodNode method)
        {
            this.methods.Add(method);
        }

        /// <summary>
        /// Add member methods to class.
        /// </summary>
        /// <param name="methods">Member methods to add.</param>
        public void AddMethods(params MethodNode[] methods)
        {
            this.methods.AddRange(methods);
        }

        /// <summary>
        /// Adds interface to class.
        /// </summary>
        /// <param name="interface">Interface to add.</param>
        public void AddInterface(string @interface)
        {
            this.interfaces.Add(@interface);
        }

        /// <summary>
        /// Adds interfaces to class.
        /// </summary>
        /// <param name="interfaces">Interfaces to add.</param>
        public void AddInterfaces(params string[] interfaces)
        {
            this.interfaces.AddRange(interfaces);
        }

        /// <summary>
        /// Adds inner class to class.
        /// </summary>
        /// <param name="class">Inner class to add.</param>
        public void AddInnerClass(ClassNode @class)
        {
            this.innerClasses.Add(@class);
        }

        /// <summary>
        /// Adds inner classes to class.
        /// </summary>
        /// <param name="classes">Classes to add.</param>
        public void AddInnerClasses(params ClassNode[] classes)
        {
            this.innerClasses.AddRange(classes);
        }

        /// <summary>
        /// Serializes a node.
        /// </summary>
        /// <param name="xdoc">Serialization document.</param>
        /// <param name="parent">Serialization parent.</param>
        public virtual void Serialize(XmlDocument xdoc, XmlElement parent)   
        {
            var xmlClass = xdoc.CreateElement("Class");
            xmlClass.SetAttribute("Name", ClassName);

            #region INTERFACES
            var xmlInterfaces = xdoc.CreateElement("Interfaces");
            xmlClass.AppendChild(xmlInterfaces);

            for (int i = 0; i < this.interfaces.Count; i++)
            {
                var xmlInterface = xdoc.CreateElement("Interface");
                xmlInterface.InnerText = this.interfaces[i];
                xmlInterfaces.AppendChild(xmlInterface);
            }
            #endregion

            #region VARIABLES
            var xmlVariables = xdoc.CreateElement("Variables");
            xmlClass.AppendChild(xmlVariables);

            foreach (var var in this.variables)
            {
                var xmlVar = xdoc.CreateElement("Variable");
                xmlVar.SetAttribute("Accessor", "private");
                xmlVar.SetAttribute("Type", var.Type);
                xmlVar.SetAttribute("Name", var.VariableName);

                xmlVariables.AppendChild(xmlVar);
            }
            #endregion

            #region METHODS
            var xmlMethods = xdoc.CreateElement("Methods");
            xmlClass.AppendChild(xmlMethods);

            foreach (var node in methods)
                node.Serialize(xdoc, xmlMethods);
            #endregion

            #region CLASSESS
            var xmlInnerClassess = xdoc.CreateElement("Classess");
            xmlClass.AppendChild(xmlInnerClassess);
            #endregion

            parent.AppendChild(xmlClass);
        }
    }
}

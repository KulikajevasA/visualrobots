﻿using RobotVL.Comparators;
using RobotVL.Logical;
using RobotVL.Loops;
using RobotVL.Members;
using RobotVL.Members.Types;
using RobotVL.Operators.Binary;

namespace RobotVL.Main
{
    /// <summary>
    /// Robot node code builder.
    /// </summary>
    public class RobotNode : ClassNode
    {
        /// <summary>
        /// Main meat method. User may do as he wishes. Should never override Return.
        /// </summary>
        public MethodNode RunFrame { get; private set; }
        private ClassNode robotNode;

        private IMotorNode leftMotorNode, rightMotorNode, rearMotorNode;
        private ISensorArrayNode ultrasonicSensor;
        private ISensorNode colorSensor, lightSensor;

        /// <summary>
        /// Left motor.
        /// </summary>
        public IMotorNode LeftMotor
        {
            get
            {
                return leftMotorNode;
            }
        }
        /// <summary>
        /// Right motor.
        /// </summary>
        public IMotorNode RightMotor
        {
            get
            {
                return rightMotorNode;
            }
        }
        
        /// <summary>
        /// Ultrasonic array.
        /// </summary>
        public ISensorArrayNode UltrasonicArray
        {
            get
            {
                return ultrasonicSensor;
            }
        }

        /// <summary>
        /// Color sensor.
        /// </summary>
        public ISensorNode ColorSensor
        {
            get
            {
                return colorSensor;
            }
        }

        /// <summary>
        /// Color sensor.
        /// </summary>
        public ISensorNode LightSensor
        {
            get
            {
                return lightSensor;
            }
        }

        /// <summary>
        /// Name of the robot.
        /// </summary>
        public string RobotName
        {
            get
            {
                return this.robotNode.ClassName;
            }
        }

        /// <summary>
        /// Robot node.
        /// </summary>
        public ClassNode Robot
        {
            get
            {
                return robotNode;
            }
        }

        /// <summary>
        /// Constructor for robot node code builder.
        /// </summary>
        /// <param name="robotName">Name of the robot class.</param>
        public RobotNode(string robotName)
        {
            this.ClassName = "EntryPoint";
            this.AddInterface("Simulation.IEntryPoint");
            this.RunFrame = new MethodNode("RunFrame", MethodNode.Accessor_T.Public);
            this.robotNode = new ClassNode(robotName);
            this.robotNode.AddMethod(this.RunFrame);
            this.robotNode.AddInterface("Simulation.IRobot");
            this.AddInnerClass(this.robotNode);

            var methCreateRobot = new MethodNode("CreateRobot");
            methCreateRobot.ReturnType = ObjectNode.ValueType_T.IRobot;
            this.AddMethod(methCreateRobot);
            var evalCreateRobotReturn = new EvalNode(string.Format("return new {0}();", robotName));
            methCreateRobot.AddNode(evalCreateRobotReturn);

            CreateClassParameters();

            var initParts = new MethodNode("InitializeParts");
            var partsParam = new ListNode("parts", ObjectNode.ValueType_T.IPart);
            initParts.AddParameter(partsParam);

            this.robotNode.AddMethod(initParts);

            var methAssignMotor = new MethodNode("AssignMotor");
            robotNode.AddMethod(methAssignMotor);
            CreateAssignMotorMethodInternals(methAssignMotor);

            var methAssignUltrasonicArray = new MethodNode("AssignUltrasonicArray");
            robotNode.AddMethod(methAssignUltrasonicArray);
            CreateAssignUltrasonicMethodInternals(methAssignUltrasonicArray);

            var methAssignColorSensor = new MethodNode("AssignColorSensor");
            robotNode.AddMethod(methAssignColorSensor);
            CreateAssignColorSensorMethodInternals(methAssignColorSensor);

            var methAssignLightSensor = new MethodNode("AssignLightSensor");
            robotNode.AddMethod(methAssignLightSensor);
            CreateAssignLightSensorMethodInternals(methAssignLightSensor);

            CreateInitializePartsMethodInternals(initParts, partsParam, methAssignMotor, methAssignUltrasonicArray, methAssignColorSensor, methAssignLightSensor);
        }

        /// <summary>
        /// Creates class parameters.
        /// </summary>
        private void CreateClassParameters()
        {
            leftMotorNode = ObjectNode.CreateNode("leftMotor", ObjectNode.ValueType_T.IMotor) as IMotorNode;
            rightMotorNode = ObjectNode.CreateNode("rightMotor", ObjectNode.ValueType_T.IMotor) as IMotorNode;
            rearMotorNode = ObjectNode.CreateNode("rearMotor", ObjectNode.ValueType_T.IMotor) as IMotorNode;
            ultrasonicSensor = ObjectNode.CreateNode("ultrasonicSensorArray", ObjectNode.ValueType_T.ISensorArray) as ISensorArrayNode;
            colorSensor = ObjectNode.CreateNode("colorSensor", ObjectNode.ValueType_T.ISensor) as ISensorNode;
            lightSensor = ObjectNode.CreateNode("lightSensor", ObjectNode.ValueType_T.ISensor) as ISensorNode;

            this.robotNode.AddVariables(leftMotorNode, rightMotorNode, rearMotorNode, ultrasonicSensor, colorSensor, lightSensor);
        }

        /// <summary>
        /// Creates AssignMotor() internals.
        /// </summary>
        /// <param name="method">AssignMotor method.</param>
        private void CreateAssignMotorMethodInternals(MethodNode method)
        {
            var methParam = (IMotorNode)ObjectNode.CreateNode("motor", ObjectNode.ValueType_T.IMotor);
            method.AddParameter(methParam);

            var varLeft = ObjectNode.CreateNode("motLeft", ObjectNode.ValueType_T.Text, "MotorLeft");
            var varRight = ObjectNode.CreateNode("motRight", ObjectNode.ValueType_T.Text, "MotorRight");
            var varRear = ObjectNode.CreateNode("motRear", ObjectNode.ValueType_T.Text, "MotorRear");

            method.AddNodes(varLeft, varRight, varRear);

            var partName = methParam.InvokeMethod(IPartNode.Methods_T.PartName);

            var ifLeft = new IfNode(new EQComparatorNode(partName, varLeft));
            var ifRight = new IfNode(new EQComparatorNode(partName, varRight));
            var ifRear = new IfNode(new EQComparatorNode(partName, varRear));

            method.AddNode(ifLeft);
            ifLeft.Otherwise.AddNode(ifRight);
            ifRight.Otherwise.AddNode(ifRear);

            var assignLeft = new AssignmentNode(this.leftMotorNode, methParam);
            var assignRight = new AssignmentNode(this.rightMotorNode, methParam);
            var assignRear = new AssignmentNode(this.rearMotorNode, methParam);

            ifLeft.Success.AddNode(assignLeft);
            ifRight.Success.AddNode(assignRight);
            ifRear.Success.AddNode(assignRear);
        }

        /// <summary>
        /// Creates InitializeParts() internals.
        /// </summary>
        /// <param name="method">InitializeParts method.</param>
        /// <param name="partsCollection">Parts collection.</param>
        /// <param name="assignMotor">AssignMotor method.</param>
        private void CreateInitializePartsMethodInternals(MethodNode method, ListNode partsCollection, MethodNode assignMotor, MethodNode assignUltrasonicArray, MethodNode assignColorSensor, MethodNode assignLightSensor)
        {
            var it = ObjectNode.CreateNode("part", ObjectNode.ValueType_T.IPart);
            var loopNode = new ForeachNode(partsCollection, it);

            method.AddNode(loopNode);

            var invokeGetMotorVal = PartTypeNode.InvokeStaticMethod(PartTypeNode.StaticMethods_T.Motor);
            var invokePartType = it.InvokeMethod(IPartNode.Methods_T.PartType);
            var eqIsMotor = new EQComparatorNode(invokePartType, invokeGetMotorVal);
            var ifMotor = new IfNode(eqIsMotor);

            var invokeGetUltrasonicSensorVal = PartTypeNode.InvokeStaticMethod(PartTypeNode.StaticMethods_T.UltrasonicSenorArray);
            var eqIsUltrasonic = new EQComparatorNode(invokePartType, invokeGetUltrasonicSensorVal);
            var ifUltrasonicArray = new IfNode(eqIsUltrasonic);

            loopNode.AddNode(ifMotor);

            ifMotor.Success.AddNode(assignMotor.Invoke(it.CastTo(ObjectNode.ValueType_T.IMotor)));
            ifMotor.Otherwise.AddNode(ifUltrasonicArray);

            ifUltrasonicArray.Success.AddNode(assignUltrasonicArray.Invoke(it.CastTo(ObjectNode.ValueType_T.ISensorArray)));

            var invokeGetColorSensorVal = PartTypeNode.InvokeStaticMethod(PartTypeNode.StaticMethods_T.ColorSensor);
            var eqIsColor = new EQComparatorNode(invokePartType, invokeGetColorSensorVal);
            var ifColorSensor = new IfNode(eqIsColor);
            ifUltrasonicArray.Otherwise.AddNode(ifColorSensor);
            ifColorSensor.Success.AddNode(assignColorSensor.Invoke(it.CastTo(ObjectNode.ValueType_T.ISensor)));

            var invokeGetLightSensorVal = PartTypeNode.InvokeStaticMethod(PartTypeNode.StaticMethods_T.LightSensor);
            var eqIsLight = new EQComparatorNode(invokePartType, invokeGetLightSensorVal);
            var ifLightSensor = new IfNode(eqIsLight);
            ifColorSensor.Otherwise.AddNode(ifLightSensor);
            ifLightSensor.Success.AddNode(assignLightSensor.Invoke(it.CastTo(ObjectNode.ValueType_T.ISensor)));
        }

        /// <summary>
        /// Creates AssignUltrasonicArray() internals.
        /// </summary>
        /// <param name="method">Method to which internals should be added.</param>
        private void CreateAssignUltrasonicMethodInternals(MethodNode method)
        {
            var methParam = ObjectNode.CreateNode("ultrasonic", ObjectNode.ValueType_T.ISensorArray);
            method.AddParameter(methParam);

            var assignNode = new AssignmentNode(this.ultrasonicSensor, methParam);
            method.AddNode(assignNode);
        }

        /// <summary>
        /// Creates AssignUltrasonicArray() internals.
        /// </summary>
        /// <param name="method">Method to which internals should be added.</param>
        private void CreateAssignColorSensorMethodInternals(MethodNode method)
        {
            var methParam = ObjectNode.CreateNode("color", ObjectNode.ValueType_T.ISensor);
            method.AddParameter(methParam);

            var assignNode = new AssignmentNode(this.colorSensor, methParam);
            method.AddNode(assignNode);
        }

        /// <summary>
        /// Creates AssignUltrasonicArray() internals.
        /// </summary>
        /// <param name="method">Method to which internals should be added.</param>
        private void CreateAssignLightSensorMethodInternals(MethodNode method)
        {
            var methParam = ObjectNode.CreateNode("light", ObjectNode.ValueType_T.ISensor);
            method.AddParameter(methParam);

            var assignNode = new AssignmentNode(this.lightSensor, methParam);
            method.AddNode(assignNode);
        }
    }
}

﻿using System;

namespace RobotVL.Operators.Unary
{
    /// <summary>
    /// Base class for unary operator code generator.
    /// </summary>
    public abstract class UnaryOperatorNode : OperatorNode
    {
        protected ISyntaxNode node;

        /// <summary>
        /// Constructor for base unary operator
        /// </summary>
        /// <param name="node">Input node.</param>
        public UnaryOperatorNode(ISyntaxNode node) : base()
        {
            ValidateNodes(node);

            this.node = node;
        }

        /// <summary>
        /// Validates node support. Should be overriden by inherited classes if custom rules are required.
        /// </summary>
        /// <param name="node">Node input.</param>
        public virtual void ValidateNodes(ISyntaxNode node)
        {
            if (!IsTypeSupported(node.GetType()))
                throw new InvalidCastException("Operator does not suport the type of left-hand node.");
        }
    }
}

﻿using RobotVL.Invocation;
using RobotVL.Members.Types;
using System;

namespace RobotVL.Operators.Binary
{
    /// <summary>
    /// Class for generating assignment operator node code.
    /// </summary>
    public class AssignmentNode : BinaryOperatorNode
    {
        /// <summary>
        /// Assignment operator node constructor.
        /// </summary>
        /// <param name="lhsNode">Left-hand node input.</param>
        /// <param name="rhsNode">Right-hand node input.</param>
        public AssignmentNode(ISyntaxNode lhsNode, ISyntaxNode rhsNode) : base(lhsNode, rhsNode) { }

        /// <summary>
        /// Used for generating code.
        /// </summary>
        /// <param name="indents">Indentation count.</param>
        /// <returns>Stringified node.</returns>
        public override string Stringify(int indents = 0)
        {
            string idnt = Utilities.Utils_Other.GetIndents(indents);

            string rhsString;

            if (rhsNode is BinaryOperatorNode)
                rhsString = ((BinaryOperatorNode)rhsNode).StringifyLocally();
            else
                rhsString = rhsNode.Stringify();

            return string.Format("{2}{0} = {1};", lhsNode.Stringify(), rhsString, idnt);
        }

        /// <summary>
        /// Should never be called, this method is created for add, sub, mul, div operators.
        /// </summary>
        public override string StringifyLocally()
        {
            throw new InvalidOperationException("This method should never be called for assignment node because it doesn't make sense.");
        }

        /// <summary>
        /// Custom ruleset for assignment node.
        /// </summary>
        /// <param name="lhsNode">Left-hand node input.</param>
        /// <param name="rhsNode">Right-hand node input.</param>
        public override void ValidateNodes(ISyntaxNode lhsNode, ISyntaxNode rhsNode)
        {
            if (lhsNode.GetType() != rhsNode.GetType() && !(rhsNode is BinaryOperatorNode || rhsNode is InvokeMethod))
                throw new InvalidCastException("Node types must be identical or right-hand node must be a two-input operator.");

            if (!IsTypeSupported(lhsNode.GetType()))
                throw new InvalidCastException("Operator does not suport the type of left-hand node.");

            if (!(rhsNode is BinaryOperatorNode) && !IsTypeSupported(rhsNode.GetType()))
                throw new InvalidCastException("Operator does not support the type of right-hand node.");
        }

        /// <summary>
        /// Creates allowed types.
        /// </summary>
        public override void CreateAllowedTypes()
        {
            allowedTypes = new Type[] {
                typeof(ObjectNode),
                typeof(ListNode),
                typeof(NumericNode),
                typeof(IPartNode),
                typeof(IMotorNode),
                typeof(ISensorArrayNode),
                typeof(InvokeGetterMethod),
                typeof(ISensorNode)
            };
        }
    }
}

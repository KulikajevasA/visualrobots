﻿using System;

namespace RobotVL.Operators.Binary
{
    /// <summary>
    /// Base class responsible for generating binary operator node code.
    /// </summary>
    public abstract class BinaryOperatorNode : OperatorNode
    {
        protected ISyntaxNode rhsNode, lhsNode;

        /// <summary>
        /// Operator node base constructor.
        /// </summary>
        /// <param name="lhsNode">Left-hand node input.</param>
        /// <param name="rhsNode">Right-hand node input.</param>
        protected BinaryOperatorNode(ISyntaxNode lhsNode, ISyntaxNode rhsNode) : base()
        {
            ValidateNodes(lhsNode, rhsNode);

            this.rhsNode = rhsNode;
            this.lhsNode = lhsNode;
        }

        /// <summary>
        /// Validates node support. Should be overriden by inherited classes if custom rules are required.
        /// </summary>
        /// <param name="lhsNode">Left-hand node input.</param>
        /// <param name="rhsNode">Right-hand node input.</param>
        public virtual void ValidateNodes(ISyntaxNode lhsNode, ISyntaxNode rhsNode)
        {
            if (lhsNode.GetType() != rhsNode.GetType())
                throw new InvalidCastException("Node types must be identical.");

            if (!IsTypeSupported(lhsNode.GetType()))
                throw new InvalidCastException("Operator does not suport the type of left-hand node.");

            if (!IsTypeSupported(rhsNode.GetType()))
                throw new InvalidCastException("Operator does not support the type of right-hand node.");
        }

        /// <summary>
        /// Stringifies without modifying left-hand side node.
        /// </summary>
        public abstract string StringifyLocally();
    }
}

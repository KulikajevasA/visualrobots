﻿using RobotVL.Members.Types;
using System;

namespace RobotVL.Operators.Binary
{
    /// <summary>
    /// Class for representing addition operator.
    /// </summary>
    public class AdditionNode : BinaryOperatorNode
    {
        /// <summary>
        /// Addition node constructor.
        /// </summary>
        /// <param name="lhsNode">Left-hand node input.</param>
        /// <param name="rhsNode">Right-hand node input.</param>
        public AdditionNode(ISyntaxNode lhsNode, ISyntaxNode rhsNode) : base(lhsNode, rhsNode) { }

        /// <summary>
        /// Creates allowed types.
        /// </summary>
        public override void CreateAllowedTypes()
        {
            allowedTypes = new Type[] {
                typeof(NumericNode)
            };
        }

        /// <summary>
        /// Used for generating code.
        /// </summary>
        /// <param name="indents">Indentation count.</param>
        /// <returns>Stringified node.</returns>
        public override string Stringify(int indents = 0)
        {
            string idnt = Utilities.Utils_Other.GetIndents(indents);

            return string.Format("{2}{0} += {1};", lhsNode.Stringify(), rhsNode.Stringify(), idnt);
        }

        /// <summary>
        /// Stringifies without modifying left-hand side node.
        /// </summary>
        public override string StringifyLocally()
        {
            return string.Format("{0} + {1}", lhsNode.Stringify(), rhsNode.Stringify());
        }
    }
}

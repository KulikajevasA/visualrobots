﻿using RobotVL.Members.Types;

namespace RobotVL.Operators.Binary
{
    /// <summary>
    /// Assignment node that assigns value by value instead of reference.
    /// </summary>
    public class AssignmentByValueNode : AssignmentNode
    {
        /// <summary>
        /// Constructor for node.
        /// </summary>
        /// <param name="lhsNode">Left hand node.</param>
        /// <param name="rhsNode">Right hand node.</param>
        public AssignmentByValueNode(ISyntaxNode lhsNode, ObjectNode rhsNode) : base(lhsNode, rhsNode) { }

        /// <summary>
        /// Used for generating code.
        /// </summary>
        /// <param name="indents">Indentation count.</param>
        /// <returns>Stringified node.</returns>
        public override string Stringify(int indents = 0)
        {
            string idnt = Utilities.Utils_Other.GetIndents(indents);

            return string.Format("{2}{0} = {1};", lhsNode.Stringify(), (rhsNode as ObjectNode).Value, idnt);
        }
    }
}

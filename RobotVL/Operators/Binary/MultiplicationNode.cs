﻿using RobotVL.Members.Types;
using System;

namespace RobotVL.Operators.Binary
{
    /// <summary>
    /// Represents multiplication node code generator.
    /// </summary>
    public class MultiplicationNode : BinaryOperatorNode
    {
        /// <summary>
        /// Multiplication node constructor.
        /// </summary>
        /// <param name="lhsNode">Left-hand node input.</param>
        /// <param name="rhsNode">Right-hand node input.</param>
        public MultiplicationNode(ISyntaxNode lhsNode, ISyntaxNode rhsNode) : base(lhsNode, rhsNode) { }

        /// <summary>
        /// Used for generating code.
        /// </summary>
        /// <param name="indents">Indentation count.</param>
        /// <returns>Stringified node.</returns>
        public override string Stringify(int indents = 0)
        {
            string idnt = Utilities.Utils_Other.GetIndents(indents);

            return string.Format("{2}{0} *= {1};", lhsNode.Stringify(), rhsNode.Stringify(), idnt);
        }

        /// <summary>
        /// Stringifies without modifying left-hand side node.
        /// </summary>
        public override string StringifyLocally()
        {
            return string.Format("{0} * {1}", lhsNode.Stringify(), rhsNode.Stringify());
        }

        /// <summary>
        /// Creates allowed types.
        /// </summary>
        public override void CreateAllowedTypes()
        {
            allowedTypes = new Type[] {
                typeof(NumericNode)
            };
        }
    }
}

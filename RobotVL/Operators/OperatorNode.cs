﻿using System;
using System.Xml;

namespace RobotVL.Operators
{
    public abstract class OperatorNode : ISyntaxNode
    {
        protected Type[] allowedTypes;

        /// <summary>
        /// Constructor for base operator node.
        /// </summary>
        protected OperatorNode()
        {
            CreateAllowedTypes();
        }

        /// <summary>
        /// Used for generating code.
        /// </summary>
        /// <param name="indents">Indentation count.</param>
        /// <returns>Stringified node.</returns>
        public abstract string Stringify(int indents = 0);

        /// <summary>
        /// Checks whether or not node type is supported.
        /// </summary>
        /// <param name="type">Type to check for support/</param>
        /// <returns>TRUE if supported, FALSE otherwise.</returns>
        public virtual bool IsTypeSupported(Type type)
        {
            bool isSupported = false;

            foreach (Type t in allowedTypes)
                isSupported |= t == type;

            return isSupported;
        }

        /// <summary>
        /// Creates allowed types.
        /// </summary>
        public abstract void CreateAllowedTypes();

        /// <summary>
        /// Serializes a node.
        /// </summary>
        /// <param name="xdoc">Serialization document.</param>
        /// <param name="parent">Serialization parent.</param>
        public void Serialize(XmlDocument xdoc, XmlElement parent)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using System;

namespace RobotVL
{
    /// <summary>
    /// Attribute that contains specified value for class.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class AttributeValue : Attribute
    {
        /// <summary>
        /// Getter for the value.
        /// </summary>
        public object Value { get; private set; }

        /// <summary>
        /// Constructor for the attribute.
        /// </summary>
        /// <param name="value">Value attribute contains.</param>
        public AttributeValue(object value)
        {
            this.Value = value;
        }
    }
}
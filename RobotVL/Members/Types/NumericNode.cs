﻿using System;
using RobotVL.Invocation;

namespace RobotVL.Members.Types
{
    /// <summary>
    /// Code generation node for numeric type.
    /// </summary>
    [AttributeValue(ValueType_T.Numeric)]
    public class NumericNode : ObjectNode
    {
        /// <summary>
        /// Object methods.
        /// </summary>
        public new enum Methods_T { }

        /// <summary>
        /// Static methods.
        /// </summary>
        public new enum StaticMethods_T { }

        /// <summary>
        /// Getter and setter for node value.
        /// </summary>
        public override object Value
        {
            get
            {
                if (this.value == null)
                    return "0.0";

                if (value is string)
                {
                    double @out;
                    if (double.TryParse(value as string, out @out))
                        this.value = @out;
                    else
                        return "double.NaN";
                }

                if (value is double || value is int || value is float)
                    return Convert.ToDouble(value).ToString(System.Globalization.CultureInfo.InvariantCulture);

                return base.Value;
            }
            set
            {
                base.Value = value;
            }
        }

        /// <summary>
        /// Creates node for method invocation.
        /// </summary>
        /// <param name="method">Method to invoke.</param>
        /// <param name="params">Invocation parameters.</param>
        /// <returns>Invocation node.</returns>
        public override InvokeMethod InvokeMethod(object method, params ObjectNode[] @params)
        {
            if (method is Methods_T)
                switch ((Methods_T)method)
                {
                    default:
                        throw new NotImplementedException();
                }

            return base.InvokeMethod(method, @params);
        }

        /// <summary>
        /// Creates a node for static invocation method.
        /// </summary>
        /// <param name="method">Method to invoke.</param>
        /// <param name="params">Parameters to invoke with.</param>
        /// <returns>Invocation node.</returns>
        public new static InvokeMethod InvokeStaticMethod(object method, params ObjectNode[] @params)
        {
            if (method is NumericNode.StaticMethods_T)
            {
                switch ((NumericNode.StaticMethods_T)method)
                {
                    default:
                        throw new NotImplementedException();
                }
            }

            return ObjectNode.InvokeStaticMethod(method, @params);
        }
    }
}

﻿using RobotVL.Invocation;
using System;
using System.Collections.Generic;
using System.Xml;

namespace RobotVL.Members.Types
{
    /// <summary>
    /// Code generation node for 'object' type.
    /// </summary>
    [AttributeValue(ValueType_T.Object)]
    public class ObjectNode : ISyntaxNode
    {
        /// <summary>
        /// Object methods.
        /// </summary>
        public enum Methods_T
        {
            Equals
        }

        /// <summary>
        /// Static methods.
        /// </summary>
        public enum StaticMethods_T { }

        /// <summary>
        /// Static constructor used to register the types.
        /// </summary>
        static ObjectNode()
        {
            RegisterType(typeof(ObjectNode));
            RegisterType(typeof(NumericNode));
            RegisterType(typeof(IRobotNode));
            RegisterType(typeof(IPartNode));
            RegisterType(typeof(PartTypeNode));
            RegisterType(typeof(IMotorNode));
            RegisterType(typeof(TextNode));
            RegisterType(typeof(ISensorArrayNode));
            RegisterType(typeof(ISensorNode));
            RegisterType(typeof(LiteralNode));
        }

        /// <summary>
        /// Supported types of variables.
        /// </summary>
        public enum ValueType_T
        {
            Unknown = 0,
            Object,
            Numeric,
            IRobot,
            IPart,
            PartType_T,
            IMotor,
            ISensor,
            ISensorArray,
            Text,
            Literal
        }

        private static Dictionary<ValueType_T, System.Type> types = new Dictionary<ValueType_T, System.Type>();
        private static Dictionary<ValueType_T, string> typeStrings = new Dictionary<ValueType_T, string>()
        {
            { ValueType_T.Object, typeof(object).Name.ToLowerInvariant() },
            { ValueType_T.Numeric, typeof(double).Name.ToLowerInvariant() },
            { ValueType_T.Text, typeof(string).Name.ToLowerInvariant() },
            { ValueType_T.IRobot, typeof(Simulation.IRobot).FullName },
            { ValueType_T.IPart, typeof(Simulation.Parts.IPart).FullName },
            { ValueType_T.PartType_T, typeof(Simulation.Parts.PartType_T).FullName },
            { ValueType_T.IMotor, typeof(Simulation.Parts.IMotor).FullName },
            { ValueType_T.ISensorArray, typeof(Simulation.Parts.Sensors.Arrays.ISensorArray).FullName },
            { ValueType_T.ISensor, typeof(Simulation.Parts.Sensors.ISensor).FullName }
        };

        protected object value;
        protected string variableName;

        /// <summary>
        /// Is this class scope variable.
        /// </summary>
        public bool IsClassScope { get; set; }

        /// <summary>
        /// Getter and setter for variable name.
        /// </summary>
        public virtual string VariableName
        {
            get
            {
                return '@' + variableName;
            }
            set
            {
                variableName = value;
            }
        }

        /// <summary>
        /// Getter for variable type.
        /// </summary>
        public virtual ValueType_T ValueType { get; protected set; }

        /// <summary>
        /// Retrieves the type variable string representation.
        /// </summary>
        public virtual string Type
        {
            get
            {
                return GetTypeString(this.ValueType);
            }
        }

        /// <summary>
        /// Getter and setter for variable value.
        /// </summary>
        public virtual object Value
        {
            set
            {
                this.value = value;
            }
            get
            {
                if (value is InvokeMethod)
                    return (this.value as InvokeMethod).Stringify();

                if (value is ObjectNode)
                    return (this.value as ObjectNode).VariableName;

                return "null";
            }
        }

        /// <summary>
        /// Getter and setter for type strings.
        /// </summary>
        public static Dictionary<ValueType_T, string> TypeStrings
        {
            get
            {
                return typeStrings;
            }

            set
            {
                typeStrings = value;
            }
        }

        /// <summary>
        /// Used for generating code.
        /// </summary>
        /// <param name="indents">Indentation count.</param>
        /// <returns>Stringified node.</returns>
        public virtual string Stringify(int indents = 0)
        {
            return IsClassScope ? "this." + VariableName : VariableName;
        }

        /// <summary>
        /// Serializes a node.
        /// </summary>
        /// <param name="xdoc">Serialization document.</param>
        /// <param name="parent">Serialization parent.</param>
        public virtual void Serialize(XmlDocument xdoc, XmlElement parent)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Creates a node.
        /// </summary>
        /// <param name="name">Name of the node.</param>
        /// <param name="nodeType">Type of the node.</param>
        /// <param name="value">Value of the node.</param>
        /// <returns>Created node if valid data. Otherwise NULL. </returns>
        public static ObjectNode CreateNode(string name, ValueType_T nodeType, object value = null)
        {
            if (nodeType == ValueType_T.Unknown || !types.ContainsKey(nodeType))
                return null;

            var type = types[nodeType];
            object instance = Activator.CreateInstance(type);

            if (!(instance is ObjectNode))
                return null;

            var node = instance as ObjectNode;
            node.Value = value;

            node.VariableName = name;
            node.ValueType = nodeType;

            return node;
        }

        public void Serialze(XmlDocument xdoc, XmlElement xmlVariables)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Creates a node with anonymous name.
        /// </summary>
        /// <param name="nodeType">Type of the node.</param>
        /// <param name="value">Value of the node.</param>
        /// <returns>Created node if valid data. Otherwise NULL. </returns>
        public static ObjectNode CreateNode(ValueType_T nodeType, object value = null)
        {
            var node = CreateNode(null, nodeType, value);

            node.VariableName = Utilities.Utils_Language.GetAnonymousName();

            return node;
        }

        /// <summary>
        /// Maps C# type to RobotVL type.
        /// </summary>
        /// <param name="type">Type to map.</param>
        /// <returns>RobotVL type if valid. Otherwise returns 'Unknown'.</returns>
        public static ValueType_T GetAssignedType(Type type)
        {
            var attr = Attribute.GetCustomAttribute(type, typeof(AttributeValue)) as AttributeValue;

            if (attr.Value is ValueType_T)
                return (ValueType_T)attr.Value;

            return ValueType_T.Unknown;
        }

        /// <summary>
        /// Registers a c# type.
        /// </summary>
        /// <param name="type">C# type to register.</param>
        public static void RegisterType(Type type)
        {
            var assigned = GetAssignedType(type);

            if (assigned == ValueType_T.Unknown)
                return;

            types.Add(assigned, type);
        }

        /// <summary>
        /// Gets type string from RobotVL representation.
        /// </summary>
        /// <param name="type">Type to retrieve.</param>
        /// <returns>String representation.</returns>
        public static string GetTypeString(ValueType_T type)
        {
            if (typeStrings.ContainsKey(type))
                return typeStrings[type];

            throw new InvalidCastException();
        }

        /// <summary>
        /// Creates invocation node for Equals() method.
        /// </summary>
        /// <param name="params">Invocation parameters.</param>
        /// <returns>Invocation node.</returns>
        private InvokeRegularMethod InvokeEquals(params ObjectNode[] @params)
        {
            if (@params.Length == 0)
                throw new IndexOutOfRangeException("Method requires a parameter.");

            var param = @params[0];

            return new InvokeRegularMethod(this, "Equals", param);
        }

        /// <summary>
        /// Creates node for method invocation.
        /// </summary>
        /// <param name="method">Method to invoke.</param>
        /// <param name="params">Invocation parameters.</param>
        /// <returns>Invocation node.</returns>
        public virtual InvokeMethod InvokeMethod(object method, params ObjectNode[] @params)
        {
            if (method is Methods_T)
            {
                switch ((Methods_T)method)
                {
                    case Methods_T.Equals:
                        return InvokeEquals(@params);
                    default:
                        throw new NotImplementedException();
                }
            }

            throw new InvalidCastException("Method not supported.");
        }

        /// <summary>
        /// Creates a node for static invocation method.
        /// </summary>
        /// <param name="method">Method to invoke.</param>
        /// <param name="params">Parameters to invoke with.</param>
        /// <returns>Invocation node.</returns>
        public static InvokeMethod InvokeStaticMethod(object method, params ObjectNode[] @params)
        {
            if (method is StaticMethods_T)
            {
                switch ((StaticMethods_T)method)
                {
                    default:
                        throw new NotImplementedException();
                }
            }

            throw new InvalidCastException("Static method not supported.");
        }

        /// <summary>
        /// Casts node to type.
        /// </summary>
        /// <param name="type">Type to cast node to.</param>
        /// <returns>Node after casting.</returns>
        public virtual ObjectNode CastTo(ValueType_T type)
        {
            return new CastNode(this, type);
        }
    }
}

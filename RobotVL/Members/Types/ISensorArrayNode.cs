﻿using RobotVL.Invocation;
using System;

namespace RobotVL.Members.Types
{
    /// <summary>
    /// Code generation node for Simulation.Parts.Sensors.Arrays.ISensorArray type.
    /// </summary>
    [AttributeValue(ValueType_T.ISensorArray)]
    public class ISensorArrayNode : IPartNode
    {
        /// <summary>
        /// Object methods.
        /// </summary>
        public new enum Methods_T
        {
            SensorDirection,
            SensorValue
        }

        /// <summary>
        /// Static methods.
        /// </summary>
        public new enum StaticMethods_T { }

        /// <summary>
        /// Creates invcation node for SensorDirection getter.
        /// </summary>
        /// <returns>Invocation node.</returns>
        private InvokeGetterMethod InvokeSensorDirection()
        {
            return new InvokeGetterMethod(this, "SensorDirection");
        }

        /// <summary>
        /// Creates invocation node for SensorValue getter.
        /// </summary>
        /// <returns>Invocation node.</returns>
        private InvokeGetterMethod InvokeSensorValue()
        {
            return new InvokeGetterMethod(this, "SensorValue");
        }

        /// <summary>
        /// Creates node for method invocation.
        /// </summary>
        /// <param name="method">Method to invoke.</param>
        /// <param name="params">Invocation parameters.</param>
        /// <returns>Invocation node.</returns>
        public override InvokeMethod InvokeMethod(object method, params ObjectNode[] @params)
        {
            if (method is Methods_T)
                switch ((Methods_T)method)
                {
                    case Methods_T.SensorDirection:
                        return InvokeSensorDirection();
                    case Methods_T.SensorValue:
                        return InvokeSensorValue();
                    default:
                        throw new NotImplementedException();
                }

            return base.InvokeMethod(method, @params);
        }

        /// <summary>
        /// Creates a node for static invocation method.
        /// </summary>
        /// <param name="method">Method to invoke.</param>
        /// <param name="params">Parameters to invoke with.</param>
        /// <returns>Invocation node.</returns>
        public new static InvokeMethod InvokeStaticMethod(object method, params ObjectNode[] @params)
        {
            if (method is StaticMethods_T)
            {
                switch ((StaticMethods_T)method)
                {
                    default:
                        throw new NotImplementedException();
                }
            }

            return ObjectNode.InvokeStaticMethod(method, @params);
        }
    }
}

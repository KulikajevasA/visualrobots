﻿using RobotVL.Invocation;
using System;

namespace RobotVL.Members.Types
{
    /// <summary>
    /// Code generation node for Simulation.Parts.IMotor type.
    /// </summary>
    [AttributeValue(ValueType_T.IMotor)]
    public class IMotorNode : IPartNode
    {
        /// <summary>
        /// Object methods.
        /// </summary>
        public new enum Methods_T
        {
            SetSpeed
        }

        /// <summary>
        /// Static methods.
        /// </summary>
        public new enum StaticMethods_T { }

        /// <summary>
        /// Creates invocation node for SetSpeed() method.
        /// </summary>
        /// <param name="params">Invocation parameters.</param>
        /// <returns>Invocation node.</returns>
        private InvokeRegularMethod InvokeSetSpeed(params ObjectNode[] @params)
        {
            if (@params.Length == 0)
                throw new IndexOutOfRangeException("SetSpeed(numeric) requires one parameter.");

            if (!(@params[0] is NumericNode) && !(@params[0] is LiteralNode))
                throw new InvalidCastException("Must be of numeric or literal type.");

            return new InvokeRegularMethod(this, "SetSpeed", @params[0]);
        }

        /// <summary>
        /// Creates node for method invocation.
        /// </summary>
        /// <param name="method">Method to invoke.</param>
        /// <param name="params">Invocation parameters.</param>
        /// <returns>Invocation node.</returns>
        public override InvokeMethod InvokeMethod(object method, params ObjectNode[] @params)
        {
            if (method is Methods_T)
                switch ((Methods_T)method)
                {
                    case Methods_T.SetSpeed:
                        return InvokeSetSpeed(@params);
                    default:
                        throw new NotImplementedException();
                }

            return base.InvokeMethod(method, @params);
        }

        /// <summary>
        /// Creates a node for static invocation method.
        /// </summary>
        /// <param name="method">Method to invoke.</param>
        /// <param name="params">Parameters to invoke with.</param>
        /// <returns>Invocation node.</returns>
        public new static InvokeMethod InvokeStaticMethod(object method, params ObjectNode[] @params)
        {
            if (method is StaticMethods_T)
            {
                switch ((StaticMethods_T)method)
                {
                    default:
                        throw new NotImplementedException();
                }
            }

            return ObjectNode.InvokeStaticMethod(method, @params);
        }
    }
}

﻿using RobotVL.Invocation;
using System;

namespace RobotVL.Members.Types
{
    /// <summary>
    /// Code generation node for Simulation.Parts.PartType_T type.
    /// </summary>
    [AttributeValue(ValueType_T.PartType_T)]
    class PartTypeNode : ObjectNode
    {
        /// <summary>
        /// Object methods.
        /// </summary>
        public new enum Methods_T { }

        /// <summary>
        /// Static methods.
        /// </summary>
        public new enum StaticMethods_T
        {
            Motor,
            UltrasonicSenorArray,
            ColorSensor,
            LightSensor
        }


        /// <summary>
        /// Creates static invocation node for PartType_T getter.
        /// </summary>
        /// <returns>Static invocation node.</returns>
        private static InvokeStaticGetterNode InvokePartTypeGetter(string partType)
        {
            return new InvokeStaticGetterNode(ValueType_T.PartType_T, partType);
        }

        /// <summary>
        /// Creates a node for static invocation method.
        /// </summary>
        /// <param name="method">Method to invoke.</param>
        /// <param name="params">Parameters to invoke with.</param>
        /// <returns>Invocation node.</returns>
        public new static InvokeMethod InvokeStaticMethod(object method, params ObjectNode[] @params)
        {
            if (method is StaticMethods_T)
            {
                switch ((StaticMethods_T)method)
                {
                    case StaticMethods_T.Motor:
                        return InvokePartTypeGetter("Motor");
                    case StaticMethods_T.UltrasonicSenorArray:
                        return InvokePartTypeGetter("UltrasonicSensorArray");
                    case StaticMethods_T.ColorSensor:
                        return InvokePartTypeGetter("ColorSensor");
                    case StaticMethods_T.LightSensor:
                        return InvokePartTypeGetter("LightSensor");
                    default:
                        throw new NotImplementedException();
                }
            }

            return ObjectNode.InvokeStaticMethod(method, @params);
        }
    }
}

﻿using RobotVL.Invocation;
using System;

namespace RobotVL.Members.Types
{
    /// <summary>
    /// Code generation node for list type.
    /// </summary>
    public class ListNode : ObjectNode
    {
        /// <summary>
        /// Object methods.
        /// </summary>
        public new enum Methods_T
        {
            Add,
            Remove,
            Clear,
            Count
        }

        /// <summary>
        /// Static methods.
        /// </summary>
        public new enum StaticMethods_T { }

        /// <summary>
        /// Constructor for list node.
        /// </summary>
        /// <param name="name">Name of the list node.</param>
        /// <param name="type">Type of the list items.</param>
        public ListNode(string name, ValueType_T type) : base()
        {
            this.VariableName = name;
            this.ValueType = type;
        }

        /// <summary>
        /// Constructor for list node with anonymous name.
        /// </summary>
        /// <param name="type">Type of the list items.</param>
        public ListNode(ValueType_T type) : base()
        {
            this.VariableName = Utilities.Utils_Language.GetAnonymousName();
            this.ValueType = type;
        }

        /// <summary>
        /// Retrieves the type variable string representation.
        /// </summary>
        public override string Type
        {
            get
            {
                return string.Format("System.Collections.Generic.List<{0}>", ObjectNode.GetTypeString(this.ValueType));
            }
        }

        /// <summary>
        /// Getter and setter for variable value.
        /// </summary>
        public override object Value
        {
            get
            {
                return string.Format("new System.Collections.Generic.List<{0}>()", ObjectNode.GetTypeString(this.ValueType));
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Creates invocation node for AddRange() method.
        /// </summary>
        /// <param name="params">Invocation parameters.</param>
        /// <returns>Invocation node.</returns>
        private InvokeRegularMethod InvokeAdd(params ObjectNode[] @params)
        {
            if (@params.Length == 0)
                throw new IndexOutOfRangeException("Method requires a parameter.");

            foreach (var p in @params)
                if (p.ValueType != this.ValueType)
                    throw new InvalidCastException("Must be of the same type.");

            return new InvokeRegularMethod(this, "AddRange", @params);
        }

        /// <summary>
        /// Creates invocation node for Clear() method.
        /// </summary>
        /// <returns>Invocation node.</returns>
        private InvokeRegularMethod InvokeClear()
        {
            return new InvokeRegularMethod(this, "Clear");
        }

        /// <summary>
        /// Creates invocation node for Remove() method.
        /// </summary>
        /// <param name="params">Invocation parameters.</param>
        /// <returns>Invocation node.</returns>
        private InvokeRegularMethod InvokeRemove(params ObjectNode[] @params)
        {
            if (@params.Length == 0)
                throw new IndexOutOfRangeException("Method requires a parameter.");

            var param = @params[0];

            if (param.ValueType != this.ValueType)
                throw new InvalidCastException("Must be of the same type.");

            return new InvokeRegularMethod(this, "Remove", param);
        }

        /// <summary>
        /// Creates invocation node for Count getter.
        /// </summary>
        /// <returns>Invocation node.</returns>
        private InvokeGetterMethod InvokeCount()
        {
            return new InvokeGetterMethod(this, "Count");
        }

        /// <summary>
        /// Creates node for method invocation.
        /// </summary>
        /// <param name="method">Method to invoke.</param>
        /// <param name="params">Invocation parameters.</param>
        /// <returns>Invocation node.</returns>
        public override InvokeMethod InvokeMethod(object method, params ObjectNode[] @params)
        {
            if (method is Methods_T)
            {
                switch ((Methods_T)method)
                {
                    case Methods_T.Add:
                        return InvokeAdd(@params);
                    case Methods_T.Clear:
                        return InvokeClear();
                    case Methods_T.Remove:
                        return InvokeRemove(@params);
                    case Methods_T.Count:
                        return InvokeCount();
                    default:
                        throw new NotImplementedException();
                }
            }

            return base.InvokeMethod(method, @params);
        }

        /// <summary>
        /// Creates a node for static invocation method.
        /// </summary>
        /// <param name="method">Method to invoke.</param>
        /// <param name="params">Parameters to invoke with.</param>
        /// <returns>Invocation node.</returns>
        public new static InvokeMethod InvokeStaticMethod(object method, params ObjectNode[] @params)
        {
            if (method is StaticMethods_T)
            {
                switch ((StaticMethods_T)method)
                {
                    default:
                        throw new NotImplementedException();
                }
            }

            return ObjectNode.InvokeStaticMethod(method, @params);
        }
    }
}

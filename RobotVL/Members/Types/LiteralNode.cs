﻿namespace RobotVL.Members.Types
{
    /// <summary>
    /// Node that represents literal value.
    /// </summary>
    [AttributeValue(ValueType_T.Literal)]
    public class LiteralNode : ObjectNode
    {
        /// <summary>
        /// Used for generating code.
        /// </summary>
        /// <param name="indents">Indentation count.</param>
        /// <returns>Stringified node.</returns>
        public override string Stringify(int indents = 0)
        {
            return value.ToString();
        }

        /// <summary>
        /// Getter for variable name.
        /// </summary>
        public override string VariableName
        {
            get
            {
                return value.ToString();
            }

            set { }
        }
    }
}

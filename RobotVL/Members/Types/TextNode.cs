﻿using System;
using RobotVL.Invocation;

namespace RobotVL.Members.Types
{
    /// <summary>
    /// Code generation node for string type.
    /// </summary>
    [AttributeValue(ValueType_T.Text)]
    public class TextNode : ObjectNode
    {
        /// <summary>
        /// Object methods.
        /// </summary>
        public new enum Methods_T { }

        /// <summary>
        /// Static methods.
        /// </summary>
        public new enum StaticMethods_T { }

        /// <summary>
        /// Getter and setter for node value.
        /// </summary>
        public override object Value
        {
            get
            {
                if (this.value is string)
                    return "\"" + this.value + "\"";

                return base.Value;
            }
            set
            {
                base.Value = value;
            }
        }

        /// <summary>
        /// Creates node for method invocation.
        /// </summary>
        /// <param name="method">Method to invoke.</param>
        /// <param name="params">Invocation parameters.</param>
        /// <returns>Invocation node.</returns>
        public override InvokeMethod InvokeMethod(object method, params ObjectNode[] @params)
        {
            if (method is Methods_T)
                switch ((Methods_T)method)
                {
                    default:
                        throw new NotImplementedException();
                }

            return base.InvokeMethod(method, @params);
        }

        /// <summary>
        /// Creates a node for static invocation method.
        /// </summary>
        /// <param name="method">Method to invoke.</param>
        /// <param name="params">Parameters to invoke with.</param>
        /// <returns>Invocation node.</returns>
        public new static InvokeMethod InvokeStaticMethod(object method, params ObjectNode[] @params)
        {
            if (method is NumericNode.StaticMethods_T)
            {
                switch ((NumericNode.StaticMethods_T)method)
                {
                    default:
                        throw new NotImplementedException();
                }
            }

            return ObjectNode.InvokeStaticMethod(method, @params);
        }
    }
}

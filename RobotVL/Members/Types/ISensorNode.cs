﻿using RobotVL.Invocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using static RobotVL.Members.Types.ObjectNode;

namespace RobotVL.Members.Types
{
    /// <summary>
    /// Code generation node for Simulation.Parts.Sensors.ISensor type.
    /// </summary>
    [AttributeValue(ValueType_T.ISensor)]
    public class ISensorNode : IPartNode
    {
        /// <summary>
        /// Object methods.
        /// </summary>
        public new enum Methods_T
        {
            SensorValue
        }

        /// <summary>
        /// Static methods.
        /// </summary>
        public new enum StaticMethods_T { }

        /// <summary>
        /// Creates invocation node for SensorValue getter.
        /// </summary>
        /// <returns>Invocation node.</returns>
        private InvokeGetterMethod InvokeSensorValue()
        {
            return new InvokeGetterMethod(this, "SensorData");
        }

        /// Creates node for method invocation.
        /// </summary>
        /// <param name="method">Method to invoke.</param>
        /// <param name="params">Invocation parameters.</param>
        /// <returns>Invocation node.</returns>
        public override InvokeMethod InvokeMethod(object method, params ObjectNode[] @params)
        {
            if (method is Methods_T)
                switch ((Methods_T)method)
                {
                    case Methods_T.SensorValue:
                        return InvokeSensorValue();
                    default:
                        throw new NotImplementedException();
                }

            return base.InvokeMethod(method, @params);
        }

        /// <summary>
        /// Creates a node for static invocation method.
        /// </summary>
        /// <param name="method">Method to invoke.</param>
        /// <param name="params">Parameters to invoke with.</param>
        /// <returns>Invocation node.</returns>
        public new static InvokeMethod InvokeStaticMethod(object method, params ObjectNode[] @params)
        {
            if (method is StaticMethods_T)
            {
                switch ((StaticMethods_T)method)
                {
                    default:
                        throw new NotImplementedException();
                }
            }

            return ObjectNode.InvokeStaticMethod(method, @params);
        }
    }
}

﻿using System;
using RobotVL.Invocation;

namespace RobotVL.Members.Types
{
    /// <summary>
    /// Code generation node for cast type.
    /// </summary>
    public class CastNode : ObjectNode
    {
        private ObjectNode node;

        /// <summary>
        /// Object methods.
        /// </summary>
        public new enum Methods_T { };

        /// <summary>
        /// Static methods.
        /// </summary>
        public new enum StaticMethods_T { };

        /// <summary>
        /// Constructor for cast node.
        /// </summary>
        /// <param name="node"></param>
        /// <param name="type"></param>
        public CastNode(ObjectNode node, ValueType_T type) : base()
        {
            this.VariableName = node.VariableName;
            this.ValueType = type;
            this.node = node;
        }

        /// <summary>
        /// Getter and setter for variable name.
        /// </summary>
        public override string VariableName
        {
            get
            {
                return string.Format("({0}){1}", ObjectNode.GetTypeString(this.ValueType), variableName);
            }

            set
            {
                base.VariableName = value;
            }
        }

        /// <summary>
        /// Creates node for method invocation.
        /// </summary>
        /// <param name="method">Method to invoke.</param>
        /// <param name="params">Invocation parameters.</param>
        /// <returns>Invocation node.</returns>
        public override InvokeMethod InvokeMethod(object method, params ObjectNode[] @params)
        {
            return this.node.InvokeMethod(method, @params);
        }

        /// <summary>
        /// Creates a node for static invocation method.
        /// </summary>
        /// <param name="method">Method to invoke.</param>
        /// <param name="params">Parameters to invoke with.</param>
        /// <returns>Invocation node.</returns>
        public new static InvokeMethod InvokeStaticMethod(object method, params ObjectNode[] @params)
        {
            throw new NotImplementedException();
        }
    }
}

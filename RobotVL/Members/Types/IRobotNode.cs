﻿namespace RobotVL.Members.Types
{
    /// <summary>
    /// Code generation node for Simulation.IRobot type.
    /// </summary>
    [AttributeValue(ValueType_T.IRobot)]
    public class IRobotNode : ObjectNode
    {
        /// <summary>
        /// Object methods.
        /// </summary>
        public new enum Methods_T { }

        /// <summary>
        /// Static methods.
        /// </summary>
        public new enum StaticMethods_T { }
    }
}

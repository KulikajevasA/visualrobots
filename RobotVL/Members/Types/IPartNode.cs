﻿using RobotVL.Invocation;
using System;

namespace RobotVL.Members.Types
{
    /// <summary>
    /// Code generation node for Simulation.Parts.IPart type.
    /// </summary>
    [AttributeValue(ValueType_T.IPart)]
    public class IPartNode : ObjectNode
    {
        /// <summary>
        /// Object methods.
        /// </summary>
        public new enum Methods_T
        {
            PartType,
            PartName,
        }

        /// <summary>
        /// Static methods.
        /// </summary>
        public new enum StaticMethods_T { }

        /// <summary>
        /// Creates invocation node for PartType getter.
        /// </summary>
        /// <returns></returns>
        private InvokeGetterMethod InvokePartType()
        {
            return new InvokeGetterMethod(this, "PartType");
        }

        /// <summary>
        /// Creates invocation node for PartType getter.
        /// </summary>
        /// <returns></returns>
        private InvokeGetterMethod InvokePartName()
        {
            return new InvokeGetterMethod(this, "PartName");
        }

        /// <summary>
        /// Creates node for method invocation.
        /// </summary>
        /// <param name="method">Method to invoke.</param>
        /// <param name="params">Invocation parameters.</param>
        /// <returns>Invocation node.</returns>
        public override InvokeMethod InvokeMethod(object method, params ObjectNode[] @params)
        {
            if (method is Methods_T)
                switch ((Methods_T)method)
                {
                    case Methods_T.PartType:
                        return InvokePartType();
                    case Methods_T.PartName:
                        return InvokePartName();
                    default:
                        throw new NotImplementedException();
                }

            return base.InvokeMethod(method, @params);
        }

        /// <summary>
        /// Creates a node for static invocation method.
        /// </summary>
        /// <param name="method">Method to invoke.</param>
        /// <param name="params">Parameters to invoke with.</param>
        /// <returns>Invocation node.</returns>
        public new static InvokeMethod InvokeStaticMethod(object method, params ObjectNode[] @params)
        {
            if (method is StaticMethods_T)
            {
                switch ((StaticMethods_T)method)
                {
                    default:
                        throw new NotImplementedException();
                }
            }

            return ObjectNode.InvokeStaticMethod(method, @params);
        }
    }
}

﻿using System;
using System.Xml;

namespace RobotVL.Members
{
    /// <summary>
    /// Node for auto eval code node.
    /// Used for advanced unsupported code.
    /// </summary>
    public class EvalNode : ISyntaxNode
    {
        /// <summary>
        /// Code that will be evaluated.
        /// </summary>
        public string Code { get; set; }

        public EvalNode(string code)
        {
            this.Code = code;
        }

        /// <summary>
        /// Used for generating code.
        /// </summary>
        /// <param name="indents">Indentation count.</param>
        /// <returns>Stringified node.</returns>
        public string Stringify(int indents = 0)
        {
            string idnt = Utilities.Utils_Other.GetIndents(indents);

            return string.Format("{0}{1}", idnt, Code);
        }

        /// <summary>
        /// Serializes a node.
        /// </summary>
        /// <param name="xdoc">Serialization document.</param>
        /// <param name="parent">Serialization parent.</param>
        public void Serialize(XmlDocument xdoc, XmlElement parent)
        {
            throw new NotImplementedException();
        }
    }
}

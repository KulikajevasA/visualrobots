﻿using RobotVL.Invocation;
using RobotVL.Members.Types;
using System.Collections.Generic;
using System;
using System.Xml;

namespace RobotVL.Members
{
    /// <summary>
    /// Node for representing a method.
    /// </summary>
    public class MethodNode : BaseMethod
    {
        /// <summary>
        /// Enumerator for accessor types.
        /// </summary>
        public enum Accessor_T
        {
            Public = 0,
            Private,
            Protected
        }

        /// <summary>
        /// Getter and setter for method parameters.
        /// </summary>
        public List<ObjectNode> Parameters { get; private set; }
        /// <summary>
        /// Getters and setter for method name.
        /// </summary>
        public string MethodName { get; set; }
        /// <summary>
        /// Getter and setter for method accessor modifier.
        /// </summary>
        public Accessor_T Accessor { get; set; }
        /// <summary>
        /// Getter and setter for method return value.
        /// </summary>
        public ObjectNode Returns { get; set; }
        /// <summary>
        /// Return type override. Only used when Returns is NULL.
        /// </summary>
        public ObjectNode.ValueType_T ReturnType { get; set; }

        /// <summary>
        /// Checks if node is empty.
        /// </summary>
        /// <returns></returns>
        public bool IsEmpty()
        {
            if (Returns == null && Nodes.Count == 0)
                return true;

            return false;
        }

        /// <summary>
        /// Constructor for method node.
        /// </summary>
        /// <param name="name">Name of the method.</param>
        /// <param name="accessor">Accessor type of the method.</param>
        public MethodNode(string name, Accessor_T accessor = Accessor_T.Public) : base()
        {
            this.Parameters = new List<ObjectNode>();
            this.MethodName = (name == string.Empty || name == null) ? Utilities.Utils_Language.GetAnonymousName() : name;
            this.Accessor = accessor;
        }

        /// <summary>
        /// Constructor for anonymous method.
        /// </summary>
        /// <param name="accessor">Accessor type.</param>
        public MethodNode(Accessor_T accessor = Accessor_T.Public) : base()
        {
            this.Parameters = new List<ObjectNode>();
            this.MethodName = Utilities.Utils_Language.GetAnonymousName();
            this.Accessor = accessor;
        }

        /// <summary>
        /// Gets string value for accessor type.
        /// </summary>
        /// <returns>Accessor string value.</returns>
        public string GetAccessorString()
        {
            switch (Accessor)
            {
                case Accessor_T.Private:
                    return "private";
                case Accessor_T.Protected:
                    return "protected";
                case Accessor_T.Public:
                default:
                    return "public";
            }
        }



        /// <summary>
        /// Gets string value for return type.
        /// </summary>
        /// <returns>Return type string value.</returns>
        public string GetReturnString()
        {
            if (Returns == null)
                return ReturnType == ObjectNode.ValueType_T.Unknown ? "void" : ObjectNode.GetTypeString(ReturnType);

            return Returns.Type;
        }

        /// <summary>
        /// Stringifies method guts.
        /// </summary>
        /// <param name="indents">Indentation count.</param>
        /// <returns>Guts of the method.</returns>
        public override string StringifyGuts(int indents = 0)
        {
            string idnt = Utilities.Utils_Other.GetIndents(indents);
            var str = base.StringifyGuts(indents);

            if (Returns != null)
                str += string.Format("{1}\treturn {0};\n", Returns.Stringify(), idnt);

            return str;
        }

        /// <summary>
        /// Used for generating code.
        /// </summary>
        /// <param name="indents">Indentation count.</param>
        /// <returns>Stringified node.</returns>
        public override string Stringify(int indents = 0)
        {
            string idnt = Utilities.Utils_Other.GetIndents(indents);

            string str = string.Format("{3}{0} {1} {2}(",
                GetAccessorString(), GetReturnString(), MethodName, idnt);

            if (Parameters.Count >= 1)
            {
                var p = Parameters[0];
                str += string.Format("{0} {1}", p.Type, p.Stringify());
            }

            for (int i = 1; i < Parameters.Count; i++)
            {
                var p = Parameters[i];
                str += string.Format(", {0} {1}", p.Type, p.Stringify());
            }

            str += string.Format(")\n{0}{{\n", idnt);

            str += StringifyGuts(indents);

            return string.Format("{0}{1}}}", str, idnt);
        }

        /// <summary>
        /// Creates self infocation node.
        /// </summary>
        /// <param name="params">Invocation parameters.</param>
        /// <returns>Invocation node.</returns>
        public InvokeMethod Invoke(params ObjectNode[] @params)
        {
            return new InvokeRegularMethod(this.MethodName, @params);
        }

        /// <summary>
        /// Adds parameter to method.
        /// </summary>
        /// <param name="param">Parameter to add.</param>
        public void AddParameter(ObjectNode param)
        {
            this.Parameters.Add(param);
        }

        /// <summary>
        /// Adds parameters to method.
        /// </summary>
        /// <param name="parameters">Parameters to add.</param>
        public void AddParameters(params ObjectNode[] parameters)
        {
            this.Parameters.AddRange(parameters);
        }

        public override void Serialize(XmlDocument xdoc, XmlElement parent)
        {
            

            var xmlMethod = xdoc.CreateElement("Method");

            xmlMethod.SetAttribute("Accessor", GetAccessorString());
            xmlMethod.SetAttribute("Type", GetReturnString());
            xmlMethod.SetAttribute("Name", MethodName);

            #region PARAMETERS
            var xmlParams = xdoc.CreateElement("Parameters");
            xmlMethod.AppendChild(xmlParams);

            for (int i = 0; i < Parameters.Count; i++)
            {
                var p = Parameters[i];
                var xmlParam = xdoc.CreateElement("Parameter");
                xmlParam.SetAttribute("Type", p.Type);
                xmlParam.SetAttribute("Name", p.Stringify());

                xmlParams.AppendChild(xmlParam);
            }
            #endregion


            #region NODES

            #endregion

            parent.AppendChild(xmlMethod);
        }
    }
}

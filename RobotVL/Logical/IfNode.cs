﻿using RobotVL.Comparators;
using RobotVL.Members;
using System;
using System.Xml;

namespace RobotVL.Logical
{
    /// <summary>
    /// IF statement node.
    /// </summary>
    public class IfNode : ISyntaxNode
    {
        /// <summary>
        /// Comparator of the node.
        /// </summary>
        public ComparatorNode Comparator { get; set; }
        /// <summary>
        /// Ran if comparator returns true.
        /// </summary>
        public MethodNode Success { get; private set; }
        /// <summary>
        /// Ran if comparator returns false.
        /// </summary>
        public MethodNode Otherwise { get; private set; }

        /// <summary>
        /// IF node constructor.
        /// </summary>
        /// <param name="comparator">Comparator for the IF node.</param>
        public IfNode(ComparatorNode comparator)
        {
            Comparator = comparator;
            Success = new MethodNode(MethodNode.Accessor_T.Private);
            Otherwise = new MethodNode(MethodNode.Accessor_T.Private);
        }

        /// <summary>
        /// Used for generating code.
        /// </summary>
        /// <param name="indents">Indentation count.</param>
        /// <returns>Stringified node.</returns>
        public string Stringify(int indents = 0)
        {
            string idnt = Utilities.Utils_Other.GetIndents(indents);

            var str = string.Format("{1}if({0})\n{1}{{\n", Comparator.Stringify(), idnt);

            str += Success.StringifyGuts(indents);

            str += string.Format("{0}}}", idnt);

            if (!Otherwise.IsEmpty())
            {
                str += string.Format("\n{0}else\n{0}{{\n", idnt);

                str += Otherwise.StringifyGuts(indents);

                str += string.Format("{0}}}", idnt);
            }

            return str;
        }

        /// <summary>
        /// Serializes a node.
        /// </summary>
        /// <param name="xdoc">Serialization document.</param>
        /// <param name="parent">Serialization parent.</param>
        public void Serialize(XmlDocument xdoc, XmlElement parent)
        {
            throw new NotImplementedException();
        }
    }
}

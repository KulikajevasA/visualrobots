﻿using System.Xml;

namespace RobotVL
{
    /// <summary>
    /// Interface for syntax node.
    /// </summary>
    public interface ISyntaxNode
    {
        /// <summary>
        /// Used for generating code.
        /// </summary>
        /// <param name="indents">Indentation count.</param>
        /// <returns>Stringified node.</returns>
        string Stringify(int indents = 0);

        /// <summary>
        /// Serializes a node.
        /// </summary>
        /// <param name="xdoc">Serialization document.</param>
        /// <param name="parent">Serialization parent.</param>
        void Serialize(XmlDocument xdoc, XmlElement parent);
    }
}

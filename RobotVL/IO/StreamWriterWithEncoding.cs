﻿using System.IO;
using System.Text;


namespace System.IO
{
    /// <summary>
    /// Adds encoding support for string writer.
    /// </summary>
    public class StringWriterWithEncoding : StringWriter
    {
        private readonly Encoding encoding;

        /// <summary>
        /// Constructor for string writer.
        /// </summary>
        /// <param name="encoding">Encoding type to use.</param>
        public StringWriterWithEncoding(Encoding encoding)
        {
            this.encoding = encoding;
        }

        /// <summary>
        /// Getter for encoding type.
        /// </summary>
        public override Encoding Encoding
        {
            get { return encoding; }
        }
    }
}

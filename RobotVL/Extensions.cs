﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;

namespace RobotVL
{
    public static class Extensions
    {
        /// <summary>
        /// Returns XML document as unformatted string.
        /// </summary>
        /// <param name="xmlDoc">XML document.</param>
        /// <returns>Unformatted XML string.</returns>
        public static string AsString(this XmlDocument xmlDoc)
        {
            using (StringWriter sw = new StringWriter())
            {
                using (XmlTextWriter tx = new XmlTextWriter(sw))
                {
                    xmlDoc.WriteTo(tx);
                    string strXmlText = sw.ToString();
                    return " <?xml version=\"1.0\" encoding=\"utf-8\"?>\n" + strXmlText;
                }
            }
        }

        /// <summary>
        /// Returns XML document as formatted string.
        /// </summary>
        /// <param name="xmlDoc">XML document to beautify.</param>
        /// <returns>XML document string.</returns>
        public static string AsBeautifiedString(this XmlDocument xmlDoc)
        {
            StringWriter sb = new StringWriterWithEncoding(Encoding.UTF8);

            XmlWriterSettings settings = new XmlWriterSettings
            {
                Indent = true,
                IndentChars = "\t",
                NewLineChars = "\r\n",
                NewLineHandling = NewLineHandling.Replace,
                Encoding = Encoding.UTF8
            };

            using (XmlWriter writer = XmlWriter.Create(sb, settings))
                xmlDoc.Save(writer);

            return sb.ToString().Replace("xmlns=\"\"", string.Empty);
        }
    }
}
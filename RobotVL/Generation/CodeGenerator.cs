﻿using RobotVL.Main;

namespace RobotVL.Generator
{
    /// <summary>
    /// Generates code for class.
    /// </summary>
    public class CodeGenerator
    {
        private static string[] namespaces = new string[] {
            "System",
        };

        /// <summary>
        /// Generates code for class.
        /// </summary>
        /// <param name="node">Class node to generate code for.</param>
        /// <returns>Class code.</returns>
        public static string Generate(ClassNode node)
        {
            string @out = string.Empty;

            for (int i = 0; i < namespaces.Length; i++)
                @out += string.Format("using {0};\n", namespaces[i]);

            @out += string.Format("\n{0}", node.Stringify());

            return @out;
        }
    }
}

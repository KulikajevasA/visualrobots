﻿using RobotVL.Main;
using System;
using System.Collections.Generic;

namespace RobotVL.Generation.DataStructures
{
    /// <summary>
    /// Project info DTO.
    /// </summary>
    public class ProjectInfo
    {
        /// <summary>
        /// Class node data.
        /// </summary>
        public ClassNode ClassNode { get; private set; }

        /// <summary>
        /// Assembly info data.
        /// </summary>
        public AssemblyInfo AssemblyInfo { get; private set; }

        private static Dictionary<string, int> assemblyRevision = new Dictionary<string, int>();

        /// <summary>
        /// Project info constructor.
        /// </summary>
        /// <param name="node">Robot node for which to construct project settings.</param>
        public ProjectInfo(RobotNode node)
        {
            if (!assemblyRevision.ContainsKey(node.RobotName))
                assemblyRevision[node.RobotName] = 0;

            this.ClassNode = node;
            this.AssemblyInfo = new AssemblyInfo()
            {
                Title = node.RobotName,
                Product = node.RobotName,
                Version = new Version(1, 0, 0, assemblyRevision[node.RobotName]++),
                Copyright = @"Copyright ©  2016"
            };
        }
    }
}

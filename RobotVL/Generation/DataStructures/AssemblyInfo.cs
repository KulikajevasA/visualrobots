﻿using System;
using System.Collections.Generic;

namespace RobotVL.Generation.DataStructures
{
    /// <summary>
    /// Assembly settings.
    /// </summary>
    public class AssemblyInfo
    {
        /// <summary>
        /// Getter and setter for AssemblyTitle.
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// Getter and setter for AssemblyDescription.
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Getter and setter for AssemblyConfiguration.
        /// </summary>
        public string Configuration { get; set; }
        /// <summary>
        /// Getter and setter for AssemblyCompany.
        /// </summary>
        public string Company { get; set; }
        /// <summary>
        /// Getter and setter for AssemblyProduct.
        /// </summary>
        public string Product { get; set; }
        /// <summary>
        /// Getter and setter for AssemblyCopyright.
        /// </summary>
        public string Copyright { get; set; }
        /// <summary>
        /// Getter and setter for AssemblyTrademark.
        /// </summary>
        public string Trademark { get; set; }
        /// <summary>
        /// Getter and setter for AssemblyCulture.
        /// </summary>
        public string Culture { get; set; }
        /// <summary>
        /// Getter and setter for ComVisible.
        /// </summary>
        public bool ComVisible { get; set; }
        /// <summary>
        /// Getter and setter for Guid.
        /// </summary>
        public Guid Guid { get; set; }
        /// <summary>
        /// Getter and setter for AssemblyVersion.
        /// </summary>
        public Version Version { get; set; }
        /// <summary>
        /// Getter and setter for AssemblyFileVersion.
        /// </summary>
        public Version FileVersion { get; set; }

        /// <summary>
        /// Constructor for assembly info.
        /// </summary>
        public AssemblyInfo()
        {
            Guid = Guid.NewGuid();
            Version = new Version(1, 0, 0, 0);
            FileVersion = new Version(1, 0, 0, 0);
        }

        /// <summary>
        /// Construct dictionary for settings.
        /// </summary>
        /// <returns>Settings dictionary.</returns>
        public Dictionary<string, string> Construct()
        {
            var @out = new Dictionary<string, string>();

            @out.Add("AssemblyTitle", string.Format("\"{0}\"", Title));
            @out.Add("AssemblyDescription", string.Format("\"{0}\"", Description));
            @out.Add("AssemblyConfiguration", string.Format("\"{0}\"", Configuration));
            @out.Add("AssemblyCompany", string.Format("\"{0}\"", Company));
            @out.Add("AssemblyProduct", string.Format("\"{0}\"", Product));
            @out.Add("AssemblyCopyright", string.Format("\"{0}\"", Copyright));
            @out.Add("AssemblyTrademark", string.Format("\"{0}\"", Trademark));
            @out.Add("AssemblyCulture", string.Format("\"{0}\"", Culture));

            @out.Add("ComVisible", ComVisible ? "true" : "false");

            @out.Add("Guid", string.Format("\"{0}\"", Guid.ToString()));

            @out.Add("AssemblyVersion", string.Format("\"{0}\"", Version.ToString()));
            @out.Add("AssemblyFileVersion", string.Format("\"{0}\"", FileVersion.ToString()));

            return @out;
        }
    }
}

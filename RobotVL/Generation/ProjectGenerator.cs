﻿using RobotVL.Generation.DataStructures;
using System.Collections.Generic;
using System.IO;
using System.Xml;

namespace RobotVL.Generation
{
    /// <summary>
    /// Class for generating project data.
    /// </summary>
    public class ProjectGenerator
    {
        //public static readonly string LIBRARIES = System.AppDomain.CurrentDomain.BaseDirectory;
        public static readonly List<string> LIB_REFERENCE_PATHS;

        /// <summary>
        /// Static constructor.
        /// </summary>
        static ProjectGenerator()
        {
            LIB_REFERENCE_PATHS = new List<string>() {
                System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location),
                System.AppDomain.CurrentDomain.BaseDirectory
            };
        }

        /// <summary>
        /// Generates project files.
        /// </summary>
        /// <param name="projectInfo">Project info.</param>
        /// <param name="root">Root project path.</param>
        /// <returns>Project path.</returns>
        public static string GenerateProject(ProjectInfo projectInfo, string root)
        {
            string entryPointString = projectInfo.ClassNode.Stringify();
            string assemblyInfoString = GenerateAssemblyInfo(projectInfo.AssemblyInfo);
            string projectString = GenerateProjectInfo(projectInfo.AssemblyInfo);

            string title = projectInfo.AssemblyInfo.Title;
            var dir = string.Format("{0}/{1}", root, title);
            var dirProps = string.Format("{0}/Properties", dir);

            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);

            if (!Directory.Exists(dirProps))
                Directory.CreateDirectory(dirProps);

            SaveToFile(string.Format("{0}/EntryPoint.cs", dir), entryPointString);
            SaveToFile(string.Format("{0}/AssemblyInfo.cs", dirProps), assemblyInfoString);

            var project = string.Format("{1}/{0}.csproj", title, dir);
            SaveToFile(project, projectString);

            return dir;
        }

        /// <summary>
        /// Generates AssemblyInfo.cs file content.
        /// </summary>
        /// <param name="assemblyInfo">Assembly settings.</param>
        /// <returns>AssemblyInfo.cs content.</returns>
        private static string GenerateAssemblyInfo(AssemblyInfo assemblyInfo)
        {
            string[] namespaces = new string[]
            {
                "System.Reflection",
                "System.Runtime.CompilerServices",
                "System.Runtime.InteropServices"
            };

            string @out = string.Empty;

            for (int i = 0; i < namespaces.Length; i++)
                @out += string.Format("using {0};\n", namespaces[i]);

            foreach (var kv in assemblyInfo.Construct())
                @out += string.Format("[assembly: {0}({1})]\n", kv.Key, kv.Value);

            return @out;
        }


        /// <summary>
        /// Generates *.csproj file content.
        /// </summary>
        /// <param name="assemblyInfo">Assembly settings.</param>
        /// <returns>*.csproj file content string.</returns>
        private static string GenerateProjectInfo(AssemblyInfo assemblyInfo)
        {
            var xdoc = new XmlDocument();

            #region DOCUMENT
            #region PROJECT
            var proj = xdoc.CreateElement("Project", "http://schemas.microsoft.com/developer/msbuild/2003");
            proj.SetAttribute("ToolsVersion", "14.0");
            proj.SetAttribute("DefaultTargets", "Build");
            xdoc.AppendChild(proj);

            #region IMPORT
            var import = xdoc.CreateElement("Import");
            import.SetAttribute("Project", @"$(MSBuildExtensionsPath)\$(MSBuildToolsVersion)\Microsoft.Common.props");
            import.SetAttribute("Condition", @"Exists('$(MSBuildExtensionsPath)\$(MSBuildToolsVersion)\Microsoft.Common.props')");
            proj.AppendChild(import);
            #endregion
            #region PropGroup
            {
                var propGroup = xdoc.CreateElement("PropertyGroup");

                #region CONFIGURATION
                var config = xdoc.CreateElement("Configuration");

                config.SetAttribute("Condition", @" '$(Configuration)' == '' ");
                config.InnerText = "Release";

                propGroup.AppendChild(config);
                #endregion
                #region PLATFORM
                var platform = xdoc.CreateElement("Platform");

                platform.SetAttribute("Condition", @" '$(Platform)' == '' ");
                platform.InnerText = "AnyCPU";

                propGroup.AppendChild(platform);
                #endregion
                #region GUID
                var guid = xdoc.CreateElement("ProjectGuid");

                guid.InnerText = string.Format("{{{0}}}", assemblyInfo.Guid.ToString());

                propGroup.AppendChild(guid);
                #endregion
                #region OUTPUT_TYPE
                var outputType = xdoc.CreateElement("OutputType");

                outputType.InnerText = "Library";

                propGroup.AppendChild(outputType);
                #endregion
                #region APP_DESIGNER_FOLDER
                var appFolder = xdoc.CreateElement("AppDesignerFolder");

                appFolder.InnerText = "Properties";

                propGroup.AppendChild(appFolder);
                #endregion
                #region ROOT_ASSEMBLY
                var rootAssembly = xdoc.CreateElement("RootNamespace");

                rootAssembly.InnerText = assemblyInfo.Title;

                propGroup.AppendChild(rootAssembly);
                #endregion
                #region ASSEMBLY_NAME
                var assemblyName = xdoc.CreateElement("AssemblyName");

                assemblyName.InnerText = assemblyInfo.Title;

                propGroup.AppendChild(assemblyName);
                #endregion
                #region TARGET_FRAMEWORK
                var target = xdoc.CreateElement("TargetFrameworkVersion");

                target.InnerText = "v3.5";

                propGroup.AppendChild(target);
                #endregion
                #region FILE_ALIGNMENT
                var alignment = xdoc.CreateElement("FileAlignment");

                alignment.InnerText = "512";

                propGroup.AppendChild(alignment);
                #endregion
                #region FRAMEWORK_PROFILE
                var frameworkProfile = xdoc.CreateElement("TargetFrameworkProfile");

                propGroup.AppendChild(frameworkProfile);
                #endregion

                proj.AppendChild(propGroup);
            }
            #endregion
            #region DEBUG
            {
                var propGroup = xdoc.CreateElement("PropertyGroup");
                propGroup.SetAttribute("Condition", @" '$(Configuration)|$(Platform)' == 'Debug|AnyCPU' ");

                #region OPTIMIZE
                var symbols = xdoc.CreateElement("DebugSymbols");
                symbols.InnerText = "true";
                propGroup.AppendChild(symbols);
                #endregion

                #region DEBUG_TYPE
                var debug = xdoc.CreateElement("DebugType");
                debug.InnerText = "full";
                propGroup.AppendChild(debug);
                #endregion

                #region OPTIMIZE
                var optimize = xdoc.CreateElement("Optimize");
                optimize.InnerText = "false";
                propGroup.AppendChild(optimize);
                #endregion

                #region OUTPUT_PATH
                var outputPath = xdoc.CreateElement("OutputPath");
                outputPath.InnerText = @"bin\Debug\";
                propGroup.AppendChild(outputPath);
                #endregion

                #region DEFINE_CONSTANTS
                var constants = xdoc.CreateElement("DefineConstants");
                constants.InnerText = "DEBUG;TRACE";
                propGroup.AppendChild(constants);
                #endregion

                #region ERRORS
                var errors = xdoc.CreateElement("ErrorReport");
                errors.InnerText = "prompt";
                propGroup.AppendChild(errors);
                #endregion

                #region WARNINGS
                var warn = xdoc.CreateElement("WarningLevel");
                warn.InnerText = "4";
                propGroup.AppendChild(warn);
                #endregion

                proj.AppendChild(propGroup);
            }
            #endregion
            #region RELEASE
            {
                var propGroup = xdoc.CreateElement("PropertyGroup");
                propGroup.SetAttribute("Condition", @" '$(Configuration)|$(Platform)' == 'Release|AnyCPU' ");

                #region DEBUG_TYPE
                var debug = xdoc.CreateElement("DebugType");
                debug.InnerText = "pdbonly";
                propGroup.AppendChild(debug);
                #endregion

                #region OPTIMIZE
                var optimize = xdoc.CreateElement("Optimize");
                optimize.InnerText = "true";
                propGroup.AppendChild(optimize);
                #endregion

                #region OUTPUT_PATH
                var outputPath = xdoc.CreateElement("OutputPath");
                outputPath.InnerText = @"bin\Release\";
                propGroup.AppendChild(outputPath);
                #endregion

                #region DEFINE_CONSTANTS
                var constants = xdoc.CreateElement("DefineConstants");
                constants.InnerText = "TRACE";
                propGroup.AppendChild(constants);
                #endregion

                #region ERRORS
                var errors = xdoc.CreateElement("ErrorReport");
                errors.InnerText = "prompt";
                propGroup.AppendChild(errors);
                #endregion

                #region WARNINGS
                var warn = xdoc.CreateElement("WarningLevel");
                warn.InnerText = "4";
                propGroup.AppendChild(warn);
                #endregion

                proj.AppendChild(propGroup);
            }
            #endregion

            /*#region REFERENCES
            {
                var refGroup = xdoc.CreateElement("PropertyGroup");

                foreach (var path in LIB_REFERENCE_PATHS)
                {
                    var @ref = xdoc.CreateElement("ReferencePath");
                    @ref.InnerText = path;

                    refGroup.AppendChild(@ref);
                }

                proj.AppendChild(refGroup);
            }
#endregion*/

            #region FILES
            {
                var itemGroup = xdoc.CreateElement("ItemGroup");

                #region ENTRY
                var entry = xdoc.CreateElement("Compile");
                entry.SetAttribute("Include", "EntryPoint.cs");
                itemGroup.AppendChild(entry);
                #endregion

                #region ASSEMBLY
                var assembly = xdoc.CreateElement("Compile");
                assembly.SetAttribute("Include", @"Properties\AssemblyInfo.cs");
                itemGroup.AppendChild(assembly);
                #endregion

                proj.AppendChild(itemGroup);
            }
            #endregion
            #region DEPENDENCIES
            {
                var itemGroup = xdoc.CreateElement("ItemGroup");

                #region SIMULATION_SHARED
                {
                    var @ref = xdoc.CreateElement("Reference");

                    @ref.SetAttribute("Include", @"SimulationShared, Version=1.0.0.0, Culture=neutral, processorArchitecture=MSIL");

                    #region VER
                    var ver = xdoc.CreateElement("SpecificVersion");
                    ver.InnerText = "false";
                    @ref.AppendChild(ver);
                    #endregion

                    #region PATH
                    foreach (var path in LIB_REFERENCE_PATHS)
                    {

                        var pathEl = xdoc.CreateElement("HintPath");
                        pathEl.SetAttribute("Condition", string.Format("Exists('{0}')", path));
                        pathEl.InnerText = string.Format(@"{0}/SimulationShared.dll", path);
                        @ref.AppendChild(pathEl);
                    }
                    #endregion

                    itemGroup.AppendChild(@ref);
                }
                #endregion

                proj.AppendChild(itemGroup);
            }
            #endregion

            #region BUILD_TARGET
            var buildTargets = xdoc.CreateElement("Import");
            buildTargets.SetAttribute("Project", @"$(MSBuildToolsPath)\Microsoft.CSharp.targets");
            proj.AppendChild(buildTargets);
            #endregion

            #region BUILD_EVENTS
            {
                var propGroup = xdoc.CreateElement("PropertyGroup");
                #region POST_BUILD
                var postGroup = xdoc.CreateElement("PostBuildEvent");

                //TODO: Add events

                propGroup.AppendChild(postGroup);
                #endregion
                proj.AppendChild(propGroup);
            }
            #endregion
            #endregion
            #endregion

            return xdoc.AsBeautifiedString();
        }

        /// <summary>
        /// Writes content to file.
        /// </summary>
        /// <param name="filename">Name of the file.</param>
        /// <param name="content">Content of the file.</param>
        private static void SaveToFile(string filename, string content)
        {
            File.WriteAllText(filename, content);
        }
    }
}

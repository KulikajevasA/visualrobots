﻿using UnityEngine;

namespace Designer
{
    /// <summary>
    /// Because unity is being a cunt and cannot create UI styles outside of OnGUI event this is created.
    /// </summary>
    public class InputStyleMotor : Style
    {
        private static GUIStyle baseStyle;
        private static bool isInitialzied;

        public GUIStyle Style { get; private set; }

        /// <summary>
        /// Private constructor.
        /// </summary>
        public InputStyleMotor()
        {
            Style = new GUIStyle(baseStyle);

            Style.normal.background = MakeTex(1, 1, new Color(0.204f, 0.000f, 0.000f));
        }

        public static void Instantiate()
        {
            if (isInitialzied)
                return;

            InputStyleMotor.baseStyle = new GUIStyle(FuckUnity.UnityIsAFuckingCunt.Instance.label);

            isInitialzied = true;
        }
    }
}

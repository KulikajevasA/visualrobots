﻿using UnityEngine;

namespace Designer
{
    /// <summary>
    /// Because unity is being a cunt and cannot create UI styles outside of OnGUI event this is created.
    /// </summary>
    public class BoxEntryStyle : Style
    {
        private static GUIStyle baseStyle;
        private static bool isInitialzied;

        public GUIStyle Style { get; private set; }

        /// <summary>
        /// Private constructor.
        /// </summary>
        public BoxEntryStyle()
        {
            Style = new GUIStyle(baseStyle);

            Style.normal.background = MakeTex(1, 1, new Color(0.077f, 0.051f, 0.102f));
        }


        public static void Instantiate()
        {
            if (isInitialzied)
                return;

            BoxEntryStyle.baseStyle = new GUIStyle(GUI.skin.box);

            isInitialzied = true;
        }
    }
}

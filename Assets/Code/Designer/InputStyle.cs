﻿using UnityEngine;

namespace Designer
{
    /// <summary>
    /// Because unity is being a cunt and cannot create UI styles outside of OnGUI event this is created.
    /// </summary>
    public class InputStyle : Style
    {
        private static GUIStyle baseStyle;
        private static bool isInitialzied;

        public GUIStyle Style { get; private set; }

        /// <summary>
        /// Private constructor.
        /// </summary>
        public InputStyle()
        {
            Style = new GUIStyle(baseStyle);

            Style.normal.background = MakeTex(1, 1, new Color(0.204f, 0.153f, 0.051f));
        }

        public static void Instantiate()
        {
            if (isInitialzied)
                return;

            InputStyle.baseStyle = new GUIStyle(FuckUnity.UnityIsAFuckingCunt.Instance.label);

            isInitialzied = true;
        }
    }
}

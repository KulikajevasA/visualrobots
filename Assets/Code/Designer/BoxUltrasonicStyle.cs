﻿using UnityEngine;

namespace Designer
{
    /// <summary>
    /// Because unity is being a cunt and cannot create UI styles outside of OnGUI event this is created.
    /// </summary>
    public class BoxUltrasonicStyle : Style
    {
        private static GUIStyle baseStyle;
        private static bool isInitialzied;

        public GUIStyle Style { get; private set; }

        /// <summary>
        /// Private constructor.
        /// </summary>
        public BoxUltrasonicStyle()
        {
            Style = new GUIStyle(baseStyle);

            Style.normal.background = MakeTex(1, 1, new Color(0.153f, 0.204f, 0.255f));
        }


        public static void Instantiate()
        {
            if (isInitialzied)
                return;

            BoxUltrasonicStyle.baseStyle = new GUIStyle(GUI.skin.box);

            isInitialzied = true;
        }
    }
}

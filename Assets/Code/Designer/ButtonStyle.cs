﻿using UnityEngine;

namespace Designer
{
    /// <summary>
    /// Because unity is being a cunt and cannot create UI styles outside of OnGUI event this is created.
    /// </summary>
    public class ButtonStyle : Style
    {
        private static GUIStyle baseStyle;
        private static bool isInitialzied;

        public GUIStyle Style { get; private set; }

        /// <summary>
        /// Private constructor.
        /// </summary>
        public ButtonStyle()
        {
            Style = new GUIStyle(baseStyle);

            Style.normal.background = MakeTex(1, 1, new Color(0.051f, 0.000f, 0.026f));

            Style.hover.background = MakeTex(1, 1, new Color(0.051f, 0.050f, 0.000f));
        }

        public static void Instantiate()
        {
            if (isInitialzied)
                return;

            ButtonStyle.baseStyle = new GUIStyle(FuckUnity.UnityIsAFuckingCunt.Instance.button);

            isInitialzied = true;
        }
    }
}

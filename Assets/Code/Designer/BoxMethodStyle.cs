﻿using UnityEngine;

namespace Designer
{
    /// <summary>
    /// Because unity is being a cunt and cannot create UI styles outside of OnGUI event this is created.
    /// </summary>
    public class BoxMethodStyle : Style
    {
        private static GUIStyle baseStyle;
        private static bool isInitialzied;

        public GUIStyle Style { get; private set; }

        /// <summary>
        /// Private constructor.
        /// </summary>
        public BoxMethodStyle()
        {
            Style = new GUIStyle(baseStyle);

            Style.normal.background = MakeTex(1, 1, new Color(0.15f, 0.194f, 0.10f));
        }


        public static void Instantiate()
        {
            if (isInitialzied)
                return;

            BoxMethodStyle.baseStyle = new GUIStyle(GUI.skin.box);

            isInitialzied = true;
        }
    }
}

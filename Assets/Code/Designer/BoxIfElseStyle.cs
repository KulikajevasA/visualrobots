﻿using UnityEngine;

namespace Designer
{
    /// <summary>
    /// Because unity is being a cunt and cannot create UI styles outside of OnGUI event this is created.
    /// </summary>
    public class BoxIfElseStyle : Style
    {
        private static GUIStyle baseStyle;
        private static bool isInitialzied;

        public GUIStyle Style { get; private set; }

        /// <summary>
        /// Private constructor.
        /// </summary>
        public BoxIfElseStyle()
        {
            Style = new GUIStyle(baseStyle);

            Style.normal.background = MakeTex(1, 1, new Color(0.000f, 0.255f, 0.051f));
        }


        public static void Instantiate()
        {
            if (isInitialzied)
                return;

            BoxIfElseStyle.baseStyle = new GUIStyle(GUI.skin.box);

            isInitialzied = true;
        }
    }
}

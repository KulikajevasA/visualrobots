﻿using System;
using UnityEngine;

/// <summary>
/// Nicked somewhere on the internet. Not used anymore.
/// </summary>
[Obsolete]
public class DrawingCurves
{
    private static Texture2D _aaLineTex = null;
    private static Texture2D _lineTex = null;

    private static Texture2D adLineTex
    {
        get
        {
            if (!_aaLineTex)
            {
                _aaLineTex = new Texture2D(1, 3, TextureFormat.ARGB32, true);
                _aaLineTex.SetPixel(0, 0, new Color(1, 1, 1, 0));
                _aaLineTex.SetPixel(0, 1, Color.white);
                _aaLineTex.SetPixel(0, 2, new Color(1, 1, 1, 0));
                _aaLineTex.Apply();
            }
            return _aaLineTex;
        }
    }

    private static Texture2D lineTex
    {
        get
        {
            if (!_lineTex)
            {
                _lineTex = new Texture2D(1, 1, TextureFormat.ARGB32, true);
                _lineTex.SetPixel(0, 1, Color.white);
                _lineTex.Apply();
            }
            return _lineTex;
        }
    }


    private static void DrawLineWindows(Vector2 pointA, Vector2 pointB, Color color, float width, bool antiAlias)
    {
        Color savedColor = GUI.color;
        Matrix4x4 savedMatrix = GUI.matrix;

        if (antiAlias) width *= 3;

        float angle = Vector3.Angle(pointB - pointA, Vector2.right) * (pointA.y <= pointB.y ? 1 : -1);
        float m = (pointB - pointA).magnitude;
        Vector3 dz = new Vector3(pointA.x, pointA.y, 0);

        GUI.color = color;
        GUI.matrix = TranslationMatrix(dz) * GUI.matrix;
        GUIUtility.ScaleAroundPivot(new Vector2(m, width), new Vector2(-0.5f, 0));
        GUI.matrix = TranslationMatrix(-dz) * GUI.matrix;
        GUIUtility.RotateAroundPivot(angle, new Vector2(0, 0));
        GUI.matrix = TranslationMatrix(dz + new Vector3(width / 2, -m / 2) * Mathf.Sin(angle * Mathf.Deg2Rad)) * GUI.matrix;

        GUI.DrawTexture(new Rect(0, 0, 1, 1), !antiAlias ? lineTex : adLineTex);
        GUI.matrix = savedMatrix;
        GUI.color = savedColor;
    }

    public static void DrawLine(Vector2 pointA, Vector2 pointB, Color color, float width, bool antiAlias)
    {
        DrawLineWindows(pointA, pointB, color, width, antiAlias);
    }

    public static void BezierLine(Vector2 start, Vector2 startTangent, Vector2 end, Vector2 endTangent, Color color, float width, bool antiAlias, int segments)
    {
        Vector2 lastV = CubeBezier(start, startTangent, end, endTangent, 0);

        for (int i = 1; i < segments; ++i)
        {
            Vector2 v = CubeBezier(start, startTangent, end, endTangent, i / (float)segments);
            DrawLine(lastV, v, color, width, antiAlias);
            lastV = v;
        }
    }

    private static Vector2 CubeBezier(Vector2 s, Vector2 st, Vector2 e, Vector2 et, float t)
    {
        float rt = 1 - t;
        return rt * rt * rt * s + 3 * rt * rt * t * st + 3 * rt * t * t * et + t * t * t * e;
    }

    private static Matrix4x4 TranslationMatrix(Vector3 v)
    {
        return Matrix4x4.TRS(v, Quaternion.identity, Vector3.one);
    }
}
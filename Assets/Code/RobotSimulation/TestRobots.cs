﻿public class EntryPoint : Simulation.IEntryPoint
{
    public class GenBot1 : Simulation.IRobot
    {
        private Simulation.Parts.IMotor @leftMotor = null;
        private Simulation.Parts.IMotor @rightMotor = null;
        private Simulation.Parts.IMotor @rearMotor = null;
        private Simulation.Parts.Sensors.Arrays.ISensorArray @ultrasonicSensorArray = null;

        private double @one = 0.0;
        private double @zero = 0.0;
        private double @curDist = 0.0;
        private double @curDir = 0.0;
        private double @prevDist = 0.0;
        private double @prevDir = 0.0;
        private double @itersLeft = 0;

        public void RunFrame()
        {
            double @__ULTRASONIC_DISTANCE__ = @ultrasonicSensorArray.SensorValue;
            double @__ULTRASONIC_DIRECTION__ = @ultrasonicSensorArray.SensorDirection;
            this.@one = 1;
            this.@zero = 0;
            this.SetCurrent();

            if (itersLeft > 0)
            {
                @leftMotor.SetSpeed(-1);
                @rightMotor.SetSpeed(0);
                itersLeft--;
            }
            else
            {

                //if (this.@one < @__ULTRASONIC_DISTANCE__)
                //{
                @leftMotor.SetSpeed(3);
                @rightMotor.SetSpeed(3);
                //}

                if (prevDir == 0)
                    rightMotor.SetSpeed(2);

                if (this.@zero < @__ULTRASONIC_DIRECTION__)
                {
                    @leftMotor.SetSpeed(0);
                }
                else
                {
                    if (this.@zero > @__ULTRASONIC_DIRECTION__)
                    {
                        @rightMotor.SetSpeed(0);
                    }
                    else
                    {
                        if (this.@one > @__ULTRASONIC_DISTANCE__)
                        {
                            @leftMotor.SetSpeed(1);
                            @rightMotor.SetSpeed(1);
                            if (this.@prevDir == this.@curDir)
                            {
                                if (this.@prevDist >= this.@curDist)
                                {
                                    itersLeft = 30;
                                }
                            }
                        }
                    }
                }
            }
            this.SetPrevious();
        }
        public void InitializeParts(System.Collections.Generic.List<Simulation.Parts.IPart> @parts)
        {
            foreach (var @part in @parts)
            {
                if (@part.PartType == Simulation.Parts.PartType_T.Motor)
                {
                    this.AssignMotor((Simulation.Parts.IMotor)@part);
                }
                else
                {
                    if (@part.PartType == Simulation.Parts.PartType_T.UltrasonicSensorArray)
                    {
                        this.AssignUltrasonicArray((Simulation.Parts.Sensors.Arrays.ISensorArray)@part);
                    }
                }
            }
        }
        public void AssignMotor(Simulation.Parts.IMotor @motor)
        {
            string @motLeft = "MotorLeft";
            string @motRight = "MotorRight";
            string @motRear = "MotorRear";
            if (@motor.PartName == @motLeft)
            {
                @leftMotor = @motor;
            }
            else
            {
                if (@motor.PartName == @motRight)
                {
                    @rightMotor = @motor;
                }
                else
                {
                    if (@motor.PartName == @motRear)
                    {
                        @rearMotor = @motor;
                    }
                }
            }
        }
        public void AssignUltrasonicArray(Simulation.Parts.Sensors.Arrays.ISensorArray @ultrasonic)
        {
            @ultrasonicSensorArray = @ultrasonic;
        }
        private void SetCurrent()
        {
            double @__ULTRASONIC_DISTANCE__ = @ultrasonicSensorArray.SensorValue;
            double @__ULTRASONIC_DIRECTION__ = @ultrasonicSensorArray.SensorDirection;
            this.@curDist = @__ULTRASONIC_DISTANCE__;
            this.@curDir = @__ULTRASONIC_DIRECTION__;
        }
        private void SetPrevious()
        {
            double @__ULTRASONIC_DISTANCE__ = @ultrasonicSensorArray.SensorValue;
            double @__ULTRASONIC_DIRECTION__ = @ultrasonicSensorArray.SensorDirection;
            this.@prevDist = @__ULTRASONIC_DISTANCE__;
            this.@prevDir = @__ULTRASONIC_DIRECTION__;
        }
    }

    public class GenBot2 : Simulation.IRobot
    {
        private Simulation.Parts.IMotor @leftMotor = null;
        private Simulation.Parts.IMotor @rightMotor = null;
        private Simulation.Parts.IMotor @rearMotor = null;
        private Simulation.Parts.Sensors.Arrays.ISensorArray @ultrasonicSensorArray = null;
        private double @one = 0.0;
        private double @zero = 0.0;
        private double @itersLeft = 0.0;
        private double @curDist = 0.0;
        private double @curDir = 0.0;
        private double @prevDist = 0.0;
        private double @prevDir = 0.0;
        public void RunFrame()
        {
            this.@one = 1;
            this.@zero = 0;
            this.SetCurrent();
            if (this.@itersLeft > this.@zero)
            {
                this.WhenInterationsLeft();
            }
            else
            {
                this.NoIterationsLeft();
            }
            this.SetPrevious();
        }
        public void InitializeParts(System.Collections.Generic.List<Simulation.Parts.IPart> @parts)
        {
            foreach (var @part in @parts)
            {
                if (@part.PartType == Simulation.Parts.PartType_T.Motor)
                {
                    this.AssignMotor((Simulation.Parts.IMotor)@part);
                }
                else
                {
                    if (@part.PartType == Simulation.Parts.PartType_T.UltrasonicSensorArray)
                    {
                        this.AssignUltrasonicArray((Simulation.Parts.Sensors.Arrays.ISensorArray)@part);
                    }
                }
            }
        }
        public void AssignMotor(Simulation.Parts.IMotor @motor)
        {
            string @motLeft = "MotorLeft";
            string @motRight = "MotorRight";
            string @motRear = "MotorRear";
            if (@motor.PartName == @motLeft)
            {
                @leftMotor = @motor;
            }
            else
            {
                if (@motor.PartName == @motRight)
                {
                    @rightMotor = @motor;
                }
                else
                {
                    if (@motor.PartName == @motRear)
                    {
                        @rearMotor = @motor;
                    }
                }
            }
        }
        public void AssignUltrasonicArray(Simulation.Parts.Sensors.Arrays.ISensorArray @ultrasonic)
        {
            @ultrasonicSensorArray = @ultrasonic;
        }
        private void SetCurrent()
        {
            double @__ULTRASONIC_DISTANCE__ = @ultrasonicSensorArray.SensorValue;
            double @__ULTRASONIC_DIRECTION__ = @ultrasonicSensorArray.SensorDirection;
            this.@curDist = @__ULTRASONIC_DISTANCE__;
            this.@curDir = @__ULTRASONIC_DIRECTION__;
        }
        private void WhenInterationsLeft()
        {
            @leftMotor.SetSpeed(-1);
            @rightMotor.SetSpeed(0);
            double @Anonymous_0 = 0.0;
            @Anonymous_0 = @itersLeft - @one;
            this.@itersLeft = @Anonymous_0;
        }
        private void SetPrevious()
        {
            double @__ULTRASONIC_DISTANCE__ = @ultrasonicSensorArray.SensorValue;
            double @__ULTRASONIC_DIRECTION__ = @ultrasonicSensorArray.SensorDirection;
            this.@prevDist = @__ULTRASONIC_DISTANCE__;
            this.@prevDir = @__ULTRASONIC_DIRECTION__;
        }
        private void NoIterationsLeft()
        {
            if (this.@prevDir == this.@zero)
            {
                @rightMotor.SetSpeed(2);
            }
            else
            {
                @rightMotor.SetSpeed(3);
            }
            @leftMotor.SetSpeed(3);
            this.UltrasonicDirLessThanZero();
        }
        private void UltrasonicDirLessThanZero()
        {
            double @__ULTRASONIC_DIRECTION__ = @ultrasonicSensorArray.SensorDirection;
            if (this.@zero < @__ULTRASONIC_DIRECTION__)
            {
                @leftMotor.SetSpeed(0);
            }
            else
            {
                this.UltrasonicDirLessThanZeroElse();
            }
        }
        private void UltrasonicDirLessThanZeroElse()
        {
            double @__ULTRASONIC_DIRECTION__ = @ultrasonicSensorArray.SensorDirection;
            if (this.@zero > @__ULTRASONIC_DIRECTION__)
            {
                @rightMotor.SetSpeed(0);
            }
            else
            {
                this.DistanceGreaterThanOne();
            }
        }
        private void DistanceGreaterThanOne()
        {
            double @__ULTRASONIC_DISTANCE__ = @ultrasonicSensorArray.SensorValue;
            if (this.@one > @__ULTRASONIC_DISTANCE__)
            {
                @leftMotor.SetSpeed(1);
                @rightMotor.SetSpeed(1);
                this.PreviousDirEquelsCurrentDir();
            }
        }
        private void PreviousDirEquelsCurrentDir()
        {
            if (this.@prevDir == this.@curDir)
            {
                this.PrevDistGreaterThanCur();
            }
        }
        private void PrevDistGreaterThanCur()
        {
            if (this.@prevDist >= this.@curDist)
            {
                this.@itersLeft = 60;
            }
        }
    }
    public class GeneratedRobot : Simulation.IRobot
    {
        private Simulation.Parts.IMotor @leftMotor = null;
        private Simulation.Parts.IMotor @rightMotor = null;
        private Simulation.Parts.IMotor @rearMotor = null;
        private Simulation.Parts.Sensors.Arrays.ISensorArray @ultrasonicSensorArray = null;
        private Simulation.Parts.Sensors.ISensor @colorSensor = null;
        private Simulation.Parts.Sensors.ISensor @lightSensor = null;
        private double @one = 0.0;
        private double @zero = 0.0;
        private double @itersLeft = 0.0;
        private double @curDist = 0.0;
        private double @curDir = 0.0;
        private double @prevDist = 0.0;
        private double @prevDir = 0.0;
        public void RunFrame()
        {
            this.@one = 1;
            this.@zero = 0;
            this.SetCurrent();
            if (this.@itersLeft > this.@zero)
            {
                this.WhenInterationsLeft();
            }
            else
            {
                this.NoIterationsLeft();
            }
            this.SetPrevious();
        }
        public void InitializeParts(System.Collections.Generic.List<Simulation.Parts.IPart> @parts)
        {
            foreach (var @part in @parts)
            {
                if (@part.PartType == Simulation.Parts.PartType_T.Motor)
                {
                    this.AssignMotor((Simulation.Parts.IMotor)@part);
                }
                else
                {
                    if (@part.PartType == Simulation.Parts.PartType_T.UltrasonicSensorArray)
                    {
                        this.AssignUltrasonicArray((Simulation.Parts.Sensors.Arrays.ISensorArray)@part);
                    }
                    else
                    {
                        if (@part.PartType == Simulation.Parts.PartType_T.ColorSensor)
                        {
                            this.AssignColorSensor((Simulation.Parts.Sensors.ISensor)@part);
                        }
                    }
                }
            }
        }
        public void AssignMotor(Simulation.Parts.IMotor @motor)
        {
            string @motLeft = "MotorLeft";
            string @motRight = "MotorRight";
            string @motRear = "MotorRear";
            if (@motor.PartName == @motLeft)
            {
                @leftMotor = @motor;
            }
            else
            {
                if (@motor.PartName == @motRight)
                {
                    @rightMotor = @motor;
                }
                else
                {
                    if (@motor.PartName == @motRear)
                    {
                        @rearMotor = @motor;
                    }
                }
            }
        }
        public void AssignUltrasonicArray(Simulation.Parts.Sensors.Arrays.ISensorArray @ultrasonic)
        {
            @ultrasonicSensorArray = @ultrasonic;
        }
        public void AssignColorSensor(Simulation.Parts.Sensors.ISensor @color)
        {
            @colorSensor = @color;
        }
        private void SetCurrent()
        {
            double @__ULTRASONIC_DISTANCE__ = @ultrasonicSensorArray.SensorValue;
            double @__ULTRASONIC_DIRECTION__ = @ultrasonicSensorArray.SensorDirection;
            this.@curDist = @__ULTRASONIC_DISTANCE__;
            this.@curDir = @__ULTRASONIC_DIRECTION__;
        }
        private void WhenInterationsLeft()
        {
            @leftMotor.SetSpeed(-1);
            @rightMotor.SetSpeed(0);
            double @Anonymous_0 = 0.0;
            @Anonymous_0 = @itersLeft - @one;
            this.@itersLeft = @Anonymous_0;
        }
        private void SetPrevious()
        {
            double @__ULTRASONIC_DISTANCE__ = @ultrasonicSensorArray.SensorValue;
            double @__ULTRASONIC_DIRECTION__ = @ultrasonicSensorArray.SensorDirection;
            this.@prevDist = @__ULTRASONIC_DISTANCE__;
            this.@prevDir = @__ULTRASONIC_DIRECTION__;
        }
        private void NoIterationsLeft()
        {
            if (this.@prevDir == this.@zero)
            {
                @rightMotor.SetSpeed(2);
            }
            else
            {
                @rightMotor.SetSpeed(3);
            }
            @leftMotor.SetSpeed(3);
            this.UltrasonicDirLessThanZero();
        }
        private void UltrasonicDirLessThanZero()
        {
            double @__ULTRASONIC_DIRECTION__ = @ultrasonicSensorArray.SensorDirection;
            if (this.@zero < @__ULTRASONIC_DIRECTION__)
            {
                @rightMotor.SetSpeed(0);
            }
            else
            {
                this.UltrasonicDirLessThanZeroElse();
            }
        }
        private void UltrasonicDirLessThanZeroElse()
        {
            double @__ULTRASONIC_DIRECTION__ = @ultrasonicSensorArray.SensorDirection;
            if (this.@zero > @__ULTRASONIC_DIRECTION__)
            {
                @leftMotor.SetSpeed(0);
            }
            else
            {
                this.DistanceGreaterThanOne();
            }
        }
        private void DistanceGreaterThanOne()
        {
            double @__ULTRASONIC_DISTANCE__ = @ultrasonicSensorArray.SensorValue;
            if (this.@one > @__ULTRASONIC_DISTANCE__)
            {
                @leftMotor.SetSpeed(1);
                @rightMotor.SetSpeed(1);
                this.PreviousDirEquelsCurrentDir();
            }
        }
        private void PreviousDirEquelsCurrentDir()
        {
            if (this.@prevDir == this.@curDir)
            {
                this.PrevDistGreaterThanCur();
            }
        }
        private void PrevDistGreaterThanCur()
        {
            if (this.@prevDist >= this.@curDist)
            {
                this.@itersLeft = 60;
            }
        }
    }
    public Simulation.IRobot CreateRobot()
    {
        return new GenBot1();
    }
}
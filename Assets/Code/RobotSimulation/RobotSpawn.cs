﻿using UnityEngine;

namespace Simulation
{
    public class RobotSpawn : MonoBehaviour
    {
        public string robotPath = "Robots";

        private RobotWorker worker;
        
        // Use this for initialization
        void Start()
        {
            SpawnRobot("RobotGeneric");
        }

        // Update is called once per frame
        void Update()
        {
        }

        void OnDrawGizmos()
        {
            Gizmos.DrawWireCube(transform.position, Vector3.one);
            Gizmos.DrawRay(transform.position, transform.forward);
        }

        public void SpawnRobot(string robotName)
        {
            var prefab = Resources.Load<GameObject>(robotPath + "/" + robotName);

            if (prefab == null)
                return;

            var robot = Instantiate(prefab);

            robot.transform.position += transform.position;
            robot.transform.rotation = transform.rotation;

            var worker = robot.GetComponent<RobotWorker>();

            if (worker == null)
            {
                Destroy(prefab);
                throw new MissingComponentException();
            }

            this.worker = worker;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Simulation.Parts
{
    /// <summary>
    /// A simple motor.
    /// </summary>
    public class Motor : BasePart, IMotor
    {
        private List<Wheel> wheels = new List<Wheel>();

        /// <summary>
        /// Constructor for motor.
        /// </summary>
        /// <param name="gameObject">Unity object to which motor is attached.</param>
        public Motor(GameObject gameObject) : base(gameObject, PartType_T.Motor)
        {
            var childrenParts = gameObject.GetComponentsInChildren<RobotPartComponent>();
            foreach (var part in childrenParts)
                if (part.type == PartType_T.Wheel)
                {
                    part.CreatePart(this.attachedWorker);
                    wheels.Add((Wheel)part.SimulationObject);
                }
        }

        /// <summary>
        /// Sets motor speed.
        /// </summary>
        /// <param name="speed">New speed for motor. Negative values for reverse.</param>
        public void SetSpeed(double speed)
        {
            foreach (var wheel in wheels)
                wheel.ApplyImpulse(speed);
        }

        /// <summary>
        /// Sets motor speed to zero.
        /// </summary>
        public void Brake()
        {
            throw new NotImplementedException();
        }
    }
}

﻿using UnityEngine;

namespace Simulation.Parts
{
    /// <summary>
    /// Abstract class for base part.
    /// </summary>
    public abstract class BasePart : IPart
    {
        protected GameObject gameObject;
        protected PartType_T partType;
        protected RobotPartComponent partComponent;
        protected RobotWorker attachedWorker;

        /// <summary>
        /// Base part transform.
        /// </summary>
        public Transform Transform
        {
            get
            {
                if (gameObject == null)
                    return null;

                return gameObject.transform;
            }
        }

        /// <summary>
        /// Constructor for base part.
        /// </summary>
        /// <param name="gameObject">Game object the part is attached to.</param>
        /// <param name="partType">Type of the part.</param>
        protected BasePart(GameObject gameObject, PartType_T partType)
        {
            this.partComponent = gameObject.GetComponent<RobotPartComponent>();
            this.attachedWorker = this.partComponent.RobotWorker;
            this.gameObject = gameObject;
            this.partType = partType;
        }

        /// <summary>
        /// Gets or sets assigned unity name for part.
        /// </summary>
        public string PartName
        {
            get
            {
                return gameObject.name;
            }

            set
            {
                gameObject.name = value;
            }
        }

        /// <summary>
        /// Gets the assigned part type.
        /// </summary>
        public PartType_T PartType
        {
            get
            {
                return partType;
            }
        }
    }
}

﻿using UnityEngine;

namespace Simulation.Parts
{
    /// <summary>
    /// A simple wheel.
    /// </summary>
    public class Wheel : BasePart
    {
        private float cacheLastImpulse;

        /// <summary>
        /// Constructor for wheel.
        /// </summary>
        /// <param name="gameObject">Unity object to which wheel is attached.</param>
        public Wheel(GameObject gameObject) : base(gameObject, PartType_T.Wheel)
        {
            this.attachedWorker.OnPreThink += Worker_PreThink;
            this.attachedWorker.OnPostThink += Worker_PostThink;
        }

        /// <summary>
        /// Applies impulse to the robot to simulate wheels.
        /// </summary>
        /// <param name="strength">Strength of impulse to apply.</param>
        public void ApplyImpulse(double strength)
        {
            this.cacheLastImpulse = (float)strength;
        }

        /// <summary>
        /// Called after worker enters PreThink().
        /// </summary>
        /// <param name="caller">Caller.</param>
        private void Worker_PreThink(RobotWorker caller)
        {
            this.cacheLastImpulse = 0;
        }

        /// <summary>
        /// Called after worker enter PostThink().
        /// </summary>
        /// <param name="caller">Caller.</param>
        private void Worker_PostThink(RobotWorker caller)
        {
            if (!Mathf.Approximately(cacheLastImpulse, 0))
                this.attachedWorker.AddWheelVector(gameObject.transform.position, cacheLastImpulse);
        }
    }
}

﻿using Simulation.Parts.Sensors;
using Simulation.Parts.Sensors.Arrays;
using System;
using UnityEngine;

namespace Simulation.Parts
{
    /// <summary>
    /// Component glue for robot part.
    /// </summary>
    public class RobotPartComponent : MonoBehaviour
    {
        public PartType_T type;
        private IPart simulationObject;

        /// <summary>
        /// Gets the simulation object that is assigned to unity component.
        /// </summary>
        public IPart SimulationObject
        {
            get
            {
                if (simulationObject == null)
                    throw new NullReferenceException("Call CreatePart for this object first!");

                return simulationObject;
            }
        }
        /// <summary>
        /// Gets the assigned robot.
        /// </summary>
        public RobotWorker RobotWorker { get; private set; }

        /// <summary>
        /// Creates appropriate part from part type.
        /// </summary>
        /// <param name="worker">Robot that the part is assigned to.</param>
        /// <returns>Robot Simulation Part.</returns>
        public IPart CreatePart(RobotWorker worker)
        {
            if (simulationObject != null)
                return simulationObject;

            RobotWorker = worker;

            switch (type)
            {
                case PartType_T.Motor:
                    simulationObject = new Motor(gameObject);
                    break;
                case PartType_T.Wheel:
                    simulationObject = new Wheel(gameObject);
                    break;
                case PartType_T.UltrasonicSensor:
                    simulationObject = new Ultrasonic(gameObject);
                    break;
                case PartType_T.UltrasonicSensorArray:
                    simulationObject = new UltrasonicArray(gameObject);
                    break;
                case PartType_T.ColorSensor:
                    simulationObject = new ColorSensor(gameObject);
                    break;
                case PartType_T.LightSensor:
                    simulationObject = new LightSensor(gameObject);
                    break;
                case PartType_T.Unknown:
                default:
                    throw new InvalidCastException();
            }

            return simulationObject;
        }
    }
}

﻿using UnityEngine;

namespace Simulation.Parts.Sensors
{
    /// <summary>
    /// Ultrasonic sensor.
    /// </summary>
    public class Ultrasonic : BaseSensor
    {
        public const float MAX_LEN = 5;

        private double cacheSensorData;

        /// <summary>
        /// Constructor of the ultrasonic sensor.
        /// </summary>
        /// <param name="gameObject">Unity object to which the sensor is attached to.</param>
        public Ultrasonic(GameObject gameObject) : base(gameObject, PartType_T.UltrasonicSensor)
        {
        }

        /// <summary>
        /// Returns sensor data.
        /// </summary>
        public override double SensorData
        {
            get
            {
                return cacheSensorData;
            }
        }

        /// <summary>
        /// Retrieves sensor data.
        /// </summary>
        public override void CacheSensorData()
        {
            var transform = gameObject.transform;
            var pos = transform.position + transform.forward * 0.1f;

            RaycastHit hit;

            if (Physics.Raycast(pos, transform.forward, out hit, MAX_LEN))
            {
                Debug.DrawLine(pos, transform.position + transform.forward * hit.distance, Color.green);
                cacheSensorData = hit.distance;
                return;
            }
            
            cacheSensorData = double.PositiveInfinity;
        }
    }
}

﻿using UnityEngine;

namespace Simulation.Parts.Sensors
{
    /// <summary>
    /// Base class for a sensor.
    /// </summary>
    public abstract class BaseSensor : BasePart, ISensor
    {
        /// <summary>
        /// Constructor for sensor.
        /// </summary>
        /// <param name="gameObject">Unity object to which sensor is attached to.</param>
        /// <param name="partType">Part type of the sensor.</param>
        public BaseSensor(GameObject gameObject, PartType_T partType) : base(gameObject, partType) { }

        /// <summary>
        /// Returns sensor data.
        /// </summary>
        public abstract double SensorData { get; }

        /// <summary>
        /// Caches sensor data.
        /// </summary>
        public abstract void CacheSensorData();
    }
}

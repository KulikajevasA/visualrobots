﻿using System;
using UnityEngine;

namespace Simulation.Parts.Sensors.Arrays
{
    /// <summary>
    /// An array of ultrasonic sensors.
    /// </summary>
    public class UltrasonicArray : BaseSensorArray
    {
        /// <summary>
        /// Constructor for the ultrasonic sensor array.
        /// </summary>
        /// <param name="gameObject">Unity game object to which sensor is attached to.</param>
        public UltrasonicArray(GameObject gameObject) : base(gameObject, PartType_T.UltrasonicSensorArray, PartType_T.UltrasonicSensor)
        {
            attachedWorker.OnPreThink += Worker_PreThink;
        }

        /// <summary>
        /// Returns sensor data for the object.
        /// </summary>
        public override double[] SensorData
        {
            get
            {
                return cacheSensorsData;
            }
        }

        /// <summary>
        /// Sensor array uses multiple sensors, sensor arrays gives direction of the sensor.
        /// Can take values:
        ///     Right =>  1
        ///     Front =>  0
        ///     Left  => -1
        /// </summary>
        public override double SensorDirection
        {
            get
            {
                return this.cacheSensorDirection;
            }
        }

        /// <summary>
        /// Distance to the hit target. Returns Infinity if no targets were hit.
        /// </summary>
        public override double SensorValue
        {
            get
            {
                return this.cacheSensorValue;
            }
        }

        /// <summary>
        /// Called when worker enters PreThink().
        /// </summary>
        /// <param name="caller">Calling worker.</param>
        private void Worker_PreThink(RobotWorker caller)
        {
            var robotForward = attachedWorker.transform.forward;
            Debug.DrawRay(attachedWorker.transform.position, robotForward, Color.blue);

            var influenceVec = new Vector3();
            var influenceCount = 0;

            for (int i = 0; i < sensorArray.Length; i++)
            {
                var sensor = sensorArray[i];
                sensor.CacheSensorData();

                var sensorForward = sensor.Transform.forward;
                Debug.DrawRay(sensor.Transform.position, sensorForward, Color.red);

                var influence = sensor.SensorData;

                cacheSensorsData[i] = influence;

                if (influence == double.PositiveInfinity)
                    continue;

                influence = Ultrasonic.MAX_LEN - influence;

                influenceVec += sensorForward * (float)influence;
                influenceCount++;
            }

            if (influenceCount == 0)
            {
                cacheSensorValue = double.PositiveInfinity;
                cacheSensorDirection = 0;
                return;
            }

            cacheSensorValue = Ultrasonic.MAX_LEN - influenceVec.magnitude / influenceCount;
            influenceVec.Normalize();

            var d = Math.Round((Vector3.Dot(robotForward, influenceVec) * 100)) / 100;

            var C = Vector3.Cross(robotForward, influenceVec);
            var angle = Math.Acos(d);
            var dir = Vector3.Dot(C, Vector3.up);

            if (dir < 0)
                angle = -angle;
            
            Debug.DrawRay(attachedWorker.transform.position, influenceVec * 4, Color.magenta);
            Debug.DrawRay(attachedWorker.transform.position, attachedWorker.transform.forward, Color.gray);

            var move = (float)angle * Mathf.Rad2Deg / 90f;

            cacheSensorDirection = Mathf.Round(move * 100) / 100f;
			
			MonoBehaviour.print(cacheSensorDirection);
        }
    }
}

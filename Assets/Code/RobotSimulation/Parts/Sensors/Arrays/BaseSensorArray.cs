﻿using System.Collections.Generic;
using UnityEngine;

namespace Simulation.Parts.Sensors.Arrays
{
    /// <summary>
    /// Base class for an array of sensors.
    /// </summary>
    public abstract class BaseSensorArray : BasePart, ISensorArray
    {
        protected BaseSensor[] sensorArray;

        protected double[] cacheSensorsData;
        protected double cacheSensorDirection;
        protected double cacheSensorValue;

        /// <summary>
        /// Constructor for array sensors.
        /// </summary>
        /// <param name="gameObject">Untity object to which sensor is attached to.</param>
        /// <param name="partType">Type of the part.</param>
        public BaseSensorArray(GameObject gameObject, PartType_T partType, PartType_T sensorType) : base(gameObject, partType)
        {
            var parts = gameObject.GetComponentsInChildren<RobotPartComponent>();

            List<BaseSensor> sensors = new List<BaseSensor>();
            foreach (var part in parts)
                if (part.type == sensorType)
                    sensors.Add((BaseSensor)part.CreatePart(this.attachedWorker));

            sensorArray = sensors.ToArray();
            cacheSensorsData = new double[sensorArray.Length];
        }

        /// <summary>
        /// Returns an array of sensor data.
        /// </summary>
        public abstract double[] SensorData { get; }

        /// <summary>
        /// Sensor array uses multiple sensors, sensor arrays gives direction of the sensor.
        /// Can take values:
        ///     Right =>  1
        ///     Front =>  0
        ///     Left  => -1
        /// </summary>
        public abstract double SensorDirection { get; }

        /// <summary>
        /// Distance to the hit target. Returns Infinity if no targets were hit.
        /// </summary>
        public abstract double SensorValue { get; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Simulation.Parts.Sensors
{
    /// <summary>
    /// Color sensor.
    /// </summary>
    public class ColorSensor : BaseSensor
    {
        public const float MAX_LEN = 5;
        private int cacheSensorData;

        public const int BLACK = 0x000000;
        public const int RED = 0xff0000;
        public const int GREEN = 0x00ff00;
        public const int BLUE = 0x0000ff;
        public const int YELLOW = 0xffff00;
        public const int CYAN = 0x00ffff;
        public const int MAGENTA = 0xff00ff;
        public const int WHITE = 0xffffff;

        /// <summary>
        /// Constructor for the color sensor array.
        /// </summary>
        /// <param name="gameObject">Unity game object to which sensor is attached to.</param>
        public ColorSensor(GameObject gameObject) : base(gameObject, PartType_T.ColorSensor)
        {
            attachedWorker.OnPreThink += Worker_PreThink;
        }

        /// <summary>
        /// Called when worker enters PreThink().
        /// </summary>
        /// <param name="caller">Calling worker.</param>
        private void Worker_PreThink(RobotWorker caller)
        {
            CacheSensorData();
        }

        /// <summary>
        /// Returns sensor data for the object.
        /// </summary>
        public override double SensorData
        {
            get
            {
                return cacheSensorData;
            }
        }

        /// <summary>
        /// Retrieves sensor data.
        /// </summary>
        public override void CacheSensorData()
        {
            var transform = gameObject.transform;
            var pos = transform.position + transform.forward * 0.1f;

            RaycastHit hit;


            cacheSensorData = BLACK;

            if (Physics.Raycast(pos, transform.forward, out hit, MAX_LEN))
            {
                Debug.DrawLine(pos, transform.position + transform.forward * hit.distance, Color.green);
                
                var renderer = hit.transform.GetComponent<Renderer>();

                if (renderer)
                {
                    var color = renderer.material.color;

                    int r = (int)Math.Round(color.r) * 255;
                    int g = (int)Math.Round(color.g) * 255;
                    int b = (int)Math.Round(color.b) * 255;

                    cacheSensorData =  (r << 16) | (g << 8) | b;   
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Simulation.Parts.Sensors
{
    /// <summary>
    /// Color sensor.
    /// </summary>
    public class LightSensor : BaseSensor
    {
        public const float MAX_LEN = 5;
        private double cacheSensorData;

        /// <summary>
        /// Constructor for the light sensor array.
        /// </summary>
        /// <param name="gameObject">Unity game object to which sensor is attached to.</param>
        public LightSensor(GameObject gameObject) : base(gameObject, PartType_T.LightSensor)
        {
            attachedWorker.OnPreThink += Worker_PreThink;
        }

        /// <summary>
        /// Called when worker enters PreThink().
        /// </summary>
        /// <param name="caller">Calling worker.</param>
        private void Worker_PreThink(RobotWorker caller)
        {
            CacheSensorData();
        }

        /// <summary>
        /// Returns sensor data for the object.
        /// </summary>
        public override double SensorData
        {
            get
            {
                return cacheSensorData;
            }
        }

        /// <summary>
        /// Retrieves sensor data.
        /// </summary>
        public override void CacheSensorData()
        {
            var transform = gameObject.transform;
            var pos = transform.position + transform.forward * 0.1f;

            RaycastHit hit;


            cacheSensorData = 150;

            if (Physics.Raycast(pos, transform.forward, out hit, MAX_LEN, World.LayerShadowBit))
            {
                Debug.DrawLine(pos, transform.position + transform.forward * hit.distance, Color.yellow);

                var luxSim = hit.transform.GetComponent<LuxSimulator>();

                if(luxSim)
                    cacheSensorData = luxSim.LuxValue;
            }
        }
    }
}

﻿using UnityEngine;
using System.Collections.Generic;
using Simulation.Parts;
using System;
using System.Reflection;
using System.IO;
using Simulation.Parts.Sensors;
using Simulation.Parts.Sensors.Arrays;

namespace Simulation
{
    /// <summary>
    /// Handles robot wrapping.
    /// </summary>
    public class RobotWorker : MonoBehaviour
    {
        private IRobot robot;

        public List<GameObject> partObjects;
        private Rigidbody physicsBody;

        private List<Vector4> velocityVectors;
        private Dictionary<Type, List<IPart>> parts;

        public delegate void PreThinkInfo(RobotWorker caller);
        public delegate void ThinkInfo(RobotWorker caller);
        public delegate void PostThinkInfo(RobotWorker caller);

        public event PreThinkInfo OnPreThink;
        public event ThinkInfo OnThink;
        public event PostThinkInfo OnPostThink;

        private Vector3 originalPosition;
        private Quaternion originalRotation;

        /// <summary>
        /// Getter for sensor arrays parts.
        /// </summary>
        public List<IPart> SensorArrays
        {
            get
            {
                return this.parts[typeof(ISensorArray)];
            }
        }

        /// <summary>
        /// Getter for senor parts.
        /// </summary>
        public List<IPart> Sensors
        {
            get
            {
                return this.parts[typeof(ISensor)];
            }
        }

        /// <summary>
        /// Getter for unsorted parts.
        /// </summary>
        public List<IPart> OtherParts
        {
            get
            {
                return this.parts[typeof(IPart)];
            }
        }

        /// <summary>
        /// Called when game initializes.
        /// </summary>
        void Start()
        {
            physicsBody = GetComponent<Rigidbody>();

            if (physicsBody == null)
                throw new MissingComponentException("Robot must have a RigidBody component.");

            velocityVectors = new List<Vector4>();

            originalRotation = transform.rotation;
            originalPosition = transform.position;
        }

        /// <summary>
        /// Called before think.
        /// </summary>
        private void PreThink()
        {
            PreThinkInfo handler = OnPreThink;
            if (handler != null)
                handler(this);
        }

        /// <summary>
        /// Called when robot thinks a frame.
        /// </summary>
        private void Think()
        {
            ThinkInfo handler = OnThink;
            if (handler != null)
                handler(this);

            if (robot != null)
                robot.RunFrame();
        }

        /// <summary>
        /// Called after think.
        /// </summary>
        private void PostThink()
        {
            PostThinkInfo handler = OnPostThink;
            if (handler != null)
                handler(this);

            ApplyWheelVectors();
        }

        /// <summary>
        /// Called every physics simulation.
        /// </summary>
        void FixedUpdate()
        {
            if (robot == null)
                return;

            PreThink();
            Think();
            PostThink();
        }

        /// <summary>
        /// Called every frame.
        /// </summary>
        void Update() { }

        /// <summary>
        /// Creates appropriate parts from game objects.
        /// </summary>
        private void InitializeParts()
        {
            InitializePartDictionary();

            List<IPart> initializeParts = new List<IPart>();

            foreach (var partObject in this.partObjects)
            {
                var component = partObject.GetComponent<RobotPartComponent>();
                if (component != null)
                {
                    var part = component.CreatePart(this);
                    initializeParts.Add(part);

                    AddPart(part);
                }
            }
            robot.InitializeParts(initializeParts);
        }

        /// <summary>
        /// Reloads robot assembly.
        /// </summary>
        /// <param name="assemblyPath">Assembly path.</param>
        public void ReloadAssembly(string assemblyPath)
        {
            var assembly = LoadAssembly(assemblyPath);
            robot = CreateRobot(assembly);

            if (robot == null)
                return;

            ClearVelocityVectors();
            InitializeParts();
        }

        /// <summary>
        /// Restores robot to primary state.
        /// </summary>
        public void Restore()
        {
            transform.position = originalPosition;
            transform.rotation = originalRotation;
            UnbindAssembly();

            var anims = FindObjectsOfType<Animation>();
            foreach (var anim in anims)
            {               
                anim.CrossFade(anim.clip.name, -1, PlayMode.StopAll);
                anim[anim.clip.name].time = 0;
                anim.Sample();
                anim.Stop();
                anim.Sample();
            }
        }

        /// <summary>
        /// Unbinds robot assembly.
        /// </summary>
        public void UnbindAssembly()
        {
            robot = null;
            ClearVelocityVectors();
        }

        /// <summary>
        /// Initializes parts dictionary.
        /// </summary>
        private void InitializePartDictionary()
        {
            var supportedTypes = new Type[]
            {
                typeof(ISensorArray),
                typeof(ISensor),
                typeof(IPart)
            };

            this.parts = new Dictionary<Type, List<IPart>>();

            foreach (var t in supportedTypes)
                this.parts.Add(t, new List<IPart>());
        }

        /// <summary>
        /// Assigns a part to dictionary.
        /// </summary>
        /// <param name="part">Part to assign.</param>
        private void AddPart(IPart part)
        {
            if (part is ISensorArray)
                this.SensorArrays.Add(part);
            else if (part is ISensor)
                this.Sensors.Add(part);
            else
                this.OtherParts.Add(part);
        }

        /// <summary>
        /// Creates user robot using reflection.
        /// </summary>
        /// <param name="robotClass">Name of the user robot.</param>
        /// <returns>User robot.</returns>
        public static IRobot CreateRobot(string robotClass)
        {
            var robotType = Type.GetType(robotClass, true, false);
            return CreateRobot(robotType);
        }

        /// <summary>
        /// Creates user robot using reflection.
        /// </summary>
        /// <param name="robotType">Type of the robot.</param>
        /// <returns>User robot.</returns>
        public static IRobot CreateRobot(Type robotType)
        {
            var robot = Activator.CreateInstance(robotType);

            if (!(robot is IRobot))
                throw new InvalidCastException();

            return robot as IRobot;
        }

        /// <summary>
        /// Creates user robot using assemly reflection.
        /// </summary>
        /// <param name="assembly">Assembly from which to load robot.</param>
        /// <returns>User robot.</returns>
        public static IRobot CreateRobot(Assembly assembly)
        {
            if (assembly == null)
                throw new MissingComponentException();

            var entryType = assembly.GetType("EntryPoint");

            if (entryType != null)
            {
                var entryObject = Activator.CreateInstance(entryType);

                if (entryObject is IEntryPoint)
                {
                    var entry = entryObject as IEntryPoint;
                    var robotObject = entry.CreateRobot();

                    if (robotObject is IRobot)
                        return robotObject as IRobot;
                    else
                        throw new InvalidCastException("Robot must inherit Simulation.IRobot interface.");
                }
                else
                    throw new InvalidCastException("EntryPoint must inherit Simulation.IEntryPoint interface.");
            }
            else
                throw new EntryPointNotFoundException();
        }

        /// <summary>
        /// Loads an assembly
        /// </summary>
        /// <param name="path">Path to assembly.</param>
        /// <returns>Assembly.</returns>
        public static Assembly LoadAssembly(string path)
        {
            if (!File.Exists(path))
                throw new DllNotFoundException();

            var bytes = File.ReadAllBytes(path);

            try
            {
                return Assembly.Load(bytes);
            }
            catch (FileNotFoundException)
            {
                return null;
            }
        }

        /// <summary>
        /// Adds wheel vector.
        /// </summary>
        /// <param name="wheelPosition">Local space wheel position.</param>
        /// <param name="impulse">Wheel torque.</param>
        public void AddWheelVector(Vector3 wheelPosition, float impulse)
        {
            this.velocityVectors.Add(wheelPosition.W(impulse));
        }

        /// <summary>
        /// Clears wheel vectors.
        /// </summary>
        private void ClearVelocityVectors()
        {
            this.velocityVectors.Clear();
        }

        /// <summary>
        /// Applies wheel vectors.
        /// </summary>
        private void ApplyWheelVectors()
        {
            var moveVec = new Vector3();

            foreach (var v in this.velocityVectors)
            {
                var wheelPos = v.XYZ();
                var impulse = v.w;
                wheelPos.y = transform.position.y;

                var offset = transform.position - wheelPos;
                var myVec = transform.forward + offset;
                moveVec += myVec * impulse;
            }

            var moveMag = moveVec.magnitude;

            if (moveMag == 0)
                return;

            var moveVecNormal = moveVec.normalized;

            var dot = Vector3.Dot(transform.forward, moveVecNormal);

            if (!Mathf.Approximately(Math.Abs(dot), 1))
            {
                var desired = Quaternion.LookRotation(moveVec);
                var lerp = Quaternion.Slerp(transform.rotation, desired, 0.01f * moveMag);
                transform.rotation = lerp;
            }

            var forward = moveVec - moveVecNormal;
            transform.position += transform.forward * forward.magnitude * 0.01f * Math.Sign(dot);

            this.ClearVelocityVectors();
        }
    }
}
﻿using UnityEngine;
using System.Collections;
using System.Diagnostics;
using Simulation;

public class TriggerFinale : MonoBehaviour
{
    private Stopwatch sw = new Stopwatch();
    public long WinTime = 5;
    private bool entered;
    private RobotWorker worker;


    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

    }

    void FixedUpdate()
    {
        if(worker && entered && sw.ElapsedMilliseconds > WinTime * 1000)
        {
            worker.Restore();
            if (GameUI.GetUI())
                GameUI.GetUI().SimulatorUI.ShowStoppedUI();
            worker = null;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        var worker = other.transform.GetComponentInParent<RobotWorker>();

        if (worker)
        {
            entered = true;
            sw.Reset();
            sw.Start();
            this.worker = worker;
        }
    }

    void OnTriggerExit(Collider other)
    {
        entered = false;
        sw.Stop();
        worker = null;
    }
}

﻿using UnityEngine;
using RobotDesigner;
using RobotDesigner.Nodes;
using System;
using RobotDesigner.Nodes.Sensors.Arrays;
using RobotDesigner.Views;
using UI.Components.Input;
using Designer;
using System.Xml;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

/// <summary>
/// Game UI.
/// </summary>
public class GameUI : MonoBehaviour
{
    private bool wasMouseRightDown;
    private bool wasMouseLeftDown;

    private float mouseLeftDownTime;
    private float mouseRightDownTime;

    /// <summary>
    /// Returns whether UI was created.
    /// </summary>
    public bool IsUICreated { get; private set; }

    /// <summary>
    /// Returns wheter UI is visible.
    /// </summary>
    public bool IsVisible { get; private set; }

    /// <summary>
    /// Checks if right mouse button is down.
    /// </summary>
    public bool IsMouseRightDown { get; private set; }

    /// <summary>
    /// Checks if left mouse button is down.
    /// </summary>
    public bool IsMouseLeftDown { get; private set; }

    /// <summary>
    /// Main scene method.
    /// </summary>
    public MethodScene MainMethod { get; private set; }

    /// <summary>
    /// Simulation UI.
    /// </summary>
    public SimulatorUI SimulatorUI { get; private set; }

    /// <summary>
    /// Method UI.
    /// </summary>
    public MethodUI MethodUI { get; private set; }

    /// <summary>
    /// Settings UI.
    /// </summary>
    //public SettingsUI SettingsUI { get; private set; }

    /// <summary>
    /// Start menu UI.
    /// </summary>
    public StartMenuUI StartMenuUI { get; private set; }

    /// <summary>
    /// Robot select UI.
    /// </summary>
    public RobotSelectUI RobotSelectUI { get; private set; }

    /// <summary>
    /// Constructor for game UI.
    /// </summary>
    public GameUI()
    {
        IsVisible = true;
    }

    /// <summary>
    /// Called on every GUI redraw.
    /// </summary>
    void OnGUI()
    {
        if (!IsVisible) return;

        if (!IsUICreated)
            CreateUI();

        HandleMouseClicks();
        HandleKeyboardKeys();

        if (SimulatorUI != null)
            SimulatorUI.CallUpdate(Event.current.mousePosition);

        if (MethodUI != null)
            MethodUI.CallUpdate(Event.current.mousePosition);

        //if (SettingsUI != null)
        //    SettingsUI.CallUpdate(Event.current.mousePosition);

        if (RobotSelectUI != null)
            RobotSelectUI.CallUpdate(Event.current.mousePosition);

        if (StartMenuUI != null)
            StartMenuUI.CallUpdate(Event.current.mousePosition);
    }

    private void LoadScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

    private void HandleKeyboardKeys()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
            LoadScene("map-demo");

        if (Input.GetKeyDown(KeyCode.Alpha2))
            LoadScene("map-test");
    }

    /// <summary>
    /// Handles mouse click events.
    /// </summary>
    private void HandleMouseClicks()
    {
        var curTime = Time.fixedTime;

        IsMouseLeftDown = Input.GetMouseButton(0);
        IsMouseRightDown = Input.GetMouseButton(1);

        if (IsMouseLeftDown && !wasMouseLeftDown)
        {
            HandleLeftDown();
            mouseLeftDownTime = curTime;
            wasMouseLeftDown = true;
        }

        if (IsMouseRightDown && !wasMouseRightDown)
        {
            HandleRightDown();
            mouseRightDownTime = curTime;
            wasMouseRightDown = true;
        }

        if (wasMouseLeftDown && !IsMouseLeftDown && (curTime - mouseLeftDownTime) < 0.25)
            HandleLeftClick();

        if (wasMouseRightDown && !IsMouseRightDown && (curTime - mouseRightDownTime) < 0.25)
            HandleRightClick();

        if (!IsMouseLeftDown && wasMouseLeftDown)
        {
            wasMouseLeftDown = false;
            HandleLeftRelease();
        }

        if (!IsMouseRightDown && wasMouseRightDown)
        {
            wasMouseRightDown = false;
            HandleRightRelease();
        }
    }

    public XmlDocument Serialize()
    {

        var buildNode = MainMethod.EntryNode;
        //var tree = buildNode.ToTree();

        var robotName = SimulatorUI.RobotName;
        //var robot = tree.ToRobot(robotName).Robot;

        var xdoc = new XmlDocument();


        var header = xdoc.CreateElement("RobotVL");
        header.SetAttribute("Version", "0.2.1");
        xdoc.AppendChild(header);

        var nodes = xdoc.CreateElement("Nodes");
        var chain = xdoc.CreateElement("Chain");

        header.AppendChild(nodes);
        //header.AppendChild(chain);

        buildNode.Serialize(xdoc, nodes, chain);


        return xdoc;
    }

    public void NewScene()
    {
        this.MainMethod = new MethodScene(null);
        this.MethodUI = new MethodUI();
        this.MethodUI.AddMethod(this.MainMethod);
        this.MethodUI.SetActiveMethod(this.MainMethod);
    }

    public void Deserialize(string path)
    {
        var xdoc = new XmlDocument();
        xdoc.Load(path);

        var mapConnections = new Dictionary<string, XmlElement>();
        var mapNodes = new Dictionary<string, XmlElement>();
        var mapNodesByType = new Dictionary<string, Dictionary<string, XmlElement>>();

        var xnodes = (xdoc.GetElementsByTagName("Nodes").Item(0) as XmlElement).GetElementsByTagName("Node");
        //var xconnections = (xdoc.GetElementsByTagName("Chain").Item(0) as XmlElement).GetElementsByTagName("Attachments");

        //foreach (XmlElement xcon in xconnections)
        //    mapConnections.Add(xcon.GetAttribute("ID"), xcon);

        string mainEntry = null;

        int unorderedCount = 0;
        var unorderedMap = new Dictionary<int, XmlElement>();

        foreach (XmlElement xnode in xnodes)
        {
            unorderedMap.Add(unorderedCount++, xnode);
            var id = xnode.GetAttribute("ID");
            var type = xnode.GetAttribute("Type");

            mapNodes.Add(id, xnode);
            if (mapConnections.ContainsKey(id))
                xnode.AppendChild(mapConnections[id]);

            if (!mapNodesByType.ContainsKey(type))
                mapNodesByType[type] = new Dictionary<string, XmlElement>();

            var typeMap = mapNodesByType[type];

            if (!typeMap.ContainsKey(id))
                typeMap[id] = xnode;

            var isGlobal = false;
            bool.TryParse(xnode.GetAttribute("IsMain"), out isGlobal);
            if (type == "Entry" && isGlobal)
                mainEntry = id;
        }

        var ordered = new List<XmlElement>();
        for (var i = 0; i < unorderedCount; i++)
            ordered.Add(unorderedMap[i]);

        var sceneMap = new Dictionary<string, MethodScene>();
        MethodScene mainScene = null;
        var mapEntry = new Dictionary<string, EntryNode>();

        this.MethodUI = new MethodUI();

        foreach (var xel in mapNodesByType["Entry"].Values)
        {
            var sceneId = xel.GetAttribute("Scene");
            var nodeId = xel.GetAttribute("ID");

            var scene = new MethodScene();

            sceneMap[sceneId] = scene;
            scene.UniqueId = sceneId;

            if (nodeId == mainEntry)
                mainScene = scene;

            scene.EntryNode.UniqueId = nodeId;
            mapEntry[nodeId] = scene.EntryNode;
            mapNodes.Remove(nodeId);

            this.MethodUI.AddMethod(scene);
        }

        //mapNodesByType.Remove("Entry");

        this.MainMethod = mainScene;
        this.MethodUI.SetActiveMethod(this.MainMethod);

        BaseNode.Deserialze(sceneMap, mapNodesByType, mapEntry, ordered);
    }

    /// <summary>
    /// Called whenever RMB is down.
    /// </summary>
    private void HandleRightDown() { }

    /// <summary>
    /// Called whenever RMB is down.
    /// </summary>
    private void HandleLeftDown() { }

    /// <summary>
    /// Creates UI elements.
    /// </summary>
    private void CreateUI()
    {
        IsUICreated = true;

        FuckUnity.UnityIsAFuckingCunt.Instance.box = new GUIStyle(GUI.skin.box);
        FuckUnity.UnityIsAFuckingCunt.Instance.button = new GUIStyle(GUI.skin.button);
        FuckUnity.UnityIsAFuckingCunt.Instance.label = new GUIStyle(GUI.skin.label);
        FuckUnity.UnityIsAFuckingCunt.Instance.input = new GUIStyle(GUI.skin.label);
        FuckUnity.UnityIsAFuckingCunt.Instance.buttonDisabled = new GUIStyle(GUI.skin.button);
        FuckUnity.UnityIsAFuckingCunt.Instance.buttonDisabled.fontStyle = FontStyle.Italic;

        FuckUnity.UnityIsAFuckingCunt.Instance.labelDsiabled = new GUIStyle(GUI.skin.label);
        FuckUnity.UnityIsAFuckingCunt.Instance.labelDsiabled.fontStyle = FontStyle.Italic;

        BoxEntryStyle.Instantiate();
        BoxIfElseStyle.Instantiate();
        BoxMethodStyle.Instantiate();
        BoxMotorStyle.Instantiate();
        BoxNumericStyle.Instantiate();
        BoxColorStyle.Instantiate();
        BoxOperatorStyle.Instantiate();
        BoxUltrasonicStyle.Instantiate();
        BoxLightStyle.Instantiate();
        ButtonStyle.Instantiate();
        InputStyle.Instantiate();

        SimulatorUI = new SimulatorUI(this);
        //SettingsUI = new SettingsUI(this);
        RobotSelectUI = new RobotSelectUI(this);
        StartMenuUI = new StartMenuUI(this);

        this.MainMethod = new MethodScene(null);
        this.MethodUI = new MethodUI();
        this.MethodUI.AddMethod(this.MainMethod);
        this.MethodUI.SetActiveMethod(this.MainMethod);

        //CreateDemoSolverCode();
        //CreateTestCode();

        bool fastStart = false;

        if (fastStart)
        {

            StartMenuUI.IsVisible = false;
            SimulatorUI.IsVisible = true;
            MethodUI.IsVisible = true;
            RobotSelectUI.IsVisible = false;
        }
        else
        {
            StartMenuUI.IsVisible = true;
            SimulatorUI.IsVisible = false;
            MethodUI.IsVisible = false;
            RobotSelectUI.IsVisible = true;
        }
    }

    public static GameUI GetUI()
    {
        return FindObjectOfType<GameUI>();
    }

    public void StartGame()
    {
        RobotSelectUI.IsVisible = true;
        StartMenuUI.IsVisible = false;
    }


    [Obsolete]
    private string nameOne = "one";
    [Obsolete]
    private string nameZero = "zero";
    [Obsolete]
    private string namePrevDist = "prevDist";
    [Obsolete]
    private string namePrevDir = "prevDir";
    [Obsolete]
    private string nameCurDist = "curDist";
    [Obsolete]
    private string nameCurDir = "curDir";
    [Obsolete]
    private string nameItersLeft = "itersLeft";


    [Obsolete]
    private MethodNode CreateInitNodeInternals(MethodScene scene)
    {
        var node = new MethodNode(scene, "MyInit");

        var initNumOne = new NumericNode(node.InnerMethod, nameOne, 1);
        initNumOne.IsGlobal = true;
        node.InnerMethod.EntryNode.Connect(DirectionalButton.Direction.East, initNumOne);

        var initNumZero = new NumericNode(node.InnerMethod, nameZero, 0);
        initNumZero.IsGlobal = true;
        initNumOne.Connect(DirectionalButton.Direction.East, initNumZero);

        return node;
    }

    [Obsolete]
    private MethodNode CreateIfGreenMethod(MethodScene scene)
    {
        var node = new MethodNode(scene, "WhenGreen");

        return node;
    }

    [Obsolete]
    private MethodNode CreateIfBlueMethod(MethodScene scene)
    {
        var node = new MethodNode(scene, "WhenBlue");

        return node;
    }

    [Obsolete]
    private MethodNode CreateIfNotBlueMethod(MethodScene scene)
    {
        var node = new MethodNode(scene, "WhenNotBlue");

        var colorNode = new ColorSensorNode(node.InnerMethod);
        node.InnerMethod.EntryNode.Connect(DirectionalButton.Direction.East, colorNode);

        var ifColor = new IfNode(node.InnerMethod);
        colorNode.Connect(DirectionalButton.Direction.South, ifColor);

        var redVal = new NumericNode(node.InnerMethod, "var_red");
        ifColor.Connect(DirectionalButton.Direction.West, redVal);
        redVal.AssignMode = NumericNode.AssignValueNode_T.ASSIGN_NODE;
        redVal.ValueType = NumericNode.TypeValueNode_T.COLOR;
        redVal.ColorValue = NumericNode.ColorValue_T.RED;

        var leftMotor = new MotorNode(node.InnerMethod, 3);
        var rightMotor = new MotorNode(node.InnerMethod, 3);


        rightMotor.Motor = MotorNode.Motor_T.RIGHT;

        ifColor.Connect(DirectionalButton.Direction.East, leftMotor);
        leftMotor.Connect(DirectionalButton.Direction.East, rightMotor);

        return node;
    }

    [Obsolete]
    private MethodNode CreateIfNotGreenMethod(MethodScene scene)
    {
        var node = new MethodNode(scene, "WhenNotGreen");

        var colorNode = new ColorSensorNode(node.InnerMethod);
        node.InnerMethod.EntryNode.Connect(DirectionalButton.Direction.East, colorNode);

        var ifColor = new IfNode(node.InnerMethod);
        colorNode.Connect(DirectionalButton.Direction.South, ifColor);

        var blueVal = new NumericNode(node.InnerMethod, "var_blue");
        ifColor.Connect(DirectionalButton.Direction.West, blueVal);
        blueVal.AssignMode = NumericNode.AssignValueNode_T.ASSIGN_NODE;
        blueVal.ValueType = NumericNode.TypeValueNode_T.COLOR;
        blueVal.ColorValue = NumericNode.ColorValue_T.BLUE;

        var leftMotor = new MotorNode(node.InnerMethod, 3);
        var rightMotor = new MotorNode(node.InnerMethod, 3);

        rightMotor.Motor = MotorNode.Motor_T.RIGHT;

        ifColor.Connect(DirectionalButton.Direction.East, leftMotor);
        leftMotor.Connect(DirectionalButton.Direction.East, rightMotor);

        var onBlueMethod = CreateIfBlueMethod(node.InnerMethod);

        rightMotor.Connect(DirectionalButton.Direction.East, onBlueMethod);

        var onNotBlueMethod = CreateIfNotBlueMethod(node.InnerMethod);
        ifColor.ElseNode.Connect(DirectionalButton.Direction.East, onNotBlueMethod);

        return node;
    }

    [Obsolete]
    private MethodNode CreateFromRightNode(MethodScene scene)
    {
        var node = new MethodNode(scene, "ObstacleFromRight");

        var leftMotor = new MotorNode(node.InnerMethod, 0);
        var rightMotor = new MotorNode(node.InnerMethod, 3);

        rightMotor.Motor = MotorNode.Motor_T.RIGHT;

        node.InnerMethod.EntryNode.Connect(DirectionalButton.Direction.East, leftMotor);
        leftMotor.Connect(DirectionalButton.Direction.East, rightMotor);

        return node;
    }

    private MethodNode CreateFromLeftNode(MethodScene scene)
    {
        var node = new MethodNode(scene, "ObstacleFromLeft");

        var leftMotor = new MotorNode(node.InnerMethod, 0);
        var rightMotor = new MotorNode(node.InnerMethod, 0);

        rightMotor.Motor = MotorNode.Motor_T.RIGHT;

        node.InnerMethod.EntryNode.Connect(DirectionalButton.Direction.East, leftMotor);
        leftMotor.Connect(DirectionalButton.Direction.East, rightMotor);

        return node;
    }

    [Obsolete]
    private MethodNode CreateCheckObstacleLeftNode(MethodScene scene)
    {
        var node = new MethodNode(scene, "CheckLeftObstacle");

        var dirNode = new UltrasonicArrayNode(node.InnerMethod);
        dirNode.OutputType = UltrasonicArrayNode.ArrayType_T.Direction;

        node.InnerMethod.EntryNode.Connect(DirectionalButton.Direction.East, dirNode);

        var ifDist = new IfNode(node.InnerMethod);
        dirNode.Connect(DirectionalButton.Direction.South, ifDist);
        ifDist.Comparator = IfNode.Comparators_T.LT;

        var zeroVar = new NumericNode(node.InnerMethod, "zero", 0);
        ifDist.Connect(DirectionalButton.Direction.West, zeroVar);

        var obstacleNode = CreateFromLeftNode(node.InnerMethod);
        ifDist.Connect(DirectionalButton.Direction.East, obstacleNode);

        return node;
    }

    [Obsolete]
    private MethodNode CreateTooCloseNode(MethodScene scene)
    {
        var node = new MethodNode(scene, "WhenTooClose");

        var dirNode = new UltrasonicArrayNode(node.InnerMethod);
        dirNode.OutputType = UltrasonicArrayNode.ArrayType_T.Direction;

        node.InnerMethod.EntryNode.Connect(DirectionalButton.Direction.East, dirNode);

        var ifDist = new IfNode(node.InnerMethod);
        dirNode.Connect(DirectionalButton.Direction.South, ifDist);
        ifDist.Comparator = IfNode.Comparators_T.GT;

        var zeroVar = new NumericNode(node.InnerMethod, "zero", 0);
        ifDist.Connect(DirectionalButton.Direction.West, zeroVar);

        var obstacleRight = CreateFromRightNode(node.InnerMethod);
        ifDist.Connect(DirectionalButton.Direction.East, obstacleRight);

        var checkObstacleLeft = CreateCheckObstacleLeftNode(node.InnerMethod);
        ifDist.ElseNode.Connect(DirectionalButton.Direction.East, checkObstacleLeft);

        return node;
    }

    [Obsolete]
    private MethodNode CreateAfterMethod(MethodScene scene)
    {
        var node = new MethodNode(scene, "AfterMethod");

        var distNode = new UltrasonicArrayNode(node.InnerMethod);
        distNode.OutputType = UltrasonicArrayNode.ArrayType_T.Distance;

        node.InnerMethod.EntryNode.Connect(DirectionalButton.Direction.East, distNode);

        var ifDist = new IfNode(node.InnerMethod);
        distNode.Connect(DirectionalButton.Direction.South, ifDist);
        ifDist.Comparator = IfNode.Comparators_T.LTE;

        var allowedDistVal = new NumericNode(node.InnerMethod, "dist_allowed", 1);
        ifDist.Connect(DirectionalButton.Direction.West, allowedDistVal);

        var ifTooCloseNode = CreateTooCloseNode(node.InnerMethod);
        ifDist.Connect(DirectionalButton.Direction.East, ifTooCloseNode);

        return node;
    }


    [Obsolete]
    private void CreateDemoSolverCode()
    {
        var main = this.MainMethod;
        var initializeNode = CreateInitNodeInternals(main);
        main.EntryNode.Connect(DirectionalButton.Direction.East, initializeNode);

        var colorNode = new ColorSensorNode(main);
        initializeNode.Connect(DirectionalButton.Direction.East, colorNode);

        var ifColor = new IfNode(main);
        colorNode.Connect(DirectionalButton.Direction.South, ifColor);

        var greenVal = new NumericNode(main, "val_green");
        ifColor.Connect(DirectionalButton.Direction.West, greenVal);
        greenVal.AssignMode = NumericNode.AssignValueNode_T.ASSIGN_NODE;
        greenVal.ValueType = NumericNode.TypeValueNode_T.COLOR;
        greenVal.ColorValue = NumericNode.ColorValue_T.GREEN;

        var leftMotor = new MotorNode(main, 3);
        var rightMotor = new MotorNode(main, 3);

        rightMotor.Motor = MotorNode.Motor_T.RIGHT;

        ifColor.Connect(DirectionalButton.Direction.East, leftMotor);
        leftMotor.Connect(DirectionalButton.Direction.East, rightMotor);

        var ifSuccess = CreateIfGreenMethod(main);
        rightMotor.Connect(DirectionalButton.Direction.East, ifSuccess);

        var ifFail = CreateIfNotGreenMethod(main);
        ifColor.ElseNode.Connect(DirectionalButton.Direction.East, ifFail);

        var afterMethod = CreateAfterMethod(main);
        ifColor.ElseNode.Connect(DirectionalButton.Direction.South, afterMethod);
    }

    [Obsolete]
    private MethodNode CreateSetCurrentMetod(DirectionalButton.Direction dir, BaseNode parentNode, MethodScene parentMethod)
    {
        var methNode = new MethodNode(parentMethod, "SetCurrent");
        parentNode.Connect(dir, methNode);

        var meth = methNode.InnerMethod;

        var ultrasonicDist = new UltrasonicArrayNode(meth);
        meth.EntryNode.Connect(DirectionalButton.Direction.East, ultrasonicDist);
        ultrasonicDist.OutputType = UltrasonicArrayNode.ArrayType_T.Distance;

        var initCurDist = new NumericNode(meth, nameCurDist);
        ultrasonicDist.Connect(DirectionalButton.Direction.East, initCurDist);
        initCurDist.AssignMode = NumericNode.AssignValueNode_T.ASSIGN_INPUT;
        initCurDist.IsGlobal = true;

        var ultrasonicDir = new UltrasonicArrayNode(meth);
        initCurDist.Connect(DirectionalButton.Direction.East, ultrasonicDir);
        ultrasonicDir.OutputType = UltrasonicArrayNode.ArrayType_T.Direction;

        var initCurDir = new NumericNode(meth, nameCurDir);
        ultrasonicDir.Connect(DirectionalButton.Direction.East, initCurDir);
        initCurDir.AssignMode = NumericNode.AssignValueNode_T.ASSIGN_INPUT;
        initCurDir.IsGlobal = true;

        return methNode;
    }

    [Obsolete]
    private MethodNode CreateItersLeftMethod(DirectionalButton.Direction dir, BaseNode parentNode, MethodScene parentScene)
    {
        var methNode = new MethodNode(parentScene, "WhenInterationsLeft");
        parentNode.Connect(dir, methNode);

        var meth = methNode.InnerMethod;

        var motorLeft = new MotorNode(meth, -1);
        motorLeft.Motor = MotorNode.Motor_T.LEFT;
        meth.EntryNode.Connect(DirectionalButton.Direction.East, motorLeft);

        var motorRight = new MotorNode(meth, 0);
        motorRight.Motor = MotorNode.Motor_T.RIGHT;
        motorLeft.Connect(DirectionalButton.Direction.East, motorRight);

        var varIterLeft1 = new NumericNode(meth, nameItersLeft);
        varIterLeft1.IsGlobal = true;
        varIterLeft1.AssignMode = NumericNode.AssignValueNode_T.ASSIGN_NONE;
        motorRight.Connect(DirectionalButton.Direction.East, varIterLeft1);

        var oper = new OperatorNode(meth);
        oper.Operator = OperatorNode.Operators_T.SUBTRACTION;
        varIterLeft1.Connect(DirectionalButton.Direction.South, oper);

        var varOne = new NumericNode(meth, nameOne);
        varOne.IsGlobal = true;
        varOne.AssignMode = NumericNode.AssignValueNode_T.ASSIGN_NONE;
        oper.Connect(DirectionalButton.Direction.West, varOne);

        var varIterLeft2 = new NumericNode(meth, nameItersLeft);
        varIterLeft2.IsGlobal = true;
        varIterLeft2.AssignMode = NumericNode.AssignValueNode_T.ASSIGN_INPUT;
        oper.Connect(DirectionalButton.Direction.South, varIterLeft2);

        return methNode;
    }

    [Obsolete]
    private MethodNode CreatePrevDistGreaterThanCurDist(DirectionalButton.Direction dir, BaseNode parentNode, MethodScene parentScene)
    {
        var methNode = new MethodNode(parentScene, "PrevDistGreaterThanCur");
        parentNode.Connect(dir, methNode);

        var meth = methNode.InnerMethod;

        var varPrevDist = new NumericNode(meth, namePrevDist);
        varPrevDist.IsGlobal = true;
        varPrevDist.AssignMode = NumericNode.AssignValueNode_T.ASSIGN_NONE;
        meth.EntryNode.Connect(DirectionalButton.Direction.East, varPrevDist);

        var ifNode = new IfNode(meth);
        varPrevDist.Connect(DirectionalButton.Direction.South, ifNode);
        ifNode.Comparator = IfNode.Comparators_T.GTE;

        var varCurDist = new NumericNode(meth, nameCurDist);
        varCurDist.IsGlobal = true;
        varCurDist.AssignMode = NumericNode.AssignValueNode_T.ASSIGN_NONE;

        ifNode.Connect(DirectionalButton.Direction.West, varCurDist);

        var varItersLeft = new NumericNode(meth, nameItersLeft, 60);
        ifNode.Connect(DirectionalButton.Direction.East, varItersLeft);
        varItersLeft.IsGlobal = true;
        varItersLeft.AssignMode = NumericNode.AssignValueNode_T.ASSIGN_NODE;

        return methNode;
    }

    [Obsolete]
    private MethodNode CreatePreviousDirEquelsCurrentDirMethod(DirectionalButton.Direction dir, BaseNode parentNode, MethodScene parentScene)
    {
        var methNode = new MethodNode(parentScene, "PreviousDirEquelsCurrentDir");
        parentNode.Connect(dir, methNode);

        var meth = methNode.InnerMethod;

        var varPrevDir = new NumericNode(meth, namePrevDir);
        varPrevDir.IsGlobal = true;
        varPrevDir.AssignMode = NumericNode.AssignValueNode_T.ASSIGN_NONE;

        meth.EntryNode.Connect(DirectionalButton.Direction.East, varPrevDir);

        var ifNode = new IfNode(meth);
        varPrevDir.Connect(DirectionalButton.Direction.South, ifNode);
        ifNode.Comparator = IfNode.Comparators_T.EQ;

        var varCurDir = new NumericNode(meth, nameCurDir);
        varCurDir.IsGlobal = true;
        varCurDir.AssignMode = NumericNode.AssignValueNode_T.ASSIGN_NONE;

        ifNode.Connect(DirectionalButton.Direction.West, varCurDir);

        CreatePrevDistGreaterThanCurDist(DirectionalButton.Direction.East, ifNode, meth);

        return methNode;
    }

    [Obsolete]
    private MethodNode CreateDistanceGreaterThanOneMethod(DirectionalButton.Direction dir, BaseNode parentNode, MethodScene parentScene)
    {
        var methNode = new MethodNode(parentScene, "DistanceGreaterThanOne");
        parentNode.Connect(dir, methNode);

        var meth = methNode.InnerMethod;

        var varOne = new NumericNode(meth, nameOne);
        varOne.IsGlobal = true;
        varOne.AssignMode = NumericNode.AssignValueNode_T.ASSIGN_NONE;
        meth.EntryNode.Connect(DirectionalButton.Direction.East, varOne);

        var ifNode = new IfNode(meth);
        ifNode.Comparator = IfNode.Comparators_T.GT;
        varOne.Connect(DirectionalButton.Direction.South, ifNode);

        var ultrasonicDistance = new UltrasonicArrayNode(meth);
        ultrasonicDistance.OutputType = UltrasonicArrayNode.ArrayType_T.Distance;
        ifNode.Connect(DirectionalButton.Direction.West, ultrasonicDistance);

        var leftMotor = new MotorNode(meth, 1);
        leftMotor.Motor = MotorNode.Motor_T.LEFT;
        ifNode.Connect(DirectionalButton.Direction.East, leftMotor);

        var rightMotor = new MotorNode(meth, 1);
        rightMotor.Motor = MotorNode.Motor_T.RIGHT;
        leftMotor.Connect(DirectionalButton.Direction.East, rightMotor);

        CreatePreviousDirEquelsCurrentDirMethod(DirectionalButton.Direction.East, rightMotor, meth);

        return methNode;
    }

    [Obsolete]
    private MethodNode CreateUltrasonicDirLessThanZeroElseMethod(DirectionalButton.Direction dir, BaseNode parentNode, MethodScene parentScene)
    {
        var methNode = new MethodNode(parentScene, "UltrasonicDirLessThanZeroElse");
        parentNode.Connect(dir, methNode);

        var meth = methNode.InnerMethod;

        var varZero = new NumericNode(meth, nameZero);
        varZero.IsGlobal = true;
        varZero.AssignMode = NumericNode.AssignValueNode_T.ASSIGN_NONE;
        meth.EntryNode.Connect(DirectionalButton.Direction.East, varZero);

        var ifNode = new IfNode(meth);
        ifNode.Comparator = IfNode.Comparators_T.GT;
        varZero.Connect(DirectionalButton.Direction.South, ifNode);

        var ultrasonicDirection = new UltrasonicArrayNode(meth);
        ultrasonicDirection.OutputType = UltrasonicArrayNode.ArrayType_T.Direction;
        ifNode.Connect(DirectionalButton.Direction.West, ultrasonicDirection);

        var leftMotor = new MotorNode(meth, 0);
        leftMotor.Motor = MotorNode.Motor_T.RIGHT;
        ifNode.Connect(DirectionalButton.Direction.East, leftMotor);

        CreateDistanceGreaterThanOneMethod(DirectionalButton.Direction.East, ifNode.ElseNode, meth);

        return methNode;
    }

    [Obsolete]
    private MethodNode CreateUltrasonicDirLessThanZeroMethod(DirectionalButton.Direction dir, BaseNode parentNode, MethodScene parentScene)
    {
        var methNode = new MethodNode(parentScene, "UltrasonicDirLessThanZero");
        parentNode.Connect(dir, methNode);

        var meth = methNode.InnerMethod;

        var varZero = new NumericNode(meth, nameZero);
        varZero.IsGlobal = true;
        varZero.AssignMode = NumericNode.AssignValueNode_T.ASSIGN_NONE;
        meth.EntryNode.Connect(DirectionalButton.Direction.East, varZero);

        var ifNode = new IfNode(meth);
        ifNode.Comparator = IfNode.Comparators_T.LT;
        varZero.Connect(DirectionalButton.Direction.South, ifNode);

        var ultrasonicDir = new UltrasonicArrayNode(meth);
        ultrasonicDir.OutputType = UltrasonicArrayNode.ArrayType_T.Direction;
        ifNode.Connect(DirectionalButton.Direction.West, ultrasonicDir);

        var rightMotor = new MotorNode(meth, 0);
        rightMotor.Motor = MotorNode.Motor_T.LEFT;
        ifNode.Connect(DirectionalButton.Direction.East, rightMotor);

        CreateUltrasonicDirLessThanZeroElseMethod(DirectionalButton.Direction.East, ifNode.ElseNode, meth);

        return methNode;
    }

    [Obsolete]
    private MethodNode CreateNoItersMethod(DirectionalButton.Direction dir, BaseNode parentNode, MethodScene parentScene)
    {
        bool leftBias = false;

        var methNode = new MethodNode(parentScene, "NoIterationsLeft");
        parentNode.Connect(dir, methNode);

        var meth = methNode.InnerMethod;

        var varPrevDir = new NumericNode(meth, namePrevDir);
        meth.EntryNode.Connect(DirectionalButton.Direction.East, varPrevDir);
        varPrevDir.IsGlobal = true;
        varPrevDir.AssignMode = NumericNode.AssignValueNode_T.ASSIGN_NONE;

        var ifPrevDirIsZero = new IfNode(meth);
        varPrevDir.Connect(DirectionalButton.Direction.South, ifPrevDirIsZero);

        var varZero = new NumericNode(meth, nameZero);
        varZero.IsGlobal = true;
        varZero.AssignMode = NumericNode.AssignValueNode_T.ASSIGN_NONE;

        ifPrevDirIsZero.Connect(DirectionalButton.Direction.West, varZero);

        var motorRight1 = new MotorNode(meth, leftBias ? 2 : 3);
        motorRight1.Motor = MotorNode.Motor_T.RIGHT;
        ifPrevDirIsZero.Connect(DirectionalButton.Direction.East, motorRight1);

        var motorRight2 = new MotorNode(meth, 3);
        motorRight2.Motor = MotorNode.Motor_T.RIGHT;
        ifPrevDirIsZero.ElseNode.Connect(DirectionalButton.Direction.East, motorRight2);

        var motorLeft1 = new MotorNode(meth, 3);
        motorLeft1.Motor = MotorNode.Motor_T.LEFT;
        ifPrevDirIsZero.ElseNode.Connect(DirectionalButton.Direction.South, motorLeft1);

        //var motorLeft2 = new MotorNode(meth, 3);
        //motorLeft2.Motor = MotorNode.Motor_T.RIGHT;
        //motorLeft1.Connect(DirectionalButton.Direction.South, motorLeft2);


        CreateUltrasonicDirLessThanZeroMethod(DirectionalButton.Direction.East, motorLeft1, meth);

        return methNode;
    }

    [Obsolete]
    private MethodNode CreateColorCheckRedMethod(DirectionalButton.Direction dir, BaseNode parentNode, MethodScene parentScene)
    {
        var methNode = new MethodNode(parentScene, "CheckIfRed");
        parentNode.Connect(dir, methNode);

        var sensor = new ColorSensorNode(methNode.InnerMethod);
        methNode.InnerMethod.EntryNode.Connect(DirectionalButton.Direction.East, sensor);

        var ifNode = new IfNode(methNode.InnerMethod);
        sensor.Connect(DirectionalButton.Direction.South, ifNode);

        var redColor = new NumericNode(methNode.InnerMethod, "var_red");
        redColor.ColorValue = NumericNode.ColorValue_T.RED;
        redColor.ValueType = NumericNode.TypeValueNode_T.COLOR;
        ifNode.Connect(DirectionalButton.Direction.West, redColor);

        var motorLeft = new MotorNode(methNode.InnerMethod, 1);
        ifNode.Connect(DirectionalButton.Direction.East, motorLeft);

        CreateStopOnBlackMethod(DirectionalButton.Direction.East, ifNode.ElseNode, methNode.InnerMethod);

        return methNode;
    }

    [Obsolete]
    private MethodNode CreateStopOnBlackMethod(DirectionalButton.Direction dir, BaseNode parentNode, MethodScene parentScene)
    {
        var methNode = new MethodNode(parentScene, "CheckIfBlack");
        parentNode.Connect(dir, methNode);

        var sensor = new ColorSensorNode(methNode.InnerMethod);
        methNode.InnerMethod.EntryNode.Connect(DirectionalButton.Direction.East, sensor);

        var ifNode = new IfNode(methNode.InnerMethod);
        sensor.Connect(DirectionalButton.Direction.South, ifNode);

        var blackColor = new NumericNode(methNode.InnerMethod, "var_black");
        blackColor.ColorValue = NumericNode.ColorValue_T.BLACK;
        blackColor.ValueType = NumericNode.TypeValueNode_T.COLOR;
        ifNode.Connect(DirectionalButton.Direction.West, blackColor);

        var motorLeft = new MotorNode(methNode.InnerMethod, 0);
        ifNode.Connect(DirectionalButton.Direction.East, motorLeft);
        var motorRight = new MotorNode(methNode.InnerMethod, 0);
        motorRight.Motor = MotorNode.Motor_T.RIGHT;
        motorLeft.Connect(DirectionalButton.Direction.East, motorRight);

        return methNode;
    }

    [Obsolete]
    private MethodNode CreateLuxUpgradeMethod(DirectionalButton.Direction dir, BaseNode parentNode, MethodScene parentScene)
    {
        var methNode = new MethodNode(parentScene, "CheckLux");
        parentNode.Connect(dir, methNode);

        var sensor = new LightSensorNode(methNode.InnerMethod);
        methNode.InnerMethod.EntryNode.Connect(DirectionalButton.Direction.East, sensor);

        var ifNode = new IfNode(methNode.InnerMethod);
        sensor.Connect(DirectionalButton.Direction.South, ifNode);
        ifNode.Comparator = IfNode.Comparators_T.LTE;

        var luxVal = new NumericNode(methNode.InnerMethod, "var_lux", 30);
        ifNode.Connect(DirectionalButton.Direction.West, luxVal);

        var motorRight = new MotorNode(methNode.InnerMethod, 0);
        motorRight.Motor = MotorNode.Motor_T.RIGHT;
        ifNode.Connect(DirectionalButton.Direction.East, motorRight);

        var motorLeft = new MotorNode(methNode.InnerMethod, 0);
        motorRight.Connect(DirectionalButton.Direction.East, motorLeft);

        return methNode;
    }

    [Obsolete]
    private MethodNode CreateColorUpgradeMethod(DirectionalButton.Direction dir, BaseNode parentNode, MethodScene parentScene)
    {
        var methNode = new MethodNode(parentScene, "CheckIfBlue");
        parentNode.Connect(dir, methNode);

        var sensor = new ColorSensorNode(methNode.InnerMethod);
        methNode.InnerMethod.EntryNode.Connect(DirectionalButton.Direction.East, sensor);

        var ifNode = new IfNode(methNode.InnerMethod);
        sensor.Connect(DirectionalButton.Direction.South, ifNode);

        var blueColor = new NumericNode(methNode.InnerMethod, "var_blue");
        blueColor.ColorValue = NumericNode.ColorValue_T.BLUE;
        blueColor.ValueType = NumericNode.TypeValueNode_T.COLOR;
        ifNode.Connect(DirectionalButton.Direction.West, blueColor);

        var motorRight = new MotorNode(methNode.InnerMethod, 1);
        motorRight.Motor = MotorNode.Motor_T.RIGHT;
        ifNode.Connect(DirectionalButton.Direction.East, motorRight);

        CreateColorCheckRedMethod(DirectionalButton.Direction.East, ifNode.ElseNode, methNode.InnerMethod);

        return methNode;
    }

    [Obsolete]
    private MethodNode CreateSetPreviousMethod(DirectionalButton.Direction dir, BaseNode parentNode, MethodScene parentScene)
    {
        var methNode = new MethodNode(parentScene, "SetPrevious");
        parentNode.Connect(dir, methNode);

        var meth = methNode.InnerMethod;

        var ultrasonicDist = new UltrasonicArrayNode(meth);
        meth.EntryNode.Connect(DirectionalButton.Direction.East, ultrasonicDist);
        ultrasonicDist.OutputType = UltrasonicArrayNode.ArrayType_T.Distance;

        var initPrevDist = new NumericNode(meth, namePrevDist);
        ultrasonicDist.Connect(DirectionalButton.Direction.East, initPrevDist);
        initPrevDist.AssignMode = NumericNode.AssignValueNode_T.ASSIGN_INPUT;
        initPrevDist.IsGlobal = true;

        var ultrasonicDir = new UltrasonicArrayNode(meth);
        initPrevDist.Connect(DirectionalButton.Direction.East, ultrasonicDir);
        ultrasonicDir.OutputType = UltrasonicArrayNode.ArrayType_T.Direction;

        var initPrevDir = new NumericNode(meth, namePrevDir);
        ultrasonicDir.Connect(DirectionalButton.Direction.East, initPrevDir);
        initPrevDir.AssignMode = NumericNode.AssignValueNode_T.ASSIGN_INPUT;
        initPrevDir.IsGlobal = true;

        return methNode;
    }

    [Obsolete]
    private void CreateTestCode()
    {
        //#region MAIN

        var main = this.MainMethod;

        var initNumOne = new NumericNode(main, nameOne, 1);
        initNumOne.IsGlobal = true;
        main.EntryNode.Connect(DirectionalButton.Direction.East, initNumOne);

        var initNumZero = new NumericNode(main, nameZero, 0);
        initNumZero.IsGlobal = true;
        initNumOne.Connect(DirectionalButton.Direction.East, initNumZero);

        var methSetCurrent = CreateSetCurrentMetod(DirectionalButton.Direction.East, initNumZero, main);

        var varItersLeft = new NumericNode(main, nameItersLeft);
        varItersLeft.IsGlobal = true;
        varItersLeft.AssignMode = NumericNode.AssignValueNode_T.ASSIGN_NONE;
        methSetCurrent.Connect(DirectionalButton.Direction.East, varItersLeft);

        var ifItersLeft = new IfNode(main);
        ifItersLeft.Comparator = IfNode.Comparators_T.GT;
        varItersLeft.Connect(DirectionalButton.Direction.South, ifItersLeft);

        var varZero = new NumericNode(main, nameZero);
        varZero.IsGlobal = true;
        varZero.AssignMode = NumericNode.AssignValueNode_T.ASSIGN_NONE;
        ifItersLeft.Connect(DirectionalButton.Direction.West, varZero);

        CreateItersLeftMethod(DirectionalButton.Direction.East, ifItersLeft, main);
        CreateNoItersMethod(DirectionalButton.Direction.East, ifItersLeft.ElseNode, main);
        var iter = CreateSetPreviousMethod(DirectionalButton.Direction.South, ifItersLeft.ElseNode, main);
        var lux = CreateColorUpgradeMethod(DirectionalButton.Direction.East, iter, main);
        CreateLuxUpgradeMethod(DirectionalButton.Direction.East, lux, main);
    }

    /// <summary>
    /// Handles LMB click.
    /// </summary>
    private void HandleLeftClick() { }

    /// <summary>
    /// Handles RMB click.
    /// </summary>
    private void HandleRightClick() { }

    /// <summary>
    /// Called whenever LMB is released.
    /// </summary>
    private void HandleLeftRelease() { }

    /// <summary>
    /// Called whenever RMB is released.
    /// </summary>
    private void HandleRightRelease() { }
}
using RobotDesigner.Views;
using UnityEngine;

namespace Utilities
{
    /// <summary>
    /// Utilities related to UI.
    /// </summary>
    public static class Utils_UI
    {
        /// <summary>
        /// Resizes vector based on custom ratio.
        /// </summary>
        /// <param name="x">X component to resize.</param>
        /// <param name="y">Y component to resize.</param>
        /// <returns>Resized vector.</returns>
        public static Vector2 ResizeVector(float x, float y)
        {
            float rectX = (x / CustomUI.BaseX) * Screen.width;
            float rectY = (y / CustomUI.BaseY) * Screen.height;

            return new Vector2(rectX, rectY);
        }

        /// <summary>
        /// Resizes rectangle based on custom ratio.
        /// </summary>
        /// <param name="rect">Rectangle to resize.</param>
        /// <returns>Resized rectangle.</returns>
        public static Rect ResizeRect(Rect rect)
        {
            float FilScreenWidth = rect.width / CustomUI.BaseX;
            float rectWidth = FilScreenWidth * Screen.width;
            float FilScreenHeight = rect.height / CustomUI.BaseY;
            float rectHeight = FilScreenHeight * Screen.height;
            float rectX = (rect.x / CustomUI.BaseX) * Screen.width;
            float rectY = (rect.y / CustomUI.BaseY) * Screen.height;

            return new Rect(rectX, rectY, rectWidth, rectHeight);
        }
    }

    /// <summary>
    /// Utilities related to math.
    /// </summary>
    public static class Utils_Math
    {
        /// <summary>
        /// Rounds number to the nearest multiple.
        /// </summary>
        /// <param name="number">Number to round.</param>
        /// <param name="multiple">Multple to round to.</param>
        /// <returns></returns>
        public static float RoundToNearestMultiple(float number, float multiple)
        {
            return Mathf.Round(number * multiple) / multiple;
        }
    }

    /// <summary>
    /// Generic utilities.
    /// </summary>
    public static class Utils_Other
    {
        public static readonly string LobbyUniqueIdentifier = "68F9w]9DSl08yns6IS06+mN7uu:97S";

        /// <summary>
        /// Deserializes color from string.
        /// </summary>
        /// <param name="color">Color representation as string.</param>
        /// <returns>Deserialized color. White if fails.</returns>
        public static Color ColorFromString(string color)
        {
            string[] elements = color.Split(' ');

            if (elements.Length >= 3)
            {
                float r, g, b;

                if (float.TryParse(elements[0], out r) && 
                    float.TryParse(elements[1], out g) && 
                    float.TryParse(elements[2], out b))
                    return new Color(r / 255, g / 255, b / 255);
            }

            return Color.white;
        }
    }
}
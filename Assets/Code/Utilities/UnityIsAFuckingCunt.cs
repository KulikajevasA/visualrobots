﻿using UnityEngine;

namespace FuckUnity
{
    /// <summary>
    /// Because unity is being a cunt and cannot create UI styles outside of OnGUI event this is created.
    /// </summary>
    public class UnityIsAFuckingCunt
    {
        private static UnityIsAFuckingCunt instance;

        /// <summary>
        /// Default box style.
        /// </summary>
        public GUIStyle box { get; set; }

        /// <summary>
        /// Default label style.
        /// </summary>
        public GUIStyle label { get; set; }

        /// <summary>
        /// Disabled label style.
        /// </summary>
        public GUIStyle labelDsiabled { get; set; }

        /// <summary>
        /// Default input style.
        /// </summary>
        public GUIStyle input { get; set; }

        /// <summary>
        /// Default button style.
        /// </summary>
        public GUIStyle button { get; set; }

        /// <summary>
        /// Default disabled button style.
        /// </summary>
        public GUIStyle buttonDisabled { get; set; }

        /// <summary>
        /// Private constructor.
        /// </summary>
        private UnityIsAFuckingCunt() { }

        /// <summary>
        /// Gets singleton instance of this wonderful class.
        /// </summary>
        public static UnityIsAFuckingCunt Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new UnityIsAFuckingCunt();
                }
                return instance;
            }
        }
    }
}

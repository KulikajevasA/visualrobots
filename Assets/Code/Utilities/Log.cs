﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Used to display in queue when unity is not in editor mode.
/// </summary>
public class Log : MonoBehaviour
{
    private static string myLog;
    private static Queue myLogQueue = new Queue();
    private string output = string.Empty;
    private string stack = string.Empty;
    private bool hidden = true;
    private const int MAX_LINES = 30;

    /// <summary>
    /// Unity OnEnable event handler.
    /// </summary>
    void OnEnable()
    {
        Application.logMessageReceived += HandleLog;
    }

    /// <summary>
    /// Unity OnDisable event handler.
    /// </summary>
    void OnDisable()
    {
        Application.logMessageReceived -= HandleLog;
    }

    /// <summary>
    /// Handles logging.
    /// </summary>
    /// <param name="logString">String to log.</param>
    /// <param name="stackTrace">Stack trace.</param>
    /// <param name="type">Log type.</param>
    private void HandleLog(string logString, string stackTrace, LogType type)
    {
        output = logString;
        stack = stackTrace;
        string newString = "\n [" + type + "] : " + output;
        myLogQueue.Enqueue(newString);
        if (type == LogType.Exception)
        {
            newString = "\n" + stackTrace;
            myLogQueue.Enqueue(newString);
        }

        while (myLogQueue.Count > MAX_LINES)
        {
            myLogQueue.Dequeue();
        }

        myLog = string.Empty;
        foreach (string s in myLogQueue)
        {
            myLog += s;
        }
    }

    /// <summary>
    /// Unity OnGUI event handler.
    /// </summary>
    void OnGUI()
    {
        if (!hidden)
        {
            GUI.TextArea(new Rect(0, 0, Screen.width / 3, Screen.height), myLog);
            if (GUI.Button(new Rect(Screen.width - 100, 10, 80, 20), "Hide"))
            {
                Hide(true);
            }
        }
        else
        {
            if (GUI.Button(new Rect(Screen.width - 100, 10, 80, 20), "Show"))
            {
                Hide(false);
            }
        }
    }

    /// <summary>
    /// Sets logs hidden level.
    /// </summary>
    /// <param name="shouldHide">Should be hidden.</param>
    public void Hide(bool shouldHide)
    {
        hidden = shouldHide;
    }
}
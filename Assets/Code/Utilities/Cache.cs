﻿using System.Collections.Generic;

using UnityEngine;

namespace Cache
{
    /// <summary>
    /// Caches 2D textures.
    /// </summary>
    public class Cache
    {
        private static Cache instance;
        private Dictionary<string, Texture2D> textureCache = new Dictionary<string, Texture2D>();

        /// <summary>
        /// Private constructor for cache.
        /// </summary>
        private Cache() { }

        /// <summary>
        /// Gets singleton instance of cacher.
        /// </summary>
        public static Cache Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Cache();
                }
                return instance;
            }
        }

        /// <summary>
        /// Precaches a texture.
        /// </summary>
        /// <param name="texture">Texture path.</param>
        /// <returns>TRUE if precached, FALSE otherwise.</returns>
        public bool PrecacheTexture(string texture)
        {
            if (textureCache.ContainsKey(texture)) return false;
            var tex2D = Resources.Load<Texture2D>(texture);

            if (tex2D == null) return false;

            textureCache.Add(texture, tex2D);
            return true;
        }

        /// <summary>
        /// Loads a texture from cache.
        /// </summary>
        /// <param name="texture">Name of the texture to load.</param>
        /// <returns>Requested texture if cached, NULL otherwise.</returns>
        public Texture2D LoadTexture(string texture)
        {
            Texture2D tex = null;

            textureCache.TryGetValue(texture, out tex);

            return tex;
        }
    }
}

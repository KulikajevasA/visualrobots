﻿using UnityEngine;
using System.Collections;
using Simulation;

public class TriggerRespawn : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        var worker = other.transform.GetComponentInParent<RobotWorker>();

        if (worker)
        {
            if (GameUI.GetUI())
                GameUI.GetUI().SimulatorUI.ShowStoppedUI();
            worker.Restore();
        }
    }
}

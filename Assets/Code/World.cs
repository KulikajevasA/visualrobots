﻿using UnityEngine;
using System.Collections;

public class World : MonoBehaviour
{
    private static int layerShadow = -1;
    private static int layerShadowBit;
    public static int LayerShadow { get { return layerShadow; } }
    public static int LayerShadowBit { get { return layerShadowBit; } }

    public string LayerShadowName = "Shadow";

    // Use this for initialization
    void Start()
    {
        layerShadow = LayerMask.NameToLayer(LayerShadowName);
        layerShadowBit = 1 << layerShadow;
    }
}

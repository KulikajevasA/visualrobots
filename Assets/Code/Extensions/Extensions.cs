﻿using UnityEngine;

/// <summary>
/// Extensions for types.
/// </summary>
public static class Extensions
{
    /// <summary>
    /// Gets XYZ components from 4 dimentional vector.
    /// </summary>
    /// <param name="vec">Vector to extract coordinates from.</param>
    /// <returns>Extracted 3 dimentional vector.</returns>
    public static Vector3 XYZ(this Vector4 vec)
    {
        return new Vector3(vec.x, vec.y, vec.z);
    }

    /// <summary>
    /// Gets XY components from 4 dimentional vector.
    /// </summary>
    /// <param name="vec">Vector to extract coordinates from.</param>
    /// <returns>Extracted 2 dimentional vector.</returns>
    public static Vector2 XY(this Vector4 vec)
    {
        return new Vector2(vec.x, vec.y);
    }

    /// <summary>
    /// Gets XY components from 3 dimentional vector.
    /// </summary>
    /// <param name="vec">Vector to extract coordinates from.</param>
    /// <returns>Extracted 2 dimentional vector.</returns>
    public static Vector2 XY(this Vector3 vec)
    {
        return new Vector2(vec.x, vec.y);
    }

    /// <summary>
    /// Converts 3 dimentional vector and W component into 4 dimentional vector.
    /// </summary>
    /// <param name="vec">3 dimentional vector.</param>
    /// <param name="w">W component to add.</param>
    /// <returns>4 dimentional vector.</returns>
    public static Vector4 W(this Vector3 vec, float w)
    {
        return new Vector4(vec.x, vec.y, vec.z, w);
    }
}


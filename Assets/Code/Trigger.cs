﻿using UnityEngine;
using System.Collections;

public class Trigger : MonoBehaviour
{
    public Animation anim;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        if (anim != null)
            anim.Play();
    }
}

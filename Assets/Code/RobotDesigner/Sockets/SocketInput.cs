﻿using RobotDesigner.Nodes;
using UnityEngine;

namespace RobotDesigner.Sockets
{
    public class SocketInput : BaseSocket
    {
        public SocketInput(BaseNode ownerNode, Vector2 offset) : base(ownerNode, offset) { }
    }
}

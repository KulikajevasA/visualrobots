﻿using RobotDesigner.Nodes;
using UnityEngine;

namespace RobotDesigner.Sockets
{
    public class BaseSocket
    {
        private Vector2 offset;

        private BaseNode ownerNode;
        private Rect rect;
        public const int NOB_SIZE = 5;

        public Rect Bounds { get { return rect; } }
        public BaseNode Parent { get { return ownerNode; } }
        public BaseSocket Connection { get; set; }


        public BaseSocket(BaseNode ownerNode, Vector2 offset)
        {
            this.ownerNode = ownerNode;
            this.offset = offset;
            this.rect = new Rect();
            this.rect.width = this.rect.height = NOB_SIZE;
            /*this.rect.width = 55;
            this.rect.height = 5;*/

            //this.rect = new Rect(position, new Vector2(NOB_SIZE, NOB_SIZE));
        }

        public void DrawSocket(GUIStyle style)
        {
            this.rect.x = ownerNode.Bounds.x + offset.x;
            this.rect.y = ownerNode.Bounds.y + offset.y;
            GUI.Box(rect, GUIContent.none, style);
        }

        public void RemoveConnection()
        {
            if (Connection != null)
            {
                Connection.Connection = null;
                Connection = null;
            }
        }
    }
}
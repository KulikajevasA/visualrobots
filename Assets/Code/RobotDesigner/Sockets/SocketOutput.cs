﻿using RobotDesigner.Nodes;
using UnityEngine;

namespace RobotDesigner.Sockets
{
    public class SocketOutput : BaseSocket
    {
        public SocketOutput(BaseNode ownerNode, Vector2 offset) : base(ownerNode, offset) { }
    }
}

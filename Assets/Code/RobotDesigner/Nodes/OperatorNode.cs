﻿using UnityEngine;
using UI;
using UI.Components;
using DesignerGlue.Chain;
using UI.Components.Input;
using DesignerGlue.Chain.Types;
using System;
using System.Xml;

namespace RobotDesigner.Nodes
{
    /// <summary>
    /// Operator node.
    /// </summary>
    public class OperatorNode : BaseNode, IValueNode
    {
        /// <summary>
        /// Registers UI element.
        /// </summary>
        public static void RegisterUIElement() { UIBuilder.AddTypeBind(typeof(OperatorNode)); }

        /// <summary>
        /// Returns node type.
        /// </summary>
        public override string NodeType { get { return "Operator"; } }

        protected override void OnDraw()
        {
            GUI.Box(Bounds, GUIContent.none, boxOperatorStyle.Style);
            DrawN();
            DrawE();
            DrawS();
            DrawW();
        }

        protected Label labelName;

        /// <summary>
        /// List of available operators.
        /// </summary>
        public enum Operators_T
        {
            ADDITION = 0,
            SUBTRACTION,
            MULTIPLICATION,
            DIVISION,
            TOTAL_SIGNS
        }

        private Operators_T operatorType;

        /// <summary>
        /// Operator getter and setter.
        /// </summary>
        public Operators_T Operator
        {
            get
            {
                return operatorType;
            }
            set
            {
                operatorType = value;
                UpdateCaption();
            }
        }
        private Button operatorButton;

        /// <summary>
        /// Supports east attachments.
        /// </summary>
        public override bool HasEast { get { return false; } }

        public OperatorNode(MethodScene method, XmlElement xel) : this(method, 0, 0)
        {
            this.operatorType = (Operators_T)Enum.Parse(typeof(Operators_T), xel.GetAttribute("Operator"));
            UpdateCaption();
        }

        /// <summary>
        /// Constructor for node.
        /// </summary>
        /// <param name="method">Method to which node is attached.</param>
        public OperatorNode(MethodScene method) : this(method, 0, 0) { }

        /// <summary>
        /// Constructor for node.
        /// </summary>
        /// <param name="method">Method to which node is attached.</param>
        /// <param name="x">X position of the node.</param>
        /// <param name="y">Y position of the node.</param>
        /// <param name="name">Name of the node.</param>
        public OperatorNode(MethodScene method, float x, float y, string name = null) : base(method, x, y, name)
        {
            labelName = new Label(14, 12, 150, 22, "Operation:", true);

            this.AddChild(labelName);

            labelName.Style.fontStyle = FontStyle.BoldAndItalic;

            operatorButton = new Button(90, 35, 25, 25, "+", true);
            operatorButton.IsEnabled = true;
            this.AddChild(operatorButton);
            operatorButton.OnCommand += HandleOperatorIncrement;
        }

        /// <summary>
        /// Increments operator.
        /// </summary>
        /// <param name="caller">Button caller.</param>
        /// <param name="params">Caller parameters.</param>
        private void HandleOperatorIncrement(BaseButton caller, object[] @params)
        {
            Operator++;

            if (Operator >= Operators_T.TOTAL_SIGNS)
                Operator = Operators_T.ADDITION;
        }

        private void UpdateCaption()
        {
            switch (operatorType)
            {
                case Operators_T.DIVISION:
                    operatorButton.Text = "/";
                    break;
                case Operators_T.SUBTRACTION:
                    operatorButton.Text = "-";
                    break;
                case Operators_T.MULTIPLICATION:
                    operatorButton.Text = "*";
                    break;
                case Operators_T.ADDITION:
                default:
                    operatorButton.Text = "+";
                    operatorType = Operators_T.ADDITION;
                    break;
            }
        }

        /// <summary>
        /// Builds syntax chain.
        /// </summary>
        /// <returns>Syntax chain.</returns>
        public override SyntaxChain BuildChain()
        {
            var leaf = new OperatorLink(this);

            var westNode = GetNode(DirectionalButton.Direction.West);

            if (westNode != null)
                leaf.ExtraLink = westNode.BuildChain();

            var eastNode = GetNode(DirectionalButton.Direction.East);
            if (eastNode != null)
                leaf.Next = eastNode.BuildChain();
            else
            {
                var southNode = GetNode(DirectionalButton.Direction.South);
                if (southNode != null)
                    leaf.Next = southNode.BuildChain();
            }

            return leaf;
        }

        public override XmlElement Serialize(XmlDocument xdoc, XmlElement xnodes, XmlElement xchain)
        {
            var node = base.Serialize(xdoc, xnodes, xchain);

            node.SetAttribute("Operator", Operator.ToString());

            return node;
        }
    }
}

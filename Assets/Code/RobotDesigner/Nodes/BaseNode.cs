﻿using Designer;
using DesignerGlue.Chain;
using RobotDesigner.Nodes.Sensors.Arrays;
using RobotDesigner.Sockets;
using System;
using System.Collections.Generic;
using UI;
using UI.Components.Input;
using UnityEngine;
using System.Xml;

namespace RobotDesigner.Nodes
{
    public abstract class BaseNode : BaseUIComponent
    {
        protected static int xNodes = 8;
        protected static int yNodes = 2;

        public static int X_NODES { get { return xNodes; } }
        public static int Y_NODES { get { return yNodes; } }

        public const float NODE_WIDTH = 200;
        public const float NODE_HEIGHT = 100;
#if true
        protected GUIStyle styleEast;
        protected GUIStyle styleNorth;
        protected GUIStyle styleSouth;
        protected GUIStyle styleWest;
#endif
        protected DirectionalButton[] buttonsDir;
        protected BaseNode[] nodeAttachments;
        protected BaseNode[] nodeAttachmentsReversed;

#if true
        protected List<BaseSocket> socketsNorth;
        protected List<BaseSocket> socketsEast;
        protected List<BaseSocket> socketsSouth;
        protected List<BaseSocket> socketsWest;
#endif
        protected List<DirectionalButton> nodeCreationButtons;

        public BoxEntryStyle boxEntryStyle;
        public BoxIfElseStyle boxIfElseStyle;
        public BoxMethodStyle boxMethodStyle;
        public BoxMotorStyle boxMotorStyle;
        public BoxNumericStyle boxNumericStyle;
        public BoxColorStyle boxColorStyle;
        public BoxOperatorStyle boxOperatorStyle;
        public BoxUltrasonicStyle boxUltrasonicStyle;
        public BoxLightStyle boxLightStyle;

        protected Button buttonDelete;

        /// <summary>
        /// Unique node identifier.
        /// </summary>
        public string UniqueId { get; set; }

        /// <summary>
        /// Parent metohod of the node.
        /// </summary>
        public MethodScene ParentMethod { get; private set; }
        /// <summary>
        /// Supports south attachments.
        /// </summary>
        public virtual bool HasSouth { get { return true; } }

        /// <summary>
        /// Supports north attachments.
        /// </summary>
        public virtual bool HasNorth { get { return false; } }

        /// <summary>
        /// Supports west attachments.
        /// </summary>
        public virtual bool HasWest { get { return true; } }

        /// <summary>
        /// Supports east attachments.
        /// </summary>
        public virtual bool HasEast { get { return true; } }


        protected BaseNode(MethodScene method, float x, float y, string name = null)
            : base(x, y, NODE_WIDTH, NODE_HEIGHT, true, null, null)
        {
            this.boxEntryStyle = new BoxEntryStyle();
            this.boxIfElseStyle = new BoxIfElseStyle();
            this.boxMethodStyle = new BoxMethodStyle();
            this.boxMotorStyle = new BoxMotorStyle();
            this.boxNumericStyle = new BoxNumericStyle();
            this.boxColorStyle = new BoxColorStyle();
            this.boxOperatorStyle = new BoxOperatorStyle();
            this.boxUltrasonicStyle = new BoxUltrasonicStyle();
            this.boxLightStyle = new BoxLightStyle();

            this.buttonsDir = new DirectionalButton[(int)DirectionalButton.Direction.MAX_DIRECTIONS];
            this.nodeAttachments = new BaseNode[(int)DirectionalButton.Direction.MAX_DIRECTIONS];
            this.nodeAttachmentsReversed = new BaseNode[(int)DirectionalButton.Direction.MAX_DIRECTIONS];

            this.ParentMethod = method;
            this.UniqueId = Guid.NewGuid().ToString();

            this.nodeCreationButtons = new List<DirectionalButton>();

            this.buttonDelete = new Button(0, 0, 17, 16, "X", true, null, this);
            this.buttonDelete.IsEnabled = true;

            method.AddChild(this);

            this.CreateSupportedButtons();

#if true
            socketsEast = new List<BaseSocket>();
            socketsNorth = new List<BaseSocket>();
            socketsSouth = new List<BaseSocket>();
            socketsWest = new List<BaseSocket>();
#endif
        }

#if true
        public void DrawN()
        {
            foreach (BaseSocket socket in socketsNorth)
                socket.DrawSocket(styleNorth);
        }

        public void DrawE()
        {
            foreach (BaseSocket socket in socketsEast)
                socket.DrawSocket(styleEast);
        }

        public void DrawS()
        {
            foreach (BaseSocket socket in socketsSouth)
                socket.DrawSocket(styleSouth);
        }

        public void DrawW()
        {
            foreach (BaseSocket socket in socketsWest)
                socket.DrawSocket(styleWest);
        }
#endif

        protected override void OnDraw()
        {
#if true
            DrawN();
            DrawE();
            DrawS();
            DrawW();
#endif
        }

        protected override void CreateScheme()
        {
            Style = new GUIStyle(FuckUnity.UnityIsAFuckingCunt.Instance.box);

#if true
            styleNorth = new GUIStyle(Style);
            styleEast = new GUIStyle(Style);
            styleSouth = new GUIStyle(Style);
            styleWest = new GUIStyle(Style);

            styleNorth.normal.background = MakeTex(1, 1, new Color(0.051f, 0.000f, 0.000f));
            styleNorth.fixedHeight = -20;
            styleNorth.fixedWidth = 5;

            styleEast.normal.background = MakeTex(1, 1, new Color(0.051f, 0.000f, 0.000f));
            styleEast.fixedHeight = 5;
            styleEast.fixedWidth = 30;

            styleSouth.normal.background = MakeTex(1, 1, new Color(0.051f, 0.000f, 0.000f));
            styleSouth.fixedHeight = 35;
            styleSouth.fixedWidth = 5;

            styleWest.normal.background = MakeTex(1, 1, new Color(0.051f, 0.000f, 0.000f));
            styleWest.fixedHeight = 5;
            styleWest.fixedWidth = -40;
#endif
        }

        /// <summary>
        /// Creates supported buttons.
        /// </summary>
        private void CreateSupportedButtons()
        {
            float buttonWidth = 17;
            float buttonHeight = 19;

            if (HasEast)
            {
                var buttonEast = new DirectionalButton(183, 40, buttonWidth, buttonHeight, "+", true, null, this);
                buttonEast.IsEnabled = true;
                AddChildren(buttonEast, buttonDelete);

                buttonEast.SetParameters(this.Bounds.position.x, this.Bounds.position.y);

                buttonEast.SetDirection(DirectionalButton.Direction.East);
                buttonEast.OnCommand += NodeDirectionButton;

                this.buttonsDir[DirectionalButton.FromDirection(DirectionalButton.Direction.East)] = buttonEast;
            }

            if (HasSouth)
            {
                var buttonSouth = new DirectionalButton(91, 81, buttonWidth, buttonHeight, "+", true, null, this);
                buttonSouth.IsEnabled = true;
                AddChildren(buttonSouth, buttonDelete);

                buttonSouth.SetParameters(this.Bounds.position.x, this.Bounds.position.y);

                buttonSouth.SetDirection(DirectionalButton.Direction.South);
                buttonSouth.OnCommand += NodeDirectionButton;

                this.buttonsDir[DirectionalButton.FromDirection(DirectionalButton.Direction.South)] = buttonSouth;
            }

            if (HasWest)
            {
                var buttonWest = new DirectionalButton(0, 40, buttonWidth, buttonHeight, "+", true, null, this);
                buttonWest.IsEnabled = true;
                AddChildren(buttonWest, buttonDelete);

                buttonWest.SetParameters(this.Bounds.position.x, this.Bounds.position.y);

                buttonWest.SetDirection(DirectionalButton.Direction.West);
                buttonWest.OnCommand += NodeDirectionButton;

                this.buttonsDir[DirectionalButton.FromDirection(DirectionalButton.Direction.West)] = buttonWest;
            }

            if (HasNorth)
            {
                throw new NotImplementedException("North buttons not implemented yet.");
            }

            buttonDelete.OnCommand += DeleteButtonNode;
        }

        public static void Deserialze(Dictionary<string, MethodScene> sceneMap, Dictionary<string, Dictionary<string, XmlElement>> mapNodesByType, Dictionary<string, EntryNode> mapEntry, List<XmlElement> ordered)
        {
            var mapNodes = new Dictionary<string, Dictionary<string, BaseNode>>();

            //var mapFlatXML = new Dictionary<string, XmlElement>();
            var mapFlatNodes = new Dictionary<string, BaseNode>();


            foreach (var entry in mapEntry)
                mapFlatNodes.Add(entry.Key, entry.Value);
            //foreach (var type in mapNodesByType.Keys)
            //    foreach (var xel in mapNodesByType[type])
            //        mapFlatXML.Add(xel.Key, xel.Value);

            foreach (var type in mapNodesByType.Keys)
            {
                if (!mapNodes.ContainsKey(type))
                    mapNodes[type] = new Dictionary<string, BaseNode>();

                var container = mapNodes[type];

                foreach (var xel in mapNodesByType[type].Values)
                {
                    var nodeId = xel.GetAttribute("ID");
                    var sceneId = xel.GetAttribute("Scene");
                    var scene = sceneMap[sceneId];

                    BaseNode node = null;

                    switch (type)
                    {
                        case "Numeric":
                        case "Color":
                            node = new NumericNode(scene, xel);
                            break;
                        case "Motor":
                            node = new MotorNode(scene, xel);
                            break;
                        case "UltrasonicSensorArray":
                            node = new UltrasonicArrayNode(scene, xel);
                            break;
                        case "LightSensor":
                            node = new LightSensorNode(scene, xel);
                            break;
                        case "ColorSensor":
                            node = new ColorSensorNode(scene, xel);
                            break;
                        case "Method":
                            var entryId = xel.GetAttribute("Entry");
                            var entryScene = sceneMap[mapEntry[entryId].ParentMethod.UniqueId];
                            node = new MethodNode(scene, entryScene, xel);
                            break;
                        case "Operator":
                            node = new OperatorNode(scene, xel);
                            break;
                        case "IfStatement":
                            node = new IfNode(scene, xel);
                            var elseId = xel.GetAttribute("ElseNode");
                            var xelse = mapNodesByType["ElseStatement"][elseId];
                            var elseNode = new ElseNode(scene, xel);
                            elseNode.UniqueId = xelse.GetAttribute("ID");
                            (node as IfNode).ElseNodeHack = elseNode;

                            if (!mapNodes.ContainsKey("ElseStatement"))
                                mapNodes["ElseStatement"] = new Dictionary<string, BaseNode>();

                            var elseContainer = mapNodes["ElseStatement"];

                            elseContainer.Add(elseId, elseNode);
                            mapFlatNodes.Add(elseId, elseNode);
                            break;

                    }

                    if (node != null)
                    {
                        node.UniqueId = nodeId;
                        container.Add(nodeId, node);
                        mapFlatNodes.Add(nodeId, node);
                    }
                }
            }

            foreach (var xel in mapNodesByType["Entry"].Values)
            {
                var nodeId = xel.GetAttribute("ID");
                var parentId = xel.GetAttribute("Parent");
                if (parentId != null && parentId.Length > 0)
                {
                    var node = mapFlatNodes[parentId];
                    var entry = mapEntry[nodeId];

                    entry.ParentMethod.Parent = node as BaseSwitchNode;
                }
              
            }

            foreach (var xel in ordered)
            {
                //var xel = kv.Value;
                var node = mapFlatNodes[xel.GetAttribute("ID")];

                var xconnections = xel.GetElementsByTagName("Attached");

                foreach (XmlElement xcon in xconnections)
                {
                    var conId = xcon.InnerText;

                    var dir = (DirectionalButton.Direction)Enum.Parse(typeof(DirectionalButton.Direction), xcon.GetAttribute("Direction"));
                    var conNode = mapFlatNodes[conId];

                    if (dir == DirectionalButton.Direction.South && node is IfNode && conNode is ElseNode)
                        continue;

                    node.Connect(dir, conNode);
                }
            }
        }

#if true
        private Texture2D MakeTex(int width, int height, Color col)
        {
            Color[] pix = new Color[width * height];
            for (int i = 0; i < pix.Length; ++i)
                pix[i] = col;

            Texture2D result = new Texture2D(width, height);
            result.SetPixels(pix);
            result.Apply();
            return result;
        }
#endif

        private void RemoveNodeButtons()
        {
            this.ParentMethod.View.RemoveChildren(this.nodeCreationButtons.ToArray());
            //this.nodeCreationButtons = null;
        }

        private void DisableButton(BaseNode node, params DirectionalButton.Direction[] directions)
        {
            foreach (var direction in directions)
            {
                var button = node.buttonsDir[DirectionalButton.FromDirection(direction)];

                if (button != null)
                {
                    button.IsEnabled = false;
                    button.IsVisible = false;
                }
            }
        }

        private void Delete(DirectionalButton.Direction dir, BaseNode node)
        {
            this.nodeAttachments[(int)dir] = null;
            this.ParentMethod.View.RemoveChild(node);

            switch (dir)
            {
                case DirectionalButton.Direction.East:
                    this.socketsEast.Clear();
                    break;
                case DirectionalButton.Direction.South:
                    this.socketsSouth.Clear();
                    break;
                case DirectionalButton.Direction.West:
                    this.socketsWest.Clear();
                    break;
                case DirectionalButton.Direction.North:
                    this.socketsNorth.Clear();
                    break;
            }

            this.CreateSupportedButtons();
        }

        public void DeleteButtonNode(BaseButton caller, params object[] _params)
        {
            var dir = this.GetAttachDir();
            var revDir = DirectionalButton.GetOppositeDirection(dir);

            var parent = this.nodeAttachmentsReversed[(int)dir];
            parent.Delete(revDir, this);

            var main = new SyntaxChain();
            DeleteNode(main);
        }

        private DirectionalButton.Direction GetAttachDir()
        {
            DirectionalButton.Direction dir = DirectionalButton.Direction.East;
            for (int i = 0; i < this.nodeAttachmentsReversed.Length; i++)
            {
                if (this.nodeAttachmentsReversed[i] != null)
                {
                    dir = DirectionalButton.ToDirection(i);
                    break;
                }
            }

            return dir;
        }

        private SyntaxChain DeleteNode(SyntaxChain main)
        {

            var westNode = GetNode(DirectionalButton.Direction.West);
            if (westNode != null)
            {
                main.Next = westNode.DeleteNode(main);
                this.ParentMethod.View.RemoveChildren(westNode);
            }

            var eastNode = GetNode(DirectionalButton.Direction.East);
            if (eastNode != null)
            {
                main.Next = eastNode.DeleteNode(main);
                this.ParentMethod.View.RemoveChildren(eastNode);
            }

            var southNode = GetNode(DirectionalButton.Direction.South);
            if (southNode != null)
            {
                main.Next = southNode.DeleteNode(main);
                this.ParentMethod.View.RemoveChildren(southNode);
            }

            this.nodeAttachments = new BaseNode[(int)DirectionalButton.Direction.MAX_DIRECTIONS];

            return main;
        }

        public void NodeDirectionButton(DirectionalButton caller, DirectionalButton.Direction direction, BaseNode node, params object[] _params)
        {
            var xSize = (float)_params[0];
            var ySize = (float)_params[1];
            var ifElseNode = _params.Length <= 2 ? false : (bool)_params[2];

            if (caller != null)
            {
                caller.IsEnabled = false;
                caller.IsVisible = false;
            }

            float X = 0;
            float Y = 0;
            switch (direction)
            {
                case DirectionalButton.Direction.East:
                    X = caller.Parent.Bounds.x + 220;
                    Y = caller.Parent.Bounds.y;
                    break;
                case DirectionalButton.Direction.South:
                    X = caller.Parent.Bounds.x + 60;
                    Y = caller.Parent.Bounds.y + 110;
                    break;
                case DirectionalButton.Direction.West:
                    X = caller.Parent.Bounds.x - 100;
                    Y = caller.Parent.Bounds.y;
                    break;
            }

            var buttonVariable = new DirectionalButton(X, Y + 10, 80, 20, "Variable", true);
            this.nodeCreationButtons.Add(buttonVariable);
            buttonVariable.SetParameters(xSize, ySize, ifElseNode);
            buttonVariable.IsEnabled = true;
            buttonVariable.Style.normal.background = MakeTex(1, 1, new Color(0.255f, 0.179f, 0.000f));
            buttonVariable.Style.hover.background = MakeTex(1, 1, new Color(0.255f, 0.179f, 0.000f));

            var buttonColor = new DirectionalButton(X, Y + 30, 80, 20, "Color", true);
            this.nodeCreationButtons.Add(buttonColor);
            buttonColor.SetParameters(xSize, ySize, ifElseNode);
            buttonColor.IsEnabled = true;
            buttonColor.Style.normal.background = MakeTex(1, 1, new Color(0.204f, 0.051f, 0.179f));
            buttonColor.Style.hover.background = MakeTex(1, 1, new Color(0.204f, 0.051f, 0.179f));

            var buttonUltrasonic = new DirectionalButton(X, Y + 50, 80, 20, "Ultrasonic", true);
            this.nodeCreationButtons.Add(buttonUltrasonic);
            buttonUltrasonic.SetParameters(xSize, ySize, ifElseNode);
            buttonUltrasonic.IsEnabled = true;
            buttonUltrasonic.Style.normal.background = MakeTex(1, 1, new Color(0.153f, 0.204f, 0.255f));
            buttonUltrasonic.Style.hover.background = MakeTex(1, 1, new Color(0.153f, 0.204f, 0.255f));

            var buttonLight = new DirectionalButton(X, Y + 70, 80, 20, "Light", true);
            this.nodeCreationButtons.Add(buttonLight);
            buttonLight.SetParameters(xSize, ySize, ifElseNode);
            buttonLight.IsEnabled = true;
            buttonLight.Style.normal.background = MakeTex(1, 1, new Color(0.179f, 0.255f, 0.0f));
            buttonLight.Style.hover.background = MakeTex(1, 1, new Color(0.179f, 0.255f, 0.0f));

            var buttonMotor = new DirectionalButton(X, Y + 90, 80, 20, "Motor", true);
            this.nodeCreationButtons.Add(buttonMotor);
            buttonMotor.SetParameters(xSize, ySize, ifElseNode);
            buttonMotor.IsEnabled = true;
            buttonMotor.Style.normal.background = MakeTex(1, 1, new Color(0.204f, 0.000f, 0.051f));
            buttonMotor.Style.hover.background = MakeTex(1, 1, new Color(0.204f, 0.000f, 0.051f));

            var buttonMethod = new DirectionalButton(X, Y + 110, 80, 20, "Method", true);
            this.nodeCreationButtons.Add(buttonMethod);
            buttonMethod.SetParameters(xSize, ySize, ifElseNode);
            buttonMethod.IsEnabled = true;
            buttonMethod.Style.normal.background = MakeTex(1, 1, new Color(0.15f, 0.194f, 0.10f));
            buttonMethod.Style.hover.background = MakeTex(1, 1, new Color(0.15f, 0.194f, 0.10f));

            var buttonIf = new DirectionalButton(X, Y + 130, 80, 20, "If/Else", true);
            this.nodeCreationButtons.Add(buttonIf);
            buttonIf.SetParameters(xSize, ySize, ifElseNode);
            buttonIf.IsEnabled = true;
            buttonIf.Style.normal.background = MakeTex(1, 1, new Color(0.000f, 0.255f, 0.051f));
            buttonIf.Style.hover.background = MakeTex(1, 1, new Color(0.000f, 0.255f, 0.051f));

            var buttonOperation = new DirectionalButton(X, Y + 150, 80, 20, "Operation", true);
            this.nodeCreationButtons.Add(buttonOperation);
            buttonOperation.SetParameters(xSize, ySize, ifElseNode);
            buttonOperation.IsEnabled = true;
            buttonOperation.Style.normal.background = MakeTex(1, 1, new Color(0.204f, 0.255f, 0.051f));
            buttonOperation.Style.hover.background = MakeTex(1, 1, new Color(0.204f, 0.255f, 0.051f));

            buttonVariable.SetDirection(direction);
            buttonMethod.SetDirection(direction);
            buttonIf.SetDirection(direction);
            buttonUltrasonic.SetDirection(direction);
            buttonLight.SetDirection(direction);
            buttonOperation.SetDirection(direction);
            buttonMotor.SetDirection(direction);
            buttonColor.SetDirection(direction);

            this.ParentMethod.View.AddChildren(buttonVariable, buttonColor, buttonIf, buttonMethod, buttonUltrasonic, buttonOperation, buttonMotor, buttonLight);

            switch (direction)
            {
                case DirectionalButton.Direction.East:
                    buttonVariable.OnCommand += CreatePointEast_Variable;
                    buttonColor.OnCommand += CreatePointEast_Color;
                    buttonMethod.OnCommand += CreatePointEast_Method;
                    buttonMotor.OnCommand += CreatePointEast_Motor;
                    buttonIf.IsVisible = false;
                    buttonOperation.IsVisible = false;
                    buttonUltrasonic.OnCommand += CreatePointEast_Ultrasonic;
                    buttonLight.OnCommand += CreatePointEast_Light;
                    xNodes = calculateX();
                    if (!ifElseNode)
                        DisableButton(this, DirectionalButton.Direction.East, DirectionalButton.Direction.West, DirectionalButton.Direction.South);
                    if (ifElseNode)
                        DisableButton(this, DirectionalButton.Direction.East);
                    break;
                case DirectionalButton.Direction.South:
                    buttonVariable.OnCommand += CreatePointSouth_Variable;
                    buttonColor.OnCommand += CreatePointSouth_Color;
                    buttonMethod.OnCommand += CreatePointSouth_Method;
                    buttonMotor.OnCommand += CreatePointSouth_Motor;
                    buttonIf.OnCommand += CreatePointSouth_IfElse;
                    buttonOperation.OnCommand += CreatePointSouth_Operation;
                    buttonUltrasonic.OnCommand += CreatePointSouth_Ultrasonic;
                    buttonLight.OnCommand += CreatePointSouth_Light;
                    yNodes = calculateY();
                    if (!ifElseNode)
                        DisableButton(this, DirectionalButton.Direction.South, DirectionalButton.Direction.East, DirectionalButton.Direction.West);
                    if (ifElseNode)
                        DisableButton(this, DirectionalButton.Direction.South);
                    break;
                case DirectionalButton.Direction.West:
                    buttonVariable.OnCommand += CreatePointWest_Variable;
                    buttonColor.OnCommand += CreatePointWest_Color;
                    buttonMotor.OnCommand += CreatePointWest_Motor;
                    buttonMethod.IsVisible = false;
                    buttonIf.IsVisible = false;
                    buttonOperation.IsVisible = false;
                    buttonUltrasonic.OnCommand += CreatePointWest_Ultrasonic;
                    buttonLight.OnCommand += CreatePointWest_Light;
                    if (!ifElseNode)
                        DisableButton(this, DirectionalButton.Direction.West, DirectionalButton.Direction.South);
                    if (ifElseNode)
                        DisableButton(this, DirectionalButton.Direction.West);
                    break;
                case DirectionalButton.Direction.North:
                    buttonVariable.OnCommand += CreatePointNorth_Variable;
                    buttonColor.OnCommand += CreatePointNorth_Color;
                    buttonMethod.OnCommand += CreatePointNorth_Method;
                    buttonMotor.OnCommand += CreatePointNorth_Motor;
                    buttonIf.IsVisible = false;
                    buttonOperation.IsVisible = false;
                    buttonUltrasonic.OnCommand += CreatePointNorth_Ultrasonic;
                    buttonLight.OnCommand += CreatePointNorth_Light;
                    break;
            }
        }

        public void Connect(DirectionalButton.Direction attachDirection, BaseNode nodeToAttach)
        {
            var xSize = this.X;
            var ySize = this.Y;

            var ifElseNode = this is ElseNode || this is IfNode || this is OperatorNode;

            nodeToAttach.SetParameterValues(xSize, ySize, true);

            switch (attachDirection)
            {
                case DirectionalButton.Direction.East:
                    if (nodeToAttach is MotorNode)
                        CreatePointEast_Motor(null, attachDirection, nodeToAttach, xSize, ySize, true);

                    if (nodeToAttach is NumericNode)
                        CreatePointEast_Variable(null, attachDirection, nodeToAttach, xSize, ySize, true);

                    if (nodeToAttach is ColorSensorNode)
                        CreatePointEast_Color(null, attachDirection, nodeToAttach, xSize, ySize, true);

                    if (nodeToAttach is MethodNode)
                        CreatePointEast_Method(null, attachDirection, nodeToAttach, xSize, ySize, true);

                    if (nodeToAttach is UltrasonicArrayNode)
                        CreatePointEast_Ultrasonic(null, attachDirection, nodeToAttach, xSize, ySize, true);

                    if (nodeToAttach is LightSensorNode)
                        CreatePointEast_Light(null, attachDirection, nodeToAttach, xSize, ySize, true);

                    if (!ifElseNode)
                        DisableButton(this, DirectionalButton.Direction.East, DirectionalButton.Direction.West, DirectionalButton.Direction.South);

                    if (ifElseNode)
                        DisableButton(this, DirectionalButton.Direction.East);
                    break;
                case DirectionalButton.Direction.South:
                    if (nodeToAttach is MotorNode)
                        CreatePointSouth_Ultrasonic(null, attachDirection, nodeToAttach, xSize, ySize, ifElseNode);

                    if (nodeToAttach is NumericNode)
                        CreatePointSouth_Variable(null, attachDirection, nodeToAttach, xSize, ySize, ifElseNode);

                    if (nodeToAttach is ColorSensorNode)
                        CreatePointSouth_Color(null, attachDirection, nodeToAttach, xSize, ySize, ifElseNode);

                    if (nodeToAttach is MethodNode)
                        CreatePointSouth_Method(null, attachDirection, nodeToAttach, xSize, ySize, ifElseNode);

                    if (nodeToAttach is IfNode)
                        CreatePointSouth_IfElse(null, attachDirection, nodeToAttach, xSize, ySize, ifElseNode);

                    if (nodeToAttach is OperatorNode)
                        CreatePointSouth_Operation(null, attachDirection, nodeToAttach, xSize, ySize, ifElseNode);

                    if (nodeToAttach is UltrasonicArrayNode)
                        CreatePointSouth_Ultrasonic(null, attachDirection, nodeToAttach, xSize, ySize, ifElseNode);

                    if (nodeToAttach is LightSensorNode)
                        CreatePointSouth_Light(null, attachDirection, nodeToAttach, xSize, ySize, ifElseNode);

                    if (!ifElseNode)
                        DisableButton(this, DirectionalButton.Direction.South, DirectionalButton.Direction.East, DirectionalButton.Direction.West);

                    if (ifElseNode)
                        DisableButton(this, DirectionalButton.Direction.South);
                    break;
                case DirectionalButton.Direction.West:
                    if (nodeToAttach is MotorNode)
                        CreatePointWest_Motor(null, attachDirection, nodeToAttach, xSize, ySize, ifElseNode);

                    if (nodeToAttach is NumericNode)
                        CreatePointWest_Variable(null, attachDirection, nodeToAttach, xSize, ySize, ifElseNode);

                    if (nodeToAttach is ColorSensorNode)
                        CreatePointWest_Color(null, attachDirection, nodeToAttach, xSize, ySize, ifElseNode);

                    if (nodeToAttach is UltrasonicArrayNode)
                        CreatePointWest_Ultrasonic(null, attachDirection, nodeToAttach, xSize, ySize, ifElseNode);

                    if (nodeToAttach is LightSensorNode)
                        CreatePointWest_Light(null, attachDirection, nodeToAttach, xSize, ySize, ifElseNode);

                    if (!ifElseNode)
                        DisableButton(this, DirectionalButton.Direction.West, DirectionalButton.Direction.South);

                    if (ifElseNode)
                        DisableButton(this, DirectionalButton.Direction.West);
                    break;
                case DirectionalButton.Direction.North:
                    if (nodeToAttach is MotorNode)
                        CreatePointNorth_Motor(null, attachDirection, nodeToAttach, xSize, ySize, ifElseNode);

                    if (nodeToAttach is NumericNode)
                        CreatePointNorth_Variable(null, attachDirection, nodeToAttach, xSize, ySize, ifElseNode);

                    if (nodeToAttach is ColorSensorNode)
                        CreatePointNorth_Color(null, attachDirection, nodeToAttach, xSize, ySize, ifElseNode);

                    if (nodeToAttach is MethodNode)
                        CreatePointNorth_Method(null, attachDirection, nodeToAttach, xSize, ySize, ifElseNode);

                    if (nodeToAttach is UltrasonicArrayNode)
                        CreatePointNorth_Ultrasonic(null, attachDirection, nodeToAttach, xSize, ySize, ifElseNode);

                    if (nodeToAttach is LightSensorNode)
                        CreatePointNorth_Light(null, attachDirection, nodeToAttach, xSize, ySize, ifElseNode);
                    break;
            }
        }

        #region CREATE POINTS MOTOR
        private void CreatePointNorth_Motor(DirectionalButton caller, DirectionalButton.Direction direction, BaseNode node, params object[] _params)
        {
            var xSize = (float)_params[0];
            var ySize = (float)_params[1];
            float x = xSize;
            float y = ySize;
            CreatePointNorth_Simple(caller, direction, _params, out x, out y);

            if (node == null)
                node = new MotorNode(this.ParentMethod, x, y);
            else
                node.SetPos(x, y);

            node.SetParameterValues(x, y, false);

            this.nodeAttachments[DirectionalButton.FromDirection(direction)] = node;
            node.nodeAttachmentsReversed[DirectionalButton.FromDirection(DirectionalButton.GetOppositeDirection(direction))] = this;
        }

        private void CreatePointEast_Motor(DirectionalButton caller, DirectionalButton.Direction direction, BaseNode node, params object[] _params)
        {
            var xSize = (float)_params[0];
            var ySize = (float)_params[1];
            float x = xSize;
            float y = ySize;
            var ifElseNode = (bool)_params[2];

            CreatePointEast_Simple(caller, direction, _params, out x, out y);

            if (node == null)
                node = new MotorNode(this.ParentMethod, x, y);
            else
                node.SetPos(x, y);

            if (ifElseNode)
            {
                DisableButton(node, DirectionalButton.Direction.South, DirectionalButton.Direction.West);
                node.SetParameterValues(x, y, true);
            }
            else
            {
                DisableButton(node, DirectionalButton.Direction.West);
                node.SetParameterValues(x, y, false);
            }

            this.nodeAttachments[DirectionalButton.FromDirection(direction)] = node;
            node.nodeAttachmentsReversed[DirectionalButton.FromDirection(DirectionalButton.GetOppositeDirection(direction))] = this;
        }

        private void CreatePointSouth_Motor(DirectionalButton caller, DirectionalButton.Direction direction, BaseNode node, params object[] _params)
        {
            var xSize = (float)_params[0];
            var ySize = (float)_params[1];
            float x = xSize;
            float y = ySize;

            CreatePointSouth_Simple(caller, direction, _params, out x, out y);

            if (node == null)
                node = new MotorNode(this.ParentMethod, x, y);
            else
                node.SetPos(x, y);

            node.SetParameterValues(x, y, false);

            this.nodeAttachments[DirectionalButton.FromDirection(direction)] = node;
            node.nodeAttachmentsReversed[DirectionalButton.FromDirection(DirectionalButton.GetOppositeDirection(direction))] = this;
        }


        private void CreatePointWest_Motor(DirectionalButton caller, DirectionalButton.Direction direction, BaseNode node, params object[] _params)
        {
            var xSize = (float)_params[0];
            var ySize = (float)_params[1];
            float x = xSize;
            float y = ySize;
            var ifElseNode = (bool)_params[2];

            CreatePointWest_Simple(caller, direction, _params, out x, out y);

            if (node == null)
                node = new MotorNode(this.ParentMethod, x, y);
            else
                node.SetPos(x, y);

            if (ifElseNode)
            {
                DisableButton(node, DirectionalButton.Direction.East, DirectionalButton.Direction.South, DirectionalButton.Direction.West);
                node.SetParameterValues(x, y, true);
            }
            else
            {
                DisableButton(node, DirectionalButton.Direction.East);
                node.SetParameterValues(x, y, false);
            }

            this.nodeAttachments[DirectionalButton.FromDirection(direction)] = node;
            node.nodeAttachmentsReversed[DirectionalButton.FromDirection(DirectionalButton.GetOppositeDirection(direction))] = this;
        }
        #endregion

        #region CREATE POINTS COLOR METHOD
        private void CreatePointNorth_Color(DirectionalButton caller, DirectionalButton.Direction direction, BaseNode node, params object[] _params)
        {
            var xSize = (float)_params[0];
            var ySize = (float)_params[1];
            float x = xSize;
            float y = ySize;
            CreatePointNorth_Simple(caller, direction, _params, out x, out y);

            if (node == null)
                node = new ColorSensorNode(this.ParentMethod, x, y);
            else
                node.SetPos(x, y);

            node.SetParameterValues(x, y, false);


            this.nodeAttachments[DirectionalButton.FromDirection(direction)] = node;
            node.nodeAttachmentsReversed[DirectionalButton.FromDirection(DirectionalButton.GetOppositeDirection(direction))] = this;
        }

        private void CreatePointEast_Color(DirectionalButton caller, DirectionalButton.Direction direction, BaseNode node, params object[] _params)
        {
            var xSize = (float)_params[0];
            var ySize = (float)_params[1];
            float x = xSize;
            float y = ySize;
            var ifElseNode = (bool)_params[2];

            CreatePointEast_Simple(caller, direction, _params, out x, out y);

            if (node == null)
                node = new ColorSensorNode(this.ParentMethod, x, y);
            else
                node.SetPos(x, y);

            if (ifElseNode)
            {
                DisableButton(node, DirectionalButton.Direction.South, DirectionalButton.Direction.West);
                node.SetParameterValues(x, y, true);
            }
            else
            {
                DisableButton(node, DirectionalButton.Direction.West);
                node.SetParameterValues(x, y, false);
            }


            this.nodeAttachments[DirectionalButton.FromDirection(direction)] = node;
            node.nodeAttachmentsReversed[DirectionalButton.FromDirection(DirectionalButton.GetOppositeDirection(direction))] = this;
        }

        private void CreatePointSouth_Color(DirectionalButton caller, DirectionalButton.Direction direction, BaseNode node, params object[] _params)
        {
            var xSize = (float)_params[0];
            var ySize = (float)_params[1];
            float x = xSize;
            float y = ySize;

            CreatePointSouth_Simple(caller, direction, _params, out x, out y);

            if (node == null)
                node = new ColorSensorNode(this.ParentMethod, x, y);
            else
                node.SetPos(x, y);

            node.SetParameterValues(x, y, false);


            this.nodeAttachments[DirectionalButton.FromDirection(direction)] = node;
            node.nodeAttachmentsReversed[DirectionalButton.FromDirection(DirectionalButton.GetOppositeDirection(direction))] = this;
        }


        private void CreatePointWest_Color(DirectionalButton caller, DirectionalButton.Direction direction, BaseNode node, params object[] _params)
        {
            var xSize = (float)_params[0];
            var ySize = (float)_params[1];
            float x = xSize;
            float y = ySize;
            var ifElseNode = (bool)_params[2];

            CreatePointWest_Simple(caller, direction, _params, out x, out y);

            if (node == null)
                node = new ColorSensorNode(this.ParentMethod, x, y);
            else
                node.SetPos(x, y);

            if (ifElseNode)
            {
                DisableButton(node, DirectionalButton.Direction.East, DirectionalButton.Direction.South, DirectionalButton.Direction.West);
                node.SetParameterValues(x, y, true);
            }
            else
            {
                DisableButton(node, DirectionalButton.Direction.East);
                node.SetParameterValues(x, y, false);
            }


            this.nodeAttachments[DirectionalButton.FromDirection(direction)] = node;
            node.nodeAttachmentsReversed[DirectionalButton.FromDirection(DirectionalButton.GetOppositeDirection(direction))] = this;
        }
        #endregion 

        #region CREATE ULTRASONIC METHOD
        private void CreatePointNorth_Ultrasonic(DirectionalButton caller, DirectionalButton.Direction direction, BaseNode node, params object[] _params)
        {
            float x = 0, y = 0;

            CreatePointNorth_Simple(caller, direction, _params, out x, out y);
            if (node == null)
                node = new UltrasonicArrayNode(this.ParentMethod, x, y);
            else
                node.SetPos(x, y);

            node.SetParameterValues(x, y, false);


            this.nodeAttachments[DirectionalButton.FromDirection(direction)] = node;
            node.nodeAttachmentsReversed[DirectionalButton.FromDirection(DirectionalButton.GetOppositeDirection(direction))] = this;
        }

        private void CreatePointWest_Ultrasonic(DirectionalButton caller, DirectionalButton.Direction direction, BaseNode node, params object[] _params)
        {
            float x = 0, y = 0;
            var ifElseNode = (bool)_params[2];

            CreatePointWest_Simple(caller, direction, _params, out x, out y);

            if (node == null)
                node = new UltrasonicArrayNode(this.ParentMethod, x, y);
            else
                node.SetPos(x, y);

            if (ifElseNode)
            {
                DisableButton(node, DirectionalButton.Direction.East, DirectionalButton.Direction.South, DirectionalButton.Direction.West);
                node.SetParameterValues(x, y, true);
            }
            else
            {
                DisableButton(node, DirectionalButton.Direction.East);
                node.SetParameterValues(x, y, false);
            }


            this.nodeAttachments[DirectionalButton.FromDirection(direction)] = node;
            node.nodeAttachmentsReversed[DirectionalButton.FromDirection(DirectionalButton.GetOppositeDirection(direction))] = this;
        }

        private void CreatePointSouth_Ultrasonic(DirectionalButton caller, DirectionalButton.Direction direction, BaseNode node, params object[] _params)
        {
            float x = 0, y = 0;

            CreatePointSouth_Simple(caller, direction, _params, out x, out y);

            if (node == null)
                node = new UltrasonicArrayNode(this.ParentMethod, x, y);
            else
                node.SetPos(x, y);

            node.SetParameterValues(x, y, false);


            this.nodeAttachments[DirectionalButton.FromDirection(direction)] = node;
            node.nodeAttachmentsReversed[DirectionalButton.FromDirection(DirectionalButton.GetOppositeDirection(direction))] = this;
        }

        private void CreatePointEast_Ultrasonic(DirectionalButton caller, DirectionalButton.Direction direction, BaseNode node, params object[] _params)
        {
            float x = 0, y = 0;
            var ifElseNode = (bool)_params[2];

            CreatePointEast_Simple(caller, direction, _params, out x, out y);

            if (node == null)
                node = new UltrasonicArrayNode(this.ParentMethod, x, y);
            else
                node.SetPos(x, y);

            if (ifElseNode)
            {
                DisableButton(node, DirectionalButton.Direction.South, DirectionalButton.Direction.West);
                node.SetParameterValues(x, y, true);
            }
            else
            {
                DisableButton(node, DirectionalButton.Direction.West);
                node.SetParameterValues(x, y, false);
            }


            this.nodeAttachments[DirectionalButton.FromDirection(direction)] = node;
            node.nodeAttachmentsReversed[DirectionalButton.FromDirection(DirectionalButton.GetOppositeDirection(direction))] = this;
        }
        #endregion

        #region CREATE LIGHT METHOD
        private void CreatePointNorth_Light(DirectionalButton caller, DirectionalButton.Direction direction, BaseNode node, params object[] _params)
        {
            float x = 0, y = 0;

            CreatePointNorth_Simple(caller, direction, _params, out x, out y);
            if (node == null)
                node = new LightSensorNode(this.ParentMethod, x, y);
            else
                node.SetPos(x, y);

            node.SetParameterValues(x, y, false);

            this.nodeAttachments[DirectionalButton.FromDirection(direction)] = node;
            node.nodeAttachmentsReversed[DirectionalButton.FromDirection(DirectionalButton.GetOppositeDirection(direction))] = this;
        }

        private void CreatePointWest_Light(DirectionalButton caller, DirectionalButton.Direction direction, BaseNode node, params object[] _params)
        {
            float x = 0, y = 0;
            var ifElseNode = (bool)_params[2];

            CreatePointWest_Simple(caller, direction, _params, out x, out y);

            if (node == null)
                node = new LightSensorNode(this.ParentMethod, x, y);
            else
                node.SetPos(x, y);

            if (ifElseNode)
            {
                DisableButton(node, DirectionalButton.Direction.East, DirectionalButton.Direction.South, DirectionalButton.Direction.West);
                node.SetParameterValues(x, y, true);
            }
            else
            {
                DisableButton(node, DirectionalButton.Direction.East);
                node.SetParameterValues(x, y, false);
            }

            this.nodeAttachments[DirectionalButton.FromDirection(direction)] = node;
            node.nodeAttachmentsReversed[DirectionalButton.FromDirection(DirectionalButton.GetOppositeDirection(direction))] = this;
        }

        private void CreatePointSouth_Light(DirectionalButton caller, DirectionalButton.Direction direction, BaseNode node, params object[] _params)
        {
            float x = 0, y = 0;

            CreatePointSouth_Simple(caller, direction, _params, out x, out y);

            if (node == null)
                node = new LightSensorNode(this.ParentMethod, x, y);
            else
                node.SetPos(x, y);

            node.SetParameterValues(x, y, false);

            this.nodeAttachments[DirectionalButton.FromDirection(direction)] = node;
            node.nodeAttachmentsReversed[DirectionalButton.FromDirection(DirectionalButton.GetOppositeDirection(direction))] = this;
        }

        private void CreatePointEast_Light(DirectionalButton caller, DirectionalButton.Direction direction, BaseNode node, params object[] _params)
        {
            float x = 0, y = 0;
            var ifElseNode = (bool)_params[2];

            CreatePointEast_Simple(caller, direction, _params, out x, out y);

            if (node == null)
                node = new LightSensorNode(this.ParentMethod, x, y);
            else
                node.SetPos(x, y);

            if (ifElseNode)
            {
                DisableButton(node, DirectionalButton.Direction.South, DirectionalButton.Direction.West);
                node.SetParameterValues(x, y, true);
            }
            else
            {
                DisableButton(node, DirectionalButton.Direction.West);
                node.SetParameterValues(x, y, false);
            }

            this.nodeAttachments[DirectionalButton.FromDirection(direction)] = node;
            node.nodeAttachmentsReversed[DirectionalButton.FromDirection(DirectionalButton.GetOppositeDirection(direction))] = this;
        }
        #endregion

        #region SET CREATE PARAMETERS
        public void SetParameterValues(float xSize, float ySize, bool IfElseNode)
        {
            if (HasSouth)
            {
                var buttonSouth = this.buttonsDir[DirectionalButton.FromDirection(DirectionalButton.Direction.South)];
                buttonSouth.SetParameters(xSize, ySize, IfElseNode);
            }
            if (HasEast)
            {
                var buttonEast = this.buttonsDir[DirectionalButton.FromDirection(DirectionalButton.Direction.East)];
                buttonEast.SetParameters(xSize, ySize, IfElseNode);
            }
            if (HasWest)
            {
                var buttonWest = this.buttonsDir[DirectionalButton.FromDirection(DirectionalButton.Direction.West)];
                buttonWest.SetParameters(xSize, ySize, IfElseNode);
            }
        }
        #endregion

        #region CREATE POINTS SIMPLE
        private void CreatePointNorth_Simple(DirectionalButton caller, DirectionalButton.Direction direction, object[] _params, out float x, out float y)
        {
            var boundsOut2 = new Rect(Bounds);
            boundsOut2.width = boundsOut2.height = BaseSocket.NOB_SIZE;
            boundsOut2.x -= boundsOut2.xMax - Bounds.width / 2;
            boundsOut2.y -= boundsOut2.yMax;

#if true
            socketsNorth.Add(new SocketOutput(this, boundsOut2.center));
#endif
            if (caller != null)
            {
                caller.IsEnabled = false;
                caller.IsVisible = false;
            }

            var xSize = (float)_params[0];
            var ySize = (float)_params[1];
            x = xSize - NODE_WIDTH + 25;
            y = ySize;

            RemoveNodeButtons();
        }

        private void CreatePointEast_Simple(DirectionalButton caller, DirectionalButton.Direction direction, object[] _params, out float x, out float y)
        {
            var boundsOut = new Rect(Bounds);
            boundsOut.width = boundsOut.height = BaseSocket.NOB_SIZE;
            boundsOut.x -= boundsOut.xMax - Bounds.width;
            boundsOut.y -= boundsOut.yMax - Bounds.height / 2;
#if true
            socketsEast.Add(new SocketInput(this, boundsOut.center));
            //this.ParentMethod.AddConnectionLine(new Line(this, this.Bounds.center, Vector2.left, 45, 2, Color.red));
#endif

            if (caller != null)
            {
                caller.IsEnabled = false;
                caller.IsVisible = false;
            }

            var xSize = (float)_params[0];
            var ySize = (float)_params[1];
            x = xSize + NODE_WIDTH + 25;
            y = ySize;

            RemoveNodeButtons();
        }

        private void CreatePointSouth_Simple(DirectionalButton caller, DirectionalButton.Direction direction, object[] _params, out float x, out float y)
        {
            var boundsOut3 = new Rect(Bounds);
            boundsOut3.width = boundsOut3.height = BaseSocket.NOB_SIZE;
            boundsOut3.x -= boundsOut3.xMax - Bounds.width / 2;
            boundsOut3.y -= boundsOut3.yMax - Bounds.height;

#if true
            socketsSouth.Add(new SocketOutput(this, boundsOut3.center));
#endif

            if (caller != null)
            {
                caller.IsEnabled = false;
                caller.IsVisible = false;
            }

            var xSize = (float)_params[0];
            var ySize = (float)_params[1];
            x = xSize;
            y = ySize + NODE_HEIGHT + 25;

            RemoveNodeButtons();
        }
        private void CreatePointWest_Simple(DirectionalButton caller, DirectionalButton.Direction direction, object[] _params, out float x, out float y)
        {
            var boundsIn = new Rect(Bounds);
            boundsIn.width = boundsIn.height = BaseSocket.NOB_SIZE;
            boundsIn.x -= boundsIn.xMax;
            boundsIn.y -= boundsIn.yMax - Bounds.height / 2;
#if true
            socketsWest.Add(new SocketInput(this, boundsIn.center));
#endif
            if (caller != null)
            {
                caller.IsEnabled = false;
                caller.IsVisible = false;
            }

            var xSize = (float)_params[0];
            var ySize = (float)_params[1];
            x = xSize - NODE_WIDTH - 25;
            y = ySize;

            RemoveNodeButtons();
        }
        #endregion

        #region CREATE POINTS VARIABLES
        private void CreatePointNorth_Variable(DirectionalButton caller, DirectionalButton.Direction direction, BaseNode node, params object[] _params)
        {
            var xSize = (float)_params[0];
            var ySize = (float)_params[1];
            float x = xSize;
            float y = ySize;
            CreatePointNorth_Simple(caller, direction, _params, out x, out y);

            if (node == null)
                node = new NumericNode(this.ParentMethod, x, y);
            else
                node.SetPos(x, y);

            node.SetParameterValues(x, y, false);


            this.nodeAttachments[DirectionalButton.FromDirection(direction)] = node;
            node.nodeAttachmentsReversed[DirectionalButton.FromDirection(DirectionalButton.GetOppositeDirection(direction))] = this;
        }

        private void CreatePointEast_Variable(DirectionalButton caller, DirectionalButton.Direction direction, BaseNode node, params object[] _params)
        {
            var xSize = (float)_params[0];
            var ySize = (float)_params[1];
            float x = xSize;
            float y = ySize;
            var ifElseNode = (bool)_params[2];

            CreatePointEast_Simple(caller, direction, _params, out x, out y);

            if (node == null)
                node = new NumericNode(this.ParentMethod, x, y);
            else
                node.SetPos(x, y);

            if (ifElseNode)
            {
                DisableButton(node, DirectionalButton.Direction.South, DirectionalButton.Direction.West);
                node.SetParameterValues(x, y, true);
            }
            else
            {
                DisableButton(node, DirectionalButton.Direction.West);
                node.SetParameterValues(x, y, false);
            }


            this.nodeAttachments[DirectionalButton.FromDirection(direction)] = node;
            node.nodeAttachmentsReversed[DirectionalButton.FromDirection(DirectionalButton.GetOppositeDirection(direction))] = this;
        }

        private void CreatePointSouth_Variable(DirectionalButton caller, DirectionalButton.Direction direction, BaseNode node, params object[] _params)
        {
            var xSize = (float)_params[0];
            var ySize = (float)_params[1];
            float x = xSize;
            float y = ySize;

            CreatePointSouth_Simple(caller, direction, _params, out x, out y);

            if (node == null)
                node = new NumericNode(this.ParentMethod, x, y);
            else
                node.SetPos(x, y);

            node.SetParameterValues(x, y, false);


            this.nodeAttachments[DirectionalButton.FromDirection(direction)] = node;
            node.nodeAttachmentsReversed[DirectionalButton.FromDirection(DirectionalButton.GetOppositeDirection(direction))] = this;
        }


        private void CreatePointWest_Variable(DirectionalButton caller, DirectionalButton.Direction direction, BaseNode node, params object[] _params)
        {
            var xSize = (float)_params[0];
            var ySize = (float)_params[1];
            float x = xSize;
            float y = ySize;
            var ifElseNode = (bool)_params[2];

            CreatePointWest_Simple(caller, direction, _params, out x, out y);

            if (node == null)
                node = new NumericNode(this.ParentMethod, x, y);
            else
                node.SetPos(x, y);

            if (ifElseNode)
            {
                DisableButton(node, DirectionalButton.Direction.East, DirectionalButton.Direction.South, DirectionalButton.Direction.West);
                node.SetParameterValues(x, y, true);
            }
            else
            {
                DisableButton(node, DirectionalButton.Direction.East);
                node.SetParameterValues(x, y, false);
            }


            this.nodeAttachments[DirectionalButton.FromDirection(direction)] = node;
            node.nodeAttachmentsReversed[DirectionalButton.FromDirection(DirectionalButton.GetOppositeDirection(direction))] = this;
        }
        #endregion

        #region CREATE POINTS METHOD
        private void CreatePointNorth_Method(DirectionalButton caller, DirectionalButton.Direction direction, BaseNode node, params object[] _params)
        {
            float x = 0, y = 0;
            CreatePointNorth_Simple(caller, direction, _params, out x, out y);

            if (node == null)
                node = new MethodNode(this.ParentMethod, x, y);
            else
                node.SetPos(x, y);

            node.SetParameterValues(x, y, false);

            this.nodeAttachments[DirectionalButton.FromDirection(direction)] = node;
            node.nodeAttachmentsReversed[DirectionalButton.FromDirection(DirectionalButton.GetOppositeDirection(direction))] = this;
        }

        private void CreatePointWest_Method(DirectionalButton caller, DirectionalButton.Direction direction, BaseNode node, params object[] _params)
        {
            float x = 0, y = 0;
            var ifElseNode = (bool)_params[2];

            CreatePointWest_Simple(caller, direction, _params, out x, out y);

            if (node == null)
                node = new MethodNode(this.ParentMethod, x, y);
            else
                node.SetPos(x, y);

            if (ifElseNode)
            {
                DisableButton(node, DirectionalButton.Direction.East, DirectionalButton.Direction.South, DirectionalButton.Direction.West);
                node.SetParameterValues(x, y, true);
            }
            else
            {
                DisableButton(node, DirectionalButton.Direction.East);
                node.SetParameterValues(x, y, false);
            }


            this.nodeAttachments[DirectionalButton.FromDirection(direction)] = node;
            node.nodeAttachmentsReversed[DirectionalButton.FromDirection(DirectionalButton.GetOppositeDirection(direction))] = this;
        }

        private void CreatePointSouth_Method(DirectionalButton caller, DirectionalButton.Direction direction, BaseNode node, params object[] _params)
        {
            float x = 0, y = 0;

            CreatePointSouth_Simple(caller, direction, _params, out x, out y);

            if (node == null)
                node = new MethodNode(this.ParentMethod, x, y);
            else
                node.SetPos(x, y);

            node.SetParameterValues(x, y, false);


            this.nodeAttachments[DirectionalButton.FromDirection(direction)] = node;
            node.nodeAttachmentsReversed[DirectionalButton.FromDirection(DirectionalButton.GetOppositeDirection(direction))] = this;
        }

        private void CreatePointEast_Method(DirectionalButton caller, DirectionalButton.Direction direction, BaseNode node, params object[] _params)
        {
            float x = 0, y = 0;
            var ifElseNode = (bool)_params[2];

            CreatePointEast_Simple(caller, direction, _params, out x, out y);

            if (node == null)
                node = new MethodNode(this.ParentMethod, x, y);
            else
                node.SetPos(x, y);

            if (ifElseNode)
            {
                DisableButton(node, DirectionalButton.Direction.South, DirectionalButton.Direction.West);
                node.SetParameterValues(x, y, true);
            }
            else
            {
                DisableButton(node, DirectionalButton.Direction.West);
                node.SetParameterValues(x, y, false);
            }


            this.nodeAttachments[DirectionalButton.FromDirection(direction)] = node;
            node.nodeAttachmentsReversed[DirectionalButton.FromDirection(DirectionalButton.GetOppositeDirection(direction))] = this;
        }
        #endregion

        #region CREATE POINTS IF/ELSE

        public void CreatePointSouth_IfElse(DirectionalButton caller, DirectionalButton.Direction direction, BaseNode node, params object[] _params)
        {
            var boundsOut3 = new Rect(Bounds);
            boundsOut3.width = boundsOut3.height = BaseSocket.NOB_SIZE;
            boundsOut3.x -= boundsOut3.xMax - Bounds.width / 2;
            boundsOut3.y -= boundsOut3.yMax - Bounds.height;

#if true
            socketsSouth.Add(new SocketOutput(this, boundsOut3.center));
#endif

            if (caller != null)
            {
                caller.IsEnabled = false;
                caller.IsVisible = false;
            }

            var xSize = (float)_params[0];
            var ySize = (float)_params[1];
            float x = xSize;
            float y = ySize + 125;

            if (node == null)
                node = new IfNode(this.ParentMethod, x, y);
            else
                node.SetPos(x, y);

            node.SetParameterValues(x, y, true);

#if true
            socketsSouth.Add(new SocketOutput(node, boundsOut3.center));
#endif

            y = y + 125;

            ElseNode elseNode = null;

            if ((node as IfNode).ElseNodeHack == null)
                elseNode = new ElseNode(this.ParentMethod, x, y);
            else
            {
                var ifNode = node as IfNode;
                elseNode = ifNode.ElseNodeHack;
                ifNode.ElseNodeHack = null;
                elseNode.SetPos(x, y);
            }
            elseNode.SetParameterValues(x, y, true);

            RemoveNodeButtons();


            this.nodeAttachments[DirectionalButton.FromDirection(direction)] = node;
            node.nodeAttachmentsReversed[DirectionalButton.FromDirection(DirectionalButton.GetOppositeDirection(direction))] = this;
            node.nodeAttachments[DirectionalButton.FromDirection(DirectionalButton.Direction.South)] = elseNode;

            yNodes++;
        }
        #endregion

        #region CREATE POINTS OPERATION
        public void CreatePointSouth_Operation(DirectionalButton caller, DirectionalButton.Direction direction, BaseNode node, params object[] _params)
        {
            var boundsOut3 = new Rect(Bounds);
            boundsOut3.width = boundsOut3.height = BaseSocket.NOB_SIZE;
            boundsOut3.x -= boundsOut3.xMax - Bounds.width / 2;
            boundsOut3.y -= boundsOut3.yMax - Bounds.height;

#if true
            socketsSouth.Add(new SocketOutput(this, boundsOut3.center));
#endif

            if (caller != null)
            {
                caller.IsEnabled = false;
                caller.IsVisible = false;
            }

            var xSize = (float)_params[0];
            var ySize = (float)_params[1];
            float x = xSize;
            float y = ySize + 125;

            if (node == null)
                node = new OperatorNode(this.ParentMethod, x, y);
            else
                node.SetPos(x, y);

            node.SetParameterValues(x, y, true);

            RemoveNodeButtons();


            this.nodeAttachments[DirectionalButton.FromDirection(direction)] = node;
            node.nodeAttachmentsReversed[DirectionalButton.FromDirection(DirectionalButton.GetOppositeDirection(direction))] = this;
        }
        #endregion

        public BaseNode GetNode(DirectionalButton.Direction dir)
        {
            return nodeAttachments[DirectionalButton.FromDirection(dir)];
        }

        public BaseNode GetReverseNode(DirectionalButton.Direction dir)
        {
            return nodeAttachmentsReversed[DirectionalButton.FromDirection(dir)];
        }

        public abstract SyntaxChain BuildChain();

        public int calculateX()
        {
            float x = 0;
            foreach (var children in ParentMethod.View.Children)
            {
                if (children.Bounds.x > x)
                {
                    x = children.Bounds.x + 200;
                }
            }

            return (int)Math.Round(x / 180);
        }

        public int calculateY()
        {
            float y = 0;
            foreach (var children in ParentMethod.View.Children)
            {
                if (children.Bounds.y > y)
                {
                    y = children.Bounds.y + 100;
                }
            }

            return (int)Math.Round(y / 80);
        }

        #region SERIALIZATION
        public abstract string NodeType { get; }

        protected static XmlElement ElementFromNode(BaseNode node, XmlDocument xdoc, XmlElement xnodes, XmlElement xchain)
        {
            var xel = xdoc.CreateElement("Node");
            xel.SetAttribute("ID", node.UniqueId);
            xel.SetAttribute("Scene", node.ParentMethod.UniqueId);
            //xel.SetAttribute("Parent", node.ParentMethod.EntryNode.UniqueId);
            xel.SetAttribute("Type", node.NodeType);

            xnodes.AppendChild(xel);

            var xattachments = xdoc.CreateElement("Attachments");
            //xattachments.SetAttribute("ID", node.UniqueId);

            for (int i = 0; i < node.nodeAttachments.Length; i++)
            {
                var attached = node.nodeAttachments[i];
                if (attached != null)
                {
                    attached.Serialize(xdoc, xnodes, xchain);
                    var nodeId = node.UniqueId;

                    var xseq = xdoc.CreateElement("Attached");
                    xseq.SetAttribute("Direction", DirectionalButton.ToDirection(i).ToString());
                    xseq.InnerText = attached.UniqueId;
                    xattachments.AppendChild(xseq);
                }
            }

            xel.AppendChild(xattachments);

            //xchain.AppendChild(xattachments);
            return xel;
        }

        public virtual XmlElement Serialize(XmlDocument xdoc, XmlElement xnodes, XmlElement xchain)
        {
            return ElementFromNode(this, xdoc, xnodes, xchain);
        }


        #endregion
    }
}
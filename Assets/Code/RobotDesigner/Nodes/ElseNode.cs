﻿using UnityEngine;
using UI;
using UI.Components;
using DesignerGlue.Chain;
using DesignerGlue.Chain.Types;
using UI.Components.Input;
using System.Xml;

namespace RobotDesigner.Nodes
{
    /// <summary>
    /// Else node.
    /// </summary>
    public class ElseNode : BaseNode
    {
        protected Label labelName;

        /// <summary>
        /// Returns node type.
        /// </summary>
        public override string NodeType { get { return "ElseStatement"; } }

        /// <summary>
        /// Registers UI component.
        /// </summary>
        public static void RegisterUIElement() { UIBuilder.AddTypeBind(typeof(ElseNode)); }

        protected override void OnDraw()
        {
            GUI.Box(Bounds, GUIContent.none, boxIfElseStyle.Style);
            DrawN();
            DrawE();
            DrawS();
            DrawW();
        }

        /// <summary>
        /// Constructor for node.
        /// </summary>
        /// <param name="method">Method to which node is attached.</param>
        public ElseNode(MethodScene method) : this(method, 0, 0) { }

        public ElseNode(MethodScene method, XmlElement xel) : this(method, 0, 0)
        {
        }

        /// <summary>
        /// Supports east attachments.
        /// </summary>
        public override bool HasWest { get { return false; } }

        /// <summary>
        /// Constructor for node.
        /// </summary>
        /// <param name="method">Method to which node is attached.</param>
        /// <param name="x">X position of the node.</param>
        /// <param name="y">Y position of the node.</param>
        /// <param name="name">Name of the node.</param>
        public ElseNode(MethodScene method, float x, float y, string name = null)
                : base(method, x, y, name)
        {
            labelName = new Label(14, 12, 150, 20, "Else:", true, null);

            this.AddChildren(labelName);

            labelName.Style.fontStyle = FontStyle.BoldAndItalic;

            buttonDelete.IsVisible = false;
        }

        /// <summary>
        /// Builds syntax chain.
        /// </summary>
        /// <returns>Syntax chain.</returns>
        public override SyntaxChain BuildChain()
        {
            var leaf = new IfOtherwiseLink(this);

            var eastNode = GetNode(DirectionalButton.Direction.East);
            if (eastNode != null)
                leaf.InternalChain = eastNode.BuildChain();

            var southNode = GetNode(DirectionalButton.Direction.South);
            if (southNode != null)
                leaf.Next = southNode.BuildChain();

            return leaf;
        }
    }
}
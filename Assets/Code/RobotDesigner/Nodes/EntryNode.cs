﻿using UI.Components;
using DesignerGlue.Chain;
using UI.Components.Input;
using UnityEngine;
using System;
using System.Xml;
using System.Collections.Generic;

namespace RobotDesigner.Nodes
{
    /// <summary>
    /// Method entry node.
    /// </summary>
    public class EntryNode : BaseNode
    {
        protected Label labelName;

        //protected Button buttonDelete;

        protected override void OnDraw()
        {
            GUI.Box(Bounds, GUIContent.none, boxEntryStyle.Style);
            DrawN();
            DrawE();
            DrawS();
            DrawW();
        }

        /// <summary>
        /// Supports south attachments.
        /// </summary>
        public override bool HasWest { get { return false; } }

        /// <summary>
        /// Supports east attachments.
        /// </summary>
        public override bool HasSouth { get { return false; } }

        /// <summary>
        /// Returns node type.
        /// </summary>
        public override string NodeType { get { return "Entry"; } }

        /// <summary>
        /// Constructor for node.
        /// </summary>
        /// <param name="method">Method to which node is attached.</param>
        /// <param name="x">X position of the node.</param>
        /// <param name="y">Y position of the node.</param>
        /// <param name="name">Name of the node.</param>
        public EntryNode(MethodScene method, float x, float y, string name = null) : base(method, x, y, name)
        {
            labelName = new Label(14, 12, 150, 22, "Entry", true, null);
            AddChild(labelName);

            buttonDelete.IsVisible = false;
        }

        /// <summary>
        /// Builds syntax chain.
        /// </summary>
        /// <returns>Syntax chain.</returns>
        public override SyntaxChain BuildChain()
        {
            var main = new SyntaxChain();

            var eastNode = GetNode(DirectionalButton.Direction.East);

            if (eastNode != null)
                main.Next = eastNode.BuildChain();
            else
            {
                var southNode = GetNode(DirectionalButton.Direction.South);
                if (southNode != null)
                    main.Next = southNode.BuildChain();
            }

            return main;
        }

        public override XmlElement Serialize(XmlDocument xdoc, XmlElement xnodes, XmlElement xchain)
        {
            var node = base.Serialize(xdoc, xnodes, xchain);
            var parent = this.ParentMethod.Parent;

            if (parent != null)
                node.SetAttribute("Parent", parent.UniqueId);
            else
            {
                node.RemoveAttribute("Parent");
                node.SetAttribute("IsMain", true.ToString());
            }

            return node;
        }
    }
}

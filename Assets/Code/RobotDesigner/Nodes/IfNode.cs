﻿using UnityEngine;
using UI;
using UI.Components;
using DesignerGlue.Chain;
using DesignerGlue.Chain.Types;
using UI.Components.Input;
using System.Xml;
using System;

namespace RobotDesigner.Nodes
{
    /// <summary>
    /// If node.
    /// </summary>
    public class IfNode : BaseNode
    {
        /// <summary>
        /// Registers UI element.
        /// </summary>
        public static void RegisterUIElement() { UIBuilder.AddTypeBind(typeof(IfNode)); }

        /// <summary>
        /// Returns node type.
        /// </summary>
        public override string NodeType { get { return "IfStatement"; } }

        protected override void OnDraw()
        {
            GUI.Box(Bounds, GUIContent.none, boxIfElseStyle.Style);
            DrawN();
            DrawE();
            DrawS();
            DrawW();
        }

        /// <summary>
        /// Available comparators.
        /// </summary>
        public enum Comparators_T
        {
            EQ = 0,
            NEQ,
            GT,
            GTE,
            LT,
            LTE,
            TOTAL_SIGNS
        }

        protected Label labelName;

        private Button operatorButton;
        private Comparators_T comparator;


        /// <summary>
        /// Supports south attachments.
        /// </summary>
        public override bool HasSouth { get { return false; } }

        /// <summary>
        /// Getter and setter for comparator.
        /// </summary>
        public Comparators_T Comparator
        {
            get
            {
                return comparator;
            }
            set
            {
                comparator = value;
                UpdateText();
            }
        }

        public IfNode(MethodScene method, XmlElement xel) : this(method, 0, 0)
        {
            this.comparator = (Comparators_T)Enum.Parse(typeof(Comparators_T), xel.GetAttribute("Comparator"));
            UpdateText();
        }

        /// <summary>
        /// Attached else node.
        /// </summary>
        public ElseNode ElseNode
        {
            get
            {
                return GetNode(DirectionalButton.Direction.South) as ElseNode;
            }
        }

        public ElseNode ElseNodeHack { get; set; }

        /// <summary>
        /// Constructor for node.
        /// </summary>
        /// <param name="method">Method to which node is attached.</param>
        public IfNode(MethodScene method) : this(method, 0, 0) { }

        /// <summary>
        /// Constructor for node.
        /// </summary>
        /// <param name="method">Method to which node is attached.</param>
        /// <param name="x">X position of the node.</param>
        /// <param name="y">Y position of the node.</param>
        /// <param name="name">Name of the node.</param>
        public IfNode(MethodScene method, float x, float y, string name = null) : base(method, x, y, name)
        {
            labelName = new Label(14, 12, 150, 20, "If:", true);

            this.AddChild(labelName);

            labelName.Style.fontStyle = FontStyle.BoldAndItalic;

            operatorButton = new Button(90, 35, 25, 25, "=", true);
            operatorButton.IsEnabled = true;
            this.AddChild(operatorButton);
            operatorButton.OnCommand += HandleIncrementComparator;
        }

        /// <summary>
        /// Increments comparator.
        /// </summary>
        public void IncrementComparator()
        {
            Comparator++;

            if (Comparator >= Comparators_T.TOTAL_SIGNS)
                Comparator = Comparators_T.EQ;

            UpdateText();
        }

        /// <summary>
        /// Handles increment comparator click event.
        /// </summary>
        /// <param name="caller">Button caller.</param>
        /// <param name="params">Call parameters.</param>
        private void HandleIncrementComparator(BaseButton caller, object[] @params)
        {
            IncrementComparator();
        }

        /// <summary>
        /// Updates text.
        /// </summary>
        private void UpdateText()
        {
            switch (Comparator)
            {
                case Comparators_T.GT:
                    operatorButton.Text = ">";
                    break;
                case Comparators_T.GTE:
                    operatorButton.Text = ">=";
                    break;
                case Comparators_T.LT:
                    operatorButton.Text = "<";
                    break;
                case Comparators_T.LTE:
                    operatorButton.Text = "<=";
                    break;
                case Comparators_T.NEQ:
                    operatorButton.Text = "!=";
                    break;
                case Comparators_T.EQ:
                    operatorButton.Text = "=";
                    break;
                default:
                    Comparator = Comparators_T.EQ;
                    break;
            }
        }

        /// <summary>
        /// Builds syntax chain.
        /// </summary>
        /// <returns>Syntax chain.</returns>
        public override SyntaxChain BuildChain()
        {
            var leaf = new IfSuccessLink(this);

            var westNode = GetNode(DirectionalButton.Direction.West);

            if (westNode != null)
                leaf.ExtraLink = westNode.BuildChain();

            var eastNode = GetNode(DirectionalButton.Direction.East);
            if (eastNode != null)
                leaf.InternalChain = eastNode.BuildChain();

            var southNode = GetNode(DirectionalButton.Direction.South);
            if (southNode != null)
                leaf.Next = southNode.BuildChain();

            return leaf;
        }

        public override XmlElement Serialize(XmlDocument xdoc, XmlElement xnodes, XmlElement xchain)
        {
            var node = base.Serialize(xdoc, xnodes, xchain);

            node.SetAttribute("Comparator", comparator.ToString());
            node.SetAttribute("ElseNode", ElseNode.UniqueId);

            return node;
        }
    }
}

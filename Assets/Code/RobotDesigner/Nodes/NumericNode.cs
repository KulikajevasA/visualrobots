﻿using UnityEngine;
using UI;
using UI.Components;
using DesignerGlue.Chain;
using DesignerGlue.Chain.Types;
using UI.Components.Input;
using System;
using System.Xml;

namespace RobotDesigner.Nodes
{
    /// <summary>
    /// Numeric node.
    /// </summary>
    public class NumericNode : BaseNode, IValueNode
    {
        private bool isGlobal;
        private AssignValueNode_T keepValue;
        private TypeValueNode_T typeValue;
        private ColorValue_T colorValue;

        /// <summary>
        /// Registers UI component.
        /// </summary>
        public static void RegisterUIElement() { UIBuilder.AddTypeBind(typeof(NumericNode)); }

        /// <summary>
        /// Returns node type.
        /// </summary>
        public override string NodeType
        {
            get
            {
                return typeValue == TypeValueNode_T.COLOR ? "Color" : "Numeric";
            }
        }

        /// <summary>
        /// Supports west attachments.
        /// </summary>
        public override bool HasWest { get { return false; } }

        protected override void OnDraw()
        {
            GUI.Box(Bounds, GUIContent.none, boxNumericStyle.Style);
            DrawN();
            DrawE();
            DrawS();
            DrawW();
        }

        protected Label labelName;
        protected VariableNameField fieldName;

        protected Label labelNumber;
        protected NumericInputField fieldNumber;

        protected Button fieldColor;

        protected Button buttonIsGlobal;
        protected Button buttonIsAssignment;

        protected Button buttonType;

        /// <summary>
        /// Gets variable name.
        /// </summary>
        public string VariableName
        {
            get
            {
                return fieldName != null ? fieldName.Value : string.Empty;
            }
        }

        public TypeValueNode_T ValueType
        {
            get { return typeValue; }
            set
            {
                typeValue = value;
                //UpdateVariableColorValue();
                UpdateVariableTypeValue();
            }
        }

        public ColorValue_T ColorValue
        {
            get { return colorValue; }
            set
            {
                colorValue = value;
                UpdateVariableColorValue();
            }
        }

        /// <summary>
        /// Gets variable value.
        /// </summary>
        public double Value
        {
            get
            {
                if (typeValue == TypeValueNode_T.COLOR)
                {
                    switch (colorValue)
                    {
                        case ColorValue_T.BLACK:
                            return Simulation.Parts.Sensors.ColorSensor.BLACK;
                        case ColorValue_T.BLUE:
                            return Simulation.Parts.Sensors.ColorSensor.BLUE;
                        case ColorValue_T.GREEN:
                            return Simulation.Parts.Sensors.ColorSensor.GREEN;
                        case ColorValue_T.RED:
                            return Simulation.Parts.Sensors.ColorSensor.RED;
                        case ColorValue_T.YELLOW:
                            return Simulation.Parts.Sensors.ColorSensor.YELLOW;
                        case ColorValue_T.MAGENTA:
                            return Simulation.Parts.Sensors.ColorSensor.MAGENTA;
                        case ColorValue_T.CYAN:
                            return Simulation.Parts.Sensors.ColorSensor.CYAN;
                        case ColorValue_T.WHITE:
                            return Simulation.Parts.Sensors.ColorSensor.WHITE;
                    }
                }

                return fieldNumber.Value;
            }
        }

        /// <summary>
        /// Assign mode.
        /// </summary>
        public enum AssignValueNode_T
        {
            ASSIGN_NODE = 0,
            ASSIGN_INPUT,
            ASSIGN_NONE,
            TOTAL_VALUES
        }

        public enum TypeValueNode_T
        {
            NUMERIC = 0,
            COLOR,
            TOTAL_VALUES
        }

        public enum ColorValue_T
        {
            BLACK = 0,
            RED,
            GREEN,
            BLUE,
            YELLOW,
            CYAN,
            MAGENTA,
            WHITE,
            TOTAL_VALUES
        }

        /// <summary>
        /// Is variable gobal.
        /// </summary>
        public bool IsGlobal
        {
            get { return isGlobal; }
            set
            {
                isGlobal = value;
                UpdateGlobalValue();
            }
        }

        /// <summary>
        /// Assignment type.
        /// </summary>
        public AssignValueNode_T AssignMode
        {
            get
            {
                return keepValue;
            }
            set
            {
                keepValue = value;
                UpdateRetainValue();
            }
        }

        /// <summary>
        /// Constructor for numeric node.
        /// </summary>
        /// <param name="method">Parent method.</param>
        /// <param name="varName">Variable name.</param>
        /// <param name="defaultValue">Default variable value.</param>
        public NumericNode(MethodScene method, string varName = "var_name", double defaultValue = 0) : this(method, 0, 0)
        {
            fieldName.Value = varName;
            fieldNumber.Value = defaultValue;
        }

        public NumericNode(MethodScene method, XmlElement xel) : this(method, 0, 0)
        {
            fieldName.Value = xel.GetAttribute("Name");
            keepValue = (AssignValueNode_T)Enum.Parse(typeof(AssignValueNode_T), xel.GetAttribute("KeepValue"));
            isGlobal = bool.Parse(xel.GetAttribute("IsGlobal"));

            switch (xel.GetAttribute("Type"))
            {
                case "Numeric":
                    typeValue = TypeValueNode_T.NUMERIC;
                    fieldNumber.Value = double.Parse(xel.GetAttribute("Value"));
                    break;
                case "Color":
                    typeValue = TypeValueNode_T.COLOR;
                    colorValue = (ColorValue_T)Enum.Parse(typeof(ColorValue_T), xel.GetAttribute("Value"));
                    break;
            }

            UpdateGlobalValue();
            UpdateRetainValue();
            UpdateVariableColorValue();
            UpdateVariableTypeValue();
        }

        /// <summary>
        /// Constructor for numeric node.
        /// </summary>
        /// <param name="method">Parent method.</param>
        /// <param name="x">X position.</param>
        /// <param name="y">Y position.</param>
        /// <param name="name">Name of the element.</param>
        public NumericNode(MethodScene method, float x, float y, string name = null) : base(method, x, y, name)
        {
            labelName = new Label(19, 12, 40, 20, "Name:", true);
            fieldName = new VariableNameField(61, 12, 120, 22, "myvar1", true, null, this);

            labelNumber = new Label(19, 38, 40, 20, "Value:", true);
            fieldNumber = new NumericInputField(61, 38, 120, 22, "0", true, null, this);

            fieldColor = new Button(61, 38, 120, 22, "Black", true, null, this);

            buttonIsGlobal = new Button(26, 75, 25, 25, "L", true, null, this);
            buttonIsAssignment = new Button(0, 75, 25, 25, "N", true, null, this);

            buttonType = new Button(125, 75, 75, 25, "Numeric", true, null, this);

            this.AddChildren(labelName, fieldName, labelNumber, fieldNumber, fieldColor, buttonIsGlobal, buttonIsAssignment, buttonType);

            labelName.Style.fontStyle = FontStyle.Bold;
            labelNumber.Style.fontStyle = FontStyle.Bold;

            fieldColor.IsVisible = false;

            buttonIsGlobal.OnCommand += GlobalButton_OnCommand;
            buttonIsAssignment.OnCommand += RetainValue_OnCommand;
            buttonType.OnCommand += VariableType_OnCommand;
            fieldColor.OnCommand += VariableColor_OnCommand;

            fieldName.IsEnabled = fieldNumber.IsEnabled = buttonIsAssignment.IsEnabled = buttonIsGlobal.IsEnabled = buttonType.IsEnabled = true;
        }

        private void UpdateRetainValue()
        {
            switch (keepValue)
            {
                case AssignValueNode_T.ASSIGN_INPUT:
                    buttonIsAssignment.Text = "I";
                    fieldNumber.IsEnabled = false;
                    break;
                case AssignValueNode_T.ASSIGN_NODE:
                    buttonIsAssignment.Text = "N";
                    fieldNumber.IsEnabled = true;
                    break;
                case AssignValueNode_T.ASSIGN_NONE:
                    buttonIsAssignment.Text = "x";
                    fieldNumber.IsEnabled = false;
                    break;
            }
        }

        private void RetainValue_OnCommand(BaseButton caller, params object[] @params)
        {
            keepValue++;

            if (keepValue >= AssignValueNode_T.TOTAL_VALUES)
                keepValue = 0;

            UpdateRetainValue();
        }

        private void UpdateGlobalValue()
        {
            if (IsGlobal)
                buttonIsGlobal.Text = "G";
            else
                buttonIsGlobal.Text = "L";
        }

        private void GlobalButton_OnCommand(BaseButton caller, params object[] @params)
        {
            IsGlobal = !IsGlobal;

            UpdateGlobalValue();
        }

        private void UpdateVariableTypeValue()
        {
            switch (typeValue)
            {
                case TypeValueNode_T.NUMERIC:
                    buttonType.Text = "Numeric";
                    buttonIsAssignment.IsVisible = true;
                    buttonIsGlobal.IsVisible = true;
                    fieldNumber.IsVisible = true;
                    fieldColor.IsVisible = false;
                    fieldColor.IsEnabled = false;
                    break;
                case TypeValueNode_T.COLOR:
                    buttonType.Text = "Color";
                    buttonIsAssignment.IsVisible = false;
                    buttonIsGlobal.IsVisible = false;
                    fieldNumber.IsVisible = false;
                    fieldColor.IsVisible = true;
                    fieldColor.IsEnabled = true;
                    break;
            }
        }

        private void VariableType_OnCommand(BaseButton caller, params object[] @params)
        {
            typeValue++;

            if (typeValue >= TypeValueNode_T.TOTAL_VALUES)
                typeValue = 0;

            UpdateVariableTypeValue();
        }

        private void UpdateVariableColorValue()
        {
            switch (colorValue)
            {
                case ColorValue_T.BLACK:
                    fieldColor.Text = "Black";
                    break;
                case ColorValue_T.BLUE:
                    fieldColor.Text = "Blue";
                    break;
                case ColorValue_T.GREEN:
                    fieldColor.Text = "Green";
                    break;
                case ColorValue_T.RED:
                    fieldColor.Text = "Red";
                    break;
                case ColorValue_T.WHITE:
                    fieldColor.Text = "White";
                    break;
                case ColorValue_T.YELLOW:
                    fieldColor.Text = "Yellow";
                    break;
                case ColorValue_T.MAGENTA:
                    fieldColor.Text = "Magenta";
                    break;
                case ColorValue_T.CYAN:
                    fieldColor.Text = "Cyan";
                    break;
            }
        }

        private void VariableColor_OnCommand(BaseButton caller, params object[] @params)
        {
            colorValue++;

            if (colorValue >= ColorValue_T.TOTAL_VALUES)
                colorValue = 0;

            UpdateVariableColorValue();
        }

        public override SyntaxChain BuildChain()
        {
            var node = new NumericLink(this);

            var eastNode = GetNode(DirectionalButton.Direction.East);
            if (eastNode != null)
                node.Next = eastNode.BuildChain();
            else
            {
                var southNode = GetNode(DirectionalButton.Direction.South);
                if (southNode != null)
                    node.Next = southNode.BuildChain();
            }

            return node;
        }

        public override XmlElement Serialize(XmlDocument xdoc, XmlElement xnodes, XmlElement xchain)
        {
            var node = base.Serialize(xdoc, xnodes, xchain);

            node.SetAttribute("Name", VariableName);
            node.SetAttribute("IsGlobal", isGlobal.ToString());
            node.SetAttribute("KeepValue", keepValue.ToString());
            node.SetAttribute("Value", typeValue == TypeValueNode_T.COLOR ? colorValue.ToString() : Value.ToString());

            return node;
        }
    }
}
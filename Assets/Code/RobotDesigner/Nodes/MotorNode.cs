﻿using System.Xml;
using DesignerGlue.Chain;
using DesignerGlue.Chain.Types;
using UI.Components.Input;
using UnityEngine;
using System;

namespace RobotDesigner.Nodes
{
    /// <summary>
    /// Motor node.
    /// </summary>
    public class MotorNode : NumericNode
    {
        /// <summary>
        /// Available motors.
        /// </summary>
        public enum Motor_T
        {
            LEFT = 0,
            RIGHT,

            MAX_MOTORS
        }

        private Button directionButton;
        private Motor_T motor;

        /// <summary>
        /// Returns node type.
        /// </summary>
        public override string NodeType { get { return "Motor"; } }

        protected override void OnDraw()
        {
            GUI.Box(Bounds, GUIContent.none, boxMotorStyle.Style);
            DrawN();
            DrawE();
            DrawS();
            DrawW();
        }

        /// <summary>
        /// Getter and setter for motor.
        /// </summary>
        public Motor_T Motor
        {
            get
            {
                return motor;
            }
            set
            {
                motor = value;
                UpdateText();
            }
        }

        public MotorNode(MethodScene method, XmlElement xel) : this(method, 0, 0)
        {
            motor = (Motor_T)Enum.Parse(typeof(Motor_T), xel.GetAttribute("Motor"));
            fieldNumber.Value = double.Parse(xel.GetAttribute("Power"));

            UpdateText();
        }

        /// <summary>
        /// Constructor for motor node.
        /// </summary>
        /// <param name="method">Method to which node is attached.</param>
        /// <param name="defaultValue">Default motor power value.</param>
        public MotorNode(MethodScene method, double defaultValue = 0) : this(method, 0, 0)
        {
            fieldNumber.Value = defaultValue;
        }

        public MotorNode() : base(null)
        {

        }

        /// <summary>
        /// Constructor for motor node.
        /// </summary>
        /// <param name="method">Method to which node is attached.</param>
        /// <param name="x">X position.</param>
        /// <param name="y">Y position.</param>
        /// <param name="name">Element name.</param>
        public MotorNode(MethodScene method, float x, float y, string name = null) : base(method, x, y, name)
        {
            this.labelNumber.Text = "Power:";

            this.directionButton = new Button(70, 15, 55, 20, "LEFT", true);
            this.directionButton.IsEnabled = true;
            this.AddChild(this.directionButton);
            this.directionButton.OnCommand += HandleIncrementMotor;

            this.RemoveChildren(this.labelName, this.fieldName, this.buttonIsGlobal, this.buttonIsAssignment, this.buttonType);
            this.labelName = null;
            this.fieldName = null;
            this.buttonIsGlobal = this.buttonIsAssignment = null;
            this.buttonType = null;
        }

        /// <summary>
        /// Increments motor.
        /// </summary>
        public void IncrementMotor()
        {
            motor++;

            if (motor >= Motor_T.MAX_MOTORS)
                motor = Motor_T.LEFT;

            UpdateText();
        }

        /// <summary>
        /// Handles increment motor click event.
        /// </summary>
        /// <param name="caller">Button caller.</param>
        /// <param name="params">Call parameters.</param>
        private void HandleIncrementMotor(BaseButton caller, object[] @params)
        {
            IncrementMotor();
        }

        /// <summary>
        /// Updates motor text.
        /// </summary>
        private void UpdateText()
        {
            switch (motor)
            {
                case Motor_T.LEFT:
                    directionButton.Text = "LEFT";
                    break;
                case Motor_T.RIGHT:
                    directionButton.Text = "RIGHT";
                    break;
                default:
                    Motor = Motor_T.LEFT;
                    break;
            }
        }

        /// <summary>
        /// Builds syntax chain.
        /// </summary>
        /// <returns>Syntax chain link.</returns>
        public override SyntaxChain BuildChain()
        {
            var node = new MotorLink(this);
            var eastNode = GetNode(DirectionalButton.Direction.East);

            if (eastNode != null)
                node.Next = eastNode.BuildChain();
            else
            {
                var southNode = GetNode(DirectionalButton.Direction.South);
                if (southNode != null)
                    node.Next = southNode.BuildChain();
            }

            return node;
        }

        public override XmlElement Serialize(XmlDocument xdoc, XmlElement xnodes, XmlElement xchain)
        {
            var node = ElementFromNode(this, xdoc, xnodes, xchain);

            node.SetAttribute("Motor", this.motor.ToString());
            node.SetAttribute("Power", this.Value.ToString());

            return node;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DesignerGlue.Chain;
using DesignerGlue.Chain.Types;

namespace RobotDesigner.Nodes
{
    /// <summary>
    /// Interface for node can output a value.
    /// </summary>
    public interface IValueNode { }
}

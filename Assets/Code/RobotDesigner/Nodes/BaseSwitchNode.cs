﻿using UI.Components.Input;

namespace RobotDesigner.Nodes
{
    /// <summary>
    /// Base for switchable scene node.
    /// </summary>
    public abstract class BaseSwitchNode : BaseNode
    {
        /// <summary>
        /// Inner method.
        /// </summary>
        public MethodScene InnerMethod { get; set; }

        /// <summary>
        /// Constructor for node.
        /// </summary>
        /// <param name="method">Method to which node is attached.</param>
        /// <param name="x">X position of the node.</param>
        /// <param name="y">Y position of the node.</param>
        /// <param name="name">Name of the node.</param>
        protected BaseSwitchNode(MethodScene method, float x, float y, string name = null) : base(method, x, y, name)
        {
            this.InnerMethod = new MethodScene(this);
            this.ParentMethod.AddSubScene(this.InnerMethod);

            var on = new Button(175, 0, 25, 25, "*", true);
            on.IsEnabled = true;
            this.AddChild(on);
            on.OnCommand += EnterScene;
        }

        /// <summary>
        /// Handles enter node click event.
        /// </summary>
        /// <param name="caller">Button caller.</param>
        /// <param name="params">Caller parameters.</param>
        protected virtual void EnterScene(BaseButton caller, object[] @params)
        {
            InnerMethod.ParentUI.SetActiveMethod(this.InnerMethod);
        }
    }
}

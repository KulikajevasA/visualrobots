﻿using UnityEngine;
using UI;
using UI.Components;
using DesignerGlue.Chain;
using DesignerGlue.Chain.Types;
using UI.Components.Input;
using System;
using System.Xml;

namespace RobotDesigner.Nodes
{
    /// <summary>
    /// Numeric node.
    /// </summary>
    public class ColorSensorNode : BaseNode, IValueNode
    {
        /// <summary>
        /// Registers UI component.
        /// </summary>
        public static void RegisterUIElement() { UIBuilder.AddTypeBind(typeof(ColorSensorNode)); }

        /// <summary>
        /// Returns node type.
        /// </summary>
        public override string NodeType { get { return "ColorSensor"; } }

        /// <summary>
        /// Supports west attachments.
        /// </summary>
        public override bool HasWest { get { return false; } }

        protected override void OnDraw()
        {
            GUI.Box(Bounds, GUIContent.none, boxColorStyle.Style);
            DrawN();
            DrawE();
            DrawS();
            DrawW();
        }

        protected Label labelName;

        public ColorSensorNode(MethodScene method, XmlElement xel) : this(method, 0, 0) { }

        /// <summary>
        /// Constructor for numeric node.
        /// </summary>
        /// <param name="method">Parent method.</param>
        public ColorSensorNode(MethodScene method) : this(method, 0, 0) { }

        /// <summary>
        /// Constructor for numeric node.
        /// </summary>
        /// <param name="method">Parent method.</param>
        /// <param name="x">X position.</param>
        /// <param name="y">Y position.</param>
        /// <param name="name">Name of the element.</param>
        public ColorSensorNode(MethodScene method, float x, float y, string name = null) : base(method, x, y, name)
        {
            labelName = new Label(19, 12, 150, 20, "Color Sensor", true);

            this.AddChildren(labelName);

            labelName.Style.fontStyle = FontStyle.Bold;
        }

        public override SyntaxChain BuildChain()
        {
            var leaf = new ColorSensorLink(this);

            var eastNode = GetNode(DirectionalButton.Direction.East);
            if (eastNode != null)
                leaf.Next = eastNode.BuildChain();
            else
            {
                var southNode = GetNode(DirectionalButton.Direction.South);

                if (southNode != null)
                    leaf.Next = southNode.BuildChain();
            }

            return leaf;
        }
    }
}
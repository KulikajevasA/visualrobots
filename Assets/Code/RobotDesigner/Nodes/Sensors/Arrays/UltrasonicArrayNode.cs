﻿using UI;
using UI.Components;
using DesignerGlue.Chain;
using DesignerGlue.Chain.Types;
using UI.Components.Input;
using UnityEngine;
using System.Xml;
using System;

namespace RobotDesigner.Nodes.Sensors.Arrays
{
    /// <summary>
    /// Ultrasonic array node.
    /// </summary>
    public class UltrasonicArrayNode : BaseNode
    {
        protected Label labelName;
        private ArrayType_T inputType;

        /// <summary>
        /// Returns node type.
        /// </summary>
        public override string NodeType { get { return "UltrasonicSensorArray"; } }

        protected override void OnDraw()
        {
            GUI.Box(Bounds, GUIContent.none, boxUltrasonicStyle.Style);
            DrawN();
            DrawE();
            DrawS();
            DrawW();
        }

        /// <summary>
        /// Array data type.
        /// </summary>
        public enum ArrayType_T
        {
            Direction = 0,
            Distance
        }

        /// <summary>
        /// Getter and setter for output type.
        /// </summary>
        public ArrayType_T OutputType
        {
            get
            {
                return this.inputType;
            }
            set
            {
                this.inputType = value;

                UpdateText();
            }
        }

        /// <summary>
        /// Registers UI element.
        /// </summary>
        public static void RegisterUIElement() { UIBuilder.AddTypeBind(typeof(UltrasonicArrayNode)); }

        /// <summary>
        /// Supports west attachments.
        /// </summary>
        public override bool HasWest { get { return false; } }

        public UltrasonicArrayNode(MethodScene method, XmlElement xel) : this(method, 0, 0)
        {
            this.inputType = (ArrayType_T)Enum.Parse(typeof(ArrayType_T), xel.GetAttribute("InputType"));
        }

        /// <summary>
        /// Constructor for node.
        /// </summary>
        /// <param name="method">Method to which node is attached.</param>
        public UltrasonicArrayNode(MethodScene method) : this(method, 0, 0) { }

        /// <summary>
        /// Constructor for node.
        /// </summary>
        /// <param name="method">Method to which node is attached.</param>
        /// <param name="x">X position of the node.</param>
        /// <param name="y">Y position of the node.</param>
        /// <param name="name">Name of the node.</param>
        public UltrasonicArrayNode(MethodScene method, float x, float y, string name = null)
            : base(method, x, y, name)
        {
            this.inputType = ArrayType_T.Direction;
            labelName = new Label(14, 12, 150, 20, "Ultrasonic Dir", true);
            AddChild(labelName);

            var on = new Button(175, 0, 25, 25, "!", true);
            on.IsEnabled = true;
            this.AddChild(on);
            on.OnCommand += HandleToggleType;
        }

        /// <summary>
        /// Toggles the node type.
        /// </summary>
        public void ToggleType()
        {
            this.inputType = this.inputType == ArrayType_T.Direction ? ArrayType_T.Distance : ArrayType_T.Direction;

            UpdateText();
        }

        /// <summary>
        /// Updates node text.
        /// </summary>
        private void UpdateText()
        {
            switch (this.inputType)
            {
                case ArrayType_T.Direction:
                    labelName.Text = "Ultrasonic Dir";
                    break;
                case ArrayType_T.Distance:
                    labelName.Text = "Ultrasonic Dist";
                    break;
            }
        }

        /// <summary>
        /// Handles toggle type click event.
        /// </summary>
        /// <param name="caller">Button caller.</param>
        /// <param name="params">Call parameters.</param>
        private void HandleToggleType(BaseButton caller, object[] @params)
        {
            ToggleType();
        }

        /// <summary>
        /// Builds a chain.
        /// </summary>
        /// <returns>Syntax chain.</returns>
        public override SyntaxChain BuildChain()
        {
            var leaf = new UltrasonicArrayLink(this);

            var eastNode = GetNode(DirectionalButton.Direction.East);
            if (eastNode != null)
                leaf.Next = eastNode.BuildChain();
            else
            {
                var southNode = GetNode(DirectionalButton.Direction.South);

                if (southNode != null)
                    leaf.Next = southNode.BuildChain();
            }

            return leaf;
        }

        public override XmlElement Serialize(XmlDocument xdoc, XmlElement xnodes, XmlElement xchain)
        {
            var node = base.Serialize(xdoc, xnodes, xchain);

            node.SetAttribute("InputType", inputType.ToString());

            return node;
        }
    }
}

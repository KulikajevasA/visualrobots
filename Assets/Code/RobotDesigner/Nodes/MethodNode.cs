﻿using UI;
using UI.Components;
using UnityEngine;
using DesignerGlue.Chain;
using DesignerGlue.Chain.Types;
using UI.Components.Input;
using System.Xml;

namespace RobotDesigner.Nodes
{
    /// <summary>
    /// Method node.
    /// </summary>
    public class MethodNode : BaseSwitchNode
    {
        /// <summary>
        /// Registers UI component.
        /// </summary>
        public static void RegisterUIElement() { UIBuilder.AddTypeBind(typeof(MethodNode)); }

        /// <summary>
        /// Returns node type.
        /// </summary>
        public override string NodeType { get { return "Method"; } }

        protected override void OnDraw()
        {
            GUI.Box(Bounds, GUIContent.none, boxMethodStyle.Style);
            DrawN();
            DrawE();
            DrawS();
            DrawW();
        }

        protected Label labelName;

        /// <summary>
        /// Supports south attachments.
        /// </summary>
        public override bool HasSouth { get { return false; } }

        /// <summary>
        /// Supports west attachment.
        /// </summary>
        public override bool HasWest { get { return false; } }

        /// <summary>
        /// Getter and setter for method name.
        /// </summary>
        public string MethodName { get; set; }

        public MethodNode(MethodScene method, MethodScene inner, XmlElement xel) : this(method, 0, 0)
        {
            this.MethodName = xel.GetAttribute("Name");
            this.InnerMethod = inner;
        }

        /// <summary>
        /// Constructor for node.
        /// </summary>
        /// <param name="method">Method to which node is attached.</param>
        public MethodNode(MethodScene method, string methodName = null) : this(method, 0, 0, methodName) { }

        /// <summary>
        /// Constructor for node.
        /// </summary>
        /// <param name="method">Method to which node is attached.</param>
        /// <param name="x">X position of the node.</param>
        /// <param name="y">Y position of the node.</param>
        /// <param name="name">Name of the node.</param>
        public MethodNode(MethodScene method, float x, float y, string methodName = null, string name = null) : base(method, x, y, name)
        {
            labelName = new Label(14, 12, 150, 20, "Method:", true);

            this.AddChild(labelName);

            labelName.Style.fontStyle = FontStyle.BoldAndItalic;
            this.MethodName = methodName;
        }

        /// <summary>
        /// Builds syntax chain.
        /// </summary>
        /// <returns>Syntax chain.</returns>
        public override SyntaxChain BuildChain()
        {
            var leaf = new MethodLink(this);
            leaf.InternalChain = InnerMethod.EntryNode.BuildChain();

            var westNode = GetNode(DirectionalButton.Direction.West);

            if (westNode != null)
                leaf.Next2 = westNode.BuildChain();

            var eastNode = GetNode(DirectionalButton.Direction.East);
            if (eastNode != null)
            {
                leaf.Next = eastNode.BuildChain();
                leaf.InternalChain = this.InnerMethod.EntryNode.BuildChain();
            }
            else
            {
                var southNode = GetNode(DirectionalButton.Direction.South);

                if (southNode != null)
                {
                    leaf.Next = southNode.BuildChain();
                    leaf.InternalChain = this.InnerMethod.EntryNode.BuildChain();              
                }
            }

            return leaf;
        }

        public override XmlElement Serialize(XmlDocument xdoc, XmlElement xnodes, XmlElement xchain)
        {
            var node = base.Serialize(xdoc, xnodes, xchain);

            this.InnerMethod.EntryNode.Serialize(xdoc, xnodes, xchain);

            node.SetAttribute("Name", MethodName);
            node.SetAttribute("Entry", this.InnerMethod.EntryNode.UniqueId);

            return node;
        }
    }
}

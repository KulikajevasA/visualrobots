﻿using RobotDesigner.Nodes;
using RobotDesigner.Views;
using System;
using System.Collections.Generic;
using System.Xml;
using UI.Components;

namespace RobotDesigner
{
    /// <summary>
    /// Handles method scene drawing.
    /// </summary>
    public class MethodScene
    {
        /// <summary>
        /// Getter and setter for entry node.
        /// </summary>
        public EntryNode EntryNode { get; private set; }

        /// <summary>
        /// Scroll view that is drawn when scene is active.
        /// </summary>
        public ScrollView View { get; set; }

        /// <summary>
        /// Name of the method.
        /// </summary>
        public string MethodName { get; private set; }

        /// <summary>
        /// Unique identificator for the method.
        /// </summary>
        public string UniqueId { get; set; }

        /// <summary>
        /// Parent UI.
        /// </summary>
        public MethodUI ParentUI { get; set; }

        public BaseSwitchNode Parent { get; set; }

        /// <summary>
        /// Constructor for method scene.
        /// </summary>
        /// <param name="parent">Parent.</param>
        public MethodScene(BaseSwitchNode parent = null)
        {
            this.Parent = parent;
            this.UniqueId = Guid.NewGuid().ToString();
            this.View = new ScrollView(0, 50, CustomUI.BaseX, CustomUI.BaseY - 50, true);

            this.EntryNode = new EntryNode(this, 25, 75);
            this.EntryNode.SetParameterValues(25, 75, false);
        }

        public MethodScene(bool specialConstructor)
        {

        }

        /// <summary>
        /// Adds a subscene.
        /// </summary>
        /// <param name="subScene">Sub scene to add.</param>
        public void AddSubScene(MethodScene subScene)
        {
            if (this.ParentUI != null)
                this.ParentUI.AddMethod(subScene);
        }

        /// <summary>
        /// Renders the scene.
        /// </summary>
        public void Render()
        {
            this.View.Draw();
        }

        /// <summary>
        /// Adds a child to the scene.
        /// </summary>
        /// <param name="child">Child to add.</param>
        public void AddChild(BaseNode child)
        {
            this.View.AddChild(child);
        }

        /// <summary>
        /// Adds children to the scene.
        /// </summary>
        /// <param name="children">List of children to add.</param>
        public void AddChildren(params BaseNode[] children)
        {
            this.View.AddChildren(children);
        }
    }
}

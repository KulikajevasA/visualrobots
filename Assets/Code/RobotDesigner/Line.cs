﻿using RobotDesigner.Nodes;
using System;
using UnityEngine;

namespace RobotDesigner
{
    /// <summary>
    /// Used for drawing connects.
    /// </summary>
    [Obsolete]
    public class Line
    {
        private BaseNode parentNode;

        /// <summary>
        /// Getter and seter for line start position vector.
        /// </summary>
        public Vector2 StartPos { get; set; }

        /// <summary>
        /// Getter and setter for line direction vector.
        /// </summary>
        public Vector2 Direction { get; set; }

        /// <summary>
        /// Getter and setter for line color.
        /// </summary>
        public Color Color { get; set; }

        /// <summary>
        /// Getter and setter for line width.
        /// </summary>
        public float Width { get; set; }

        /// <summary>
        /// Getter and setter for line length.
        /// </summary>
        public float Length { get; set; }

        /// <summary>
        /// Constructor for line.
        /// </summary>
        /// <param name="start">Line start pos.</param>
        /// <param name="direction">Line direction.</param>
        /// <param name="color">Line color.</param>
        /// <param name="width">Line width.</param>
        public Line(BaseNode parentNode, Vector2 start, Vector2 direction, float length, float width, Color color)
        {
            this.parentNode = parentNode;
            this.StartPos = start;
            this.Direction = direction;
            this.Length = length;
            this.Color = color;
            this.Width = width;
        }

        /// <summary>
        /// Renders a line.
        /// </summary>
        public void Render()
        {
            var start = parentNode.ParentMethod.View.Bounds.position + StartPos;
           
            DrawingCurves.DrawLine(start, start + Direction * Length, Color, Width, false);
        }
    }
}

﻿using UI.Components;
using UI.Components.Input;
using UnityEngine;

namespace RobotDesigner.Views
{
    public class SettingsUI : CustomUI
    {
        private GameUI gameUI;

        private Holder settingsHolder;
        private Button buttonOpenSettings;

        public string OutputPath { get; private set; }
        public string MSBuildPath { get; private set; }

        public SettingsUI(GameUI gameUI) : base()
        {
            this.gameUI = gameUI;

            settingsHolder = new Holder();
            buttonOpenSettings = new Button(BaseX - 200, 0, 200, 25, "Show Settings", true);
            buttonOpenSettings.OnCommand += HandleOpenSettings;
            buttonOpenSettings.IsEnabled = true;

            AddChildren(settingsHolder, buttonOpenSettings);

            UpdateSettings();
            CreateSettings();
        }

        public void UpdateSettings()
        {
            var kv = FileSystem.FileSystem.ReadKeyValuesFromFile(Application.dataPath, "settings.res");
            if (kv != null)
            {
                var kvSettings = kv.GetSubKey("settings");
                if (kvSettings != null)
                {
                    OutputPath = kvSettings.GetString("output");
                    MSBuildPath = kvSettings.GetString("msbuild");
                }
            }
        }

        private void CreateSettings()
        {
            var frameOutput = new Frame(0, 0, BaseX - 75, 25, true);
            var labelOutput = new Label(5, 5, 100, 25, "Output Directory:", true);
            var labelOutputDir = new Label(100, 5, BaseX, 25, OutputPath, true);
            frameOutput.AddChildren(labelOutput, labelOutputDir);

            var frameBuild = new Frame(0, 25, BaseX - 75, 25, true);
            var labelBuild = new Label(5, 5, 100, 25, "MS Build:", true);
            var labelBuildFile = new Label(100, 5, BaseX, 25, MSBuildPath, true);
            frameBuild.AddChildren(labelBuild, labelBuildFile);

            settingsHolder.AddChildren(frameOutput, frameBuild);
        }

        private void HandleOpenSettings(BaseButton caller, params object[] _params)
        {
            settingsHolder.IsVisible = !settingsHolder.IsVisible;

            if (settingsHolder.IsVisible)
            {
                gameUI.MethodUI.Hide();
                gameUI.SimulatorUI.Hide();
            }
            else
            {
                gameUI.MethodUI.Show();
                gameUI.SimulatorUI.Show();
            }
        }
    }
}

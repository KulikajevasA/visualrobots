﻿using FileSystem;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UI.Components;
using UI.Components.Input;

namespace RobotDesigner.Views
{
    public class StartMenuUI : CustomUI
    {
        private const string BUTTON_START = "button_start";

        private string pathToRobots;
        private string pathToBuild;

        private GameUI gameUI;

        private Frame frameBackground;

        private FileBrowserDialog fileBrowserMSBuild;
        private FileBrowserDialog fileBrowserOutput;

        private InputField inputBuildPath;
        private InputField inputRobotsPath;

        private BaseButton buttonSelectRobotPath;
        private BaseButton buttonSelectMSBuild;

        private BaseButton buttonStart;

        private Frame frameIputs;

        public string OutputPath
        {
            get { return this.pathToRobots; }
        }

        public string MSBuildPath
        {
            get { return this.pathToBuild; }
        }

        public StartMenuUI(GameUI gameUI) : base()
        {
            this.gameUI = gameUI;

            var x = BaseX / 4;
            var y = BaseY / 4;

            var w = BaseX - x * 2;
            var h = BaseY - y * 2;

            this.frameBackground = new Frame(0, 0, BaseX, BaseY, true);
            this.buttonStart = new ImageButton((BaseX - 256) / 2, BaseY / 4, 400, 200, BUTTON_START, true);
            this.fileBrowserMSBuild = new FileBrowserDialog(new FileBrowserDialog.DialogSettings(FileBrowserDialog.DialogMode_T.MODE_FILE, FileBrowserDialog.DialogAction_T.MODE_OPEN, "MSBuild.exe"), x, y, w, h, false);
            this.fileBrowserOutput = new FileBrowserDialog(new FileBrowserDialog.DialogSettings(FileBrowserDialog.DialogMode_T.MODE_DIRECTORY, FileBrowserDialog.DialogAction_T.MODE_OPEN), x, y, w, h, false);
            this.fileBrowserMSBuild.OnFileSelected += FileBrowserMSBuild_OnFileSelected;
            this.fileBrowserOutput.OnFileSelected += FileBrowserOutput_OnFileSelected;

            this.frameIputs = new Frame((BaseX - 256) / 2, BaseY / 4 + 200 + 32, 256, 64, true);

            this.frameBackground.AddChild(this.frameIputs);

            this.inputBuildPath = new InputField(0, 0, 1024, 32, "", true);
            this.inputRobotsPath = new InputField(0, 32, 1024, 32, "", true);

            this.inputRobotsPath.IsEnabled = this.inputBuildPath.IsEnabled = false;
            this.inputRobotsPath.MaxLength = this.inputBuildPath.MaxLength = 0;

            AddChildren(frameBackground);

            this.frameBackground.AddChildren(buttonStart, fileBrowserMSBuild, fileBrowserOutput);
            this.frameIputs.AddChildren(inputBuildPath, inputRobotsPath);

            LoadSettings();

            this.inputBuildPath.Text = this.pathToBuild;
            this.inputRobotsPath.Text = this.pathToRobots;

            this.buttonSelectRobotPath = new Button((BaseX - 256) / 2 + 256, BaseY / 4 + 264, 142, 24, "Select output path...", true);
            this.buttonSelectMSBuild = new Button((BaseX - 256) / 2 + 256, BaseY / 4 + 232, 142, 24, "Select MSBuild path...", true);
            this.AddChildren(buttonSelectRobotPath, buttonSelectMSBuild);

            this.buttonSelectMSBuild.IsEnabled = this.buttonSelectRobotPath.IsEnabled = true;

            this.buttonStart.OnCommand += HandleGameStart;
            this.buttonSelectRobotPath.OnCommand += HandleButtonSelectRobotPath;
            this.buttonSelectMSBuild.OnCommand += HandleButtonSelectMSBuild;
        }

        private void FileBrowserOutput_OnFileSelected(FileBrowserDialog caller, string directory, string file)
        {
            this.inputRobotsPath.Text = this.pathToRobots = directory;
            SaveSettings();
        }

        private void FileBrowserMSBuild_OnFileSelected(FileBrowserDialog caller, string directory, string file)
        {
            if (!file.ToLowerInvariant().Equals("msbuild.exe"))
            {
                caller.IsVisible = true;
                return;
            }

            this.inputBuildPath.Text = this.pathToBuild = (directory + "/" + file);
            SaveSettings();
        }

        private void HandleButtonSelectMSBuild(BaseButton caller, params object[] @params)
        {
            this.fileBrowserMSBuild.IsVisible = true;
        }

        private void HandleButtonSelectRobotPath(BaseButton caller, params object[] @params)
        {
            this.fileBrowserOutput.IsVisible = true;
        }

        private void HandleGameStart(BaseButton caller, params object[] @params)
        {
            this.gameUI.StartGame();
        }

        private void SaveSettings()
        {
            var root = UnityEngine.Application.dataPath;

            var kv = new KeyValues();

            KeyValues kvSettings = kv.AddKey("settings");
            kvSettings.AddKeyValue("output", this.pathToRobots);
            kvSettings.AddKeyValue("msbuild", this.pathToBuild);

            FileSystem.FileSystem.SaveKeyValuesToFile(root, kv, "settings.res");
        }

        private void LoadSettings()
        {
            var root = UnityEngine.Application.dataPath;

            var kv = FileSystem.FileSystem.ReadKeyValuesFromFile(root, "settings.res");
            KeyValues kvSettings = null;

            if (kv != null)
            {
                kvSettings = kv.GetSubKey("settings");

                if (kvSettings != null)
                {
                    this.pathToBuild = kvSettings.GetString("msbuild").Replace('\\', '/');
                    this.pathToRobots = kvSettings.GetString("output", root).Replace('\\', '/');

                    if (!this.pathToBuild.Equals(string.Empty))
                        this.buttonStart.IsEnabled = true;
                }
            }
            else
            {
                kv = new KeyValues();
                kvSettings = kv.AddKey("settings");

                var pathMSBuild = FindMSBuild();

                if (pathMSBuild != null)
                {
                    kvSettings.AddKeyValue("output", root);
                    kvSettings.AddKeyValue("msbuild", pathMSBuild);
                    buttonStart.IsEnabled = true;

                    FileSystem.FileSystem.SaveKeyValuesToFile(root, kv, "settings.res");

                    this.pathToRobots = pathToBuild;
                    this.pathToBuild = pathMSBuild;
                }
            }
        }

        public static string FindMSBuild()
        {
            var dir = string.Empty;
            var dirMSBuildx86 = Environment.GetEnvironmentVariable("programfiles(x86)") + "\\MSBuild";
            var dirMSBuild = Environment.GetEnvironmentVariable("programfiles") + "\\MSBuild";
            if (Directory.Exists(dirMSBuildx86))
                dir = dirMSBuildx86;
            else if (Directory.Exists(dirMSBuildx86))
                dir = dirMSBuildx86;
            else
                return null;

            var supportedVersions = new string[] { "14.0", "11.0" };

            for (var i = 0; i < supportedVersions.Length; i++)
            {
                var p = dir + "\\" + supportedVersions[i] + "\\bin";
                if (Directory.Exists(p))
                {
                    dir = p;
                    break;
                }
            }

            var path = dir + "\\msbuild.exe";

            if (File.Exists(path))
                return path;

            return null;
        }
    }
}

﻿using Designer;
using RobotVL;
using RobotVL.Generation;
using RobotVL.Generation.DataStructures;
using Simulation;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using UI;
using UI.Components;
using UI.Components.Input;
using UnityEngine;

namespace RobotDesigner.Views
{
    /// <summary>
    /// Draws all simulation related UI components.
    /// </summary>
    public class SimulatorUI : CustomUI
    {
        private ImageButton buttonPlay;
        private ImageButton buttonPause;
        private ImageButton buttonStop;
        private ImageButton buttonContinue;
        private VariableNameField robotNameField;

        private GameUI gameUI;

        private BoxNumericStyle boxNumericStyle = new BoxNumericStyle();

        private FileBrowserDialog fileBrowserSaveRobot;
        private FileBrowserDialog fileBrowserLoadRobot;

        public string RobotName { get { return robotNameField.Value; } }

        [DllImport("user32.dll")]
        private static extern bool SetForegroundWindow(IntPtr hWnd);

        /// <summary>
        /// Constructor for SimulatorUI.
        /// </summary>
        /// <param name="gameUI"></param>
        public SimulatorUI(GameUI gameUI) : base()
        {
            this.gameUI = gameUI;

            var x = BaseX / 4;
            var y = BaseY / 4;

            var w = BaseX - x * 2;
            var h = BaseY - y * 2;

            this.fileBrowserSaveRobot = new FileBrowserDialog(new FileBrowserDialog.DialogSettings(FileBrowserDialog.DialogMode_T.MODE_FILE, FileBrowserDialog.DialogAction_T.MODE_SAVE, "*.rvl"), x, y, w, h, false);
            this.fileBrowserSaveRobot.OnFileSelected += HandleSaveFileSelected;
            this.fileBrowserSaveRobot.OnDialogCanceled += HandleCancelDialog;

            this.fileBrowserLoadRobot = new FileBrowserDialog(new FileBrowserDialog.DialogSettings(FileBrowserDialog.DialogMode_T.MODE_FILE, FileBrowserDialog.DialogAction_T.MODE_OPEN, "*.rvl"), x, y, w, h, false);
            this.fileBrowserLoadRobot.OnFileSelected += HandleLoadFileSelected;
            this.fileBrowserLoadRobot.OnDialogCanceled += HandleCancelDialog;

            buttonContinue = new ImageButton(BaseX / 2 - 35, 20, 30, 30, "button_play", false);
            buttonPause = new ImageButton(BaseX / 2 + 35, 20, 30, 30, "button_pause", false);
            buttonPlay = new ImageButton(BaseX / 2, 20, 30, 30, "button_play", true);
            buttonStop = new ImageButton(BaseX / 2, 20, 30, 30, "button_stop", false);

            buttonContinue.IsEnabled = buttonPause.IsEnabled = buttonPlay.IsEnabled = buttonStop.IsEnabled = true;

            buttonContinue.OnCommand += HandleOnContinue;
            buttonPause.OnCommand += HandleOnPause;
            buttonPlay.OnCommand += HandleOnPlay;
            buttonStop.OnCommand += HandleOnStop;

            AddChildren(buttonPlay, buttonContinue, buttonPause, buttonStop);

            var buttonShowMain = new Button(0, 0, 200, 25, "Show Main", true);
            buttonShowMain.IsEnabled = true;
            buttonShowMain.OnCommand += HandleShowMain;

            var buttonClear = new ImageButton(200, 0, 50, 50, "button_new", true);
            buttonClear.IsEnabled = true;
            buttonClear.OnCommand += HandleClearButton;

            var buttonSaveRobot = new ImageButton(250, 0, 50, 50, "button_save", true);
            buttonSaveRobot.IsEnabled = true;
            buttonSaveRobot.OnCommand += HandleSaveRobotButton;

            var buttonLoadRobot = new ImageButton(300, 0, 50, 50, "button_open", true);
            buttonLoadRobot.IsEnabled = true;
            buttonLoadRobot.OnCommand += HandleLoadRobotButton;

            var frame = new Frame(0, 25, 200, 25, true);

            //frame.Style.normal.background = GUI.Box(, "", boxNumericStyle.Style);
            robotNameField = new VariableNameField(5, 5, 100, 25, "GeneratedRobot", true);
            robotNameField.IsEnabled = true;
            frame.AddChild(this.robotNameField);

            /*var createCodeButton = new Button(0, 50, 100, 25, "Build Code", true);
            createCodeButton.Enabled = true;
            createCodeButton.OnCommand += HandleCreateCode;*/

            AddChildren(buttonShowMain, /*createCodeButton,*/ frame, buttonClear, buttonSaveRobot, buttonLoadRobot, fileBrowserSaveRobot, fileBrowserLoadRobot);
        }

        private void HandleClearButton(BaseButton caller, params object[] @params)
        {
            gameUI.NewScene();
        }

        private void HandleCancelDialog(FileBrowserDialog caller)
        {
            this.gameUI.MethodUI.IsVisible = true;
        }

        private void HandleLoadFileSelected(FileBrowserDialog caller, string directory, string file)
        {
            //file = file + ".rvl";
            var path = directory + "/" + file;
            if (!File.Exists(path))
            {
                this.gameUI.MethodUI.IsVisible = false;
                this.fileBrowserLoadRobot.ShowDialog();
                return;
            }

            gameUI.Deserialize(path);
        }

        private void HandleSaveFileSelected(FileBrowserDialog caller, string directory, string file)
        {
            if (!file.Contains(".rvl"))
                file = file + ".rvl";
            var path = directory + "/" + file;
            var serialized = gameUI.Serialize();
            this.gameUI.MethodUI.IsVisible = true;

            File.WriteAllText(path, serialized.AsBeautifiedString());
        }

        private void HandleLoadRobotButton(BaseButton caller, params object[] @params)
        {
            this.fileBrowserLoadRobot.ShowDialog();
            this.fileBrowserLoadRobot.FileName = RobotName + ".rvl";
            this.gameUI.MethodUI.IsVisible = false;
        }

        private void HandleSaveRobotButton(BaseButton caller, params object[] @params)
        {
            this.fileBrowserSaveRobot.ShowDialog();
            this.fileBrowserSaveRobot.FileName = RobotName + ".rvl";
            this.gameUI.MethodUI.IsVisible = false;
        }

        /// <summary>
        /// Handles stop button click event.
        /// </summary>
        /// <param name="caller">Button caller.</param>
        /// <param name="params">Call parameters.</param>
        private void HandleOnStop(BaseButton caller, params object[] @params)
        {
            ShowStoppedUI();
            ResetRobot();
        }

        public void ShowStoppedUI()
        {
            buttonPause.IsVisible = false;
            buttonStop.IsVisible = false;
            buttonContinue.IsVisible = false;
            buttonPlay.IsVisible = true;
            gameUI.MethodUI.IsVisible = true;
        }

        /// <summary>
        /// Handles play button click event.
        /// </summary>
        /// <param name="caller">Button caller.</param>
        /// <param name="params">Call parameters.</param>
        private void HandleOnPlay(BaseButton caller, params object[] @params)
        {
            buttonPause.IsVisible = true;
            buttonStop.IsVisible = true;
            buttonContinue.IsVisible = true;
            buttonPlay.IsVisible = false;
            gameUI.MethodUI.IsVisible = false;
            RebuildRobotAssembly();
        }

        /// <summary>
        /// Handles pause button click event.
        /// </summary>
        /// <param name="caller">Button caller.</param>
        /// <param name="params">Call parameters.</param>
        private void HandleOnPause(BaseButton caller, params object[] @params)
        {
            gameUI.MethodUI.IsVisible = true;
            PauseRobot();
        }

        /// <summary>
        /// Handles continue click event.
        /// </summary>
        /// <param name="caller">Button caller.</param>
        /// <param name="params">Call parameters.</param>
        private void HandleOnContinue(BaseButton caller, params object[] @params)
        {
            gameUI.MethodUI.IsVisible = false;
            RebuildRobotAssembly();
        }

        /// <summary>
        /// Handles create code button click.
        /// </summary>
        /// <param name="caller">Button caller.</param>
        /// <param name="params">Call parameters.</param>
        private void HandleCreateCode(BaseButton caller, params object[] @params)
        {
            RebuildRobotAssembly();
        }

        /// <summary>
        /// Rebuilds assembly.
        /// </summary>
        private void RebuildRobotAssembly()
        {
            var main = gameUI.MainMethod;
            var buildNode = main.EntryNode.BuildChain();
            var tree = buildNode.ToTree();

            var robotName = robotNameField.Value;
            var robot = tree.ToRobot(robotName);

            var saveDir = gameUI.StartMenuUI.OutputPath;
            var msBuild = gameUI.StartMenuUI.MSBuildPath;

            var projectInfo = new ProjectInfo(robot);

            string projectDir = ProjectGenerator.GenerateProject(projectInfo, saveDir);
            var projectLoc = string.Format("{0}/{1}.csproj", projectDir, robot.RobotName);

            var args = string.Format("{0} /t:rebuild /verbosity:quiet /fileLogger /fileLoggerParameters:LogFile={1}/msbuild.log;Summary;Verbosity=normal", projectLoc, projectDir);

            using (var exeProcess = Process.Start(msBuild, args))
            {
                SetForegroundWindow(exeProcess.MainWindowHandle);
                exeProcess.WaitForExit();
                var exitCode = exeProcess.ExitCode;

                if (exitCode != 0)
                    throw new Exception(string.Format("Compiler exception. Exited with code: {0}", exitCode));

                var assemblyLoc = string.Format("{0}/bin/release/{1}.dll", projectDir, robot.RobotName);
                ReloadRobotWorker(assemblyLoc);
            }
        }

        /// <summary>
        /// Pauses the robot.
        /// </summary>
        private void PauseRobot()
        {
            var worker = UnityEngine.Object.FindObjectOfType<RobotWorker>();
            worker.UnbindAssembly();
        }

        /// <summary>
        /// Moves the robot to starting point.
        /// </summary>
        private void ResetRobot()
        {
            var worker = UnityEngine.Object.FindObjectOfType<RobotWorker>();
            worker.Restore();
        }

        /// <summary>
        /// Reloads robot worker assembly.
        /// </summary>
        /// <param name="assemblyPath">Assembly path to load.</param>
        private void ReloadRobotWorker(string assemblyPath)
        {
            var worker = UnityEngine.Object.FindObjectOfType<RobotWorker>();

            if (worker == null)
                return;

            worker.ReloadAssembly(assemblyPath);
        }

        /// <summary>
        /// Handles show main button click.
        /// </summary>
        /// <param name="caller">Button caller.</param>
        /// <param name="params">Call parameters.</param>
        private void HandleShowMain(BaseButton caller, object[] @params)
        {
            gameUI.MethodUI.SetActiveMethod(gameUI.MainMethod);
        }
    }
}

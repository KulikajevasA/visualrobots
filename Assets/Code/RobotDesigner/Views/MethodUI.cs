﻿using System;
using System.Collections.Generic;

namespace RobotDesigner.Views
{
    /// <summary>
    /// Class for handling UI for a method.
    /// </summary>
    public class MethodUI : CustomUI
    {
        private Dictionary<string, MethodScene> methods;
        private string activeMethod;

        /// <summary>
        /// Constructor for MethodUI class.
        /// </summary>
        public MethodUI() : base()
        {
            methods = new Dictionary<string, MethodScene>();
        }

        /// <summary>
        /// Adds a scene method to the UI.
        /// </summary>
        /// <param name="method">Method scene to add.</param>
        public void AddMethod(MethodScene method)
        {
            this.methods.Add(method.UniqueId, method);
            method.ParentUI = this;
        }

        /// <summary>
        /// Renders the active scene.
        /// </summary>
        protected override void Render()
        {
            if (!this.IsVisible)
                return;

            if (this.activeMethod != null && this.methods.ContainsKey(this.activeMethod))
                this.methods[this.activeMethod].Render();
        }

        /// <summary>
        /// Sets the active method scene.
        /// </summary>
        /// <param name="method">Method to set as active.</param>
        public void SetActiveMethod(MethodScene method)
        {
            if (!this.methods.ContainsKey(method.UniqueId))
                throw new InvalidOperationException("Cannot set a method as active that is not part of the UI.");

            this.activeMethod = method.UniqueId;
        }
    }
}

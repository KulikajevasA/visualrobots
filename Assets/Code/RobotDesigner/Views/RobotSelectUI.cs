﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UI.Components;
using UI.Components.Input;

namespace RobotDesigner.Views
{
    public class RobotSelectUI : CustomUI
    {
        private GameUI gameUI;

        private Frame frameBackground;
        private Label labelRobotType;
        private ImageComponent imageRobot;

        private Frame frameLabel;
        private Frame frameSelectRobot;

        private Label labelGenericRobot;
        private Button buttonGenericButton;

        private Label labelOthersComing;

        public RobotSelectUI(GameUI gameUI) : base()
        {
            this.gameUI = gameUI;

            this.frameBackground = new Frame(0, 0, BaseX, BaseY, true);
            this.labelRobotType = new Label(0, 0, 100, 100, "Select Robot Type...", true);
            this.imageRobot = new ImageComponent(200, 50, BaseX - 225, BaseY - 100, "Robots/RobotGeneric", true);

            this.frameLabel = new Frame(25, 50, 155, 125, true);
            this.frameSelectRobot = new Frame(25, 200, 155, BaseY - 250, true);

            this.labelGenericRobot = new Label(0, 0, 155, 50, "Generic Robot\n\tFront ultrasonic Sensor Array.\n\tTwo motors.", true);
            this.buttonGenericButton = new Button(0, 50, 155, 15, "Use Generic Robot", true);
            this.labelOthersComing = new Label(0, 80, 155, 50, "Other Robots Coming Soon...", true);

            frameSelectRobot.AddChildren(labelGenericRobot, buttonGenericButton, labelOthersComing);
            frameLabel.AddChildren(labelRobotType);
            frameBackground.AddChildren(imageRobot, frameLabel, frameSelectRobot);

            AddChildren(frameBackground);

            buttonGenericButton.OnCommand += CreateRobotGeneric;
            buttonGenericButton.IsEnabled = true;
        }

        private void CreateRobotGeneric(BaseButton caller, params object[] @params)
        {
            this.Hide();
            this.gameUI.MethodUI.Show();
            this.gameUI.SimulatorUI.Show();
        }
    }
}

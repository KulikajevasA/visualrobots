﻿using System.Collections.Generic;
using FileSystem;
using UnityEngine;
using UI;

namespace RobotDesigner.Views
{
    public abstract class CustomUI
    {
        protected List<BaseUIComponent> children;
        protected int basex, basey;

        public static int BaseX { get; private set; }
        public static int BaseY { get; private set; }

        public bool IsVisible { get; set; }

        static CustomUI()
        {
            BaseX = UnityEngine.Screen.width;
            BaseY = UnityEngine.Screen.height;
        }

        protected CustomUI()
        {
            children = new List<BaseUIComponent>();
            IsVisible = true;
            basex = BaseX;
            basey = BaseY;
        }

        protected CustomUI(string file)
        {
            var kv = FileSystem.FileSystem.ReadKeyValuesFromFile(Application.dataPath, file);

            IsVisible = kv.GetBool("visible", true);
            children = UIBuilder.Instance.BuildUIFromKeyValues(kv, out basex, out basey);
        }

        public CustomUI(KeyValues kv)
        {
            IsVisible = kv.GetBool("visible", true);
            children = UIBuilder.Instance.BuildUIFromKeyValues(kv, out basex, out basey);
        }

        protected virtual void Render()
        {
            if (IsVisible)
                foreach (BaseUIComponent c in children)
                    if (c.IsVisible)
                        c.Draw();
        }

        public BaseUIComponent GetComponent(string name)
        {
            foreach (BaseUIComponent c in children)
                if (c.Name == name)
                    return c;

            return null;
        }

        public BaseUIComponent GetComponentInChildren(string name)
        {
            foreach (BaseUIComponent c in children)
            {
                if (c.Name == name)
                    return c;
                else
                {
                    var component = c.GetComponentInChildren(name);

                    if (component != null) return component;
                }
            }

            return null;
        }

        protected Vector2 FitVector(Vector2 vec)
        {
            var ratiox = basex / (float)Screen.width;
            var ratioy = basey / (float)Screen.height;

            return new Vector2(vec.x * ratiox, vec.y * ratioy);
        }

        protected void Update(Vector2 mousePosition)
        {
            PreUpdate(mousePosition);
            OnUpdate(mousePosition);
            PostUpdate(mousePosition);
        }

        protected virtual void PreUpdate(Vector2 mousePosition) { }
        protected virtual void OnUpdate(Vector2 mousePosition) { }
        protected virtual void PostUpdate(Vector2 mousePosition) { }

        public virtual void Show()
        {
            IsVisible = true;
        }

        public virtual void Hide()
        {
            IsVisible = false;
        }

        public virtual void AddChild(BaseUIComponent c)
        {
            children.Add(c);
        }

        public virtual void AddChildren(params BaseUIComponent[] children)
        {
            foreach (var c in children)
                AddChild(c);
        }

        public void CallUpdate(Vector2 mouse)
        {
            if (!IsVisible) return;

            Update(mouse);
            Render();
        }
    }
}
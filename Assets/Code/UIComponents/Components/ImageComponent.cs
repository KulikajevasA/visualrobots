﻿using UnityEngine;
using FileSystem;

namespace UI.Components
{
    /// <summary>
    /// Image UI component.
    /// </summary>
    public class ImageComponent : BaseUIComponent
    {
        /// <summary>
        /// Registers UI component.
        /// </summary>
        public static void RegisterUIElement() { UIBuilder.AddTypeBind(typeof(ImageComponent)); }
        protected Texture2D texture;

        /// <summary>
        /// Parametrized constructor for image component.
        /// </summary>
        /// <param name="x">X position of UI component.</param>
        /// <param name="y">Y position of UI component.</param>
        /// <param name="w">Width of the UI component.</param>
        /// <param name="h">Height of the UI component.</param>
        /// <param name="visible">Is UI component visible.</param>
        /// <param name="name">Name of the UI component.</param>
        /// <param name="parent">Parent element of the UI component.</param>
        public ImageComponent(float x, float y, float w, float h, string texture, bool visible = false, string name = null, BaseUIComponent parent = null)
            : base(x, y, w, h, visible, name, parent)
        {
            Cache.Cache.Instance.PrecacheTexture(texture);
            this.texture = Cache.Cache.Instance.LoadTexture(texture);
        }

        /// <summary>
        /// Constructs image component from KeyValues.
        /// </summary>
        /// <param name="kv">KeyValues to construct from.</param>
        public ImageComponent(KeyValues kv)
            : base(kv)
        {
            var tex = kv.GetString("texture");

            Cache.Cache.Instance.PrecacheTexture(tex);
            this.texture = Cache.Cache.Instance.LoadTexture(tex);
        }

        /// <summary>
        /// Draw the component.
        /// </summary>
        protected override void OnDraw()
        {
            if (texture != null)
                GUI.DrawTexture(Bounds, texture);
        }

        /// <summary>
        /// Sets texture for image compoent.
        /// </summary>
        /// <param name="texture">Name of the texture to set.</param>
        public void SetTexture(string texture)
        {
            this.texture = Cache.Cache.Instance.LoadTexture(texture);
        }
    }
}

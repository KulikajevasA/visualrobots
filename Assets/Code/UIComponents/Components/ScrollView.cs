﻿using UnityEngine;
using FileSystem;
using RobotDesigner.Nodes;

namespace UI.Components
{
    /// <summary>
    /// Scroll UI component.
    /// </summary>
    public class ScrollView : BaseUIComponent
    {
        private Vector2 scrollPosition = Vector2.zero;
        private bool alwaysShowHorizontal, alwaysShowVertical;

        public static Rect ScrollSize = new Rect();

        /// <summary>
        /// Registers UI element.
        /// </summary>
        public static void RegisterUIElement() { UIBuilder.AddTypeBind(typeof(ScrollView)); }

        /// <summary>
        /// Parametrized constructor for scroll component.
        /// </summary>
        /// <param name="x">X position of UI component.</param>
        /// <param name="y">Y position of UI component.</param>
        /// <param name="w">Width of the UI component.</param>
        /// <param name="h">Height of the UI component.</param>
        /// <param name="visible">Is UI component visible.</param>
        /// <param name="name">Name of the UI component.</param>
        /// <param name="parent">Parent element of the UI component.</param>
        public ScrollView(float x, float y, float w, float h, bool visible = false, string name = null, BaseUIComponent parent = null)
            : base(x, y, w, h, visible, name, parent)
        {
            alwaysShowHorizontal = true;
            alwaysShowVertical = true;
        }

        /// <summary>
        /// Constructor for scroll from KeyValues.
        /// </summary>
        /// <param name="kv">KeyValues to construct from.</param>
        public ScrollView(KeyValues kv) : base(kv)
        {
            alwaysShowHorizontal = kv.GetBool("always_show_horizontal", false);
            alwaysShowVertical = kv.GetBool("always_show_vertical", true);
        }

        /// <summary>
        /// Draws scroll element.
        /// </summary>
        protected override void OnDraw()
        {
            ScrollSize.width = Screen.width / 6 * BaseNode.X_NODES;
            ScrollSize.height = Screen.height / 4 * BaseNode.Y_NODES;
            scrollPosition = GUI.BeginScrollView(Bounds, scrollPosition, ScrollSize, alwaysShowHorizontal, alwaysShowVertical);

            if (Children != null)
                foreach (BaseUIComponent c in Children)
                    if (c.IsVisible)
                        c.Draw();

            GUI.EndScrollView();
        }

        protected override void DrawChildren() { }
    }
}

﻿using UnityEngine;
using FileSystem;
using Designer;

namespace UI.Components
{
    /// <summary>
    /// Frame UI component representation.
    /// </summary>
    public class Frame : BaseUIComponent
    {

        private BoxNumericStyle boxNumericStyle;

        /// <summary>
        /// Registers UI component.
        /// </summary>
        public static void RegisterUIElement() { UIBuilder.AddTypeBind(typeof(Frame)); }

        /// <summary>
        /// Parametrized constructor for frame component.
        /// </summary>
        /// <param name="x">X position of UI component.</param>
        /// <param name="y">Y position of UI component.</param>
        /// <param name="w">Width of the UI component.</param>
        /// <param name="h">Height of the UI component.</param>
        /// <param name="visible">Is UI component visible.</param>
        /// <param name="name">Name of the UI component.</param>
        /// <param name="parent">Parent element of the UI component.</param>
        public Frame(float x, float y, float w, float h, bool visible = false, string name = null, BaseUIComponent parent = null)
            : base(x, y, w, h, visible, name, parent) {
            this.boxNumericStyle = new BoxNumericStyle();
        }

        /// <summary>
        /// Constructs frame from KeyValues.
        /// </summary>
        /// <param name="kv">KeyValues to construct from.</param>
        public Frame(KeyValues kv) : base(kv) { }

        /// <summary>
        /// Draws component.
        /// </summary>
        protected override void OnDraw()
        {
            GUI.Box(Bounds, GUIContent.none, boxNumericStyle.Style);
        }

        /// <summary>
        /// Creates scheme.
        /// </summary>
        protected override void CreateScheme()
        {
            Style = new GUIStyle(FuckUnity.UnityIsAFuckingCunt.Instance.box);
        }
    }
}

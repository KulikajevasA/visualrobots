﻿using System;

using UnityEngine;
using FileSystem;
using UI.Components.Input;

namespace UI.Components
{
    /// <summary>
    /// Label component for UI.
    /// </summary>
    public class Label : BaseUIComponent
    {
        /// <summary>
        /// Registers UI component.
        /// </summary>
        public static void RegisterUIElement() { UIBuilder.AddTypeBind(typeof(Label)); }

        /// <summary>
        /// Getter and setter for label caption.
        /// </summary>
        public string Text { get; set; }

        #region STYLE
        /// <summary>
        /// Getter and setter for used font.
        /// </summary>
        public Font Font
        {
            get
            {
                return Style.font;
            }
            set
            {
                try
                {
                    Style.font = value;
                }
                catch (Exception) { }
            }
        }

        /// <summary>
        /// Getter and setter for font size.
        /// </summary>
        public int FontSize
        {
            get
            {
                return Style.fontSize;
            }
            set
            {
                try
                {
                    Style.fontSize = value;
                }
                catch (Exception) { }
            }
        }

        /// <summary>
        /// Getter and setter for font style.
        /// </summary>
        public FontStyle FontStyle
        {
            get
            {
                return Style.fontStyle;
            }
            set
            {
                try
                {
                    Style.fontStyle = value;
                }
                catch (Exception) { }
            }
        }

        /// <summary>
        /// Getter and setter for foreground color.
        /// </summary>
        public Color Foreground
        {
            get
            {
                return Style.normal.textColor;
            }
            set
            {
                Style.normal.textColor = value;
            }
        }

        /// <summary>
        /// Getter and setter for text alignment.
        /// </summary>
        public TextAnchor TextAlignment
        {
            get
            {
                return Style.alignment;
            }

            set
            {
                Style.alignment = value;
            }
        }

        #endregion

        /// <summary>
        /// Parametrized constructor for label component.
        /// </summary>
        /// <param name="x">X position of UI component.</param>
        /// <param name="y">Y position of UI component.</param>
        /// <param name="w">Width of the UI component.</param>
        /// <param name="h">Height of the UI component.</param>
        /// <param name="visible">Is UI component visible.</param>
        /// <param name="name">Name of the UI component.</param>
        /// <param name="parent">Parent element of the UI component.</param>
        public Label(float x, float y, float w, float h, string text,
            bool visible = false, string name = null, BaseUIComponent parent = null)
            : base(x, y, w, h, visible, name, parent)
        {
            Text = text;
        }

        /// <summary>
        /// Constructor for KeyValues label element.
        /// </summary>
        /// <param name="kv">KeyValues label element.</param>
        public Label(KeyValues kv) : base(kv)
        {
            Text = kv.GetString("text");
        }

        #region SCHEME
        /// <summary>
        /// Applies scheme for label.
        /// </summary>
        /// <param name="kv">KeyValues for scheme data.</param>
        protected override void ApplyScheme(KeyValues kv)
        {
            Font = Resources.Load(kv.GetString("font", "ui/fonts/arial")) as Font;

            var fontSize = kv.GetInt("font_size");

            if (fontSize > 0)
                FontSize = Mathf.CeilToInt(Utilities.Utils_UI.ResizeVector(0, fontSize).y);
            else FontSize = Mathf.CeilToInt(Bounds.height);

            switch (kv.GetString("font_style", "normal"))
            {
                default:
                case "normal":
                    FontStyle = UnityEngine.FontStyle.Normal;
                    break;
                case "bold":
                    FontStyle = UnityEngine.FontStyle.Bold;
                    break;
                case "italic":
                    FontStyle = UnityEngine.FontStyle.Italic;
                    break;
                case "bold_italic":
                    FontStyle = UnityEngine.FontStyle.BoldAndItalic;
                    break;
            }

            switch (kv.GetString("text_alignment", "mid_center"))
            {
                default:
                case "up_left":
                    TextAlignment = TextAnchor.UpperLeft;
                    break;
                case "up_center":
                    TextAlignment = TextAnchor.UpperCenter;
                    break;
                case "up_right":
                    TextAlignment = TextAnchor.UpperRight;
                    break;
                case "mid_left":
                    TextAlignment = TextAnchor.MiddleLeft;
                    break;
                case "mid_center":
                    TextAlignment = TextAnchor.MiddleCenter;
                    break;
                case "mid_right":
                    TextAlignment = TextAnchor.LowerRight;
                    break;
                case "low_left":
                    TextAlignment = TextAnchor.LowerLeft;
                    break;
                case "low_center":
                    TextAlignment = TextAnchor.LowerCenter;
                    break;
                case "low_right":
                    TextAlignment = TextAnchor.LowerRight;
                    break;
            }

            Foreground = Utilities.Utils_Other.ColorFromString(kv.GetString("foreground"));
        }
        #endregion

        /// <summary>
        /// Draws the label.
        /// </summary>
        protected override void OnDraw()
        {
            GUI.Label(Bounds, Text, Style);
        }

        /// <summary>
        /// Creates scheme data.
        /// </summary>
        protected override void CreateScheme()
        {
            Style = new GUIStyle(FuckUnity.UnityIsAFuckingCunt.Instance.input);
            Style.alignment = TextAnchor.UpperLeft;
        }
    }
}

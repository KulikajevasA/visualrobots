﻿using UnityEngine;

namespace UI.Components.Input
{
    /// <summary>
    /// Input field UI component representation.
    /// </summary>
    public class InputField : BaseUIComponent
    {
        /// <summary>
        /// Registers UI component.
        /// </summary>
        public static void RegisterUIElement() { UIBuilder.AddTypeBind(typeof(InputField)); }

        /// <summary>
        /// Getter and setter for input caption.
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Getter and setter for max input length. If less or equals zero field has no limit.
        /// </summary>
        public int MaxLength { get; set; }

        /// <summary>
        /// Parametrized constructor for input component.
        /// </summary>
        /// <param name="x">X position of UI component.</param>
        /// <param name="y">Y position of UI component.</param>
        /// <param name="w">Width of the UI component.</param>
        /// <param name="h">Height of the UI component.</param>
        /// <param name="visible">Is UI component visible.</param>
        /// <param name="name">Name of the UI component.</param>
        /// <param name="parent">Parent element of the UI component.</param>
        public InputField(float x, float y, float w, float h, string text, bool visible = false, string name = null, BaseUIComponent parent = null)
            : base(x, y, w, h, visible, name, parent)
        {
            Text = text;
            MaxLength = 20;
        }

        /// <summary>
        /// Draws the element.
        /// </summary>
        protected override void OnDraw()
        {
            if (IsEnabled)
                Text = GUI.TextField(Bounds, Text, Style);
            else
                GUI.TextField(Bounds, Text, Style);

            if (MaxLength > 0)
            {
                if (Text.Length > MaxLength)
                    Text = Text.Substring(0, MaxLength);
            }
        }

        /// <summary>
        /// Creates scheme.
        /// </summary>
        protected override void CreateScheme()
        {
            Style = new GUIStyle(FuckUnity.UnityIsAFuckingCunt.Instance.label);
            Style.alignment = TextAnchor.UpperLeft;
        }
    }
}

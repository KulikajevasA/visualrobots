﻿using Designer;
using System.Text.RegularExpressions;
using UnityEngine;

namespace UI.Components.Input
{
    /// <summary>
    /// Variable input name UI element.
    /// </summary>
    public class VariableNameField : BaseUIComponent
    {

        private InputStyle inputStyle;

        /// <summary>
        /// Registers UI element.
        /// </summary>
        public static void RegisterUIElement() { UIBuilder.AddTypeBind(typeof(VariableNameField)); }

        private string value;

        /// <summary>
        /// Max length of the variable name.
        /// </summary>
        public int MaxLenght { get; set; }

        /// <summary>
        /// Style of the disabled button.
        /// </summary>
        public GUIStyle StyleDisabled { get; private set; }

        /// <summary>
        /// Getter and setter for variable name.
        /// </summary>
        public string Value
        {
            get
            {
                if (value == null || value == string.Empty)
                    value = RobotVL.Utilities.Utils_Language.GetAnonymousName();

                return value.ToString();
            }
            set
            {
                this.value = value.ToString(System.Globalization.CultureInfo.InvariantCulture);
            }
        }

        /// <summary>
        /// Parametrized constructor for variable name field component.
        /// </summary>
        /// <param name="x">X position of UI component.</param>
        /// <param name="y">Y position of UI component.</param>
        /// <param name="w">Width of the UI component.</param>
        /// <param name="h">Height of the UI component.</param>
        /// <param name="visible">Is UI component visible.</param>
        /// <param name="name">Name of the UI component.</param>
        /// <param name="parent">Parent element of the UI component.</param>
        public VariableNameField(float x, float y, float w, float h, string text, bool visible = false, string name = null, BaseUIComponent parent = null)
            : base(x, y, w, h, visible, name, parent)
        {
            this.inputStyle = new InputStyle();
            this.value = text;
            MaxLenght = 15;
        }

        /// <summary>
        /// Draws the component.
        /// </summary>
        protected override void OnDraw()
        {
            var text = GUI.TextField(Bounds, value, IsEnabled ? inputStyle.Style : StyleDisabled);

            if (!IsEnabled)
                return;

            if (text == string.Empty)
            {
                value = string.Empty;
                return;
            }
            else
            {
                string pattern = @"^[a-zA-Z]([a-zA-Z0-9]*)?$";
                var result = Regex.Match(text, pattern);
                if (result != null && result.ToString() != string.Empty)
                    value = result.ToString();

                text = value;
            }

            if (value.Length > MaxLenght)
                value = text.Substring(0, MaxLenght);
        }

        /// <summary>
        /// Creates scheme.
        /// </summary>
        protected override void CreateScheme()
        {
            Style = new GUIStyle(FuckUnity.UnityIsAFuckingCunt.Instance.label);
            Style.alignment = TextAnchor.UpperLeft;

            StyleDisabled = new GUIStyle(FuckUnity.UnityIsAFuckingCunt.Instance.labelDsiabled);
            Style.alignment = TextAnchor.UpperLeft;
        }
    }
}

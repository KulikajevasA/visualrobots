﻿using Designer;
using System.Text.RegularExpressions;
using UnityEngine;

namespace UI.Components.Input
{
    /// <summary>
    /// Numeric input field.
    /// </summary>
    public class NumericInputFieldMotor : BaseUIComponent
    {

        private InputStyleMotor inputStyleMotor;

        /// <summary>
        /// Registers UI component.
        /// </summary>
        public static void RegisterUIElement() { UIBuilder.AddTypeBind(typeof(NumericInputFieldMotor)); }

        private string value;

        /// <summary>
        /// Style of the disabled button.
        /// </summary>
        public GUIStyle StyleDisabled { get; private set; }

        /// <summary>
        /// Getter and setter for value.
        /// </summary>
        public double Value
        {
            get
            {
                double @out = 0;
                if (!double.TryParse(value, out @out))
                    return 0;
                return @out;
            }
            set
            {
                this.value = value.ToString(System.Globalization.CultureInfo.InvariantCulture);
            }
        }

        /// <summary>
        /// Parametrized constructor for numeric input field component.
        /// </summary>
        /// <param name="x">X position of UI component.</param>
        /// <param name="y">Y position of UI component.</param>
        /// <param name="w">Width of the UI component.</param>
        /// <param name="h">Height of the UI component.</param>
        /// <param name="visible">Is UI component visible.</param>
        /// <param name="name">Name of the UI component.</param>
        /// <param name="parent">Parent element of the UI component.</param>
        public NumericInputFieldMotor(float x, float y, float w, float h, string text, bool visible = false, string name = null, BaseUIComponent parent = null)
            : base(x, y, w, h, visible, name, parent)
        {
            this.inputStyleMotor = new InputStyleMotor();
            this.value = text;
        }

        /// <summary>
        /// Draws element.
        /// </summary>
        protected override void OnDraw()
        {
            var text = GUI.TextField(Bounds, value, IsEnabled ? inputStyleMotor.Style : StyleDisabled);

            if (!IsEnabled)
                return;

            if (text == string.Empty)
            {
                value = string.Empty;
                return;
            }

            if (text != "0" || text != null || text != string.Empty)
            {
                string pattern = @"^-?[0-9]*(?:\.[0-9]*)?$";
                var result = Regex.Match(text, pattern);
                if (result != null && result.ToString() != string.Empty)
                    value = result.ToString();

                text = value;
            }

            if (value.Length > 10)
                value = text.Substring(0, 10);
        }

        /// <summary>
        /// Creates scheme.
        /// </summary>
        protected override void CreateScheme()
        {
            Style = new GUIStyle(FuckUnity.UnityIsAFuckingCunt.Instance.label);
            Style.alignment = TextAnchor.UpperLeft;

            StyleDisabled = new GUIStyle(FuckUnity.UnityIsAFuckingCunt.Instance.labelDsiabled);
            Style.alignment = TextAnchor.UpperLeft;
        }
    }
}

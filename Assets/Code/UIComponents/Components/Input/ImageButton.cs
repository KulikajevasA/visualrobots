﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace UI.Components.Input
{
    /// <summary>
    /// UI element for button.
    /// </summary>
    public class ImageButton : BaseButton
    {
        private const string PATH_RESOURCES = "UI/{0}";
        private const string PATH_RESOURCES_DISABLED = "UI/disabled/{0}";

        private Texture texture;
        private Texture textureDisabled;

        /// <summary>
        /// Registers the element.
        /// </summary>
        public static void RegisterUIElement() { UIBuilder.AddTypeBind(typeof(Button)); }

        /// <summary>
        /// Parametrized constructor for image button component.
        /// </summary>
        /// <param name="x">X position of UI component.</param>
        /// <param name="y">Y position of UI component.</param>
        /// <param name="w">Width of the UI component.</param>
        /// <param name="h">Height of the UI component.</param>
        /// <param name="resource">Resource name for button.</param>
        /// <param name="visible">Is UI component visible.</param>
        /// <param name="name">Name of the UI component.</param>
        /// <param name="parent">Parent element of the UI component.</param>
        public ImageButton(float x, float y, float w, float h, string resource, bool visible = false, string name = null, BaseUIComponent parent = null) : base(x, y, w, h, visible, name, parent)
        {
            Cache.Cache.Instance.PrecacheTexture(string.Format(PATH_RESOURCES, resource));
            this.texture = Cache.Cache.Instance.LoadTexture(string.Format(PATH_RESOURCES, resource));

            Cache.Cache.Instance.PrecacheTexture(string.Format(PATH_RESOURCES_DISABLED, resource));
            this.textureDisabled = Cache.Cache.Instance.LoadTexture(string.Format(PATH_RESOURCES_DISABLED, resource));

            this.textureDisabled = this.textureDisabled ?? this.texture;
        }

        /// <summary>
        /// Draws the button.
        /// </summary>
        protected override void OnDraw()
        {
            if (this.texture && this.textureDisabled)
            {
                if (GUI.Button(Bounds, IsEnabled ? texture : textureDisabled, Style))
                    DoCommand();
            }
            else
            {
                if (GUI.Button(Bounds, "<<<Missing>>>", Style))
                    DoCommand();
            }
        }
    }
}

﻿using System;

using FileSystem;
using UnityEngine;
using RobotDesigner.Nodes;

namespace UI.Components.Input
{
    /// <summary>
    /// Represents directional button UI component.
    /// </summary>
    public class DirectionalButton : Label
    {
        /// <summary>
        /// Available button directions.
        /// </summary>
        public enum Direction
        {
            East = 0,
            South,
            West,
            North,

            MAX_DIRECTIONS
        }

        private Direction direction;

        public event ButtonInfo OnCommand;
        public delegate void ButtonInfo(DirectionalButton caller, Direction direction, BaseNode node, params object[] @params);
        private object[] parameters;

        /// <summary>
        /// Registers UI component.
        /// </summary>
        public new static void RegisterUIElement() { UIBuilder.AddTypeBind(typeof(DirectionalButton)); }

        /// <summary>
        /// Invokes OnCommand delegates.
        /// </summary>
        private void DoCommand()
        {
            if (!IsEnabled) return;

            ButtonInfo handler = OnCommand;
            if (null != handler) handler(this, direction, null, parameters);

            if (GUI.Button(Bounds, Text, Style)) DoCommand();
        }

        /// <summary>
        /// Sets direction of the button.
        /// </summary>
        /// <param name="direction">Button direction.</param>
        public void SetDirection(Direction direction)
        {
            this.direction = direction;
        }

        /// <summary>
        /// Sets button delegate invokation parameters.
        /// </summary>
        /// <param name="params">Invokation parameters.</param>
        public void SetParameters(params object[] @params)
        {
            parameters = @params;
        }

        /// <summary>
        /// Parametrized constructor for directional button component.
        /// </summary>
        /// <param name="x">X position of UI component.</param>
        /// <param name="y">Y position of UI component.</param>
        /// <param name="w">Width of the UI component.</param>
        /// <param name="h">Height of the UI component.</param>
        /// <param name="visible">Is UI component visible.</param>
        /// <param name="name">Name of the UI component.</param>
        /// <param name="parent">Parent element of the UI component.</param>
        public DirectionalButton(float x, float y, float w, float h, string label, bool visible = false, string name = null, BaseUIComponent parent = null)
            : base(x, y, w, h, label, visible, name, parent) { }

        /// <summary>
        /// Constructs directional button from KeyValues.
        /// </summary>
        /// <param name="kv">KeyValues to construct from.</param>
        public DirectionalButton(KeyValues kv) : base(kv) { }

        /// <summary>
        /// Creates scheme.
        /// </summary>
        protected override void CreateScheme()
        {
            Style = new GUIStyle(FuckUnity.UnityIsAFuckingCunt.Instance.button);
        }

        /// <summary>
        /// Draws the element.
        /// </summary>
        protected override void OnDraw()
        {
            if (GUI.Button(Bounds, Text, Style)) DoCommand();
        }

        /// <summary>
        /// Converts direction to integer.
        /// </summary>
        /// <param name="dir"></param>
        /// <returns>Direction as integer.</returns>
        public static int FromDirection(Direction dir)
        {
            if ((int)dir >= (int)Direction.MAX_DIRECTIONS)
                throw new IndexOutOfRangeException();

            return (int)dir;
        }

        /// <summary>
        /// Returns the opposite direction.
        /// </summary>
        /// <param name="dir">Direction.</param>
        /// <returns>Opposite direction.</returns>
        public static Direction GetOppositeDirection(Direction dir)
        {
            switch (dir)
            {
                case Direction.East:
                    return Direction.West;
                case Direction.West:
                    return Direction.East;
                case Direction.North:
                    return Direction.South;
                case Direction.South:
                    return Direction.North;
                default:
                    throw new IndexOutOfRangeException();
            }
        }

        /// <summary>
        /// Converts integer to direction.
        /// </summary>
        /// <param name="dir">Direction to convert.</param>
        /// <returns>Direction as enumerator.</returns>
        public static Direction ToDirection(int dir)
        {
            if (dir < 0 || dir >= (int)Direction.MAX_DIRECTIONS)
                throw new IndexOutOfRangeException();

            return (Direction)dir;
        }
    }
}
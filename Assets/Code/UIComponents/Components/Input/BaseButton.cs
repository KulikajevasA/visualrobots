﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FileSystem;

namespace UI.Components.Input
{
    public abstract class BaseButton : BaseUIComponent
    {
        public event ButtonInfo OnCommand;
        public delegate void ButtonInfo(BaseButton caller, params object[] @params);
        protected object[] parameters;

        protected BaseButton(float x, float y, float w, float h, bool visible = false, string name = null, BaseUIComponent parent = null) : base(x, y, w, h, visible, name, parent)
        {
        }

        /// <summary>
        /// Invokes OnCommand delegates.
        /// </summary>
        protected void DoCommand()
        {
            if (!IsEnabled) return;

            var handler = OnCommand;
            if (handler != null) handler(this, parameters);
        }

        /// <summary>
        /// Sets button invocation parameters.
        /// </summary>
        /// <param name="params">Parameters to invoke delegates with.</param>
        public void SetParameters(params object[] @params)
        {
            parameters = @params;
        }
    }
}

﻿using System;
using Designer;
using FileSystem;
using UnityEngine;

namespace UI.Components.Input
{
    /// <summary>
    /// UI element for button.
    /// </summary>
    public class Button : BaseButton
    {
        private ButtonStyle buttonStyle;

        /// <summary>
        /// Getter and setter for button caption.
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Registers the element.
        /// </summary>
        public static void RegisterUIElement() { UIBuilder.AddTypeBind(typeof(Button)); }

        /// <summary>
        /// Style of the disabled button.
        /// </summary>
        public GUIStyle StyleDisabled { get; private set; }


        /// <summary>
        /// Parametrized constructor for button component.
        /// </summary>
        /// <param name="x">X position of UI component.</param>
        /// <param name="y">Y position of UI component.</param>
        /// <param name="w">Width of the UI component.</param>
        /// <param name="h">Height of the UI component.</param>
        /// <param name="label">Button label.</param>
        /// <param name="visible">Is UI component visible.</param>
        /// <param name="name">Name of the UI component.</param>
        /// <param name="parent">Parent element of the UI component.</param>
        public Button(float x, float y, float w, float h, string label, bool visible = false, string name = null, BaseUIComponent parent = null)
            : base(x, y, w, h, visible, name, parent)
        {
            this.buttonStyle = new ButtonStyle();
            this.Text = label;
        }

        /// <summary>
        /// Draws the button.
        /// </summary>
        protected override void OnDraw()
        {
            if (GUI.Button(Bounds, Text, IsEnabled ? buttonStyle.Style : StyleDisabled))
                DoCommand();
        }

        /// <summary>
        /// Creates scheme.
        /// </summary>
        protected override void CreateScheme()
        {
            Style = new GUIStyle(FuckUnity.UnityIsAFuckingCunt.Instance.button);
            StyleDisabled = new GUIStyle(FuckUnity.UnityIsAFuckingCunt.Instance.buttonDisabled);
        }
    }
}
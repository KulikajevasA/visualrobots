﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UI.Components.Input;
using UnityEngine;

namespace UI.Components
{
    public class FileBrowserDialog : BaseUIComponent
    {
        public enum DialogMode_T
        {
            MODE_FILE,
            MODE_DIRECTORY
        }

        public enum DialogAction_T
        {
            MODE_SAVE,
            MODE_OPEN
        }

        public struct DialogSettings
        {
            public DialogMode_T Mode { get; private set; }
            public DialogAction_T Action { get; private set; }
            public string FileSearchPattern { get; private set; }

            public DialogSettings(DialogMode_T mode, DialogAction_T action, string pattern = "")
            {
                this.Mode = mode;
                this.Action = action;
                this.FileSearchPattern = pattern;
            }
        }

        public event DialogInfo OnFileSelected;
        public delegate void DialogInfo(FileBrowserDialog caller, string directory, string file);

        public event DialogCanceled OnDialogCanceled;
        public delegate void DialogCanceled(FileBrowserDialog caller);

        private Dictionary<string, Vector3> scrollPositions = new Dictionary<string, Vector3>();

        private GUIStyle styleFileFrame;
        private GUIStyle styleFileList;
        private GUIStyle styleDirectoryPath;
        private GUIStyle styleButton;

        private Rect boundsFileFrame = new Rect();
        private Rect boundsFileName = new Rect();
        private Rect boundsFileList = new Rect();
        private Rect boundsScroll = new Rect();
        private Rect boundsDirectory = new Rect();
        private Rect boundsButtonAction = new Rect();
        private Rect boundsButtonCancel = new Rect();

        private string path = Application.persistentDataPath;

        private InputField inputFileName = new InputField(0, 0, 0, 0, string.Empty, true);

        private Vector3 scrollPosition = new Vector3();

        private List<FileSystemInfo> items = new List<FileSystemInfo>();

        public static void RegisterUIElement() { UIBuilder.AddTypeBind(typeof(FileBrowserDialog)); }

        public DialogSettings Settings { get; private set; }

        public string FileName
        {
            get
            {
                return inputFileName.Text;
            }
            set
            {
                inputFileName.Text = value;
            }
        }

        public FileBrowserDialog(DialogSettings settings, float x, float y, float w, float h, bool visible = true, string name = null, BaseUIComponent parent = null) : base(x, y, w, h, visible, name, parent)
        {
            this.Settings = settings;

            this.inputFileName.MaxLength = 255;
            this.inputFileName.IsEnabled = true;

            AddChild(this.inputFileName);
            RepopulateList();
        }

        public void ShowDialog()
        {
            this.IsVisible = true;
            RepopulateList();
        }

        public void HideDialog()
        {
            this.IsVisible = false;
        }

        protected override void OnDraw()
        {
            GUI.Box(Bounds, GUIContent.none, Style);
            GUI.Box(boundsFileName, GUIContent.none, styleFileFrame);
            GUI.Box(boundsFileList, GUIContent.none, styleFileList);
            GUI.Label(boundsDirectory, this.path, styleDirectoryPath);

            if (GUI.Button(boundsButtonAction, Settings.Action == DialogAction_T.MODE_OPEN ? "Open" : "Save", styleButton))
            {
                this.IsVisible = false;

                var handler = OnFileSelected;
                if (handler != null) handler(this, this.path, this.inputFileName.Text);
            }

            if (GUI.Button(boundsButtonCancel, "Cancel", styleButton))
            {
                this.IsVisible = false;

                var handler = OnDialogCanceled;
                if (handler != null) handler(this);
            }

            scrollPosition = GUI.BeginScrollView(boundsFileList, scrollPosition, boundsScroll, false, true);

            if (GUI.Button(new Rect(0, 0, boundsFileFrame.width, boundsFileFrame.height), "..", Style))
            {
                this.scrollPositions[this.path] = scrollPosition;
                path = Path.GetFullPath(path + "/..");
                RepopulateList();
            }

            for (int i = 0; i < this.items.Count; i++)
            {
                if (GUI.Button(new Rect(0, boundsFileFrame.height * (i + 1), boundsFileFrame.width, boundsFileFrame.height), items[i].Name, Style))
                {
                    if (items[i] is DirectoryInfo)
                    {
                        this.scrollPositions[this.path] = scrollPosition;
                        path = (items[i] as DirectoryInfo).FullName;
                        RepopulateList();

                        if (Settings.Mode == DialogMode_T.MODE_DIRECTORY)
                            SelectItem(items[i]);
                    }
                    else if (items[i] is FileInfo)
                    {
                        SelectItem(items[i]);
                    }
                }
            }

            GUI.EndScrollView();
        }

        public void SelectItem(FileSystemInfo file)
        {
            inputFileName.Text = file.Name;
        }

        public override void SetBounds(float x, float y, float w, float h, bool resize = true)
        {
            base.SetBounds(x, y, w, h, resize);

            var width5Percent = w * 0.05f;
            var height5Percent = h * 0.05f;
            var width10Percent = w * 0.1f;
            var height20Percent = h * 0.2f;
            var height55Percent = h * 0.55f;

            boundsFileFrame.Set(x + width5Percent, y + height5Percent, w - width10Percent, height20Percent);
            boundsFileList.Set(x + width5Percent, y + height20Percent + height5Percent * 3, w - width10Percent, height55Percent);
            boundsFileName.Set(x + width5Percent, y + height5Percent, (w / 2) - width10Percent, height20Percent);
            boundsDirectory.Set(x + width5Percent, y + height20Percent + height5Percent * 1.5f, w - width10Percent, height5Percent * 1.5f);

            inputFileName.SetBounds(width5Percent, height5Percent, w - width10Percent, height20Percent);

            boundsButtonAction.Set(x + width5Percent + (w / 2), y + height5Percent, ((w / 2) - width10Percent) / 2 - width5Percent, height20Percent);
            boundsButtonCancel.Set(x + width5Percent + (w / 2) + width5Percent + ((w / 2)) / 2 - width5Percent, y + height5Percent, ((w / 2) - width10Percent) / 2 - width5Percent, height20Percent);
        }

        protected override void CreateScheme()
        {
            Style = new GUIStyle(FuckUnity.UnityIsAFuckingCunt.Instance.box);
            styleFileFrame = new GUIStyle(FuckUnity.UnityIsAFuckingCunt.Instance.box);
            styleFileList = new GUIStyle(FuckUnity.UnityIsAFuckingCunt.Instance.box);
            styleDirectoryPath = new GUIStyle(FuckUnity.UnityIsAFuckingCunt.Instance.label);
            styleButton = new GUIStyle(FuckUnity.UnityIsAFuckingCunt.Instance.button);
        }

        private void RepopulateList()
        {
            this.items.Clear();

            var directoryInfo = new DirectoryInfo(this.path);

            var itemHeight = boundsFileFrame.height;

            switch (Settings.Mode)
            {
                case DialogMode_T.MODE_FILE:
                    this.items.AddRange(directoryInfo.GetDirectories());
                    this.items.AddRange(directoryInfo.GetFiles(Settings.FileSearchPattern));
                    break;
                case DialogMode_T.MODE_DIRECTORY:
                    this.items.AddRange(directoryInfo.GetDirectories());
                    break;
                default:
                    break;
            }

            boundsScroll.Set(0, 0, boundsFileList.width, itemHeight * (items.Count+1.5f));

            if (!this.scrollPositions.TryGetValue(this.path, out this.scrollPosition))
                this.scrollPosition = Vector3.zero;
        }
    }
}

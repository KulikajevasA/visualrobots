﻿using FileSystem;

namespace UI.Components
{
    /// <summary>
    /// Used for grouping elements. Has no bounds or visual representation.
    /// </summary>
    public class Holder : BaseUIComponent
    {
        /// <summary>
        /// Registers UI component.
        /// </summary>
        public static void RegisterUIElement() { UIBuilder.AddTypeBind(typeof(Holder)); }

        /// <summary>
        /// Holder has no visual representation, however OnDraw must be overriden because BaseUIComponent is now abstract.
        /// </summary>
        protected override void OnDraw() { }

        /// <summary>
        /// Parametrized constructor for holder component.
        /// </summary>
        /// <param name="visible">Is UI component visible.</param>
        /// <param name="name">Name of the UI component.</param>
        /// <param name="parent">Parent element of the UI component.</param>
        public Holder(bool visible = false, string name = null, BaseUIComponent parent = null)
            : base(0, 0, 0, 0, visible, name, parent) { }

        /// <summary>
        /// Constructor from KeyValues.
        /// </summary>
        /// <param name="kv">KeyValues to construct from.</param>
        public Holder(KeyValues kv) : base(kv) { }
    }
}

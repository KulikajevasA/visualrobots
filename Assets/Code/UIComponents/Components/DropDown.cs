﻿using UnityEngine;
using FileSystem;
using System;

namespace UI.Components
{
    [Obsolete]
    public class DropDown : BaseUIComponent
    {
        public static void RegisterUIElement() { UIBuilder.AddTypeBind(typeof(DropDown)); }
        public string Text { get; set; }

        public DropDown(float x, float y, float w, float h, string text, bool visible = false, string name = null, BaseUIComponent parent = null)
                : base(x, y, w, h, visible, name, parent)
        {
            Text = text;
        }

        public DropDown(KeyValues kv)
                : base(kv)
        {
            Text = kv.GetString("text");
        }

        protected override void OnDraw()
        {
            GUI.Box(Bounds, Text, Style);

            GUI.BeginGroup(Bounds);
            /*GUILayout.Label("Popup Options Example", EditorStyles.boldLabel);
            toggle1 = EditorGUILayout.Toggle("Toggle 1", toggle1);
            toggle2 = EditorGUILayout.Toggle("Toggle 2", toggle2);
            toggle3 = EditorGUILayout.Toggle("Toggle 3", toggle3);*/
            GUI.EndGroup();


            /*var forward = cameraTarget.transform.forward;
            var forward2d = new Vector2(forward.x, forward.z);
            var nearbyObjects = Physics.OverlapSphere(localPlayer.transform.position, 128);
            var angle = Vector2.Angle(Vector2.up, forward2d);

            angle = Vector3.Cross(Vector2.up, forward2d).z > 0 ? 360 - angle : angle;

            GUI.Box(Bounds, GUIContent.none, Style);

            GUI.BeginGroup(Bounds);
            GUIUtility.RotateAroundPivot(angle, playerPosition);
            GUI.color = localPlayer.playerRenderer.renderer.material.color;

            //Draw player
            GUI.DrawTexture(playerRect, starTexture);



            //Draw pickups
            foreach (var obj in nearbyObjects)
                if (obj.tag == "Pickup")
                {
                    var pickup = obj.GetComponent<BasePickup>();
                    var type = pickup == null ? Pickup_T.UNKNOWN : pickup.PickupType;

                    var dif = localPlayer.transform.position - obj.transform.position;
                    var pickupRect = new Rect(dif.x + playerPosition.x - 4, dif.z + playerPosition.y - 4, 8, 8);

                    if (type == BasePickup.Pickup_T.UNKNOWN || type == BasePickup.Pickup_T.POWERUP)
                    {
                        GUI.color = Color.white;
                        GUI.DrawTexture(pickupRect, powerupTexture);
                    }
                    else
                    {
                        GUI.color = obj.renderer.material.color;
                        GUI.DrawTexture(pickupRect, starTexture);
                    }
                }
                else if (obj.tag == "Player")
                {
                    var player = obj.GetComponent<BasePlayer>();

                    if (player.networkView.isMine) continue;

                    var dif = localPlayer.transform.position - obj.transform.position;
                    var pickupRect = new Rect(dif.x + playerPosition.x - 4, dif.z + playerPosition.y - 4, 8, 8);

                    GUI.color = player.playerRenderer.renderer.material.color;
                    GUI.DrawTexture(pickupRect, starTexture);
                }

            GUIUtility.RotateAroundPivot(-angle, playerPosition);
            GUI.EndGroup();

            base.OnDraw();*/
        }

        protected override void CreateScheme()
        {
            Style = new GUIStyle(FuckUnity.UnityIsAFuckingCunt.Instance.box);
            Style.alignment = TextAnchor.UpperLeft;
        }


    }
}
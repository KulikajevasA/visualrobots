﻿using System.Collections.Generic;

using UnityEngine;
using FileSystem;

namespace UI
{
    /// <summary>
    /// Base UI component.
    /// </summary>
    public abstract class BaseUIComponent
    {
        public static readonly Rect ViewPortSize = new Rect(0, 0, Screen.width, Screen.height);

        /// <summary>
        /// X position of UI component.
        /// </summary>
        public float X { get; private set; }

        /// <summary>
        /// Y position of UI component.
        /// </summary>
        public float Y { get; private set; }

        /// <summary>
        /// Width of UI component.
        /// </summary>
        public float W { get; private set; }

        /// <summary>
        /// Height of UI component.
        /// </summary>
        public float H { get; private set; }

        /// <summary>
        /// Getter and setter for UI component name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Getter and setter for UI component visibility.
        /// </summary>
        public bool IsVisible { get; set; }

        /// <summary>
        /// Getter and setter for UI component enabled state.
        /// </summary>
        public bool IsEnabled { get; set; }

        /// <summary>
        /// Bounds of the UI component.
        /// </summary>
        public Rect Bounds { get; private set; }

        /// <summary>
        /// UI component parent getter and setter.
        /// </summary>
        public BaseUIComponent Parent { get; set; }

        /// <summary>
        /// Getter and setter for UI component children.
        /// </summary>
        public List<BaseUIComponent> Children { get; set; }

        /// <summary>
        /// Getter and setter for UI component children.
        /// </summary>
        public Color Color { get; set; }

        /// <summary>
        /// UI component style.
        /// </summary>
        public GUIStyle Style { get; set; }

        private List<BaseUIComponent> childrenToAdd;
        private List<BaseUIComponent> childrenToRemove;

        #region ANIMATION
        /// <summary>
        /// Getter and setter for UI component animation state.
        /// </summary>
        public bool IsAnimating { get; protected set; }
        private float animationCycle;
        private float nextAnimation;
        private int frames;
        private int currentFrame;
        private bool incrementingFrames;
        private string animation;
        #endregion

        /// <summary>
        /// Basic constructor for UI component.
        /// </summary>
        protected BaseUIComponent() { }

        /// <summary>
        /// Parametrized constructor for UI component.
        /// </summary>
        /// <param name="x">X position of UI component.</param>
        /// <param name="y">Y position of UI component.</param>
        /// <param name="w">Width of the UI component.</param>
        /// <param name="h">Height of the UI component.</param>
        /// <param name="visible">Is UI component visible.</param>
        /// <param name="name">Name of the UI component.</param>
        /// <param name="parent">Parent element of the UI component.</param>
        protected BaseUIComponent(float x, float y, float w, float h, bool visible = false, string name = null, BaseUIComponent parent = null)
        {
            this.Children = new List<BaseUIComponent>();

            SetBounds(x, y, w, h);
            this.Name = name;
            this.IsVisible = visible;
            this.Parent = parent;

            CreateScheme();
            Color = Color.white;
        }

        /// <summary>
        /// Adds a child to the UI component.
        /// </summary>
        /// <param name="child">Child to add.</param>
        public void AddChild(BaseUIComponent child)
        {
            if (childrenToAdd == null)
                childrenToAdd = new List<BaseUIComponent>();

            childrenToAdd.Add(child);
        }

        /// <summary>
        /// Adds children to UI component.
        /// </summary>
        /// <param name="children">Children to add.</param>
        public void AddChildren(params BaseUIComponent[] children)
        {
            if (childrenToAdd == null)
                childrenToAdd = new List<BaseUIComponent>();

            childrenToAdd.AddRange(children);
        }

        /// <summary>
        /// Removes child from UI component.
        /// </summary>
        /// <param name="child">Child to remove.</param>
        public void RemoveChild(BaseUIComponent child)
        {
            if (childrenToRemove == null)
                childrenToRemove = new List<BaseUIComponent>();

            childrenToRemove.Add(child);
        }

        /// <summary>
        /// Rempoves children from UI component.
        /// </summary>
        /// <param name="children">Children to remove.</param>
        public void RemoveChildren(params BaseUIComponent[] children)
        {
            if (childrenToRemove == null)
                childrenToRemove = new List<BaseUIComponent>();

            childrenToRemove.AddRange(children);
        }

        /// <summary>
        /// Constructs UI component from KeyValues.
        /// </summary>
        /// <param name="kv"></param>
        protected BaseUIComponent(KeyValues kv)
        {
            Color = Color.white;
            float x = kv.GetFloat("posx");
            float y = kv.GetFloat("posy");
            float w = kv.GetFloat("width");
            float h = kv.GetFloat("height");
            IsVisible = kv.GetBool("visible", true);
            IsEnabled = kv.GetBool("enabled", true);

            SetBounds(x, y, w, h);

            CreateScheme();
            ApplyScheme(kv);
        }

        /// <summary>
        /// Draws UI component.
        /// </summary>
        public void Draw()
        {
            PreDraw();
            Animate();
            OnDraw();
            DrawChildren();
            PostDraw();
        }

        /// <summary>
        /// Animates UI component.
        /// </summary>
        protected virtual void Animate()
        {
            if (!IsAnimating || Time.time < nextAnimation) return;

            nextAnimation = Time.time + animationCycle;

            var col = Color;

            switch (animation)
            {
                case "Blink":
                    if (incrementingFrames) currentFrame++; else currentFrame--;
                    incrementingFrames = currentFrame >= frames ? false : currentFrame <= 0 ? true : incrementingFrames;
                    col.a = 1 - (float)currentFrame / (float)frames;
                    break;
            }
            Color = col;
        }

        /// <summary>
        /// Resets animation states.
        /// </summary>
        public virtual void ResetAnimationStates()
        {
            var col = Color;
            col.a = 1;
            Color = col;
            currentFrame = 0;
        }

        /// <summary>
        /// Sets animation of the UI component.
        /// </summary>
        /// <param name="interval">Animation interval.</param>
        /// <param name="frames">Animation frames.</param>
        /// <param name="animation">Animation name.</param>
        public virtual void SetAnimation(float interval, int frames, string animation)
        {
            switch (animation)
            {
                case "Blink":
                    animationCycle = interval;
                    this.frames = frames;
                    this.animation = animation;
                    incrementingFrames = true;
                    currentFrame = 0;
                    break;
            }
        }

        /// <summary>
        /// Called before component is drawn.
        /// </summary>
        protected virtual void PreDraw()
        {
            GUI.color = Color;
        }

        /// <summary>
        /// Called to draw UI component.
        /// </summary>
        protected abstract void OnDraw();

        /// <summary>
        /// Called after component is drawn.
        /// </summary>
        protected virtual void PostDraw()
        {
            GUI.color = Color.white;

            if (childrenToAdd != null)
            {
                Children.AddRange(childrenToAdd);
                childrenToAdd = null;
            }

            if (childrenToRemove != null)
            {
                foreach (var ch in childrenToRemove)
                    Children.Remove(ch);

                childrenToRemove = null;
            }
        }

        /// <summary>
        /// Draws the children of the component.
        /// </summary>
        protected virtual void DrawChildren()
        {
            GUI.BeginGroup(Bounds);

            if (Children != null)
                foreach (BaseUIComponent c in Children)
                    if (c.IsVisible)
                        c.Draw();

            GUI.EndGroup();
        }

        /// <summary>
        /// Sets the component position.
        /// </summary>
        /// <param name="x">New X position for the component.</param>
        /// <param name="y">New Y position for the component.</param>
        public void SetPos(float x, float y)
        {
            SetBounds(x, y, W, H);
        }

        /// <summary>
        /// Sets new component bounds.
        /// </summary>
        /// <param name="x">New X position for the component.</param>
        /// <param name="y">New Y position for the component.</param>
        /// <param name="w">New width for the component.</param>
        /// <param name="h">New height for the component.</param>
        /// <param name="resize">Should resize for ratio.</param>
        public virtual void SetBounds(float x, float y, float w, float h, bool resize = true)
        {
            this.X = x;
            this.Y = y;
            this.W = w;
            this.H = h;

            Bounds = resize ? Utilities.Utils_UI.ResizeRect(new Rect(x, y, w, h)) : new Rect(x, y, w, h);
        }

        /// <summary>
        /// Sets new bounds for compnent.
        /// </summary>
        /// <param name="rect">New bounds of the component.</param>
        /// <param name="resize">Should resize for ratio.</param>
        public void SetBounds(Rect rect, bool resize = true)
        {
            SetBounds(rect.x, rect.y, rect.width, rect.height, resize);
        }

        /// <summary>
        /// Creates scheme.
        /// </summary>
        protected virtual void CreateScheme()
        {
            Style = new GUIStyle(GUIStyle.none);
        }

        /// <summary>
        /// Applies scheme from KeyValues.
        /// </summary>
        /// <param name="kv">KeyValues data for scheme.</param>
        protected virtual void ApplyScheme(KeyValues kv) { }

        /// <summary>
        /// Gets component by name.
        /// </summary>
        /// <param name="name">Name of the component.</param>
        /// <returns>Component if found, NULL otherwise.</returns>
        public BaseUIComponent GetComponent(string name)
        {
            foreach (BaseUIComponent c in Children)
                if (c.Name == name)
                    return c;

            return null;
        }

        /// <summary>
        /// Gets component by name recursively searching through all children.
        /// </summary>
        /// <param name="name">Name of the component to look for.</param>
        /// <returns>Returns component if found, NULL otherwise.</returns>
        public BaseUIComponent GetComponentInChildren(string name)
        {
            foreach (BaseUIComponent c in Children)
            {
                if (c.Name == name)
                    return c;
                else
                {
                    var component = c.GetComponentInChildren(name);

                    if (component != null) return component;
                }
            }

            return null;
        }

    }
}
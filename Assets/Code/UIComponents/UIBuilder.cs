﻿using System;
using System.Collections.Generic;
using FileSystem;
using UI.Components;
using UI.Components.Input;
using RobotDesigner.Views;

namespace UI
{
    /// <summary>
    /// Builds UI from KeyValues.
    /// </summary>
    public sealed class UIBuilder
    {
        private static Dictionary<string, Type> typeBinds = new Dictionary<string, Type>();
        private static readonly UIBuilder instance = new UIBuilder();

        /// <summary>
        /// Retrieves singleton instance of UIBuilder.
        /// </summary>
        public static UIBuilder Instance
        {
            get { return instance; }
        }

        /// <summary>
        /// Adds a type bind.
        /// </summary>
        /// <param name="type">Binds string to type.</param>
        public static void AddTypeBind(Type type)
        {
            typeBinds.Add(type.Name, type);
        }

        /// <summary>
        /// Static constructor that registers all UI elements.
        /// </summary>
        private UIBuilder()
        {
            Button.RegisterUIElement();
            ScrollView.RegisterUIElement();
            ImageComponent.RegisterUIElement();
            Frame.RegisterUIElement();
            Label.RegisterUIElement();
            Holder.RegisterUIElement();
            DirectionalButton.RegisterUIElement();
            NumericInputField.RegisterUIElement();
            InputField.RegisterUIElement();
            VariableNameField.RegisterUIElement();
            ImageButton.RegisterUIElement();
        }

        /// <summary>
        /// Builds UI from KeyValues.
        /// </summary>
        /// <param name="kv">KeyValues to build UI from.</param>
        /// <param name="baseWidth">Used scale base width.</param>
        /// <param name="baseHeigh">Used scale base height.</param>
        /// <returns></returns>
        public List<BaseUIComponent> BuildUIFromKeyValues(KeyValues kv, out int baseWidth, out int baseHeigh)
        {
            List<BaseUIComponent> components = new List<BaseUIComponent>();
            baseWidth = kv.GetInt("basex", CustomUI.BaseX);
            baseHeigh = kv.GetInt("basey", CustomUI.BaseY);

            foreach (KeyValuePair<string, object> pair in kv.GetKeyValues())
            {
                if (pair.Value.GetType() == typeof(KeyValues))
                {
                    BaseUIComponent component = GetComponentFromKeyValue((KeyValues)pair.Value);
                    if (component != null)
                    {
                        component.Name = pair.Key;
                        component.Children = BuildComponentsFromKeyValuesForChildren((KeyValues)pair.Value);
                        components.Add(component);
                    }
                }
            }

            return components;
        }

        /// <summary>
        /// Builds UI components recursively.
        /// </summary>
        /// <param name="kv">Keyvalues to build from.</param>
        /// <returns>UI components list.</returns>
        private List<BaseUIComponent> BuildComponentsFromKeyValuesForChildren(KeyValues kv)
        {
            List<BaseUIComponent> components = new List<BaseUIComponent>();

            foreach (KeyValuePair<string, object> pair in kv.GetKeyValues())
            {
                if (pair.Value.GetType() == typeof(KeyValues))
                {
                    BaseUIComponent component = GetComponentFromKeyValue((KeyValues)pair.Value);
                    if (component != null)
                    {
                        component.Name = pair.Key;
                        component.Children = BuildComponentsFromKeyValuesForChildren((KeyValues)pair.Value);
                        components.Add(component);
                    }
                }
            }

            return components;
        }

        /// <summary>
        /// Builds component from KeyValue.
        /// </summary>
        /// <param name="kv">KeyValue to build component from.</param>
        /// <returns>Constructed component if valid, NULL otherwise.</returns>
        public BaseUIComponent GetComponentFromKeyValue(KeyValues kv)
        {
            string compClass = kv.GetString("class");

            if (!typeBinds.ContainsKey(compClass))
                return null;

            return IndentifyComponent(kv);
        }

        /// <summary>
        /// Identifies and creates a component from KeyValues.
        /// </summary>
        /// <param name="kv">KeyValues to identify from.</param>
        /// <returns>Component if valid, NULL otherwise.</returns>
        private BaseUIComponent IndentifyComponent(KeyValues kv)
        {
            var types = new Type[1];
            types[0] = typeof(KeyValues);
            var constructor = typeBinds[kv.GetString("class")].GetConstructor(types);

            if (constructor != null)
            {
                var param = new KeyValues[1];
                param[0] = kv;
                return constructor.Invoke(param) as BaseUIComponent;
            }

            return null;
        }
    }
}
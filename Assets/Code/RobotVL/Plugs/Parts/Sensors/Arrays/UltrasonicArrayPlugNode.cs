﻿using RobotDesigner.Nodes.Sensors.Arrays;
using System;
using RobotVL.Members.Types;
using DesignerGlue;
using System.Xml;

namespace RobotVL.Plugs.Parts.Sensors.Arrays
{
    /// <summary>
    /// Plug for ultrasonic array.
    /// </summary>
    public class UltrasonicArrayPlugNode : PlugNode
    {
        public const string ULTRASONIC_DISTANCE = "__ULTRASONIC_DISTANCE__";
        public const string ULTRASONIC_DIRECTION = "__ULTRASONIC_DIRECTION__";

        private UltrasonicArrayNode.ArrayType_T type;

        /// <summary>
        /// Constructor for ultrasonic array.
        /// </summary>
        /// <param name="type">Type of data sensor plug provides.</param>
        public UltrasonicArrayPlugNode(UltrasonicArrayNode.ArrayType_T type)
        {
            this.type = type;
        }

        /// <summary>
        /// Converts plug into numeric node that contains array data.
        /// </summary>
        /// <param name="robotIO">Robot parts.</param>
        /// <returns>Numeric node containing proper data.</returns>
        public override ISyntaxNode ToNode(RobotIO robotIO)
        {
            var sensor = robotIO.UltrasonicSensorArray;
            switch (this.type)
            {
                case UltrasonicArrayNode.ArrayType_T.Distance:
                    return ObjectNode.CreateNode(ULTRASONIC_DISTANCE, ObjectNode.ValueType_T.Numeric, sensor.InvokeMethod(ISensorArrayNode.Methods_T.SensorValue));

                case UltrasonicArrayNode.ArrayType_T.Direction:
                    return ObjectNode.CreateNode(ULTRASONIC_DIRECTION, ObjectNode.ValueType_T.Numeric, sensor.InvokeMethod(ISensorArrayNode.Methods_T.SensorDirection));
                default:
                    throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Used for generating code.
        /// </summary>
        /// <param name="indents">Indentation count.</param>
        /// <returns>Stringified node.</returns>
        public override string Stringify(int indents = 0)
        {
            throw new NotSupportedException("UltrasonicArrayPlugNode does not support stringification.");
        }

        /// <summary>
        /// Serializes a node.
        /// </summary>
        /// <param name="xdoc">Serialization document.</param>
        /// <param name="parent">Serialization parent.</param>
        public override void Serialize(XmlDocument xdoc, XmlElement parent)
        {
            throw new NotImplementedException();
        }
    }
}

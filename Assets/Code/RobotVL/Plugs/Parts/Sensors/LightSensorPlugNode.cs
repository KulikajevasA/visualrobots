﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DesignerGlue;
using RobotVL.Members.Types;
using System.Xml;

namespace RobotVL.Plugs.Parts.Sensors
{
    /// <summary>
    /// Plug for color sensor node.
    /// </summary>
    public class LightSensorPlugNode : PlugNode
    {
        public const string LIGHT_SENSOR_VALUE = "__LIGHT_SENSOR_VALUE__";

        /// <summary>
        /// Used for generating code.
        /// </summary>
        /// <param name="indents">Indentation count.</param>
        /// <returns>Stringified node.</returns>
        public override string Stringify(int indents = 0)
        {
            throw new NotSupportedException("LightSensorPlugNode does not support stringification.");
        }

        /// <summary>
        /// Converts plug into numeric node that contains array data.
        /// </summary>
        /// <param name="robotIO">Robot parts.</param>
        /// <returns>Numeric node containing proper data.</returns>
        public override ISyntaxNode ToNode(RobotIO robotIO)
        {
            var sensor = robotIO.LightSensor;

            return ObjectNode.CreateNode(LIGHT_SENSOR_VALUE, ObjectNode.ValueType_T.Numeric, sensor.InvokeMethod(ISensorNode.Methods_T.SensorValue));
        }

        /// <summary>
        /// Serializes a node.
        /// </summary>
        /// <param name="xdoc">Serialization document.</param>
        /// <param name="parent">Serialization parent.</param>
        public override void Serialize(XmlDocument xdoc, XmlElement parent)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using System;
using RobotDesigner.Nodes;
using RobotVL.Members.Types;
using DesignerGlue;
using System.Xml;

namespace RobotVL.Plugs.Parts
{
    /// <summary>
    /// Plug for IMotor node.
    /// </summary>
    public class MotorPlugNode : PlugNode
    {
        private MotorNode.Motor_T motor;
        private double power;

        /// <summary>
        /// Constructor for motor plug.
        /// </summary>
        /// <param name="motor">Which motor is represented by this plug.</param>
        /// <param name="power">Power of the motor.</param>
        public MotorPlugNode(MotorNode.Motor_T motor, double power)
        {
            this.power = power;
            this.motor = motor;
        }

        /// <summary>
        /// Used for generating code.
        /// </summary>
        /// <param name="indents">Indentation count.</param>
        /// <returns>Stringified node.</returns>
        public override string Stringify(int indents = 0)
        {
            var literal = ToNode(robotIO);
            return string.Format("\t{0};", literal.Stringify(indents));
        }

        /// <summary>
        /// Converts motor plug into a motor power invocation.
        /// </summary>
        /// <param name="robotIO">Robot parts.</param>
        /// <returns></returns>
        public override ISyntaxNode ToNode(RobotIO robotIO)
        {
            if (robotIO == null)
                throw new NullReferenceException("Plugs were not initialized with RobotIO.");

            switch (motor)
            {
                case MotorNode.Motor_T.LEFT:
                    return robotIO.LeftMotor.InvokeMethod(IMotorNode.Methods_T.SetSpeed,
                        ObjectNode.CreateNode(ObjectNode.ValueType_T.Literal, this.power));
                case MotorNode.Motor_T.RIGHT:
                    return robotIO.RightMotor.InvokeMethod(IMotorNode.Methods_T.SetSpeed,
                        ObjectNode.CreateNode(ObjectNode.ValueType_T.Literal, this.power));
                default:
                    throw new NotImplementedException();
            }
        }

        /// <summary>
        /// Serializes a node.
        /// </summary>
        /// <param name="xdoc">Serialization document.</param>
        /// <param name="parent">Serialization parent.</param>
        public override void Serialize(XmlDocument xdoc, XmlElement parent)
        {
            throw new NotImplementedException();
        }
    }
}

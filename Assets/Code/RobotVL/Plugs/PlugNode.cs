﻿using DesignerGlue;
using System;
using System.Xml;

namespace RobotVL.Plugs
{
    /// <summary>
    /// Base for plug node that is used for temporarily substituting a node until more information is known about robot.
    /// </summary>
    public abstract class PlugNode : ISyntaxNode
    {
        protected RobotIO robotIO;

        /// <summary>
        /// Used for generating code.
        /// </summary>
        /// <param name="indents">Indentation count.</param>
        /// <returns>Stringified node.</returns>
        public abstract string Stringify(int indents = 0);

        /// <summary>
        /// Converts to appropriate syntax node.
        /// </summary>
        /// <param name="robotIO">Robot parts.</param>
        /// <returns>Appropriate syntax node.</returns>
        public abstract ISyntaxNode ToNode(RobotIO robotIO);

        /// <summary>
        /// Plugs in robot part data.
        /// </summary>
        /// <param name="robotIO">Robot parts.</param>
        public void PlugIn(RobotIO robotIO)
        {
            this.robotIO = robotIO;
        }

        /// <summary>
        /// Serializes a node.
        /// </summary>
        /// <param name="xdoc">Serialization document.</param>
        /// <param name="parent">Serialization parent.</param>
        public abstract void Serialize(XmlDocument xdoc, XmlElement parent);
    }
}

﻿using RobotVL.Comparators;
using RobotVL.Logical;
using RobotVL.Members.Types;
using System;

namespace RobotVL.Main
{
    /// <summary>
    /// Complete robot node with colision avoidance.
    /// This is an example bot.
    /// </summary>
    [Obsolete]
    public class AvoidingRobotNode : RobotNode
    {
        /// <summary>
        /// Constructor for the robot.
        /// </summary>
        /// <param name="robotName">Name of the robot.</param>
        public AvoidingRobotNode(string robotName) : base(robotName)
        {
            CreateRunFrame();
        }

        /// <summary>
        /// Creates RunFrame internals.
        /// TODO: This is temporary and will be removed.
        /// </summary>
        private void CreateRunFrame()
        {
            var invokeUltrasonicDir = UltrasonicArray.InvokeMethod(ISensorArrayNode.Methods_T.SensorDirection);
            var invokeUltrasonicVal = UltrasonicArray.InvokeMethod(ISensorArrayNode.Methods_T.SensorValue);

            var speedNode = ObjectNode.CreateNode("speed", ObjectNode.ValueType_T.Numeric, 3);
            var dirNode = ObjectNode.CreateNode("dir", ObjectNode.ValueType_T.Numeric, invokeUltrasonicDir);
            var valNode = ObjectNode.CreateNode("val", ObjectNode.ValueType_T.Numeric, invokeUltrasonicVal);

            var oneNode = ObjectNode.CreateNode("one", ObjectNode.ValueType_T.Numeric, 1);
            var zeroNode = ObjectNode.CreateNode("zero", ObjectNode.ValueType_T.Numeric, 0);

            this.RunFrame.AddNodes(dirNode, valNode, speedNode, oneNode, zeroNode);

            var valGtCmp = new GTComparatorNode(valNode, oneNode);
            var valLtCmp = new LTComparatorNode(valNode, oneNode);

            var dirGtCmp = new GTComparatorNode(dirNode, zeroNode);
            var dirLtCmp = new LTComparatorNode(dirNode, zeroNode);

            var ifNodeOn = new IfNode(valGtCmp);
            var ifNodeDirRight = new IfNode(dirGtCmp);
            var ifNodeDirLeft = new IfNode(dirLtCmp);
            var ifNodeOff = new IfNode(valLtCmp);

            this.RunFrame.AddNodes(ifNodeOn, ifNodeDirRight);

            ifNodeDirRight.Otherwise.AddNode(ifNodeDirLeft);
            ifNodeDirLeft.Otherwise.AddNode(ifNodeOff);

            var invokeLeftMotorSpeed = LeftMotor.InvokeMethod(IMotorNode.Methods_T.SetSpeed, speedNode);
            var invokeRightMotorSpeed = RightMotor.InvokeMethod(IMotorNode.Methods_T.SetSpeed, speedNode);
            var invokeLeftMotorBreak = LeftMotor.InvokeMethod(IMotorNode.Methods_T.SetSpeed, zeroNode);
            var invokeRightMotorBreak = RightMotor.InvokeMethod(IMotorNode.Methods_T.SetSpeed, zeroNode);

            ifNodeOn.Success.AddNodes(invokeLeftMotorSpeed, invokeRightMotorSpeed);
            ifNodeOff.Success.AddNodes(invokeLeftMotorBreak, invokeRightMotorBreak);
            ifNodeDirRight.Success.AddNode(invokeLeftMotorBreak);
            ifNodeDirLeft.Success.AddNode(invokeRightMotorBreak);
        }
    }
}

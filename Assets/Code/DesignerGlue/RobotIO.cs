﻿using RobotVL.Members.Types;
using RobotVL.Main;

namespace DesignerGlue
{
    /// <summary>
    /// Used to prove info about robot I/O parts.
    /// </summary>
    public class RobotIO
    {
        private RobotNode robotNode;

        /// <summary>
        /// Ultrasonic array.
        /// </summary>
        public ISensorArrayNode UltrasonicSensorArray { get; private set; }

        /// <summary>
        /// Color sensor.
        /// </summary>
        public ISensorNode ColorSensor { get; private set; }

        /// <summary>
        /// Light sensor.
        /// </summary>
        public ISensorNode LightSensor { get; private set; }

        /// <summary>
        /// Left motor.
        /// </summary>
        public IMotorNode LeftMotor { get; private set; }

        /// <summary>
        /// Right motor.
        /// </summary>
        public IMotorNode RightMotor { get; private set; }

        /// <summary>
        /// Constructor for robot I/O info.
        /// </summary>
        /// <param name="robotNode"></param>
        public RobotIO(RobotNode robotNode)
        {
            this.robotNode = robotNode;

            this.UltrasonicSensorArray = robotNode.UltrasonicArray;
            this.LeftMotor = robotNode.LeftMotor;
            this.RightMotor = robotNode.RightMotor;
            this.ColorSensor = robotNode.ColorSensor;
            this.LightSensor = robotNode.LightSensor;
        }
    }
}

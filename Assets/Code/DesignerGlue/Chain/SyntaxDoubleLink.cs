﻿using RobotDesigner.Nodes;

namespace DesignerGlue.Chain
{
    /// <summary>
    /// Syntax link node that supports double chaining.
    /// </summary>
    public class SyntaxDoubleLink : SyntaxLink
    {
        /// <summary>
        /// Next link in chain.
        /// </summary>
        public SyntaxChain Next2 { get; set; }

        /// <summary>
        /// Constructor for chain link.
        /// </summary>
        /// <param name="node">Attached node.</param>
        public SyntaxDoubleLink(BaseNode node) : base(node) { }

        /// <summary>
        /// Visualizes link as string.
        /// </summary>
        /// <param name="indents">Indent level.</param>
        /// <returns>Visualization string.</returns>
        public override string Visualize(int indents = 0)
        {
            var indent = RobotVL.Utilities.Utils_Other.GetIndents(indents);
            string @out = indent + "|-" + ToString() + "\n";

            if (Next != null)
                @out += Next.Visualize(indents + 1);
            if (Next2 != null)
                @out += Next2.Visualize(indents + 1);

            return @out;
        }
    }
}

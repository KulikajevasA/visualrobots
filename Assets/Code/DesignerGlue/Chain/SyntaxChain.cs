﻿namespace DesignerGlue.Chain
{
    /// <summary>
    /// Used for constructing syntax chain.
    /// </summary>
    public class SyntaxChain
    {
        private SyntaxChain next;
        private SyntaxChain previous;

        /// <summary>
        /// Previous chain element.
        /// </summary>
        public SyntaxChain Previous
        {
            get
            {
                return previous;
            }
        }

        /// <summary>
        /// Next chain element.
        /// </summary>
        public SyntaxChain Next
        {
            get
            {
                return next;
            }

            set
            {
                next = value;
                value.previous = this;
            }
        }

        /// <summary>
        /// Constructs a visualization string.
        /// </summary>
        /// <param name="indents">Indent level.</param>
        /// <returns>Visualization string.</returns>
        public virtual string Visualize(int indents = 0)
        {
            var indent = RobotVL.Utilities.Utils_Other.GetIndents(indents);
            string @out = indent + "|-" + ToString() + "\n";

            if (Next != null)
                @out += Next.Visualize(indents + 1);

            return @out;
        }

        /// <summary>
        /// Converts syntax chain to syntax tree.
        /// </summary>
        /// <param name="tree">Syntax tree to which to append chain.</param>
        /// <returns>Syntax tree.</returns>
        public virtual SyntaxTree ToTree(SyntaxTree tree = null)
        {
            tree = tree ?? new SyntaxTree();

            if (Next != null)
                Next.ToTree(tree);

            return tree;
        }
    }
}

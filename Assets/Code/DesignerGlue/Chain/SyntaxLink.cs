﻿using RobotDesigner.Nodes;

namespace DesignerGlue.Chain
{
    /// <summary>
    /// Syntax chain link.
    /// </summary>
    public class SyntaxLink : SyntaxChain
    {
        /// <summary>
        /// Syntax chain link node.
        /// </summary>
        public BaseNode Node { get; protected set; }

        /// <summary>
        /// Syntax link constructor.
        /// </summary>
        /// <param name="node">Link node.</param>
        public SyntaxLink(BaseNode node) : base()
        {
            this.Node = node;
        }
    }
}

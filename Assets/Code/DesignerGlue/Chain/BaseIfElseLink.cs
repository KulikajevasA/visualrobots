﻿using RobotDesigner.Nodes;

namespace DesignerGlue.Chain
{
    /// <summary>
    /// Base for If-Else link.
    /// </summary>
    public abstract class BaseIfElseLink : SyntaxLink
    {
        /// <summary>
        /// Internal chain.
        /// </summary>
        public SyntaxChain InternalChain { get; set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="node">Node that belongs to the link.</param>
        protected BaseIfElseLink(BaseNode node) : base(node) { }
    }
}

﻿using RobotDesigner.Nodes;
using RobotVL;
using RobotVL.Invocation;

namespace DesignerGlue.Chain.Types
{
    /// <summary>
    /// Syntax chain method link.
    /// </summary>
    public class MethodLink : SyntaxDoubleLink
    {
        /// <summary>
        /// Internal chain.
        /// </summary>
        public SyntaxChain InternalChain { get; set; }

        /// <summary>
        /// Constructor for method link.
        /// </summary>
        /// <param name="node">Attached node.</param>
        public MethodLink(MethodNode node) : base(node) { }

        /// <summary>
        /// Visualizes link as string.
        /// </summary>
        /// <param name="indents">Indent level.</param>
        /// <returns>Visualization string.</returns>
        public override string Visualize(int indents = 0)
        {
            var indent = RobotVL.Utilities.Utils_Other.GetIndents(indents);
            string @out = indent + "|-" + ToString() + "\n";

            if (InternalChain != null)
                @out += InternalChain.Visualize(indents + 1);

            if (Next != null)
                @out += Next.Visualize(indents);

            return @out;
        }

        private ISyntaxNode ToNode(out SyntaxTree subTree)
        {
            var node = new RobotVL.Members.MethodNode((Node as MethodNode).MethodName, RobotVL.Members.MethodNode.Accessor_T.Private);

            if (InternalChain != null)
            {
                subTree = new SyntaxTree();
                subTree.Method = node as RobotVL.Members.MethodNode;
                InternalChain.ToTree(subTree).PushInternals(node);
            }
            else subTree = null;

            return node;
        }

        public override SyntaxTree ToTree(SyntaxTree tree = null)
        {
            SyntaxTree subTree = null;
            var node = ToNode(out subTree) as RobotVL.Members.MethodNode;
            var root = tree.Root != null ? tree.Root : tree;

            root.AddMethod(node);
            tree.AddAction(new InvokeRegularMethod(node.MethodName));

            if (subTree != null)
                root.AddBranch(subTree);

            return base.ToTree(tree);
        }
    }
}

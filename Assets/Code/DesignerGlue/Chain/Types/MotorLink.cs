﻿using RobotDesigner.Nodes;
using RobotVL.Plugs.Parts;

namespace DesignerGlue.Chain.Types
{
    /// <summary>
    /// Motor chain link.
    /// </summary>
    public class MotorLink : SyntaxLink
    {
        /// <summary>
        /// Constructor for motor link.
        /// </summary>
        /// <param name="node">Attached node.</param>
        public MotorLink(MotorNode node) : base(node) { }

        /// <summary>
        /// Converts link to tree.
        /// </summary>
        /// <param name="tree">Input tree.</param>
        /// <returns>Output tree.</returns>
        public override SyntaxTree ToTree(SyntaxTree tree = null)
        {
            var node = this.Node as MotorNode;
            tree.AddAction(new MotorPlugNode(node.Motor, node.Value));
            return base.ToTree(tree);
        }
    }
}

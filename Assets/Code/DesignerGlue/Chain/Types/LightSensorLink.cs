﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RobotDesigner.Nodes;
using RobotVL;
using RobotVL.Members.Types;
using RobotVL.Plugs.Parts.Sensors;

namespace DesignerGlue.Chain.Types
{
    public class LightSensorLink : SyntaxLink, IVariableLink
    {
        public LightSensorLink(LightSensorNode node) : base(node) { }

        public ISyntaxNode ToNode()
        {
            //var node = this.Node as ColorSensorNode;
            var name = LightSensorPlugNode.LIGHT_SENSOR_VALUE;

            return ObjectNode.CreateNode(name, ObjectNode.ValueType_T.Numeric);
        }

        public override SyntaxTree ToTree(SyntaxTree tree = null)
        {
            var node = new LightSensorPlugNode();
            tree.AddVariable(node);

            return base.ToTree(tree);
        }
    }
}

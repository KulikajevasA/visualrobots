﻿using RobotDesigner.Nodes;

namespace DesignerGlue.Chain.Types
{
    /// <summary>
    /// Link for ElseNode.
    /// </summary>
    public class IfOtherwiseLink : BaseIfElseLink
    {
        /// <summary>
        /// Constructor for ElseNode link.
        /// </summary>
        /// <param name="node">Attached ElseNode.</param>
        public IfOtherwiseLink(ElseNode node) : base(node) { }

        /// <summary>
        /// Visualizes link as chain.
        /// </summary>
        /// <param name="indents">Indent level.</param>
        /// <returns>Visualization string.</returns>
        public override string Visualize(int indents = 0)
        {
            var indent = RobotVL.Utilities.Utils_Other.GetIndents(indents);
            string @out = indent + "|-" + ToString() + "\n";

            if (InternalChain != null)
                @out += indent + "-- internal -- \n" + InternalChain.Visualize(indents + 1);

            if (Next != null)
                @out += Next.Visualize(indents);

            return @out;
        }

        /// <summary>
        /// Turns syntax chain into a syntax tree.
        /// </summary>
        /// <param name="tree">Tree to append the link.</param>
        /// <returns>Syntax tree.</returns>
        public override SyntaxTree ToTree(SyntaxTree tree)
        {
            base.ToTree(tree);

            var internalTree = new SyntaxTree(tree);

            if (InternalChain != null)
                InternalChain.ToTree(internalTree);

            return internalTree;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RobotDesigner.Nodes;
using RobotVL;
using RobotVL.Plugs.Parts.Sensors;
using RobotVL.Members.Types;

namespace DesignerGlue.Chain.Types
{
    public class ColorSensorLink : SyntaxLink, IVariableLink
    {
        public ColorSensorLink(ColorSensorNode node) : base(node) { }

        public ISyntaxNode ToNode()
        {
            //var node = this.Node as ColorSensorNode;
            var name = ColorSensorPlugNode.COLOR_SENSOR_VALUE;

            return ObjectNode.CreateNode(name, ObjectNode.ValueType_T.Numeric);
        }

        public override SyntaxTree ToTree(SyntaxTree tree = null)
        {
            var node = new ColorSensorPlugNode();
            tree.AddVariable(node);

            return base.ToTree(tree);
        }
    }
}

﻿using System;
using RobotDesigner.Nodes;
using RobotVL.Comparators;
using RobotVL.Members.Types;

namespace DesignerGlue.Chain.Types
{
    /// <summary>
    /// IfNode chain link.
    /// </summary>
    public class IfSuccessLink : BaseIfElseLink
    {
        /// <summary>
        /// Extra link chain.
        /// </summary>
        public SyntaxChain ExtraLink { get; set; }

        /// <summary>
        /// Constructor for IfNode link.
        /// </summary>
        /// <param name="node">Attached node.</param>
        public IfSuccessLink(IfNode node) : base(node) { }

        /// <summary>
        /// Visualizes link as chain.
        /// </summary>
        /// <param name="indents">Indent level.</param>
        /// <returns>Visualization string.</returns>
        public override string Visualize(int indents = 0)
        {
            var indent = RobotVL.Utilities.Utils_Other.GetIndents(indents);
            string @out = indent + "|-" + ToString() + "\n";

            if (ExtraLink != null)
                @out += indent + "-- extra -- \n" + ExtraLink.Visualize(indents + 1);

            if (InternalChain != null)
                @out += indent + "-- internal -- \n" + InternalChain.Visualize(indents + 1);

            if (Next != null)
                @out += Next.Visualize(indents);

            return @out;
        }

        /// <summary>
        /// Converts syntax chain to syntax tree.
        /// </summary>
        /// <param name="tree">Tree to which to construct.</param>
        /// <returns>Syntax tree.</returns>
        public override SyntaxTree ToTree(SyntaxTree tree = null)
        {
            if (this.Previous is IVariableLink && this.ExtraLink is IVariableLink)
            {
                var lhs = (this.Previous as IVariableLink).ToNode() as ObjectNode;
                var rhs = (this.ExtraLink as IVariableLink).ToNode() as ObjectNode;

                var lhsNode = (this.Previous as SyntaxLink).Node as RobotDesigner.Nodes.NumericNode;
                var rhsNode = (this.ExtraLink as SyntaxLink).Node as RobotDesigner.Nodes.NumericNode;

                if (lhsNode != null)
                    lhs.IsClassScope = lhsNode.IsGlobal;
                if (rhsNode != null)
                    rhs.IsClassScope = rhsNode.IsGlobal;

                this.ExtraLink.ToTree(tree);

                ComparatorNode comparator = null;

                switch ((this.Node as IfNode).Comparator)
                {
                    case IfNode.Comparators_T.GT:
                        comparator = new GTComparatorNode(lhs, rhs);
                        break;
                    case IfNode.Comparators_T.GTE:
                        comparator = new GTEComparatorNode(lhs, rhs);
                        break;
                    case IfNode.Comparators_T.LT:
                        comparator = new LTComparatorNode(lhs, rhs);
                        break;
                    case IfNode.Comparators_T.LTE:
                        comparator = new LTEComparatorNode(lhs, rhs);
                        break;
                    case IfNode.Comparators_T.NEQ:
                        comparator = new NEQComparatorNode(lhs, rhs);
                        break;
                    case IfNode.Comparators_T.EQ:
                        comparator = new EQComparatorNode(lhs, rhs);
                        break;
                    default:
                        throw new InvalidOperationException("Comparator not supported.");
                }

                var ifNode = new RobotVL.Logical.IfNode(comparator);
                if (InternalChain != null)
                {
                    var subTree = new SyntaxTree(tree);
                    tree.AddBranch(subTree);
                    InternalChain.ToTree(subTree).PushInternals(ifNode.Success);
                }

                tree.AddAction(ifNode);

                if (Next != null)
                {
                    var subTree = Next.ToTree(tree);
                    subTree.PushInternals(ifNode.Otherwise);
                    tree.AddBranch(subTree);
                }
            }

            return tree;
        }
    }
}

﻿using System;
using RobotDesigner.Nodes;
using RobotVL;
using RobotVL.Members.Types;
using RobotVL.Operators.Binary;

namespace DesignerGlue.Chain.Types
{
    /// <summary>
    /// Operator chain link.
    /// </summary>
    public class OperatorLink : SyntaxLink, IVariableLink
    {
        private readonly string varName;

        /// <summary>
        /// Constructor for link.
        /// </summary>
        /// <param name="node">Attached node.</param>
        public OperatorLink(OperatorNode node) : base(node)
        {
            varName = RobotVL.Utilities.Utils_Language.GetAnonymousName();
        }

        /// <summary>
        /// Extra link chain.
        /// </summary>
        public SyntaxChain ExtraLink { get; set; }
        
        /// <summary>
        /// Converts link to tree.
        /// </summary>
        /// <param name="tree">Input tree.</param>
        /// <returns>Output tree.</returns>
        public override SyntaxTree ToTree(SyntaxTree tree)
        {
            var castNode = this.Node as OperatorNode;

            var lhs = (this.Previous as IVariableLink).ToNode() as ObjectNode;
            var rhs = (this.ExtraLink as IVariableLink).ToNode() as ObjectNode;

            this.ExtraLink.ToTree(tree);

            RobotVL.Operators.OperatorNode @operator = null;

            switch (castNode.Operator)
            {
                case OperatorNode.Operators_T.ADDITION:
                    @operator = new AdditionNode(lhs, rhs);
                    break;
                case OperatorNode.Operators_T.SUBTRACTION:
                    @operator = new SubtractionNode(lhs, rhs);
                    break;
                case OperatorNode.Operators_T.DIVISION:
                    @operator = new DivisionNode(lhs, rhs);
                    break;
                case OperatorNode.Operators_T.MULTIPLICATION:
                    @operator = new MultiplicationNode(lhs, rhs);
                    break;
                default:
                    throw new InvalidOperationException("Operator not supported.");
            }

            var myNode = ToNode() as RobotVL.Members.Types.NumericNode;
            tree.AddAction(myNode);

            tree.AddAction(new AssignmentNode(myNode, @operator));

            return base.ToTree(tree);
        }

        /// <summary>
        /// Generates numeric node.
        /// </summary>
        /// <returns>Syntax node.</returns>
        public ISyntaxNode ToNode()
        {
            return ObjectNode.CreateNode(varName, ObjectNode.ValueType_T.Numeric);
        }
    }
}

﻿using RobotVL;

namespace DesignerGlue.Chain.Types
{
    /// <summary>
    /// Interface for variable type link.
    /// </summary>
    public interface IVariableLink
    {
        /// <summary>
        /// Creates appropriate syntax node.
        /// </summary>
        /// <returns>Syntax node.</returns>
        ISyntaxNode ToNode();
    }
}

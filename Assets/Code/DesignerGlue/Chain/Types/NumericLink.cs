﻿using RobotDesigner.Nodes;
using RobotVL;
using RobotVL.Operators.Binary;
using System;

namespace DesignerGlue.Chain.Types
{
    /// <summary>
    /// Numeric chain link.
    /// </summary>
    public class NumericLink : SyntaxLink, IVariableLink
    {
        /// <summary>
        /// Constructor for link.
        /// </summary>
        /// <param name="node">Attached node.</param>
        public NumericLink(NumericNode node) : base(node) { }

        /// <summary>
        /// Assign this value for link.
        /// </summary>
        public IVariableLink AssignValue { get; set; }

        /// <summary>
        /// Converts link to tree.
        /// </summary>
        /// <param name="tree">Input tree.</param>
        /// <returns>Output tree.</returns>
        public override SyntaxTree ToTree(SyntaxTree tree)
        {
            var castNode = this.Node as NumericNode;
            var node = ToNode() as RobotVL.Members.Types.ObjectNode;
            var nodeVal = ToNode() as RobotVL.Members.Types.ObjectNode;
            nodeVal.Value = castNode.Value;

            //var prev = this.Previous;

            if (!castNode.IsGlobal)
                tree.AddVariable(node);
            else
            {
                node.IsClassScope = true;
                tree.AddGlobalVariable(node);
            }

            switch (castNode.AssignMode)
            {
                case NumericNode.AssignValueNode_T.ASSIGN_INPUT:
                    if (Previous is IVariableLink)
                        tree.AddAction(new AssignmentNode(node, (Previous as IVariableLink).ToNode()));

                    if (AssignValue != null)
                        tree.AddAction(new AssignmentNode(node, AssignValue.ToNode()));
                    break;
                case NumericNode.AssignValueNode_T.ASSIGN_NODE:
                    tree.AddAction(new AssignmentByValueNode(node, nodeVal));
                    break;
                case NumericNode.AssignValueNode_T.ASSIGN_NONE:
                default:
                    break;
            }

            return base.ToTree(tree);
        }

        /// <summary>
        /// Converts link to node.
        /// </summary>
        /// <returns>Syntax node.</returns>
        public ISyntaxNode ToNode()
        {
            var node = this.Node as NumericNode;
            return RobotVL.Members.Types.ObjectNode.CreateNode(node.VariableName,
                RobotVL.Members.Types.ObjectNode.ValueType_T.Numeric);
        }
    }
}

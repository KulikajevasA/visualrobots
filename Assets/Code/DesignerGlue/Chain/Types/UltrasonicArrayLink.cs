﻿using System;
using RobotDesigner.Nodes.Sensors.Arrays;
using RobotVL;
using RobotVL.Members.Types;
using RobotVL.Plugs.Parts.Sensors.Arrays;

namespace DesignerGlue.Chain.Types
{
    /// <summary>
    /// Ultrasonic array chain link.
    /// </summary>
    public class UltrasonicArrayLink : SyntaxLink, IVariableLink
    {
        /// <summary>
        /// Constructor for ultrasonic array link.
        /// </summary>
        /// <param name="node">Attached node.</param>
        public UltrasonicArrayLink(UltrasonicArrayNode node) : base(node) { }

        /// <summary>
        /// Converts link to syntax node.
        /// </summary>
        /// <returns>Syntax node.</returns>
        public ISyntaxNode ToNode()
        {
            var node = this.Node as UltrasonicArrayNode;
            string name = null;

            switch (node.OutputType)
            {
                case UltrasonicArrayNode.ArrayType_T.Direction:
                    name = UltrasonicArrayPlugNode.ULTRASONIC_DIRECTION;
                    break;
                case UltrasonicArrayNode.ArrayType_T.Distance:
                    name = UltrasonicArrayPlugNode.ULTRASONIC_DISTANCE;
                    break;
                default:
                    throw new NotImplementedException();
            }

            return ObjectNode.CreateNode(name, ObjectNode.ValueType_T.Numeric);
        }

        /// <summary>
        /// Converts link to tree.
        /// </summary>
        /// <param name="tree">Input tree.</param>
        /// <returns>Output tree.</returns>
        public override SyntaxTree ToTree(SyntaxTree tree)
        {
            var node = new UltrasonicArrayPlugNode((this.Node as UltrasonicArrayNode).OutputType);
            tree.AddVariable(node);

            return base.ToTree(tree);
        }
    }
}

﻿using RobotVL;
using RobotVL.Logical;
using RobotVL.Main;
using RobotVL.Members;
using RobotVL.Members.Types;
using RobotVL.Plugs;
using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace DesignerGlue
{
    /// <summary>
    /// Designer glue class responsible for tree code construction and conversion to robot.
    /// </summary>
    public class SyntaxTree
    {
        private List<ISyntaxNode> variables;
        private List<ObjectNode> globalVariables;
        private List<MethodNode> methods;
        private List<ISyntaxNode> actions;
        private List<SyntaxTree> branches;

        /// <summary>
        /// Attached method node.
        /// </summary>
        public MethodNode Method { get; set; }

        /// <summary>
        /// Gets the tree root node.
        /// </summary>
        public SyntaxTree Root { get; private set; }

        /// <summary>
        /// Contructor for SyntaxTree.
        /// </summary>
        /// <param name="root">Syntax tree root.</param>
        public SyntaxTree(SyntaxTree root) : this()
        {
            this.Root = root.Root != null ? root.Root : root;
        }

        /// <summary>
        /// SyntaxTree constructor.
        /// </summary>
        public SyntaxTree()
        {
            this.variables = new List<ISyntaxNode>();
            this.methods = new List<MethodNode>();
            this.actions = new List<ISyntaxNode>();
            this.branches = new List<SyntaxTree>();
            this.globalVariables = new List<ObjectNode>();
        }

        /// <summary>
        /// Adds a variable to the syntax tree.
        /// </summary>
        /// <param name="variable">Variable node.</param>
        public void AddVariable(ISyntaxNode variable)
        {
            this.variables.Add(variable);
        }

        /// <summary>
        /// Adds a global variable to the syntax tree.
        /// </summary>
        /// <param name="variable">Variable node.</param>
        public void AddGlobalVariable(ObjectNode variable)
        {
            this.globalVariables.Add(variable);
        }

        /// <summary>
        /// Adds a method to the syntax tree.
        /// </summary>
        /// <param name="method">Method node.</param>
        public void AddMethod(MethodNode method)
        {
            this.methods.Add(method);
        }

        /// <summary>
        /// Adds an action to the syntax tree.
        /// </summary>
        /// <param name="action">Action node.</param>
        public void AddAction(ISyntaxNode action)
        {
            this.actions.Add(action);
        }

        /// <summary>
        /// Adds a branch to the syntax tree.
        /// </summary>
        /// <param name="branch">Branch to add.</param>
        public void AddBranch(SyntaxTree branch)
        {
            this.branches.Add(branch);
        }

        /// <summary>
        /// Pushes method internals to the method node.
        /// </summary>
        /// <param name="method">Method to which to push internals.</param>
        public void PushInternals(MethodNode method)
        {
            method.AddNodes(this.actions.ToArray());
        }

        /// <summary>
        /// Gets global variables recursively.
        /// </summary>
        /// <param name="list">Variable list to add to.</param>
        /// <returns>Global variables.</returns>
        private List<ObjectNode> GetGlobalVariablesRecursively(List<ObjectNode> list = null)
        {
            list = list ?? new List<ObjectNode>();

            list.AddRange(this.globalVariables);

            foreach (var branch in this.branches)
                branch.GetGlobalVariablesRecursively(list);

            return list;
        }

        /// <summary>
        /// Recursively constructs method list.
        /// </summary>
        /// <param name="list">Method list to add to.</param>
        /// <param name="robotIO">Robot I/O system.</param>
        /// <returns>Method list.</returns>
        private List<MethodNode> GetMethodsRecursively(RobotIO robotIO, List<MethodNode> list = null)
        {
            list = list ?? new List<MethodNode>();

            list.AddRange(methods);
            PlugBranch(robotIO);

            foreach (var branch in this.branches)
                branch.GetMethodsRecursively(robotIO, list);

            return list;
        }

        /// <summary>
        /// Gets method delcarations.
        /// </summary>
        /// <param name="robotIO">Robotot I/O system.</param>
        /// <returns>Method declarations.</returns>
        private MethodNode[] GetMethodDeclarations(RobotIO robotIO)
        {
            var methods = GetMethodsRecursively(robotIO)
                .GroupBy(meth => meth.MethodName)
                .Select(g => g.First())
                .ToArray();

            return methods;
        }

        /// <summary>
        /// Gets global variable declarations.
        /// </summary>
        /// <returns>Global variable declarations.</returns>
        private ObjectNode[] GetGlobalVariableDeclarations()
        {
            var variables = GetGlobalVariablesRecursively()
                .GroupBy(var => var.VariableName)
                .Select(g => g.First())
                .ToArray();

            return variables;
        }

        /// <summary>
        /// Plugs current branch with robot info.
        /// </summary>
        /// <param name="robotIO">Robot parts.</param>
        private void PlugBranch(RobotIO robotIO)
        {
            foreach (var node in this.actions)
                if (node is PlugNode)
                    (node as PlugNode).PlugIn(robotIO);
        }

        /// <summary>
        /// Recursively constructs variable declarations.
        /// </summary>
        /// <param name="robotIO">Robot I/O system.</param>
        private void ConstructVariableDeclarations(RobotIO robotIO)
        {
            foreach (var branch in this.branches)
                branch.ConstructVariableDeclarations(robotIO);

            if (this.Method == null)
                return;

            var nodes = new List<ISyntaxNode>();

            foreach (var node in this.variables)
            {
                if (node is ObjectNode)
                    nodes.Add(node);
                else if (node is PlugNode)
                    nodes.Add((node as PlugNode).ToNode(robotIO));
            }

            this.Method.PrependNodes(nodes
                .GroupBy(var => (var as ObjectNode).VariableName)
                .Select(g => g.First())
                .ToArray());
        }

        /// <summary>
        /// Converts syntax tree to robot code node.
        /// </summary>
        /// <param name="robotName">Name of the robot.</param>
        /// <returns>Created robot node.</returns>
        public RobotNode ToRobot(string robotName)
        {
            var robotWrapperNode = new RobotNode(robotName);
            var robotIO = new RobotIO(robotWrapperNode);
            var robotNode = robotWrapperNode.Robot;
            var methods = GetMethodDeclarations(robotIO);
            var globals = GetGlobalVariableDeclarations();

            robotNode.AddVariables(globals);
            robotNode.AddMethods(methods);

            PushInternals(robotWrapperNode.RunFrame);
            this.Method = robotWrapperNode.RunFrame;

            ConstructVariableDeclarations(robotIO);

            return robotWrapperNode;
        }
    }
}

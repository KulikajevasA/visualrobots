﻿using System.Collections.Generic;
using System.IO;

namespace FileSystem
{
    /// <summary>
    /// File system to deal with KeyValues.
    /// </summary>
    public class FileSystem
    {
        /// <summary>
        /// Reads KeyValues from file.
        /// </summary>
        /// <param name="root">Root directory of the file.</param>
        /// <param name="fileName">Name of the file.</param>
        /// <returns>Created KeyValues.</returns>
        public static KeyValues ReadKeyValuesFromFile(string root, string fileName)
        {
            if (!File.Exists(root + '/' + fileName))
                return null;

            var kv = new KeyValues();

            var file = new StreamReader(root + '/' + fileName);

            ReadRecursively(file, kv);

            file.Close();

            return kv;
        }

        /// <summary>
        /// Reads file recursively.
        /// </summary>
        /// <param name="file">File to read.</param>
        /// <param name="kv">KeyValues to add data to.</param>
        private static void ReadRecursively(StreamReader file, KeyValues kv)
        {
            bool breakOut = false;

            var key = string.Empty;
            var value = string.Empty;

            var quotesOpen = false;
            var readingKey = true;

            while (!file.EndOfStream && !breakOut)
            {
                char ch = (char)file.Read();

                switch (ch)
                {
                    case '"':
                        if (quotesOpen && readingKey)
                            readingKey = false;
                        else if (quotesOpen && !readingKey)
                        {
                            readingKey = true;
                            kv.AddKeyValue(key, value);
                            key = value = string.Empty;
                        }

                        quotesOpen = !quotesOpen;
                        break;

                    case '{':
                        var subKv = new KeyValues(key);
                        ReadRecursively(file, subKv);
                        kv.AddKeyValue(key, subKv);
                        readingKey = true;
                        key = value = string.Empty;
                        break;

                    case '}':
                        breakOut = true;
                        break;

                    case ' ':
                    case '\t':
                    case '\n':
                    case '\r':
                        if (!quotesOpen)
                        {
                            if (readingKey && key.Length > 0)
                                readingKey = !readingKey;
                            else if (!readingKey && value.Length > 0)
                            {
                                readingKey = true;
                                kv.AddKeyValue(key, value);
                                key = value = string.Empty;
                            }
                        }
                        else
                        {
                            if (readingKey)
                                key += ch;
                            else
                                value += ch;
                        }
                        break;

                    default:
                        if (readingKey)
                            key += ch;
                        else
                            value += ch;
                        break;
                }
            }
        }

        /// <summary>
        /// Gets string representation for KeyValues.
        /// </summary>
        /// <param name="kv">KeyValues to stringify.</param>
        /// <returns>String representation.</returns>
        public static string GetString(KeyValues kv)
        {
            using (var writer = new StringWriter())
            {
                SaveRecursively(writer, kv);
                return writer.ToString();
            }
        }

        /// <summary>
        /// Saves KeyValues to a file.
        /// </summary>
        /// <param name="root">Root directory to save to.</param>
        /// <param name="kv">KeyValues to save.</param>
        /// <param name="fileName">Name of the file to save to.</param>
        public static void SaveKeyValuesToFile(string root, KeyValues kv, string fileName)
        {
            using (var file = new StreamWriter(root + '/' + fileName))
                file.Write(GetString(kv));
        }

        /// <summary>
        /// Saves KeyValues to a file recursively.
        /// </summary>
        /// <param name="file">File to write to.</param>
        /// <param name="kv">KeyValues to save.</param>
        /// <param name="level">Indentation level.</param>
        private static void SaveRecursively(TextWriter file, KeyValues kv, int level = 0)
        {
            var kvs = kv.GetKeyValues();

            if (kvs != null)
                foreach (var pair in kvs)
                {
                    AddPadding(file, level);

                    file.Write("\"{0}\"\t", pair.Key);

                    if (pair.Value is string)
                        file.WriteLine("\"{0}\"", pair.Value);
                    else if (pair.Value is KeyValues)
                    {
                        file.WriteLine();
                        AddPadding(file, level);
                        file.WriteLine('{');
                        SaveRecursively(file, pair.Value as KeyValues, level + 1);
                        AddPadding(file, level);
                        file.WriteLine("}\n");
                    }
                }
        }

        /// <summary>
        /// Prints KeyValues to console recursively.
        /// </summary>
        /// <param name="kv">KeyValues to print.</param>
        /// <param name="level">Indentation level.</param>
        public static void Print(KeyValues kv, int level = 0)
        {
            foreach (var pair in kv.GetKeyValues())
            {
                if (pair.Value is string)
                    System.Console.WriteLine(pair.Key + " " + pair.Value + " " + level);
                //Debug.Log(pair.Key + " " + pair.Value + " " + level);
                else if (pair.Value is KeyValues)
                    Print(pair.Value as KeyValues, level + 1);
            }
        }

        /// <summary>
        /// Adds padding to the file.
        /// </summary>
        /// <param name="file">File to add padding to.</param>
        /// <param name="level">Padding level.</param>
        private static void AddPadding(TextWriter file, int level)
        {
            for (int i = 0; i < level; i++)
                file.Write('\t');
        }
    }
}
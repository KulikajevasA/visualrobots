﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace FileSystem
{
    /// <summary>
    /// Valve key value format reader and writer.
    /// </summary>
    public class KeyValues
    {
        private string name;
        private Dictionary<string, object> keyValues;

        public Dictionary<string, object> GetKeyValues() { return ReturnMyKeyValuesTree(); }

        /// <summary>
        /// Constructor for KeyValues.
        /// </summary>
        /// <param name="name">Name of the KeyValues.</param>
        public KeyValues(string name)
        {
            this.name = name;
            keyValues = new Dictionary<string, object>();
        }

        /// <summary>
        /// Constructor for KeyValues with default name for root node.
        /// </summary>
        public KeyValues()
        {
            name = "root";
            keyValues = new Dictionary<string, object>();
        }
        #region ADD
        /// <summary>
        /// Adds key-value relationship.
        /// </summary>
        /// <param name="name">Name of the key.</param>
        /// <param name="value">Value of the key.</param>
        public void AddKeyValue(string name, object value)
        {
            if (value is string || value is KeyValues)
                keyValues.Add(name, value);
            else keyValues.Add(name, value.ToString());

        }

        /// <summary>
        /// Adds KeyValues.
        /// </summary>
        /// <param name="name">Name of the KeyValues to add.</param>
        /// <returns>Returns added keyvalue.</returns>
        public KeyValues AddKey(string name)
        {
            var kv = new KeyValues(name);
            keyValues.Add(name, kv);
            return kv;
        }
        #endregion

        #region GET
        /// <summary>
        /// Gets integer from key.
        /// </summary>
        /// <param name="key">Name of the key.</param>
        /// <param name="defaultValue">Default value.</param>
        /// <returns>Value if exists, otherwise default.</returns>
        public int GetInt(string key, int defaultValue = 0)
        {
            object value = null;
            var kv = ReturnMyKeyValuesTree();

            int result = defaultValue;

            if (kv != null)
                if (kv.TryGetValue(key, out value))
                    if (value is string)
                        if (int.TryParse(value as string, out result))
                            return result;

            return defaultValue;
        }

        /// <summary>
        /// Gets long from key.
        /// </summary>
        /// <param name="key">Name of the key.</param>
        /// <param name="defaultValue">Default value.</param>
        /// <returns>Value if exists, otherwise default.</returns>
        public long GetLong(string key, long defaultValue = 0)
        {
            object value = null;
            var kv = ReturnMyKeyValuesTree();

            long result = defaultValue;

            if (kv != null)
                if (kv.TryGetValue(key, out value))
                    if (value is string)
                        if (long.TryParse(value as string, out result))
                            return result;

            return defaultValue;
        }

        /// <summary>
        /// Gets byte from key.
        /// </summary>
        /// <param name="key">Name of the key.</param>
        /// <param name="defaultValue">Default value.</param>
        /// <returns>Value if exists, otherwise default.</returns>
        public byte GetByte(string key, byte defaultValue = 0)
        {
            object value = null;
            var kv = ReturnMyKeyValuesTree();

            byte result = defaultValue;

            if (kv != null)
                if (kv.TryGetValue(key, out value))
                    if (value is string)
                        if (byte.TryParse(value as string, out result))
                            return result;

            return defaultValue;
        }

        /// <summary>
        /// Gets boolean from key.
        /// </summary>
        /// <param name="key">Name of the key.</param>
        /// <param name="defaultValue">Default value.</param>
        /// <returns>Value if exists, otherwise default.</returns>
        public bool GetBool(string key, bool defaultValue = false)
        {
            object value = null;
            var kv = ReturnMyKeyValuesTree();

            byte result;

            if (kv != null)
                if (kv.TryGetValue(key, out value))
                    if (value is string)
                        if (byte.TryParse(value as string, out result))
                            return result == 1 ? true : false;

            return defaultValue;
        }

        /// <summary>
        /// Gets float from key.
        /// </summary>
        /// <param name="key">Name of the key.</param>
        /// <param name="defaultValue">Default value.</param>
        /// <returns>Value if exists, otherwise default.</returns>
        public float GetFloat(string key, float defaultValue = 0)
        {
            object value = null;
            Dictionary<string, object> kv = ReturnMyKeyValuesTree();

            float result = defaultValue;

            if (kv != null)
                if (kv.TryGetValue(key, out value))
                    if (value is string)
                        if (float.TryParse(value as string, NumberStyles.Any, CultureInfo.InvariantCulture, out result))
                            return result;

            return defaultValue;
        }

        /// <summary>
        /// Gets double from key.
        /// </summary>
        /// <param name="key">Name of the key.</param>
        /// <param name="defaultValue">Default value.</param>
        /// <returns>Value if exists, otherwise default.</returns>
        public double GetDouble(string key, double defaultValue = 0)
        {
            object value = null;
            var kv = ReturnMyKeyValuesTree();

            double result = defaultValue;

            if (kv != null)
                if (kv.TryGetValue(key, out value))
                    if (value is string)
                        if (double.TryParse(value as string, NumberStyles.Any, CultureInfo.InvariantCulture, out result))
                            return result;

            return defaultValue;
        }

        /// <summary>
        /// Gets string from key.
        /// </summary>
        /// <param name="key">Name of the key.</param>
        /// <param name="defaultValue">Default value.</param>
        /// <returns>Value if exists, otherwise default.</returns>
        public string GetString(string key, string defaultValue = "")
        {
            object value = null;
            var kv = ReturnMyKeyValuesTree();

            if (kv != null)
                if (kv.TryGetValue(key, out value))
                    if (value is string)
                        return value as string;

            return defaultValue;
        }

        /// <summary>
        /// Gets sub key.
        /// </summary>
        /// <param name="subKey">Name of the sub key.</param>
        /// <returns>Sub key if exists, NULL otherwise.</returns>
        public KeyValues GetSubKey(string subKey)
        {
            object value = null;
            var kv = ReturnMyKeyValuesTree();

            if (kv != null)
                if (kv.TryGetValue(subKey, out value))
                    if (value is KeyValues)
                        return value as KeyValues;

            return null;
        }
        #endregion

        #region UPDATE
        /// <summary>
        /// Updates integer value for key. Creates field if didn't exist previously.
        /// </summary>
        /// <param name="key">Key to update.</param>
        /// <param name="newValue">New value for key.</param>
        public void SetInt(string key, int newValue)
        {
            var kv = ReturnMyKeyValuesTree();

            if (kv != null)
                if (!kv.ContainsKey(key))
                    AddKeyValue(key, newValue.ToString());
                else
                    kv[key] = newValue.ToString();
        }

        /// <summary>
        /// Updates byte value for key. Creates field if didn't exist previously.
        /// </summary>
        /// <param name="key">Key to update.</param>
        /// <param name="newValue">New value for key.</param>
        public void SetByte(string key, byte newValue)
        {
            var kv = ReturnMyKeyValuesTree();

            if (kv != null)
                if (!kv.ContainsKey(key))
                    AddKeyValue(key, newValue.ToString());
                else
                    kv[key] = newValue.ToString();
        }

        /// <summary>
        /// Updates boolean value for key. Creates field if didn't exist previously.
        /// </summary>
        /// <param name="key">Key to update.</param>
        /// <param name="newValue">New value for key.</param>
        public void SetBool(string key, bool newValue)
        {
            var kv = ReturnMyKeyValuesTree();

            int value = newValue == true ? 1 : 0;

            if (kv != null)
                if (!kv.ContainsKey(key))
                    AddKeyValue(key, value.ToString());
                else
                    kv[key] = value.ToString();
        }

        /// <summary>
        /// Updates long value for key. Creates field if didn't exist previously.
        /// </summary>
        /// <param name="key">Key to update.</param>
        /// <param name="newValue">New value for key.</param>
        public void SetLong(string key, long newValue)
        {
            var kv = ReturnMyKeyValuesTree();

            if (kv != null)
                if (!kv.ContainsKey(key))
                    AddKeyValue(key, newValue.ToString());
                else
                    kv[key] = newValue.ToString();
        }

        /// <summary>
        /// Updates float value for key. Creates field if didn't exist previously.
        /// </summary>
        /// <param name="key">Key to update.</param>
        /// <param name="newValue">New value for key.</param>
        public void SetFloat(string key, float newValue)
        {
            var kv = ReturnMyKeyValuesTree();

            if (kv != null)
                if (!kv.ContainsKey(key))
                    AddKeyValue(key, newValue.ToString(CultureInfo.InvariantCulture));
                else
                    kv[key] = newValue.ToString(CultureInfo.InvariantCulture);
        }

        /// <summary>
        /// Updates double value for key. Creates field if didn't exist previously.
        /// </summary>
        /// <param name="key">Key to update.</param>
        /// <param name="newValue">New value for key.</param>
        public void SetDouble(string key, double newValue)
        {
            var kv = ReturnMyKeyValuesTree();

            if (kv != null)
                if (!kv.ContainsKey(key))
                    AddKeyValue(key, newValue.ToString(CultureInfo.InvariantCulture));
                else
                    kv[key] = newValue.ToString(CultureInfo.InvariantCulture);
        }
        #endregion

        #region REMOVE
        /// <summary>
        /// Removes key-value pair.
        /// </summary>
        /// <param name="key">Name of the key to remove.</param>
        public void RemoveKey(string key)
        {
            var kv = ReturnMyKeyValuesTree();

            if (kv != null)
                if (kv.ContainsKey(key))
                    kv.Remove(key);
        }
        #endregion

        /// <summary>
        /// Returns the dictionary of the available key-value pairs.
        /// </summary>
        /// <returns>Key-value pairs.</returns>
        private Dictionary<string, object> ReturnMyKeyValuesTree()
        {
            return keyValues;

            /*if (name == "root")
            {
                if (keyValues.Count == 0)
                    return null;

                var kv = keyValues.First().Value as KeyValues;

                if (kv == null)
                    return null;

                return kv.GetKeyValues();
            }
            else return keyValues;*/
        }

        /// <summary>
        /// Stringifies KeyValues.
        /// </summary>
        /// <returns>String representation.</returns>
        public override string ToString()
        {
            return FileSystem.GetString(this).ToString();
        }
    }
}
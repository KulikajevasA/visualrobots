# README #

RobotVL is a virtual robot design platform targeted towards kids. Software allows it's user to create, implement algorithms and test them on a virtual robot. The software was created as a Masters for Software System Engineering project.

####[Full documentation and specification.](https://sites.google.com/site/pbifgjglnh/)####

![rsz_robotvl.jpg](https://bitbucket.org/repo/RkGx8z/images/1235778822-rsz_robotvl.jpg)

### Parts ###

* Main Software - contains the meat of the software.
* RobotVL - contains code transformations to C# language.
* FileSystem - contains rudimentary file system. This is a dependency component required for the framework that the UI is built on.
* SimulationShared - Interfaces for all robots to use.

### TODO ###

* Support for multiple robots in the scene.
* Networking support.
* Server-client architecture.
* Transformations to JAVA and other languages.
* Code transformations for Lego MindStorms to allow testing on real robots.
* Visual code generation from C# code.
* Allowing user to configure his own robot.
* Map generation.
* Greater sensor support.
* Greater part variety.
* Other.

### Authors ###

* [Audrius 'Ratchet' Kulikajevas'](mailto:akulikajevas@gmail.com)
* [Devidas Stonys](mailto:deivis404@gmail.com)

### License ###

GNU General Public License v2.0

### Dependencies ###

* [C# Compiler](https://www.visualstudio.com/) (.NET 3.5 or newer)
* [Unity Game Engine](https://unity3d.com/) (5.3.6 or newer)

![rsz_robotvl2.jpg](https://bitbucket.org/repo/RkGx8z/images/813064502-rsz_robotvl2.jpg)
﻿using FileSystem;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace Launcher
{
    public partial class formStartup : Form
    {
        private KeyValues kvRoot;
        private KeyValues kvSettings;
        private FormSettings formSettings;

        public formStartup()
        {
            InitializeComponent();
        }

        private void buttonSettings_Click(object sender, EventArgs e)
        {
            if (formSettings.ShowDialog() == DialogResult.OK)
            {
                var pathBuild = formSettings.PathBuild;
                if (!File.Exists(pathBuild))
                    return;

                var root = Application.StartupPath;

                var pathOutput = formSettings.PathOutput;

                if (pathOutput.Equals(string.Empty))
                    pathOutput = root;

                kvSettings.AddKeyValue("msbuild", pathBuild);
                kvSettings.AddKeyValue("output", pathOutput);

                FileSystem.FileSystem.SaveKeyValuesToFile(root, kvSettings, "settings.res");
            }
        }

        private void formStartup_Load(object sender, EventArgs e)
        {
            formSettings = new FormSettings();

            var root = Application.StartupPath;

            if (File.Exists(root + "\\settings.res"))
            {
                kvRoot = FileSystem.FileSystem.ReadKeyValuesFromFile(root, "settings.res");
                buttonStart.Enabled = true;

                formSettings.PathBuild = kvRoot.GetSubKey("settings").GetString("msbuild");
                formSettings.PathOutput = kvRoot.GetSubKey("settings").GetString("output", root);
            }
            else
            {
                formSettings.PathOutput = root;

                kvRoot = new KeyValues();
                
                kvSettings = kvRoot.AddKey("settings");

                var pathMSBuild = FindMSBuild();

                if (pathMSBuild == null) return;
                formSettings.PathBuild = pathMSBuild;

                kvSettings.AddKeyValue("output", root);
                kvSettings.AddKeyValue("msbuild", pathMSBuild);
                buttonStart.Enabled = true;

                FileSystem.FileSystem.SaveKeyValuesToFile(root, kvRoot, "settings.res");
            }
        }

        public static string FindMSBuild()
        {
            var dir = string.Empty;
            var dirMSBuildx86 = Environment.GetEnvironmentVariable("programfiles(x86)") + "\\MSBuild";
            var dirMSBuild = Environment.GetEnvironmentVariable("programfiles") + "\\MSBuild";
            if (Directory.Exists(dirMSBuildx86))
                dir = dirMSBuildx86;
            else if (Directory.Exists(dirMSBuildx86))
                dir = dirMSBuildx86;
            else
                return null;

            var supportedVersions = new string[] { "14.0", "11.0" };

            for (var i = 0; i < supportedVersions.Length; i++)
            {
                var p = dir + "\\" + supportedVersions[i] + "\\bin";
                if (Directory.Exists(p))
                {
                    dir = p;
                    break;
                }
            }

            var path = dir + "\\msbuild.exe";

            if (File.Exists(path))
                return path;

            return null;
        }
    }
}

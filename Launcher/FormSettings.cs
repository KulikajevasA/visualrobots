﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Launcher
{
    public partial class FormSettings : Form
    {
        public string PathOutput
        {
            get { return inputOutput.Text; }
            set { inputOutput.Text = value; }
        }

        public string PathBuild
        {
            get { return inputBuild.Text; }
            set { inputBuild.Text = value; }
        }

        public FormSettings()
        {
            InitializeComponent();
        }

        private void buttonOutput_Click(object sender, EventArgs e)
        {
            using (var diag = new FolderBrowserDialog())
            {
                if (diag.ShowDialog() == DialogResult.OK)
                {
                    string name = diag.SelectedPath;
                    inputOutput.Text = name;
                }
            }
        }

        private void buttonBuild_Click(object sender, EventArgs e)
        {
            using (var diag = new OpenFileDialog())
            {
                string initialDir = diag.InitialDirectory;
                string dirMSBuildx86 = Environment.GetEnvironmentVariable("programfiles(x86)") + "\\MSBuild";
                string dirMSBuild = Environment.GetEnvironmentVariable("programfiles") + "\\MSBuild";

                if (Directory.Exists(dirMSBuildx86))
                    initialDir = dirMSBuildx86;
                else if (Directory.Exists(dirMSBuildx86))
                    initialDir = dirMSBuildx86;

                var supportedVersions = new string[] { "14.0", "11.0" };

                for (var i = 0; i < supportedVersions.Length; i++)
                {
                    var path = initialDir + "\\" + supportedVersions[i] + "\\bin";
                    if (Directory.Exists(path))
                    {
                        initialDir = path;
                        break;
                    }
                }

                diag.InitialDirectory = initialDir;
                diag.Filter = "MSBuild (msbuild.exe)|msbuild.exe";

                if (diag.ShowDialog() == DialogResult.OK)
                {
                    inputBuild.Text = diag.FileName;
                }
            }
        }

        private void FormSettings_Load(object sender, EventArgs e)
        {
        }
    }
}

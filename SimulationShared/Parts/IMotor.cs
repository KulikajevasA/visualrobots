﻿namespace Simulation.Parts
{
    /// <summary>
    /// Interface shared by motors.
    /// </summary>
    public interface IMotor : IPart
    {
        /// <summary>
        /// Sets the torque of the motor.
        /// </summary>
        /// <param name="speed">Torque value.</param>
        void SetSpeed(double speed);
    }
}

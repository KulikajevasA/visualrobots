﻿namespace Simulation.Parts
{
    /// <summary>
    /// Enumerator for part type.
    /// </summary>
    public enum PartType_T
    {
        Unknown = 0,
        Motor,
        Wheel,
        UltrasonicSensorArray,
        UltrasonicSensor,
        ColorSensor,
        LightSensor
    }
}

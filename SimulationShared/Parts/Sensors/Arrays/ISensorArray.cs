﻿namespace Simulation.Parts.Sensors.Arrays
{
    /// <summary>
    /// Interface for all sensor array type parts.
    /// </summary>
    public interface ISensorArray : IPart
    {
        /// <summary>
        /// Sensor array uses multiple sensors, sensor arrays gives direction of the sensor.
        /// Can take values:
        ///     Right =>  1
        ///     Front =>  0
        ///     Left  => -1
        /// </summary>
        double SensorDirection { get; }

        /// <summary>
        /// Distance to the hit target. Returns Infinity if no targets were hit.
        /// </summary>
        double SensorValue { get; }

        /// <summary>
        /// Returns the array for each sensor data. There really isn't much use for it right now.
        /// </summary>
        double[] SensorData { get; }
    }
}

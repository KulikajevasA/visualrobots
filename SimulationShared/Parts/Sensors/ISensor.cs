﻿namespace Simulation.Parts.Sensors
{
    /// <summary>
    /// Interface for all sensor type parts to use.
    /// </summary>
    public interface ISensor : IPart
    {
        /// <summary>
        /// Retrieves sensor data.
        /// </summary>
        double SensorData { get; }
    }
}

﻿namespace Simulation.Parts
{
    /// <summary>
    /// Interface for part.
    /// </summary>
    public interface IPart
    {
        /// <summary>
        /// Getter & setter for part name.
        /// </summary>
        string PartName { get; set; }

        /// <summary>
        /// Getter for part type.
        /// </summary>
        PartType_T PartType { get; }
    }
}

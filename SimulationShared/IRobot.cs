﻿using Simulation.Parts;
using System.Collections.Generic;

namespace Simulation
{
    /// <summary>
    /// Interface for a robot.
    /// </summary>
    public interface IRobot
    {
        /// <summary>
        /// Passes parts to robot to initialize them.
        /// </summary>
        /// <param name="parts"></param>
        void InitializeParts(List<IPart> parts);

        /// <summary>
        /// Runs a simulation frame.
        /// </summary>
        void RunFrame();
    }
}
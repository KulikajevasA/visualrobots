﻿namespace Simulation
{
    /// <summary>
    /// Entry point interface.
    /// </summary>
    public interface IEntryPoint
    {
        /// <summary>
        /// Creates robot.
        /// </summary>
        /// <returns>Robot object.</returns>
        Simulation.IRobot CreateRobot();
    }
}
